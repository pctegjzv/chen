![chen_dota.jpg](./chen.jpg)

Recommended markdown tools: [Macdown](https://github.com/MacDownApp/macdown), [Typora](http://www.typora.io/)

# Chen: Application Catalog Handler

Processing application templates, generate docker-compose.yml, deploy the application using druid(support node_tag, volumn)

Supported Application:

* ZooKeeper

## Before checkin

Run the following command to make sure there is no Pep8 violation:

```
make build
```

TODO:

```
make test
```

## How do I get set up

1. [Install Docker](https://docs.docker.com/engine/installation/)
2. Get an [alauda.cn](https://www.alauda.cn) account
3. Pull the image or build from scratch:

    * Pull instructions:

        ```
        $ docker login index.alauda.cn    # will prompt for your username and password and then
        $ docker pull index.alauda.cn/mathildedev/chen:latest
        ```
        
    * Build(optional):  
    In the Chen root folder(You can change chen-dev to anything else). 
    
        ```
        $ docker build -t chen-dev .
        ```

4. Get all the environment variables from INT-Chen
5. Suppose you have your Chen repository in the following folder `$HOME/chen`, you can run a docker container and attach your folder:

	```
	$ # assuming you have an envs/ali/yourname/chen.env file copied from INT env.
	$ docker run -d --name chen-dev \
	  -p 8080:8080 \
	  -v $HOME/chen:/chen \
	  -v $HOME/logs:/var/log/mathilde \
	  --env-file=envs/ali/yourname/chen.env \
	  index.alauda.cn/mathildedev/chen:latest
	$ # Change the last line to chen-dev if you built in your machine   
	```
6.  You should be able to verify it is working with

   ```
   $ curl localhost:8080/v1/ping
   ```
7. You can access your container using:

	```
	$ docker exec -it chen-dev bash
	```
8. Run tests  
TODO


## Contribution guidelines
[CONTRIBUTING.md](./CONTRIBUTING.md)

## Who do I talk to

[mail to owner](mailto:junh@alauda.io)

[AUTHORS](./AUTHORS)

