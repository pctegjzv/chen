#!/usr/bin/env python
# -*- coding: utf-8 -*-
import datetime
import logging
import json
import os
from tests.api.base import ChenAPIBase
from django.core.urlresolvers import reverse

logger = logging.getLogger(__name__)


class PluginBaseClass(ChenAPIBase):
    now = datetime.datetime.now()
    EXISTED_UUID = '447e8e4f-ff4d-455b-b1fc-1194565cd03f'

    def setUp(self):
        super(PluginBaseClass, self).setUp()
        self.plugin_json_file = os.path.join(os.path.dirname(__file__), 'jsons')

    def list_supported_plugin_api(self):
        url = reverse('/v1/plugin_types')
        return self.send_get(url)

    def get_plugin_config_template(self, plugin_type):
        url = reverse('/v1/plugin_types/plugin_type', kwargs={
            'plugin_type': plugin_type
        })
        return self.send_get(url)

    def create_plugin_api(self, payload, plugin_type):
        url = reverse('/v1/plugin_types/plugin_type/plugins', kwargs={
            'plugin_type': plugin_type
        })
        return self.send_post(url, data=payload)

    def get_plugin_info_api(self, plugin_type, plugin_uuid):
        url = reverse('/v1/plugin_types/plugin_type/plugins/plugin_uuid',
                      kwargs={'plugin_type': plugin_type, 'plugin_uuid': plugin_uuid})
        data = {
            'namespace': 'alaodaorg',
            'project_name': 'project-test',
            'user_token': 'knighttoken'
        }
        return self.send_get(url, data=data)

    def list_running_plugins_api(self, plugin_type):
        url = reverse('/v1/plugin_types/plugin_type/plugins', kwargs={
            'plugin_type': plugin_type
        })
        return self.send_get(url)

    def generate_druid_create_payload(self):
        file_path = os.path.join(self.plugin_json_file, 'create_sonarqube.json')
        with open(file_path) as f_handler:
            return json.load(f_handler)

    def generate_druid_create_response(self):
        return {
            "uuid": "52188a75-6ceb-4781-8b4a-90cb2fb3b382",
            "app_name": "test-app"
        }

    def generate_integrate_create_response(self):
        file_path = os.path.join(self.plugin_json_file, 'integrate_create_response.json')
        with open(file_path) as f_handler:
            return json.load(f_handler)

    def generate_sonar_token_response(self):
        file_path = os.path.join(self.plugin_json_file, 'create_sonar_token_response.json')
        with open(file_path) as f_handler:
            return json.load(f_handler)

    def generate_druid_list_running_apps_response(self):
        pass

    def genegrate_get_app_info_response(self):
        file_path = os.path.join(self.plugin_json_file, 'get_app_info.json')
        with open(file_path) as f_handler:
            return json.load(f_handler)
