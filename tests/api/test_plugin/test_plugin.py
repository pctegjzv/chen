# -*- coding: utf-8 -*-
import logging
import mock
from tests.api.test_plugin.base import PluginBaseClass
from rest_framework import status
from client.jakiro.client import JakiroClient
from plugin_center.plugins.plugin_client.sonarqube import SonarQubeClient
from client.meudsa.client import MedusaClient
from client.mirana2.client import Mirana2Client

logger = logging.getLogger(__name__)


class PluginTest(PluginBaseClass):

    def test_list_support_plugin(self):
        response = self.list_supported_plugin_api()
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_sonarqube_config_template(self):
        response = self.get_plugin_config_template('SonarQube')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    @mock.patch.object(JakiroClient, 'create')
    @mock.patch.object(MedusaClient, 'create_schedule_event_config')
    def test_create_sonarqube_plugin(self, create_schedule, jakiro_create):
        create_schedule.return_value = ''
        jakiro_create.return_value = self.generate_druid_create_response()
        payload = self.generate_druid_create_payload()

        response = self.create_plugin_api(payload=payload,
                                          plugin_type='SonarQube')
        data = self.get_response_data(response)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertTrue('uuid' in data)

    @mock.patch.object(Mirana2Client, 'get_alb_domain')
    @mock.patch.object(JakiroClient, 'create')
    @mock.patch.object(JakiroClient, 'get_app')
    @mock.patch.object(JakiroClient, 'delete_app')
    @mock.patch.object(JakiroClient, 'create_integration')
    @mock.patch.object(SonarQubeClient, 'grant_scan_permission')
    @mock.patch.object(SonarQubeClient, 'generate_token')
    @mock.patch.object(SonarQubeClient, 'check_valid')
    @mock.patch.object(MedusaClient, 'create_schedule_event_config')
    def test_get_clair_plugin_info(self, create_schedule, sonar_valid, sonar_token,
                                   sonar_permission, integrate_create, jakiro_delete,
                                   jakiro_get, jakiro_create, get_alb_domain):
        create_schedule.return_value = ''
        sonar_valid.return_value = True
        jakiro_create.return_value = self.generate_druid_create_response()
        jakiro_get.side_effect = self.genegrate_get_app_info_response()
        jakiro_delete.return_value = {}
        integrate_create.return_value = self.generate_integrate_create_response()
        sonar_permission.return_value = True
        sonar_token.return_value = self.generate_sonar_token_response()
        get_alb_domain.return_value = 'sonarqube-test.sonarqube-test.alaudaorg.' \
                                      'int-mesos-test-lb.myalauda.cn'

        payload = self.generate_druid_create_payload()
        response = self.create_plugin_api(payload=payload, plugin_type='SonarQube')

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        data = self.get_response_data(response)
        plugin_uuid = data.get('uuid')

        response = self.get_plugin_info_api(plugin_type='SonarQube', plugin_uuid=plugin_uuid)
        data = self.get_response_data(response)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        self.assertEqual(data['status'], 'Deploying')
        self.assertEqual(data['pluginapplication']['status'], 'Creating')

        response = self.get_plugin_info_api(plugin_type='SonarQube', plugin_uuid=plugin_uuid)
        data = self.get_response_data(response)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(data['status'], 'Deploying')
        self.assertEqual(data['pluginapplication']['status'], 'Starting')

        response = self.get_plugin_info_api(plugin_type='SonarQube', plugin_uuid=plugin_uuid)
        data = self.get_response_data(response)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(data['status'], 'Succeed')
        self.assertEqual(data['pluginapplication']['status'], 'Running')
        self.assertEqual(data['pluginintegration']['status'], 'Succeed')
        # test list running plugins api
        response = self.list_running_plugins_api('SonarQube')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        response = self.list_supported_plugin_api()
        self.assertEqual(response.status_code, status.HTTP_200_OK)
