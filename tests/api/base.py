#!/usr/bin/env python
# -*- coding: utf-8 -*-


import logging
import datetime
import json
import uuid

from rest_framework import status
from rest_framework.test import APITestCase, APIClient
from rest_framework.response import Response
from django.http.response import HttpResponse

from catalog.models import Application

logger = logging.getLogger(__name__)


class ChenAPIBase(APITestCase):
    now = datetime.datetime.now()

    def setUp(self):
        self.client = APIClient()
        pass

    def tearDown(self):
        pass

    def send_request(self, method, url, data=None, format='json', **kwargs):
        function = getattr(self.client, method, self.client.get)
        if not callable(function):
            function = self.client.get
        return function(url, data=data, format=format, **kwargs)

    def send_post(self, url, data=None, format='json', **kwargs):
        return self.send_request(method='post', url=url, data=data, format=format, **kwargs)

    def send_get(self, url, data=None, format=None, **kwargs):
        return self.send_request(method='get', url=url, data=data, format=format, **kwargs)

    def send_put(self, url, data=None, format='json', **kwargs):
        return self.send_request(method='put', url=url, data=data, format=format, **kwargs)

    def send_delete(self, url, data=None, format=None, **kwargs):
        return self.send_request(method='delete', url=url, data=data, format=format, **kwargs)

    def get_response_data(self, response):
        if isinstance(response, HttpResponse):
            return json.loads(response.content)
        else:
            return response.data

    def create_apps(self, app_id=None):
        validated_data = {
            "type": "zookeeper",
            "uuid": app_id if app_id is not None else uuid.uuid4(),
            "region_id": uuid.uuid4()
        }

        app = Application.objects.create(**validated_data)
        return app

    def generate_druid_create_response(self):
        # so we can test delete with the specific uuid
        data = {'app': '347e8e4f-ff4d-455b-b1fc-1194565cd03f'}
        return Response(status=status.HTTP_201_CREATED, data=data)

    def generate_druid_application_info_zookeeper(self):
        result = {
            'uuid': '00de1bd1-6416-4931-a15a-436aa188fbb9',
            'services': [
                {
                    'instance_envvars': {"ZOO_PORT": "2183"},
                    'node_tag': 'ip:10.0.0.5'
                },
                {
                    'instance_envvars': {"ZOO_PORT": "2183"},
                    'node_tag': 'ip:10.0.0.7'
                },
                {
                    'instance_envvars': {"ZOO_PORT": "2183"},
                    'node_tag': 'ip:10.0.0.6'
                }
            ],
        }
        return result

    def generate_druid_list_running_apps_response(self):
        result = [
            {
                'unique_name': '547e8e4f-ff4d-455b-b1fc-1194565cd031',
                'current_status': 'Running',
                'region': '347e8e4f-ff4d-455b-b1fc-1194565cd031'
            },
            {
                'unique_name': '547e8e4f-ff4d-455b-b1fc-1194565cd032',
                'current_status': 'Running',
                'region': '347e8e4f-ff4d-455b-b1fc-1194565cd031'
            },
            {
                'unique_name': '547e8e4f-ff4d-455b-b1fc-1194565cd033',
                'current_status': 'Running',
                'region': '347e8e4f-ff4d-455b-b1fc-1194565cd032'
            }
        ]
        return result
