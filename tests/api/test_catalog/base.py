#!/usr/bin/env python
# -*- coding: utf-8 -*-

import datetime
import logging

from tests.api.base import ChenAPIBase
from django.core.urlresolvers import reverse

logger = logging.getLogger(__name__)


class CatalogBaseClass(ChenAPIBase):
    now = datetime.datetime.now()
    EXISTED_UUID = '447e8e4f-ff4d-455b-b1fc-1194565cd03f'

    def setUp(self):
        super(CatalogBaseClass, self).setUp()
        self.apps = [self.create_apps(),
                     self.create_apps(app_id=self.EXISTED_UUID)]

    def list_running_apps_api(self, app_name, uuids):
        url = reverse('catalog/app_name/apps', kwargs={
            'app_name': app_name
        })
        url += '?uuids={}'.format(uuids)
        return self.send_get(url)

    def create_app_api(self, app_name, data):
        url = reverse('catalog/app_name/apps', kwargs={
            'app_name': app_name
        })
        return self.send_post(url, data)

    def get_app_detail_api(self, app_name):
        url = reverse('catalog/app_name/', kwargs={
            'app_name': app_name
        })
        return self.send_get(url)

    def delete_app_api(self, app_id):
        url = reverse('catalog/app_name/', kwargs={
            'app_name': app_id
        })
        return self.send_delete(url)

    def list_supported_api(self):
        url = reverse('catalog/')
        return self.send_get(url)
