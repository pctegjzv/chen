#!/usr/bin/env python
# -*- coding: utf-8 -*-


import logging

import mock
from rest_framework import status
from tests.api.test_catalog.base import CatalogBaseClass
from client.druid.client import DruidClient


logger = logging.getLogger(__name__)


class CatalogTest(CatalogBaseClass):

    def setUp(self):
        super(CatalogTest, self).setUp()

    @mock.patch.object(DruidClient, '_do_req')
    def test_list_running_apps(self, _do_req):
        _do_req.return_value = self.generate_druid_list_running_apps_response()
        response = self.list_running_apps_api(app_name='zookeeper', uuids=self.EXISTED_UUID)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        data = self.get_response_data(response)
        logger.debug("[test]list running apps, response data: {}".format(data))

    @mock.patch.object(DruidClient, '_do_req')
    def test_create_app(self, _do_req):
        _do_req.return_value = self.generate_druid_create_response()

        payload = {
            "name": "hejuntestzookeeper3",
            "region": ["7c3e3668-1540-4c3a-9f4b-1be1b4e0e48f"],
            "namespace": ["claasv1"],
            "created_by": ["junh"],
            "domain_name": ["hejuntest-claasv33"],
            "basic_config": {
                "num_of_nodes": 3,
                "zookeeper_size": {
                    "size": "CUSTOMIZED",
                    "cpu": 1,
                    "mem": 1024
                },
                "ip_tag": ["10.171.129.10", "10.172.230.213", "10.171.128.37"],
                "image_tag": "3.4.9"
            },
            "advanced_config": {
                "client_port": 2181,
                "server_port1": 2888,
                "server_port2": 3888,
                "admin_port": 9098,
                "mount_volume": "true",
                "mount_log_dir": "/tmp/zookeeperlog",
                "mount_data_dir": "/tmp/zookeeper",
                "data_dir": "/tmp/zookeeper",
                "data_log_dir": "/tmp/zookeeperlog",
                "tick_time": 2000,
                "init_limit": 10,
                "sync_limit": 2
            },
        }
        _do_req.return_value = {"uuid": "00de1bd1-6416-4931-a15a-436aa188fbb9"}
        response = self.create_app_api(app_name='zookeeper', data=payload)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        data = self.get_response_data(response)
        logger.debug("[test]create app, response data: {}".format(data))

    @mock.patch.object(DruidClient, '_do_req')
    def test_create_kafka_app(self, _do_req):
        _do_req.return_value = self.generate_druid_create_response()

        payload = {
            "name": "hejuntestzookeeper3",
            "region": ["7c3e3668-1540-4c3a-9f4b-1be1b4e0e48f"],
            "namespace": ["claasv1"],
            "created_by": ["junh"],
            "domain_name": ["hejuntest-claasv33"],
            "basic_config": {
                "num_of_nodes": 3,
                "kafka_size": {
                    "size": "CUSTOMIZED",
                    "cpu": 1,
                    "mem": 1024
                },
                "ip_tag": ["10.171.129.10", "10.172.230.213", "10.171.128.37"],
                "image_tag": "3.4.9"
            },
            "advanced_config": {
                "zookeeper_application": "b4b24c10-946b-45fa-bd48-a018d215f943",
                "ip_tag_zoo": ["10.171.129.10", "10.172.230.213", "10.171.128.37"],
                "mount_volume": "true",
                "log_dir": "/kafka",
                "mount_data_dir": "/mkafka",
                "advertised_port": "9092",
                "heap_opts": "-Xmx2G -Xms2G",
                "log_roll_hours": 24,
                "log_retention_bytes": "100000000",
                "log_retention_hours": 168,
                "max_request_size": 1048576,
                "max_message_bytes": 1000012,
                "message_max_bytes": 1000012,
                "num_partitions": 1,
                "replica_fetch_max_bytes": 1048576,
                "replica_fetch_response_max_bytes": 10485760,
                "socket_request_max_bytes": 104857600
            },
        }
        # _do_req.return_value = {"uuid": "00de1bd1-6416-4931-a15a-436aa188fbb9"}
        _do_req.return_value = self.generate_druid_application_info_zookeeper()
        response = self.create_app_api(app_name='kafka', data=payload)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        data = self.get_response_data(response)
        logger.debug("[test]create app, response data: {}".format(data))

    @mock.patch.object(DruidClient, '_do_req')
    def test_create_mongodb_app_replset(self, _do_req):
        _do_req.return_value = self.generate_druid_create_response()
        payload = {
            "name": "yfmongodb",
            "region": ["7c3e3668-1540-4c3a-9f4b-1be1b4e0e48f"],
            "namespace": ["alauda"],
            "created_by": ["fanyang"],
            "domain_name": ["yfmongodb-claasv33"],
            "basic_config": {
                "mongodb_size": {
                    "size": "CUSTOMIZED",
                    "cpu": 0.5,
                    "mem": 1024
                },
                "ip_tag": ["10.171.129.10", "10.172.230.213", "10.171.128.37"],
                "image_tag": "3.4.6",
                "mount_data_dir": "/home/alauda/mongodb",
                "mode": "replica_set",
                "primary_node_addr": "10.171.129.10",
                "secondary_node_number": 2,
                "replset_name": "replset"
            },
            "advanced_config": {
                "port": 27017,
                "root_user": "Alauda123",
                "root_password": "Alauda123"
            },
        }
        _do_req.return_value = {"uuid": "00de1bd1-6416-4931-a15a-436aa188f123"}
        response = self.create_app_api(app_name='mongodb', data=payload)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        data = self.get_response_data(response)
        logger.debug("[test]create app, response data: {}".format(data))

    @mock.patch.object(DruidClient, '_do_req')
    def test_create_mongodb_app_standalone(self, _do_req):
        _do_req.return_value = self.generate_druid_create_response()
        payload = {
            "name": "yfmongodb",
            "region": ["7c3e3668-1540-4c3a-9f4b-1be1b4e0e48f"],
            "namespace": ["alauda"],
            "created_by": ["fanyang"],
            "domain_name": ["yfmongodb-claasv33"],
            "basic_config": {
                "mongodb_size": {
                    "size": "CUSTOMIZED",
                    "cpu": 0.5,
                    "mem": 1024
                },
                "ip_tag": ["10.171.129.10"],
                "image_tag": "3.4.6",
                "mount_data_dir": "/home/alauda/mongodb",
                "mode": "standalone",
            },
            "advanced_config": {
                "port": 27017,
                "root_user": "Alauda123",
                "root_password": "Alauda123"
            },
        }
        _do_req.return_value = {"uuid": "00de1bd1-6416-4931-a15a-436aa188f321"}
        response = self.create_app_api(app_name='mongodb', data=payload)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        data = self.get_response_data(response)
        logger.debug("[test]create app, response data: {}".format(data))

    def test_list_app_detail(self):
        response = self.get_app_detail_api(app_name='zookeeper')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        data = self.get_response_data(response)
        logger.debug("[test]list app detail: {}".format(data))

    def test_delete_app(self):
        uuid = "447e8e4f-ff4d-455b-b1fc-1194565cd03f"
        logger.debug("[test]delete app, with uuid: {}".format(uuid))
        response = self.delete_app_api(app_id=uuid)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_list_supported(self):
        response = self.list_supported_api()
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        data = self.get_response_data(response)
        logger.debug("[test]list supported, response data:{}".format(data))
