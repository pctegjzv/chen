from settings import *    # noqa

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': 'tmp.db',
        'USER': '',
        'HOST': '127.0.0.1',
        'PORT': '5432',
        'PASSWORD': '',
    }
}

print("Database: {}".format(DATABASES))
