#!/usr/bin/env python
# -*- coding: utf-8 -*-

from mekansm.exceptions import MekAPIException

__author__ = 'junh@alauda.io'


class ChenException(MekAPIException):
    errors_map = {
        "not_support": {
            "message": "Current action is not supported! | {}",
            "type": "bad_request"
        }
    }
