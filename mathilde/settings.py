"""
Django settings for chen project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import json
import os

from alauda.common.log import get_log_config

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
PRE_BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'ptz)#ma@l7bls%@24^3l$7@p!@f*u+(d4%2$i8per4zc!m9^3='

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = ['*']


# Application definition

INSTALLED_APPS = (
    'rest_framework',
    'catalog',
    'plugin_center'
)


REST_FRAMEWORK = {
    'DEFAULT_MODEL_SERIALIZER_CLASS': 'rest_framework.serializers.ModelSerializer',

    # Default to 10
    'PAGINATE_BY': 1000,

    # Allow plugin_client to override, using `?page_size=xxx`. 'page_size'
    'PAGINATE_BY_PARAM': None,

    # Maximum limit allowed when using `?page_size=xxx`.
    'MAX_PAGINATE_BY': 100,

    # 'EXCEPTION_HANDLER': 'mekansm.contrib.rest_framework.exception_handler.rest2_exception_handler'    # noqa

}

REST_FRAMEWORK['DEFAULT_RENDERER_CLASSES'] = (
    'rest_framework.renderers.JSONRenderer',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.csrf.CsrfViewMiddleware',
    'mekansm.contrib.django.middleware.MekAPIMiddleware'
)

ROOT_URLCONF = 'mathilde.urls'

WSGI_APPLICATION = 'mathilde.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases
DEFAULT_DB_ENGINE = "django.db.backends.postgresql_psycopg2"
if os.getenv("DB_ENGINE") == "postgresql":
    DB_ENGINE = "django.db.backends.postgresql_psycopg2"
elif os.getenv("DB_ENGINE") == "mysql":
    DB_ENGINE = "django.db.backends.mysql"
else:
    DB_ENGINE = os.getenv("DB_ENGINE", DEFAULT_DB_ENGINE)
DATABASES = {
    'default': {
        'ENGINE': DB_ENGINE,
        'NAME': os.getenv('DB_NAME'),
        'USER': os.getenv('DB_USER'),
        'HOST': os.getenv('DB_HOST'),
        'PASSWORD': os.getenv('DB_PASSWORD'),
        'PORT': os.getenv('DB_PORT', 5432)
    }
}


# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/

STATIC_URL = '/static/'


# custom settings

# logging
MATHILDE_COMPONENT = 'chen'

LOG_PATH = os.getenv('LOG_PATH', '/var/log/mathilde/')
LOG_LEVEL = os.getenv('LOG_LEVEL', 'DEBUG')
LOG_HANDLER = os.getenv('LOG_HANDLER', 'debug,info,error').split(',')

LOGGING = get_log_config(MATHILDE_COMPONENT, LOG_HANDLER, LOG_LEVEL, LOG_PATH)

# plugin_client

CHEN_API_VERSION = os.getenv('CHEN_API_VERSION', 'v1')
URL_REGEX = '[A-Za-z0-9-_.]+'
UUID_REGEX = '[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}'

DRUID_CLIENT = {
    'name': 'druid',
    'endpoint': os.getenv('DRUID_URL', 'testtest'),
    'api_version': os.getenv('DRUID_API_VERSION', 'v1')
}

JAKIRO_CLIENT = {
    'name': 'jakiro',
    'endpoint': os.getenv('JAKIRO_URL', 'http://api-int.alauda.cn'),
    'api_version': os.getenv('JAKIRO_API_VERSION', 'v1')
}

MIRANA2_ENDPOINT = os.getenv('MIRANA2_ENDPOINT', 'http://172.20.0.4:30003')

INTEGRATION_CLIENT = {
    'name': 'integration',
    'endpoint': os.getenv('INTEGRATION_URL'),
    'api_version': os.getenv('INTEGRATION_API_VERSION', 'v1')
}


def get_version_list(env_name, default):
    version_list_str = os.getenv(
        env_name, default)
    version_list = json.loads(version_list_str)
    version_array = []
    for version in version_list:
        version_array.append({'version': version})
    return version_array


BATCH_PROCESSING_VERSION_LIST = get_version_list(
    'BATCH_PROCESSING_VERSION_LIST', '["2.0.0-2.7.3-1.7.0","2.0.0-2.7.3-1.7.0-no-ip","latest"]')

KAFKA_VERSION_LIST = get_version_list(
    'KAFKA_VERSION_LIST', '["2.12"]'
)

ZOOKEEPER_VERSION_LIST = get_version_list(
    'ZOOKEEPER_VERSION_LIST', '["3.5.0","3.4.9","3.4.6"]'
)

micr_service = os.getenv('MICR_SERVICE', '').split(',')
middleware = os.getenv('MIDDLEWARE', '').split(',')
big_data = os.getenv('BIG_DATA', '').split(',')

APP_TYPE = {
    'MICR_SERVICE': micr_service,
    'MIDDLEWARE': middleware,
    'BIG_DATA': big_data
}

SYNC_COUNT = os.getenv('SYNC_COUNT', 3)
DEFAULT_TIMEOUT = os.getenv('DEFAULT_TIMEOUT', 1440)
MEDUSA_ENDPOINT = os.getenv('MEDUSA_ENDPOINT')
MEDUSA_API_VERSION = os.getenv('MEDUSA_API_VERSION', 'v1')
STATUS_SYNC_RULE = os.getenv('STATUS_SYNC_RULE', '*/3 * * * *')
CHEN2_ENDPOINT = os.getenv('CHEN2_ENDPOINT')
CHEN2_API_VERSION = os.getenv('CHEN2_API_VERSION', 'v1')
PUBLIC_CHART_REPO_NAME = os.getenv('PUBLIC_CHART_REPO_NAME', 'public_plugin_center_repo')
CHART_CLAIR_TEMPLATE = {
    'name': os.getenv('CHART_CLAIR_TEMPLATE_NAME', 'clair'),
    'parent_repo': {
        'name': PUBLIC_CHART_REPO_NAME,
        'is_public': True
    },
    'version': {
        'number': os.getenv('CHART_CLAIR_TEMPLATE_VERSION_NUMBER', '1.0.0')
    }

}
CHART_SONARQUBE_TEMPLATE = {
    'name': os.getenv('CHART_CLAIR_TEMPLATE_NAME', 'sonarqube'),
    'parent_repo': {
        'name': PUBLIC_CHART_REPO_NAME,
        'is_public': True
    },
    'version': {
        'number': os.getenv('CHART_CLAIR_TEMPLATE_VERSION_NUMBER', '1.0.0')
    }
}
IMAGE_INDEX = os.getenv('IMAGE_INDEX', 'index.alauda.cn')
