from django.conf.urls import patterns, include, url
from django.conf import settings

from alauda.common.web import ping
from common.views import HealthCheckViewSet


__author__ = "junh@alauda.io"


urlpatterns = patterns(
    '',
    url(r'^{}/'.format(settings.CHEN_API_VERSION), include([
        url(r'^ping/?$', ping(saying='chen: You can learn faith at the end of a sword.')),
        url(r'^catalog/', include('catalog.urls'))
    ])),
    url(r'^ping/?$', ping(saying='chen: You can learn faith at the end of a sword.')),
    url(r'^_ping/?$', ping(saying='chen: You can learn faith at the end of a sword.')),
    url(r'^_diagnose/?$', HealthCheckViewSet.as_view({"get": "diagnose"})),
    url(r'^v2/catalog/', include('catalog.urls_v2')),
    url(r'^v1/plugin_types/', include('plugin_center.urls')),
)
