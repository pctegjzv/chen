#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import absolute_import
from __future__ import print_function
from __future__ import unicode_literals
import requests
import time
from mathilde import settings


def _get_druid_info():
    detail = {"status": "OK"}
    resp = requests.get('{}/{}/ping'.format(
        settings.DRUID_CLIENT['endpoint'],
        settings.DRUID_CLIENT['api_version'],
        timeout=5
    ))
    if resp.status_code != 200:
        detail['status'] = 'ERROR'
    detail['message'] = resp.status_code
    detail['name'] = 'druid'
    return detail


def _get_integration_info():
    detail = {"status": "OK"}
    resp = requests.get('{}/_ping'.format(
        settings.INTEGRATION_CLIENT['endpoint'],
        timeout=5
    ))
    if resp.status_code != 200:
        detail['status'] = 'ERROR'
    detail['message'] = resp.status_code
    detail['name'] = 'integration'
    return detail


def diagnose():
    result = {
        "status": "OK",
        "name": "chen",
        "details": []
    }

    checks = {
        "druid": _get_druid_info,
        "integration": _get_integration_info
    }

    for name, func in checks.items():
        detail = {"name": name, "status": "OK"}
        start = time.time()
        try:
            detail.update(func())
            if detail["status"] == "ERROR":
                result["status"] = "ERROR"
        except Exception as e:
            result["status"] = "ERROR"
            detail["status"] = "ERROR"
            detail["message"] = e.message
        detail['latency'] = time.time() - start
        result["details"].append(detail)
    return result
