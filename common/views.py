from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK
from common.diagnose import diagnose


class HealthCheckViewSet(viewsets.ModelViewSet):

    def diagnose(self, request):
        return Response(data=diagnose(), status=HTTP_200_OK)
