FROM index.alauda.cn/alaudaorg/alaudabase:ubuntu-14.04-global-1.1

MAINTAINER junh@alauda.io

RUN apt-get update && apt-get install -y \
    python-m2crypto \
    git && rm -rf /var/lib/apt/lists/*

RUN mkdir -p /var/log/mathilde/ \
    && chmod 775 /var/log/mathilde/ \
    && mkdir -p /var/log/uwsgi/ \
    && chmod 755 /var/log/uwsgi/

RUN pip install --no-cache-dir pyopenssl ndg-httpsclient pyasn1

RUN rm -rf /etc/nginx/conf.d/default.conf \
    && echo "daemon off;" >> /etc/nginx/nginx.conf

WORKDIR /chen

EXPOSE 8080
COPY . /chen

RUN pip install --no-cache-dir -r /chen/requirements.txt
RUN pip install --no-cache-dir --trusted-host pypi.alauda.io --extra-index-url http://mathildetech:Mathilde1861@pypi.alauda.io/simple/ -r /chen/requirements-alauda.txt

RUN ln -s /chen/docker/nginx.conf /etc/nginx/conf.d/nginx.conf \
    && ln -s /chen/docker/supervisord.conf /etc/supervisord.conf \
    && chmod +x /chen/chen.sh

CMD ["/chen/chen.sh"]
