import logging
from django.conf import settings
from mekansm.request import MekRequest

logger = logging.getLogger(__name__)


class Chen2Request(MekRequest):
    endpoint = '{}/{}'.format(settings.CHEN2_ENDPOINT, settings.CHEN2_API_VERSION)
    headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'User-Agent': 'chen/v1.0'
    }


class Chen2Client(object):
    @classmethod
    def create_application(cls, create_app_req):
        response = Chen2Request.send(
            'catalog/applications/', method='POST', data=create_app_req)['data']
        return {
            "uuid": response.get('resource', {}).get('uuid'),
            "app_name": response.get('resource', {}).get('name')
        }
