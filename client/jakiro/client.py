#!/usr/bin/env python
# -*- coding: utf-8 -*-

import datetime
import logging
import json
from io import BytesIO

import requests
from django.conf import settings
from catalog.exceptions import CatalogServiceError


logger = logging.getLogger(__name__)
JAKIRO = settings.JAKIRO_CLIENT
ENDPOINT = JAKIRO.get('endpoint')
API_VERSION = JAKIRO.get('api_version')

__author__ = "jliao@alauda.io"


class JakiroClient(object):
    @classmethod
    def _do_req(cls, path, method, data=None, files=None, headers={},
                accpect_file=False, api_version="v1"):
        assert ENDPOINT, 'path is not assigned'
        logger.debug('headers={}'.format(headers))
        my_header = {
            'User-Agent': 'chen/v1.0'
        }
        my_header.update(headers)

        try:
            url = "{}/{}{}".format(ENDPOINT, api_version, path)
            start_time = datetime.datetime.now()
            logger.debug("{} {} data={}, headers={}".format(method, url, data, my_header))
            resp = requests.request(method, url, data=data,
                                    headers=my_header, timeout=30, files=files)
            cost = datetime.datetime.now() - start_time
            logger.info('Get Jakiro from jakiro server. | url={}, resp={}, cost={}'.format(
                url, resp, cost))
        except Exception as e:
            logger.error('Jakiro request error {}'.format(e.message))
            raise

        logger.info('Jakiro response code {}'.format(resp.status_code))
        if resp.status_code >= 300:
            logger.info('Jakiro response error {}'.format(resp.text))
            code, error_message = cls.parse_error_resp(resp)
            if data and data.get('source') == 'PLUGIN':
                raise CatalogServiceError(
                    http_status=resp.status_code,
                    error_code='invalid_args',
                    message=error_message
                )
            raise CatalogServiceError(http_status=resp.status_code, error_code=code,
                                      message=error_message)

        if accpect_file:
            try:
                return BytesIO(resp.content)
            except:
                return BytesIO()
        else:
            try:
                return resp.json()
            except:
                return resp.text

    @classmethod
    def get_app_list(cls, app_type, namespace, user_token):
        headers = {
            'Authorization': 'Token {}'.format(user_token)
        }
        resp = cls._do_req(
            "/catalog/{}/{}/apps/".format(namespace, app_type),
            'GET', headers=headers)
        return resp

    @classmethod
    def create(cls, region_name, app_name, namespace, user_token,
               space_name, project_name, compose_content, app_type,
               strict_mode=False, source='CATALOG'):
        # validate data
        payload = {
            'region': region_name,
            'app_name': app_name,
            'space_name': space_name,
            'source': source,
            'type': app_type

        }
        if strict_mode:
            payload.update(
                {
                    'strict_mode': True,
                    'timeout': 150
                })
        headers = {
            'Authorization': 'Token {}'.format(user_token)
        }
        files = {'services': ('compose.yaml', compose_content)}
        resp = cls._do_req(
            "/applications/{}/?project_name={}".format(namespace, project_name),
            'POST', data=payload, files=files, headers=headers)
        return resp

    @classmethod
    def update(cls, namespace, app_id, user_token,
               compose_content, strict_mode=False):
        # validate data
        payload = {
        }
        if strict_mode:
            payload.update(
                {
                    'strict_mode': True,
                    'timeout': 150
                })
        headers = {
            'Authorization': 'Token {}'.format(user_token)
        }
        files = {'services': ('compose.yaml', compose_content)}
        resp = cls._do_req(
            "/applications/{}/{}/".format(namespace, app_id),
            'PUT', data=payload, files=files, headers=headers)
        return resp

    @classmethod
    def get_app(cls, namespace, app_id, user_token, project_name, is_new_k8s=False):
        headers = {
            'Authorization': 'Token {}'.format(user_token)
        }
        try:
            if is_new_k8s:
                app_resp = cls._do_req(
                    "/apps/{}".format(app_id),
                    'GET', headers=headers, api_version="v2")
                resp = {}
                resp['current_status'] = app_resp['resource']['status']
                resp['uuid'] = app_resp['resource']['uuid']
                resp['services'] = []
                for service in app_resp['services']:
                    if service.get('type') == 'AlaudaService':
                        resource = service.get('resource')
                        resp['services'].append({
                            'service_name': resource.get('name'),
                            'uuid': resource.get('uuid'),
                        })
            else:
                resp = cls._do_req(
                    "/applications/{}/{}/?project_name={}&meta=true".format(namespace, app_id,
                                                                            project_name),
                    'GET', headers=headers)
        except CatalogServiceError as e:
            logger.error('{}:{}'.format(e.error_code, e.message))
            if e.error_code == 'resource_not_exist':
                return None
            raise
        return resp

    @classmethod
    def delete_app(cls, namespace, app_id, user_token, project_name, is_new_k8s=False):
        headers = {
            'Authorization': 'Token {}'.format(user_token)
        }
        if is_new_k8s:
            resp = cls._do_req(
                "/apps/{}".format(app_id),
                'DELETE', headers=headers, api_version="v2")
        else:
            resp = cls._do_req(
                "/applications/{}/{}/?project_name={}".format(namespace, app_id,
                                                              project_name),
                'DELETE', headers=headers)
        return resp

    @classmethod
    def create_integration(cls, payload, namespace, user_token, project_name):
        headers = {
            'Authorization': 'Token {}'.format(user_token),
            'Content-Type': 'application/json'
        }
        resp = cls._do_req(
            "/integrations/{}/?project_name={}".format(namespace, project_name),
            'POST', data=json.dumps(payload), headers=headers)
        return resp

    @classmethod
    def get_integration(cls, namespace, uuid, user_token, project_name):
        headers = {
            'Authorization': 'Token {}'.format(user_token)
        }
        try:
            resp = cls._do_req(
                "/integrations/{}/{}/?project_name={}".format(namespace, uuid, project_name),
                "GET", headers=headers
            )
        except CatalogServiceError as e:
            logger.error('{}:{}'.format(e.error_code, e.message))
            if e.error_code == 'resource_not_exist':
                return None
            raise
        return resp

    @classmethod
    def delete_integration(cls, namespace, uuid, user_token, project_name):
        headers = {
            'Authorization': 'Token {}'.format(user_token)
        }
        resp = cls._do_req(
            "/integrations/{}/{}/?project_name={}".format(namespace, uuid, project_name),
            "DELETE", headers=headers
        )
        return resp

    @classmethod
    def parse_error_resp(cls, resp):
        try:
            resp_message = resp.json()
            code = resp_message['errors'][0]['code']
            error_message = resp_message['errors'][0]['message']
            logger.info('error code:{}, msg: {}'.format(code, error_message))
            if code == 'app_already_exist':
                code = 'resource_already_exist'
        except:
            code = 'unknown_issue'
            error_message = resp.text
        return code, error_message
