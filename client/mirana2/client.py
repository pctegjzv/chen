import logging

from django.conf import settings

from mekansm.request import MekRequest

logger = logging.getLogger(__name__)


class Mirana2Request(MekRequest):
    endpoint = '{}/v2'.format(settings.MIRANA2_ENDPOINT)
    headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'User-Agent': 'chen/v1.0'
    }


class Mirana2Client(object):

    @classmethod
    def get_alb_address(cls, region_id, service_id):
        result = cls.list(region_id, service_id)
        if not result:
            return
        listeners = result[0].get('listeners', [])
        for listener in listeners:
            rules = listener.get('rules')
            if rules:
                port = listener.get('listener_port')
                domain = rules[0].get('domain')
                return '{}://{}:{}'.format(listener.get('protocol'), domain, port)

    @classmethod
    def get_alb_domain(cls, region_id, service_id, alb_name):
        results = cls.list(region_id, service_id)
        if not results:
            return
        for result in results:
            if result['name'] != alb_name:
                continue
            listeners = result.get('listeners', [])
            logging.info('listeners:{}'.format(listeners))
            for listener in listeners:
                if listener['protocol'] == 'http':
                    rules = listener.get('rules')
                    if rules:
                        return rules[0].get('domain')
                elif listener['protocol'] == 'tcp':
                    return result['address']

    @classmethod
    def list(cls, region_id, service_id):
        params = {
            'region_id': region_id,
            'service_id': service_id,
            'detail': True
        }
        return Mirana2Request.send('load_balancers/',
                                   method='GET', params=params)['data']
