#!/usr/bin/env python
# -*- coding: utf-8 -*-

import datetime
import json
import logging
import traceback
from io import BytesIO

import requests
from django.conf import settings
from rest_framework.response import Response
from rest_framework import status

from .exceptions import DruidException, DruidServiceError    # noqa
from catalog.exceptions import CatalogException
from catalog.claas.catalog_factory import CatalogFactory
from catalog.models import Application

logger = logging.getLogger(__name__)

HEADER = {
    'content-type': 'application/json',
    'User-Agent': 'chen/v1.0'
}


DRUID = settings.DRUID_CLIENT
API_VERSION = DRUID.get('api_version')
ENDPOINT = DRUID.get('endpoint')
DRUID_URL = '{}/{}'.format(ENDPOINT, API_VERSION)

__author__ = "junh@alauda.io"


class DruidClient(object):
    @classmethod
    def _do_req(cls, path, method, data=None, files=None, headers={},
                accpect_file=False):
        assert ENDPOINT, 'druid path is not assigned'
        headers = HEADER.update(headers)

        try:
            url = DRUID_URL + "/applications/" + path
            start_time = datetime.datetime.now()
            logger.debug("{} {} data={}".format(method, url, data))
            resp = requests.request(method, url, data=data,
                                    headers=headers, timeout=30, files=files)
            cost = datetime.datetime.now() - start_time
            logger.info('Get response from druid server. | url={}, resp={}, cost={}'.format(
                url, resp, cost))
        except Exception as e:
            logger.error('druid request error {}'.format(e.message))
            raise

        logger.info('druid response code {}'.format(resp.status_code))
        if resp.status_code >= 300:
            logger.info('druid response error {}'.format(resp.text))
            try:
                resp_json = resp.json()
                message = resp_json['message']
                error_code = resp_json['explanation'] or 'unknown_issue'
            except:
                message = resp.text
                error_code = 'unknown_issue'
            raise DruidServiceError(resp.status_code, error_code, message)

        if accpect_file:
            try:
                return BytesIO(resp.content)
            except:
                return BytesIO()
        else:
            try:
                return resp.json()
            except:
                return resp.text

    @classmethod
    def create(cls, request, *args, **kwargs):
        app_name = kwargs['app_name']
        logging.info("Druid client create with data: {}, app_name:{}".format(
            request.data, app_name))
        data = request.data
        namespace = request.query_params.get('namespace')
        space_name = data.get('space_name')

        factory = CatalogFactory.get_factory(app_name, data, namespace=namespace)
        try:
            compose_content = factory.get_compose_data()
        except CatalogException as err:
            logger.error("get yaml failed:{}".format(traceback.format_exc()))
            raise CatalogException(err.code)
        except Exception as err:
            logger.error("get yaml failed: {}".format(traceback.format_exc()))
            raise CatalogException('catalog_invalid_config')

        # validate data
        payload = {
            'region': data.get('region', ''),
            'namespace': data.get('namespace', ''),
            'app_name': [data.get('name', '')],
            'created_by': data.get('created_by', ''),
            'domain_name': [data.get('domain_name', '')],
            'space_name': space_name,
            'source': 'CATALOG',
            'type': app_name
        }

        logger.debug("payload for druid: {}".format(payload))
        logger.debug("compose_content for druid: {}".format(compose_content))
        files = {'services': ('compose.yaml', compose_content)}
        headers = {
            'content-type': 'multipart/form-data',
        }
        _resp = cls._do_req('', 'POST', data=payload, files=files, headers=headers)
        # import uuid
        # _resp = Response(data={"uuid": uuid.uuid4()}, status=status.HTTP_201_CREATED).data
        logger.debug("chen resp data: {}".format(_resp))

        logger.info('Creating the application resource with following data:')
        logger.info('uuid={}, namespace={}, created_by={}, name={} region_id={}'.format(
            _resp['uuid'], payload['namespace'], payload['created_by'],
            payload['app_name'], payload['region']))

        resp_data = _resp
        uuid = resp_data.get('uuid')
        region_id = data.get('region')[0]

        logger.debug("save to database, catalog_type:{}, uuid:{}, region:{}".format(
            app_name, uuid, region_id))
        Application.objects.create(
            type=app_name, uuid=uuid, region_id=region_id, config_json=json.dumps(data))
        result = {"uuid": uuid}
        logger.debug("Applications created: {}".format(result))

        return Response(status=status.HTTP_201_CREATED, data=result)

    @classmethod
    def application_info(cls, uuid, detail):
        """
        Get application info from druid
        :param uuid:
        :param detail:
        :return:
        """

        if uuid is None:
            raise DruidException('invalid_parameter')

        url = "{}/?detail={}".format(uuid, detail)
        _resp = cls._do_req(url, 'GET')
        logger.debug("druid_client_application_info:url={} resp={}".format(url, _resp))
        return Response(status=status.HTTP_200_OK, data=_resp)

    @classmethod
    def list(cls, app_name, uuids):
        """
        Get application status from druid.
        :param app_name:
        :param uuids:
        :return:
        """
        # TODO: validate app_name

        if uuids is None:
            raise DruidException('invalid_parameter')

        app_uuids = uuids.strip(' ,').split(',')

        applications = Application.objects.filter(type=app_name, uuid__in=app_uuids)
        logger.debug("start list running apps....")

        service_uuid = '?uuids='
        uuids = [i.uuid for i in applications]
        if len(uuids) == 0:
            return Response(status=status.HTTP_200_OK, data=dict())
        uuid = ','.join(uuids)
        service_uuid = '{}{}'.format(service_uuid, uuid)
        _resp = cls._do_req(service_uuid, 'GET')
        logger.debug("get resp from druid: {}".format(_resp))

        # build the uuid list
        result = dict()
        for item in _resp:
            app = dict()
            app.update({
                'uuid': str(item['unique_name']),
                'status': item['current_status']
            })
            if item['region'] not in result:
                result[item['region']] = list()
            result[item['region']].append(app)

        logger.debug("list running apps, result: {}".format(result))

        return Response(status=status.HTTP_200_OK, data=result)
