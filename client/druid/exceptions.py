from mekansm.exceptions import MekAPIException, MekBaseAPIException

__author__ = "junh@alauda.io"


class DruidException(MekAPIException):
    errors_map = {
        'app_already_exist': {
            'message': '{} app already exist.'
        },
        'field_is_required': {
            'message': '{} field is required.'
        },
        'invalid_parameter': {
            'message': 'uuids parameter not found'
        }
    }


class DruidServiceError(MekBaseAPIException):

    def __init__(self, http_status, error_code, message):
        self._status_code = http_status
        self.error_code = error_code
        self.message = message
        self.error_type = 'service_error'

    @property
    def status_code(self):
        return self._status_code

    @property
    def data(self):
        return {
            'code': self.error_code,
            'message': self.message,
            'source': '10027'
        }
