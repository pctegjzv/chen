#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.db import models
from django.utils.encoding import python_2_unicode_compatible


__author__ = 'junh@alauda.io'


@python_2_unicode_compatible
class Application(models.Model):
    type = models.CharField(max_length=30)
    uuid = models.CharField(max_length=36, unique=True)
    region_id = models.CharField(max_length=40, blank=False, null=False)
    # store the original config what used to create the app
    config_json = models.TextField(blank=True, default='{}')
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['created_at']

    def __str__(self):
        return "type: {}-uuid: {}-region_id: {}".format(self.type, self.uuid, self.region_id)
