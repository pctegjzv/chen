#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
RESTful view classes for presenting Catalog API objects
"""

from __future__ import absolute_import
from __future__ import unicode_literals

import logging
from rest_framework import viewsets, status
from rest_framework.response import Response

from catalog.utils.catalog_handler import (get_application_detail, list_supported_data,
                                           delete_app, list_app_num_data)
from client.druid.client import DruidClient
from client.jakiro.client import JakiroClient
from .claas.catalog_factory import CatalogFactory
from catalog.exceptions import CatalogException

logger = logging.getLogger(__name__)

__author__ = "junh@alauda.io"


class CatalogViewSet(viewsets.ModelViewSet):

    def list_app_num(self, request):
        return_data = list_app_num_data(request)
        return Response(status=status.HTTP_200_OK, data=return_data)

    def list_running_apps(self, request, *args, **kwargs):
        app_name = kwargs['app_name']
        uuids = request.query_params.get('uuids', None)
        return DruidClient.list(app_name, uuids)

    def create(self, request, *args, **kwargs):
        """
        create an application
        :param request:
        :return:
        """
        logger.info('Start creating application...with request.data:{}'.format(request.data))
        _resp = DruidClient.create(request, *args, **kwargs)
        logger.debug("_resp.data from DruidClient Response{}".format(_resp.data))

        return Response(status=status.HTTP_201_CREATED, data=_resp.data)

    def list_app_detail(self, request, app_name):
        return_data = get_application_detail(app_name)
        return Response(status=status.HTTP_200_OK, data=return_data)

    def delete_app(self, request, *args, **kwargs):
        app_id = kwargs['app_name']
        logger.debug("Deleting apps, app_id:{}".format(app_id))
        delete_app(app_id)
        return Response(status=status.HTTP_204_NO_CONTENT)

    def list_supported(self, request):
        return_data = list_supported_data(request)
        return Response(status=status.HTTP_200_OK, data=return_data)

    def get_app_detail(self, request, *args, **kwargs):
        app_name = kwargs['app_name']
        uuid = kwargs['uuid']
        namespace = request.query_params.get('namespace')
        dashboard_service_id = request.query_params.get('dashboard_service_id')
        app_cls = CatalogFactory.get_factory(app_name, None, namespace)
        app_cls.set_dashboard_service_id(dashboard_service_id)
        app_detail = app_cls.get_app_detail(uuid)
        return Response(data=app_detail)

    def update_app(self, request, **kwargs):
        app_name = kwargs['app_name']
        uuid = kwargs['uuid']
        data = request.data
        namespace = data.get('namespace')
        app_cls = CatalogFactory.get_factory(app_name, None, namespace)
        app_cls.init_by_app_id(uuid)
        new_yaml_content = app_cls.get_updated_yaml_content(data)
        if new_yaml_content:
            if kwargs['app_name'] == 'batch_processing':
                strick_mode = True
            else:
                strick_mode = False
            JakiroClient.update(namespace, uuid, data.get('user_token'),
                                new_yaml_content, strick_mode)
        app_cls.update_config_json()
        return Response(status=status.HTTP_201_CREATED, data={"uuid": uuid})

    def get_dashboard_url(self, request, **kwargs):
        app_name = kwargs['app_name']
        uuid = kwargs['uuid']
        namespace = request.query_params.get('namespace')
        app_cls = CatalogFactory.get_factory(app_name, None, namespace)
        dashboard_service_id = request.query_params.get('dashboard_service_id')
        app_cls.set_dashboard_service_id(dashboard_service_id)
        app_cls.init_by_app_id(uuid)
        url = app_cls.get_dashboard_url(request.query_params)
        return Response(data={"dashboard_url": url})

    def show_update_info(self, request, *args, **kwargs):
        logger.debug('run in show_update_info')
        app_name = kwargs['app_name']
        uuid = kwargs['uuid']
        # assert app_uuid, 'app_uuid is null'
        if uuid == '':
            logger.debug("[chen] show_update_info => uuid is empty.")
            raise CatalogException('invalid_args', 'uuid is empty!')
        app_cls = CatalogFactory.get_factory(app_name, None, None)
        result = app_cls.get_app_detail(uuid)
        logger.debug('\n resp:{}\n'.format(result))
        return Response(status=status.HTTP_200_OK, data=result)
