#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
RESTful view classes for presenting Catalog API objects
"""

from __future__ import absolute_import
from __future__ import unicode_literals

import logging
import json
from rest_framework import viewsets, status
from rest_framework.response import Response

from catalog.claas.catalog_factory import CatalogFactory
from catalog.models import Application
from client.jakiro.client import JakiroClient

logger = logging.getLogger(__name__)

__author__ = "jliao@alauda.io"


class CatalogViewSet(viewsets.ModelViewSet):

    def create(self, request, *args, **kwargs):
        """
        create an application
        :param request:
        :return:
        """
        logger.info('Start creating application...with request.data:{}'.format(request.data))
        # generate yaml
        app_name = kwargs['app_name']
        data = request.data
        app_factory = CatalogFactory.get_factory(app_name, data, namespace=None)
        compose_content = app_factory.get_compose_data()
        logger.info('Create app yaml:{}'.format(compose_content))

        # call jakiro create applicaton
        if kwargs['app_name'] == 'batch_processing':
            response = JakiroClient.create(
                data.get('region'), data.get('name'), data.get('namespace'),
                data.get('user_token'), data.get('space_name'), data.get('project_name'),
                compose_content, app_name, True)
        else:
            response = JakiroClient.create(
                data.get('region'), data.get('name'), data.get('namespace'),
                data.get('user_token'), data.get('space_name'), data.get('project_name'),
                compose_content, app_name)
        app_uuid = response.get('uuid')
        Application.objects.create(type=app_name, uuid=app_uuid,
                                   region_id=data.get('region_uuid'),
                                   config_json=json.dumps(data))

        return Response(status=status.HTTP_201_CREATED, data=response)
