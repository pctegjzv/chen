from django.conf import settings
from django.conf.urls import patterns, url


from catalog.views import CatalogViewSet
REGEX = settings.URL_REGEX

__author__ = "junh@alauda.io"


urlpatterns = patterns(
    '',
    url('^app_num/?$', CatalogViewSet.as_view({'get': 'list_app_num'})),

    url('^(?P<app_name>{})/apps/?$'.format(REGEX),
        CatalogViewSet.as_view({'get': 'list_running_apps', 'post': 'create'}),
        name='catalog/app_name/apps'),

    url('^(?P<app_name>{})/apps/(?P<uuid>{})/?$'.format(REGEX, REGEX),
        CatalogViewSet.as_view({'get': 'get_app_detail', 'put': 'update_app'}),
        name='catalog/app_name/apps'),

    url('^(?P<app_name>{})/apps/(?P<uuid>{})/dashboard/url/?$'.format(REGEX, REGEX),
        CatalogViewSet.as_view({'get': 'get_dashboard_url'}),
        name='catalog/app_name/apps'),

    url('^(?P<app_name>{})/?$'.format(REGEX),
        CatalogViewSet.as_view({'get': 'list_app_detail', 'delete': 'delete_app'}),
        name='catalog/app_name/'),

    url('^/?$', CatalogViewSet.as_view({'get': 'list_supported'}),
        name='catalog/'),

    url('^(?P<app_name>{})/elastic/(?P<uuid>{})/?$'.format(REGEX, REGEX),
        CatalogViewSet.as_view({'get': 'show_update_info'}),
        name='catalog/app_name/elastic'),
)
