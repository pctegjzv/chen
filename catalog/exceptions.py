from mekansm.exceptions import MekAPIException, MekBaseAPIException

__author__ = "junh@alauda.io"

image_tag_options_is_empty = 'image_tag_options_is_empty'


class CatalogException(MekAPIException):
    source = 1041
    errors_map = {
        "catalog_invalid_config": {
            "message": "Not properly configured"
        },
        "catalog_invalid_node_ip_tag": {
            "message": "The number of nodes does not match the number of ip_tag"
        },
        "catalog_invalid_zookeeper_cluster": {
            "message": "zookeeper cluster is invalid"
        },
        "catalog_mount_data_dir_cant_be_empty": {
            "message": "The data directory can not be empty"
        },
        "catalog_mount_data_dir_format_invalid": {
            "message": "The data directory format is invalid, \
                        it can only contain characters like slashes, numbers, letters, \
                        hyphens(-), underscores(_) and dots(.), and it must be absolute path"
        },
        "catalog_replicas_per_shard_cant_be_empty": {
            "message": "The replicas per shard can not be zero"
        },
        "catalog_mongodb_secondary_node_number_cant_less_two": {
            "message": "secondary node number has to be greater than or equal to 2"
        },
        "catalog_replicas_number_cant_be_empty": {
            "message": "The number of replicas can not be zero"
        },
        "catalog_invalid_shards_num": {
            "message": "The number of shards should not be less than 3"
        },
        "catalog_invalid_master_node_addr": {
            "message": "The master node address must in the ip tag list"
        },
        "catalog_invalid_primary_node_addr": {
            "message": "The primary node address must in the ip tag list"
        },
        "catalog_invalid_port": {
            "message": "The {} must between {} and {}"
        },
        "catalog_port_conflict": {
            "message": "The {} is used by {}, please change one"
        },
        "dbproxy_not_support_mysql_version": {
            "message": "dbproxy is not support mysql version {}"
        },
        "catalog_redis_password_too_weak": {
            "message": "The password of redis is too weak. \
                        It should contain number and upper and lowercase letters"
        },
        "redis_password_too_short": {
            "message": "The length of password should not be less than 8"
        },
        "redis_password_too_long": {
            "message": "The length of password should not be more than 32"
        },
        "redis_password_invalid": {
            "message": "The password must be printable ASCII characters, \
                        and can not contain space/single quote/double quote"
        },
        "password_too_short": {
            "message": "The length of password ({}) should not be less than 8"
        },
        "password_too_long": {
            "message": "The length of password ({}) should not be more than 32"
        },
        "password_cant_be_empty": {
            "message": "The password ({}) can not be empty"
        },
        "password_invalid": {
            "message": "The password ({}) must be printable ASCII characters, \
                            and can not contain space/single quote/double quote"
        },
        "catalog_password_too_weak": {
            "message": "The password ({}) is too weak. \
                            It should contain number and upper and lowercase letters"
        },
        "catalog_name_format_invalid": {
            "message": "The {} format is invalid, \
                            it can only contain characters like numbers, letters, \
                            hyphens(-) and underscores(_)"
        },
        "mysql_nodes_num_invalid": {
            "message": "The number of mysql nodes should not be less than 3"
        },
        "mysql_root_password_cant_be_empty": {
            "message": "The mysql root password can not be empty"
        },
        "mysql_client_password_cant_be_empty": {
            "message": "The mysql client password can not be empty"
        },
        "catalog_mongodb_root_password_cant_be_empty": {
            "message": "The mongodb root password can not be empty when the user exists"
        },
        "mysql_memory_is_too_small": {
            "message": "The memory allocated to mysql is too small"
        },
        "dbproxy_should_use_in_replication_mode": {
            "message": "The dbproxy should use in replication mode"
        },
        "mysql_repl_user_conflict_with_client_user": {
            "message": "The replication username can not be the same with client username, \
                        please change one"
        },
        "zookeeper_even_number": {
            "message": "The number of zookeeper nodes should be odd"
        },
        "redis_cluster_invalid_node_num": {
            "message": "Redis cluster mode, the number of nodes must be greater than 5"
        },
        "secondary_name_node_invalid_node_ip_tag": {
            "message": "backup_master can not be deployed on the same node with master."
        },
        "nodes_not_enough": {
            "message": "nodes is not enough to deploy the application"
        },
        "image_tag_options_is_empty": {
            "message": "image tag options can't be empty"
        },
        "host_port_is_not_match": {
            "message": "host num is not the same as port num"
        },
        "master_ip_repeated": {
            "message": "different master cannot be deployed on same node."
        }

    }


class CatalogServiceError(MekBaseAPIException):

    def __init__(self, http_status, error_code, message):
        self._status_code = http_status
        self.error_code = error_code
        self.message = message
        self.error_type = 'service_error'

    @property
    def status_code(self):
        return self._status_code

    @property
    def data(self):
        return {
            'code': self.error_code,
            'message': self.message,
            'source': 1041
        }
