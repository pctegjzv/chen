# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0003_auto_20170313_0237'),
    ]

    operations = [
        migrations.AddField(
            model_name='application',
            name='config_json',
            field=models.TextField(default='{}', blank=True),
        ),
    ]
