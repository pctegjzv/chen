from django.conf import settings
from django.conf.urls import patterns, url


from catalog.views_v2 import CatalogViewSet
REGEX = settings.URL_REGEX

__author__ = "jliao@alauda.io"


urlpatterns = patterns(
    '',
    url('^(?P<app_name>{})/apps/?$'.format(REGEX),
        CatalogViewSet.as_view({'post': 'create'}),
        name='v2/catalog/app_name/apps'),
)
