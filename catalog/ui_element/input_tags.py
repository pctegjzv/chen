# -*- coding: utf-8 -*-
from base_element import BaseElement


class InputTags(BaseElement):
    type = "tags_input"

    def __init__(self,
                 display_name_en, display_name_cn):
        self.display_name_en = display_name_en
        self.display_name_cn = display_name_cn

    def to_json(self):
        result = {
            "attribute_name": self.name,
            "type": self.type,
            "display_name": {
                "en": self.display_name_en,
                "zh": self.display_name_cn
            }
        }
        return result


class ElementPodAntiAffinity(InputTags):
    name = 'pod_anti_affinity'

    def __init__(self):
        super(ElementPodAntiAffinity, self).__init__(
            'pod anti affinity', '反亲和标签')
