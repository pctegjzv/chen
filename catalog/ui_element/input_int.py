# -*- coding: utf-8 -*-
from base_element import BaseElement


class InputInt(BaseElement):
    type = 'int'

    def __init__(self, min_value, max_value, display_name_en, display_name_cn, default_value,
                 pattern=None, pattern_hint=None):
        self.min_value = min_value
        self.default_value = default_value
        self.display_name_en = display_name_en
        self.display_name_cn = display_name_cn
        self.max_value = max_value
        self.pattern = pattern
        self.pattern_hint = pattern_hint

    def to_json(self):
        result = {
            "attribute_name": self.name,
            "type": self.type,
            "display_name": {
                "en": self.display_name_en,
                "zh": self.display_name_cn
            }
        }
        if self.min_value:
            result['min_value'] = self.min_value
        if self.default_value:
            result['default_value'] = self.default_value
        if self.max_value:
            result['max_value'] = self.max_value
        if self.pattern:
            result['pattern'] = self.pattern
        if self.pattern_hint:
            result['pattern_hint'] = self.pattern_hint
        return result

    @property
    def get_default_value(self):
        return self.default_value


class ElementInstanceNum(InputInt):
    name = 'num_of_instances'

    def __init__(self, init_num=1):
        super(ElementInstanceNum, self).__init__(
            init_num, None, 'Number of instances', '集群实例数', init_num)


class ElementInternalPort(InputInt):
    name = 'internal_port'

    def __init__(self, port=80):
        super(ElementInternalPort, self).__init__(
            None, None, 'internal port', '容器端口号', port)


class ElementNodePort(InputInt):
    name = 'node_port'

    def __init__(self, port=30000):
        super(ElementNodePort, self).__init__(
            30000, 32000, 'node port', '主机端口号', port)
