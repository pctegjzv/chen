# -*- coding: utf-8 -*-
from catalog.ui_element.base_element import BaseElement
from catalog.exceptions import CatalogException


class InputClusterSize(BaseElement):
    type = "cluster_size"
    min_size = None

    def __init__(self, description_en, description_cn, option_array,
                 display_name_en, display_name_cn, option_key=None,
                 option_value=None, default_value={}):
        self.description_en = description_en
        self.description_cn = description_cn
        self.option_array = option_array
        self.display_name_en = display_name_en
        self.display_name_cn = display_name_cn
        self.option_key = option_key
        self.option_value = option_value
        self.default_value = default_value

    def to_json(self):
        result = {
            "attribute_name": self.name,
            "type": self.type,
            "description": {
                "en": self.description_en,
                "zh": self.description_cn
            },
            "option": self.option_array,
            "display_name": {
                "en": self.display_name_en,
                "zh": self.display_name_cn
            },
            "default_value": self.default_value
        }
        if self.option_key:
            result['key'] = self.option_key
        if self.option_value:
            result['value'] = self.option_value
        return result

    @classmethod
    def is_valid(cls, data):
        size = cls.get_value(data)
        if size is None or cls.min_size is None:
            return
        if float(size.get('cpu', 0)) < cls.min_size['cpu'] or \
           float(size.get('mem', 0)) < cls.min_size['mem']:
            raise CatalogException(
                code='invalid_args',
                message='The minimum value of CPU and memory is: '
                        '{}, {}'.format(cls.min_size['cpu'], cls.min_size['mem']))
