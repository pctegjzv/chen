# -*- coding: utf-8 -*-
from base_element import BaseElement


class InputString(BaseElement):
    type = "string"

    def __init__(self, display_name_en, display_name_cn, default_value=None,
                 description=None, required=True, pattern=None, pattern_hint=None):
        self.display_name_en = display_name_en
        self.display_name_cn = display_name_cn
        self.description = description
        self.default_value = default_value
        self.required = required
        self.pattern = pattern
        self.pattern_hint = pattern_hint

    def to_json(self):
        result = {
            "attribute_name": self.name,
            "type": self.type,
            "display_name": {
                "en": self.display_name_en,
                "zh": self.display_name_cn
            },
            "required": self.required,
        }
        if self.default_value:
            result['default_value'] = self.default_value
        if self.description:
            result['description'] = self.description
        if self.pattern:
            result['pattern'] = self.pattern
        if self.pattern_hint:
            result['pattern_hint'] = self.pattern_hint
        return result


class ElementKafkaHost(InputString):
    name = 'kafka_host'

    def __init__(self):
        super(ElementKafkaHost, self).__init__('Kafka host', 'Kafka 地址')


class ElementZooKeeperHost(InputString):
    name = 'zookeeper_host'

    def __init__(self):
        super(ElementZooKeeperHost, self).__init__('ZooKeeper host', 'ZooKeeper 地址')


class ElementKafkaPort(InputString):
    name = 'kafka_port'

    def __init__(self):
        super(ElementKafkaPort, self).__init__(
            'kafka port', 'kafka端口号', '9092')


class ElementZooKeeperPort(InputString):
    name = 'zookeeper_port'

    def __init__(self):
        super(ElementZooKeeperPort, self).__init__(
            'zookeeper port', 'zookeeper端口号', '2181')
