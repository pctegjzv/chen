# -*- coding: utf-8 -*-
from base_element import BaseElement


class InputTable(BaseElement):
    type = "table_input"

    def __init__(self,
                 display_name_en, display_name_cn):
        self.display_name_en = display_name_en
        self.display_name_cn = display_name_cn

    def to_json(self):
        result = {
            "attribute_name": self.name,
            "type": self.type,
            "display_name": {
                "en": self.display_name_en,
                "zh": self.display_name_cn
            }
        }
        return result


class ElementAppTag(InputTable):
    name = 'app_tag'

    def __init__(self):
        super(ElementAppTag, self).__init__(
            'App Tag', '应用标签')
