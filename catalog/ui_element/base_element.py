# -*- coding: utf-8 -*-
class BaseElement(object):
    name = None

    def __init__(self):
        pass

    def to_json(self):
        pass

    @classmethod
    def get_value(cls, data):
        if not data:
            return None
        return data.get(cls.name)

    @classmethod
    def get_name(cls):
        return cls.name

    @classmethod
    def get_type(cls):
        return cls.type
