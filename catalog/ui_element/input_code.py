# -*- coding: utf-8 -*-
from base_element import BaseElement


class InputCode(BaseElement):
    type = 'code'

    def __init__(self,
                 display_name_en, display_name_cn):
        self.display_name_en = display_name_en
        self.display_name_cn = display_name_cn

    def to_json(self):
        result = {
            "attribute_name": self.name,
            "type": self.type,
            "display_name": {
                "en": self.display_name_en,
                "zh": self.display_name_cn
            }
        }
        return result


class ElementConfigServerRule(InputCode):
    name = 'config_server_rule'

    def __init__(self):
        super(ElementConfigServerRule, self).__init__('config server rule', 'config server rule')


class ElementZuulRouteRule(InputCode):
    name = 'zuul_route_rule'

    def __init__(self):
        super(ElementZuulRouteRule, self).__init__(
            'zuul route rule', 'zuul 路由规则')
