# -*- coding: utf-8 -*-
from base_element import BaseElement


class InputMultiOption(BaseElement):
    type = "multi_option"

    def __init__(self, description_en, description_cn, option_array,
                 display_name_en, display_name_cn, option_key=None, option_value=None):
        self.description_en = description_en
        self.description_cn = description_cn
        self.option_array = option_array
        self.display_name_en = display_name_en
        self.display_name_cn = display_name_cn
        self.option_key = option_key
        self.option_value = option_value

    def to_json(self):
        result = {
            "attribute_name": self.name,
            "type": self.type,
            "description": {
                "en": self.description_en,
                "zh": self.description_cn
            },
            "option": self.option_array,
            "display_name": {
                "en": self.display_name_en,
                "zh": self.display_name_cn
            }
        }
        if self.option_key:
            result['key'] = self.option_key
        if self.option_value:
            result['value'] = self.option_value
        return result


class ElementNodeTag(InputMultiOption):
    name = 'node_tag'

    def __init__(self, options=None):
        if not options:
            options = []
        super(ElementNodeTag, self).__init__(
            'Deploy to specific host',
            '定点部署', options, 'Node Tag', '主机标签')


class ElementIpTag(InputMultiOption):
    name = 'ip_tag'

    def __init__(self, options=None):
        if not options:
            options = []
        super(ElementIpTag, self).__init__(
            'Deploy to specific host',
            '定点部署', options, 'Ip Tag', '主机IP标签')
