# -*- coding: utf-8 -*-
from base_element import BaseElement


class AppInstruction(BaseElement):
    def __init__(self, en, cn):
        self.en = en
        self.cn = cn

    def to_json(self):
        return {
            'en': self.en,
            'cn': self.cn
        }
