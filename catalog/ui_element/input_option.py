# -*- coding: utf-8 -*-
from base_element import BaseElement
from ..exceptions import CatalogException, image_tag_options_is_empty


class InputOption(BaseElement):
    type = "option"

    def __init__(self, description_en, description_cn, option_array,
                 display_name_en, display_name_cn, option_key=None, option_value=None):
        self.description_en = description_en
        self.description_cn = description_cn
        self.option_array = option_array
        self.display_name_en = display_name_en
        self.display_name_cn = display_name_cn
        self.option_key = option_key
        self.option_value = option_value

    def to_json(self):
        result = {
            "attribute_name": self.name,
            "type": self.type,
            "description": {
                "en": self.description_en,
                "zh": self.description_cn
            },
            "option": self.option_array,
            "display_name": {
                "en": self.display_name_en,
                "zh": self.display_name_cn
            }
        }
        if self.option_key:
            result['key'] = self.option_key
        if self.option_value:
            result['value'] = self.option_value
        return result


class ElementClusterSize(InputOption):
    name = 'cluster_size'

    def __init__(self, options=None):
        if not options:
            options = ['XXS', 'XS', 'S', 'M', 'L', 'XL', 'CUSTOMIZED']
        super(ElementClusterSize, self).__init__(
            'Size of instances',
            '实例大小', options, 'Instance size', '实例大小')


class ElementImageTag(InputOption):
    name = 'image_tag'

    def __init__(self, options):
        if not options:
            raise CatalogException(image_tag_options_is_empty)
        formatted_options = []
        key = 'version'
        for option in options:
            formatted_options.append({key: option})
        super(ElementImageTag, self).__init__(
            '', '', formatted_options, 'image tag', '镜像标签', key, key)


class ElementAlaudaLBName(InputOption):
    name = 'alauda_lb_name'

    def __init__(self, options=[]):
        super(ElementAlaudaLBName, self).__init__(
            '', '', options, 'load balance', '负载均衡')


class ElementConfigServer(InputOption):
    name = 'alauda_config_server'

    def __init__(self, options=[]):
        super(ElementConfigServer, self).__init__(
            '', '', options, 'config server list', 'config server 列表', 'name', 'uuid')
