#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import absolute_import
from __future__ import unicode_literals

import logging

from catalog.models import Application
from catalog.claas import SUPPORTED_APPS, ALL_APPS, LIST_APPS_NUM, \
    app_name_detail_dict
from django.conf import settings

logger = logging.getLogger(__name__)

__author__ = "junh@alauda.io"


def list_supported_data(request):
    app_type = request.query_params.get("app_type")
    list_app = {}
    if app_type in settings.APP_TYPE:
        result = [
            app for app in ALL_APPS.get('result')
            if app['app_name'] in settings.APP_TYPE[app_type]
        ]
    else:
        result = ALL_APPS.get('result')
    list_app['result'] = result
    return list_app


def list_app_num_data(request):
    result = LIST_APPS_NUM
    uuids = request.query_params.get('uuids', None)
    uuids_list = uuids.strip(' ,').split(',')

    logger.debug("SUPPORTED_APPS:{}".format(SUPPORTED_APPS))
    result['result'] = filter(lambda x: x['app_name'] in SUPPORTED_APPS, result['result'])

    apps = Application.objects.filter(uuid__in=uuids_list)
    apps_num_dict = dict()
    for i in apps:
        if i.type not in apps_num_dict:
            apps_num_dict[i.type] = 0
        apps_num_dict[i.type] += 1

    for item in result['result']:
        if item['app_name'] in apps_num_dict:
            item['app_num'] = apps_num_dict[item['app_name']]
    return result


def get_application_detail(app_name):
    data = app_name_detail_dict[app_name]
    return data


def delete_app(app_id):
    if not Application.objects.filter(uuid=app_id).exists():
        logger.debug("[TODO]raise exception")
        logger.error("Chen deleting catalog apps, app_id: {}".format(app_id))
    else:
        Application.objects.filter(uuid=app_id).delete()
        logger.debug("{} has been deleted".format(app_id))
    return
