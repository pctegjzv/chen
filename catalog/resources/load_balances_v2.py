import logging

LB_TYPE_ALAUDA_HA = 'HA'
LB_TYPE_ALAUDA_ALB = 'ALB'

logger = logging.getLogger(__name__)


class LoadBalance(object):
    def __init__(self, lb_name, lb_type):
        self.lb_name = lb_name
        self.lb_type = lb_type

    def get_type(self):
        return self.lb_type

    def get_name(self):
        return self.lb_name


class AlaudaLoadBalance(LoadBalance):
    def __init__(self, lb_name, alb_version):
        if alb_version and alb_version.lower() == 'v2':
            lb_type = LB_TYPE_ALAUDA_ALB
        else:
            lb_type = LB_TYPE_ALAUDA_HA
        super(AlaudaLoadBalance, self).__init__(lb_name, lb_type)
