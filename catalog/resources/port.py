import logging

PROTOCOL_HTTP = 'http'
PROTOCOL_TCP = 'tcp'

logger = logging.getLogger(__name__)


class Port(object):

    def __init__(self, **kwargs):
        self.protocol = kwargs.get('protocol') or []
        self.port = kwargs.get('port') or []

    def get_port_yaml(self):
        pass


class PortHttp(Port):

    def __init__(self, **kwargs):
        kwargs['protocol'] = PROTOCOL_HTTP
        super(PortHttp, self).__init__(**kwargs)

    def get_port_yaml(self):
        return '{}/{}'.format(self.port, self.protocol)


class PortTcp(Port):

    def __init__(self, **kwargs):
        kwargs['protocol'] = PROTOCOL_TCP
        super(PortTcp, self).__init__(**kwargs)

    def get_port_yaml(self):
        return self.port
