import logging

PROTOCOL_HTTP = 'http'
PROTOCOL_TCP = 'tcp'

logger = logging.getLogger(__name__)


class Port(object):

    def __init__(self, **kwargs):
        self.outer_port = kwargs.get('outer_port') or []
        self.inner_port = kwargs.get('inner_port') or []

    # return the yaml without the alb
    def to_yaml_object(self):
        pass


class PortHttp(Port):

    def __init__(self, **kwargs):
        super(PortHttp, self).__init__(**kwargs)
        self.protocol = PROTOCOL_HTTP

    # return the yaml without the alb
    def to_yaml_object(self):
        return '{}:{}/{}'.format(self.outer_port, self.inner_port, self.protocol)


class PortTcp(Port):

    def __init__(self, **kwargs):
        super(PortTcp, self).__init__(**kwargs)
        self.protocol = PROTOCOL_TCP

    # return the yaml without the alb
    def to_yaml_object(self):
        return '{}:{}'.format(self.outer_port, self.inner_port)
