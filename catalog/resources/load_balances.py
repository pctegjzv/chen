import json
import logging

LB_TYPE_ALAUDA_HA = 'HA'
LB_TYPE_ALAUDA_ALB = 'ALB'

logger = logging.getLogger(__name__)


class AlaudaLB(object):
    def __init__(self, **kwargs):
        self.external_ports = kwargs.get('external_ports') or []

    def get_alb_type(self):
        pass

    def get_external_ports_str(self):
        alauda_port_yaml_list = []
        for port in self.external_ports:
            alauda_port_yaml_list.append(self._get_external_port_str(port))
        return json.dumps(alauda_port_yaml_list)

    # get the yaml with lb
    def _get_external_port_str(self, port):
        pass


class AlaudaLBHA(AlaudaLB):
    def __init__(self, **kwargs):
        super(AlaudaLBHA, self).__init__(**kwargs)

    def get_alb_type(self):
        return "LB_TYPE_ALAUDA_HA"

    def _get_external_port_str(self, port):
        return port.get_port_yaml()


class AlaudaLBALB(AlaudaLB):
    def __init__(self, **kwargs):
        super(AlaudaLBALB, self).__init__(**kwargs)
        self.alb_name = kwargs.get('alb_name')

    def get_alb_type(self):
        return "LB_TYPE_ALAUDA_ALB"

    def _get_external_port_str(self, port):
        return '{}:{}'.format(self.alb_name, port.get_port_yaml())


class AlaudaLBFactory(object):

    @staticmethod
    def get_alauda_lb_instance(region_alb_version, alauda_lb_name, external_ports=[]):
        arguments = {
            'alb_name': alauda_lb_name,
            'external_ports': external_ports
        }
        if str(region_alb_version).lower() == 'v2':
            lb_type = LB_TYPE_ALAUDA_ALB
            lb_instance = AlaudaLBALB(**arguments)
        else:
            lb_type = LB_TYPE_ALAUDA_HA
            lb_instance = AlaudaLBHA(**arguments)
        return lb_type, lb_instance
