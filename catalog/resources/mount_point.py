class MountPoint(object):
    def __init__(self, full_path_file_name, config_key):
        self.full_path_file_name = full_path_file_name
        self.config_key = config_key

    def to_yaml_object(self):
        return {
            'path': self.full_path_file_name,
            'config': self.config_key
        }
