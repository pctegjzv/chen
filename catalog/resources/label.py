class Tag(object):
    def __init__(self, label_key, label_value):
        self.label_key = label_key
        self.label_value = label_value

    def to_yaml_object(self):
        return "{}:{}".format(self.label_key, self.label_value)


class NodeLabel(Tag):
    label_key_to_node_key = {
        "ip_tag": "ip"
    }

    def to_yaml_object(self):
        key = self.label_key_to_node_key.get(self.label_key)
        if not key:
            key = self.label_key
        return "constraint:node_selector=={}:{}".format(
            key, self.label_value)


class PodAntaffinity(Tag):
    def to_yaml_object(self):
        return {
            'labelSelector': {
                'matchExpressions': [
                    {
                        'key': self.label_key,
                        'operator': 'In',
                        'values': [self.label_value]
                    }
                ]
            },
            'topologyKey': 'kubernetes.io/hostname'
        }
