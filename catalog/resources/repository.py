class Repository(object):
    def __init__(self, registry_host, repo_path, tag):
        self.registry_host = registry_host
        self.repo_path = repo_path
        self.tag = tag

    def to_yaml_object(self):
        return '{}/{}:{}'.format(self.registry_host, self.repo_path, self.tag)
