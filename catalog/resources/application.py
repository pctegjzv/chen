
class Application(object):
    def __init__(self):
        self.service_list = []

    def add_service(self, service):
        self.service_list.append(service)

    def to_yml_object(self):
        app_yaml_object = {
            'version': '2',
            'services': {}
        }
        for service in self.service_list:
            app_yaml_object['services'].update(service.to_yaml_object())
        return app_yaml_object
