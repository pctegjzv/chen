import logging

logger = logging.getLogger(__name__)


class K8sService(object):
    def __init__(self, **kwargs):
        self.name = kwargs.get('name')
        self.type = kwargs.get('type')

    def to_yaml_object(self):
        return {
            'type': self.type,
            'name': self.name
        }


class NodePortService(K8sService):
    def __init__(self, **kwargs):
        logger.error("liaojian:{}".format(kwargs))
        kwargs['type'] = 'NodePort'
        super(NodePortService, self).__init__(**kwargs)
        self.container_port = kwargs.get('container_port')
        self.node_port = kwargs.get('node_port')

    def to_yaml_object(self):
        return {
            'type': self.type,
            'name': self.name,
            'container_port': self.container_port,
            'node_port': self.node_port
        }


class HeadLessService(K8sService):
    def __init__(self, **kwargs):
        kwargs['type'] = 'Headless'
        super(HeadLessService, self).__init__(**kwargs)
        self.container_port = kwargs.get('container_port')
        self.node_port = kwargs.get('node_port')
