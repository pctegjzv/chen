class Volume(object):
    def __init__(self, outer_path, inner_path):
        self.outer_path = outer_path
        self.inner_path = inner_path

    def to_yaml_object(self):
        return '{}:{}'.format(self.outer_path, self.inner_path)
