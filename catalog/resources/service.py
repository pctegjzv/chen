import logging

from catalog.resources.mount_point import MountPoint
from volume import Volume
from repository import Repository
from load_balances_v2 import AlaudaLoadBalance, LB_TYPE_ALAUDA_ALB
from port_v2 import PortHttp, PortTcp
from label import NodeLabel, Tag, PodAntaffinity
from k8s_service import NodePortService, HeadLessService

logger = logging.getLogger(__name__)


class Service(object):
    def __init__(self, name):
        self.name = name
        self.volume_list = []
        self.repository = None
        self.alauda_lb = None
        self.size = None
        self.env_var_dict = {}
        self.port_list = []
        self.node_lable_list = []
        self.network_mode = None
        self.number = 1
        self.app_tag_list = []
        self.pod_ant_affinity_list = []
        self.k8s_service_list = []
        self.links = []
        self.mount_point_list = []

    def add_node_port_service(self, name, node_port, container_port):
        kwargs = {
            "name": name,
            "node_port": node_port,
            "container_port": container_port
        }
        service = NodePortService(**kwargs)
        self.k8s_service_list.append(service)

    def add_head_less_service(self, name):
        service = HeadLessService(name=name)
        self.k8s_service_list.append(service)

    def set_number(self, number):
        self.number = number

    def set_network_mode(self, network_mode):
        if not network_mode:
            network_mode = 'host'
        self.network_mode = network_mode

    def get_name(self):
        return self.name

    def add_volume(self, outer_path, inner_path):
        volume = Volume(outer_path, inner_path)
        self.volume_list.append(volume)

    def set_repository(self, registry_host, repo_path, tag):
        self.repository = Repository(registry_host, repo_path, tag)

    def set_alauda_lb(self, alauda_lb_name, alauda_lb_verion):
        if not alauda_lb_verion:
            return
        alauda_lb = AlaudaLoadBalance(alauda_lb_name, alauda_lb_verion)
        self.alauda_lb = alauda_lb

    def set_size(self, size):
        self.size = size

    def add_env_var(self, env_var_key, env_var_value):
        self.env_var_dict[env_var_key] = env_var_value

    def add_http_port(self, outer_port, inner_port):
        if not outer_port:
            outer_port = 80
        port = PortHttp(outer_port=outer_port, inner_port=inner_port)
        self.port_list.append(port)

    def add_tcp_port(self, outer_port, inner_port):
        port = PortTcp(outer_port=outer_port, inner_port=inner_port)
        self.port_list.append(port)

    def add_lable(self, label_key, label_value):
        label = NodeLabel(label_key, label_value)
        self.node_lable_list.append(label)

    def set_lable_list(self, label_ist):
        if not label_ist:
            return
        for label in label_ist:
            self.add_lable(label.get('key'), label.get('value'))

    def add_app_tag(self, app_tag_key, app_tag_value):
        app_tag = Tag(app_tag_key, app_tag_value)
        self.app_tag_list.append(app_tag)

    def set_app_tag_list(self, app_tag_list):
        if not app_tag_list:
            return
        for app_tag in app_tag_list:
            self.add_app_tag(app_tag.get('key'), app_tag.get('value'))

    def add_pod_ant_affinity(self, pod_ant_affinity_key, pod_ant_affinity_value):
        pod_ant_affinity = PodAntaffinity(pod_ant_affinity_key, pod_ant_affinity_value)
        self.pod_ant_affinity_list.append(pod_ant_affinity)

    def set_pod_ant_affinity_list(self, pod_ant_affinity_list):
        if not pod_ant_affinity_list:
            return
        for pod_ant_affinity in pod_ant_affinity_list:
            self.add_pod_ant_affinity(pod_ant_affinity.get('key'),
                                      pod_ant_affinity.get('value'))

    def add_links(self, service_name):
        if service_name:
            self.links.append(service_name)

    def add_mount_point(self, full_path_file_name, config_key):
        mount_point = MountPoint(full_path_file_name, config_key)
        self.mount_point_list.append(mount_point)

    def to_yaml_object(self):
        service_yaml_object = dict()
        service_yaml_object['labels'] = []
        service_yaml_object['kubernetes_config'] = {}

        if self.number > 0:
            service_yaml_object['number'] = self.number
        else:
            service_yaml_object['number'] = 1

        service_yaml_object['image'] = self.repository.to_yaml_object()
        service_yaml_object['network_mode'] = self.network_mode
        if self.alauda_lb:
            service_yaml_object['alauda_lb'] = self.alauda_lb.get_type()
        if self.port_list:
            if self.alauda_lb and self.alauda_lb.get_name() and \
                            self.alauda_lb.get_type().upper() == LB_TYPE_ALAUDA_ALB:
                alauda_lb_name = self.alauda_lb.get_name()
                service_yaml_object['ports'] = ['{}:{}'.format(alauda_lb_name,
                                                               port.to_yaml_object())
                                                for port in self.port_list]
            else:
                service_yaml_object['ports'] = [port.to_yaml_object()
                                                for port in self.port_list]

        if isinstance(self.size, dict):
            service_yaml_object['size'] = self.size.get('size')
            service_yaml_object['cpu_quota'] = self.size.get('cpu')
            service_yaml_object['mem_limit'] = self.size.get('mem')
        else:
            service_yaml_object['size'] = self.size

        if self.volume_list:
            service_yaml_object['volumes'] = [
                volume.to_yaml_object() for volume in self.volume_list]

        if self.env_var_dict:
            service_yaml_object['environment'] = self.env_var_dict

        if self.node_lable_list:
            label_yaml_list = [label.to_yaml_object() for label in self.node_lable_list]
            service_yaml_object['labels'].extend(label_yaml_list)

        if self.app_tag_list:
            tag_yaml_list = [app_tag.to_yaml_object() for app_tag in self.app_tag_list]
            service_yaml_object['labels'].extend(tag_yaml_list)

        if self.pod_ant_affinity_list:
            service_yaml_object['kubernetes_config']['affinity'] = \
                self._generate_pod_ant_affinity_yaml_object()

        if self.k8s_service_list:
            service_yaml_object['kubernetes_config']['services'] = [
                k8s_service.to_yaml_object()
                for k8s_service in self.k8s_service_list]

        if not service_yaml_object['labels']:
            service_yaml_object.pop('labels')
        if not service_yaml_object['kubernetes_config'].keys():
            service_yaml_object.pop('kubernetes_config')

        if self.links:
            service_yaml_object['links'] = self.links

        if self.mount_point_list:
            service_yaml_object['mount_points'] = [
                mount.to_yaml_object() for mount in self.mount_point_list]

        return {
            self.name: service_yaml_object
        }

    def _generate_pod_ant_affinity_yaml_object(self):
        result = {}
        result['podAntiAffinity'] = {}
        result['podAntiAffinity']['requiredDuringSchedulingIgnoredDuringExecution'] \
            = []
        for pod_ant_affinity in self.pod_ant_affinity_list:
            result['podAntiAffinity']['requiredDuringSchedulingIgnoredDuringExecution'] \
                .append(pod_ant_affinity.to_yaml_object())

        return result
