# -*- coding: utf-8 -*-
import base64
import logging
import yaml

from base_factory import AbstractFactory
from spring_config_server import SpringConfigServer
from ..ui_element.app_info import AppInfo
from ..ui_element.app_instruction import AppInstruction
from ..ui_element.input_int import ElementInternalPort, ElementNodePort, ElementInstanceNum
from ..ui_element.input_option import ElementClusterSize, ElementImageTag, ElementConfigServer
from ..ui_element.input_multi_option import ElementNodeTag
from ..ui_element.input_code import ElementZuulRouteRule
from ..ui_element.input_tags import ElementPodAntiAffinity
from ..ui_element.input_table import ElementAppTag
from ..resources.application import Application

logger = logging.getLogger(__name__)
__author__ = "jliao@alauda.io"


SPRINGZUUL = {
    "name": "zuul",
    "info": AppInfo("micro service gateway", "微服务网关").to_json(),
    "instruction": AppInstruction("", "").to_json(),
    "basic_config": [
        ElementImageTag(['v0.0.1']).to_json(),
        ElementInstanceNum().to_json(),
        ElementNodeTag().to_json(),
        ElementInternalPort().to_json(),
        ElementNodePort().to_json(),
        ElementPodAntiAffinity().to_json(),
        ElementClusterSize(['S', 'M', 'L', 'XL', 'CUSTOMIZED']).to_json(),
        ElementAppTag().to_json(),
    ],
    "advanced_config": [
        ElementZuulRouteRule().to_json(),
        ElementConfigServer().to_json()
    ]
}


class SpringZuul(AbstractFactory):
    def __init__(self, raw_data, namespace):
        super(SpringZuul, self).__init__(raw_data, namespace)

    def get_compose_data(self):
        app = Application()
        service = self._get_service()
        app.add_service(service)
        return yaml.safe_dump(app.to_yml_object(), allow_unicode=True)

    def _get_service(self):
        service = super(SpringZuul, self)._get_service()

        basic_config_dict = self.raw_data['basic_config']
        service.set_repository(
            self.image_index, 'claas/zuul',
            ElementImageTag.get_value(basic_config_dict))
        internal_port = ElementInternalPort.get_value(basic_config_dict)
        service.add_tcp_port(internal_port, internal_port)
        service.add_env_var('ALAUDA_SERVER_PORT', internal_port)
        service.add_env_var('ALAUDA_SPRING_APPLICATION_NAME', service.get_name())

        node_port = ElementNodePort.get_value(basic_config_dict)
        node_port_service_name = '{}-np-01'.format(service.get_name())
        service.add_node_port_service(node_port_service_name, node_port, internal_port)

        advance_config_dict = self.raw_data['advanced_config']

        config_server_id = ElementConfigServer.get_value(advance_config_dict)
        config_server = SpringConfigServer(None, None)
        config_server.get_app_detail(config_server_id)

        service.add_env_var('ALAUDA_KAFKA_INFO', config_server.config_json.get_kafka_info())
        service.add_env_var('ALAUDA_ZOOKEEPER_INFO', config_server.config_json.get_zookeeper_info())
        service.add_env_var('ALAUDA_ZUUL_ROUTE_RULE', base64.b64encode(
            ElementZuulRouteRule.get_value(advance_config_dict)))
        service.add_env_var('ALAUDA_CONFIG_SERVER_INFO',
                            "{}:{}".format(config_server.config_json.get_k8s_service_name(),
                                           config_server.config_json.get_internal_port()))
        return service

    def _format_app_detail(self, data):
        detail_template = super(SpringZuul, self)._format_app_detail(data)
        zuul_route_rule = str(self.config_json.get_zuul_route_rule())
        detail_template.add_editable_info(
            ElementZuulRouteRule.get_name(),
            'zuul 路由规则', 'zuul route rule',
            ElementZuulRouteRule.get_type(),
            zuul_route_rule)

        return detail_template

    def get_updated_yaml_content(self, data):
        self.raw_data['advanced_config'][ElementZuulRouteRule.get_name()] = \
            data.get(ElementZuulRouteRule.get_name())
        return None
