#!/usr/bin/env python
# -*- coding: utf-8 -*-


class AppDetailTemplate(object):

    def __init__(self):
        self.has_dashboard = False
        self.dashboard_url = ""
        self.basic_info = []
        self.editable_info = []
        self.metrics = []
        self.management = []

    def to_json(self):
        return {
            "has_dashboard": self.has_dashboard,
            "dashboard_url": self.dashboard_url,
            "display_info": {
                "basic_info": self.basic_info,
                "editable_info": self.editable_info
            },
            "metrics": self.metrics,
            "management": self.management
        }

    def add_basic_info(self, display_name_cn, display_name_en, value):
        info = {
            "display_name": {
                "en": display_name_en,
                "zh": display_name_cn
            },
            "value": value
        }
        self.basic_info.append(info)

    def set_dashboard_url(self, dashboard_url):
        self.has_dashboard = True
        self.dashboard_url = dashboard_url

    def add_editable_info(self, attribute_name, display_name_cn,
                          display_name_en, input_type, value):
        info = {
            "attribute_name": attribute_name,
            "type": input_type,
            "display_name": {
              "en": display_name_en,
              "zh": display_name_cn
            },
            "value": value
        }
        self.editable_info.append(info)

    def set_metrics(self, metrics):
        self.metrics = metrics

    def set_management(self, management):
        self.management = management
