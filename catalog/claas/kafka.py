#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import absolute_import

import logging
import client
from datetime import datetime
from jinja2 import Template
from catalog.claas.base_factory import AbstractFactory
from catalog.exceptions import CatalogException
from django.conf import settings

logger = logging.getLogger(__name__)
__author__ = "junh@alauda.io"


KAFKA = {
    "name": "kafka",
    "info": {
        "en": "This template creates a multinodes Kafka cluster.",
        "zh": "该模板用于部署 Kafka 集群"
    },
    "instruction": {
        "en": "**Kafka relies on Zookeeper to work, \n \
         Please make sure that there is already a zookeeper cluster \n\
         deployed through App Catalog under the selected region, \n\
         When you want to create a Kafka topic, you can get the zookeeper connection \
         information from the environment variable KAFKA_ZOOKEEPER_CONNECT.**    \n\
                Change the following ZooKeeper default parameters, if you needed.",
        "zh": "**Kafka 依赖于 zookeeper， \n\
         请确保选择的 region 下已经有通过应用市场部署的 zookeeper，否则 kafka 将不能正常使用， \n\
         当想要创建一个Kafka的主题时，可以从环境变量KAFKA_ZOOKEEPER_CONNECT中获知zookeeper的连接信息。**    \n\
           如果需要的话，可以修改下面的默认参数。"
    },
    "basic_config": [{
        "type": "int",
        "attribute_name": "num_of_nodes",
        "display_name": {
            "en": "Number of nodes",
            "zh": "集群节点数"
        },
        "description": {
            "en": "Desired cluster size. 3, 5, or 7 are good choices. You will need the same amount of hosts to deploy.",   # noqa
            "zh": "实例数量，最好是填3，5，7这样的奇数。当然你需要有这么多的主机才能成功部署"
        }
    }, {
        "type": "cluster_size",
        "attribute_name": "kafka_size",
        "display_name": {
            "en": "Instance size",
            "zh": "实例大小"

        },
        "description": {
            "en": "Size of instances",
            "zh": "实例大小"
        },
        "min_value": {
            "mem": 1024,
            "cpu": 0.5
        }
    }, {
        "type": "multi_option",
        "attribute_name": "ip_tag",
        "display_name": {
            "en": "Node tag",
            "zh": "主机标签"
        },
        "description": {
            "en": "Deploy to specific host",
            "zh": "定点部署",
        },
        "option": {
        }
    }, {
        "type": "option",
        "attribute_name": "image_tag",
        "display_name": {
            "en": "image tag",
            "zh": "镜像标签"
        },
        "description": {
            "en": "",
            "zh": ""
        },
        "key": "version",
        "value": "version",
        "option": settings.KAFKA_VERSION_LIST
    }],
    "advanced_config": [{
        "type": "option",
        "attribute_name": "zookeeper_application",
        "display_name": {
            "en": "Zookepper App Name",
            "zh": "Zookepper 应用名"
        },
        "description": {
            "en": "Deployed zookeeper cluster",
            "zh": "已部署的 zookeeper 集群"
        },
    }, {
        "type": "string",
        "attribute_name": "advertised_port",
        "display_name": {
            "en": "advertised.port",
            "zh": "advertised.port"
        },
        "description": {
            "en": "The port to publish to ZooKeeper for clients to use",
            "zh": "给 ZooKeeper 的客户端使用的端口"
        },
        "default_value": "9092"
    }, {
        "type": "string",
        "attribute_name": "log_dir",
        "display_name": {
            "en": "Data directory",
            "zh": "数据目录"
        },
        "description": {
            "en": "kafka data directory",
            "zh": "Kafka 数据目录"
        },
        "default_value": "/kafka"
    }, {
        "type": "radio_group_tab",
        "attribute_name": "mount_volume",
        "display_name": {
            "en": "Mount volume",
            "zh": "启用挂载数据卷"
        },
        "description": {
            "en": "",
            "zh": "指定数据卷挂载到容器内部使用"
        },
        "option": [
            "true", "false"
        ],
        "default_value": "false"
    }, {
        "type": "string",
        "attribute_name": "heap_opts",
        "display_name": {
            "en": "heap.opts",
            "zh": "heap.opts"
        },
        "description": {
            "en": "set the Java option",
            "zh": "配置 java 相关选项"
        },
        "default_value": "-Xmx2G -Xms2G"
    }, {
        "type": "int",
        "attribute_name": "log_roll_hours",
        "display_name": {
            "en": "log.roll.hours",
            "zh": "log.roll.hours",
        },
        "description": {
            "en": "The maximum time before a new log segment is rolled out",
            "zh": "日志保留的最长时间"
        },
        "default_value": 24
    }, {
        "type": "string",
        "attribute_name": "log_retention_bytes",
        "display_name": {
            "en": "log.retention.bytes",
            "zh": "log.retention.bytes"
        },
        "description": {
            "en": "The maximum size of the log before deleting it",
            "zh": "日志数据存储的最大字节数",
        },
        "default_value": 100000000
    }, {
        "type": "int",
        "attribute_name": "log_retention_hours",
        "display_name": {
            "en": "log.retention.hours",
            "zh": "log.retention.hours",
        },
        "description": {
            "en": 'Controls the maximum time we will retain a log before we will discard old log segments to free up space if we are using the "delete" retention policy',  # noqa
            "zh": "默认采用 delete 策略，定期地清理数据，释放空间，该参数可以控制保存日志的最大时间",
        },
        "default_value": 168
    }, {
        "type": "int",
        "attribute_name": "max_request_size",
        "display_name": {
            "en": "max_request_size",
            "zh": "max_request_size",
        },
        "description": {
            "en": "The maximum size of a request in bytes",
            "zh": "请求的最大字节大小"
        },
        "default_value": 1048576
    }, {
        "type": "int",
        "attribute_name": "max_message_bytes",
        "display_name": {
            "en": "max.message.bytes",
            "zh": "max.message.bytes",
        },
        "description": {
            "en": "This is largest message size Kafka will allow to be appended.",
            "zh": "kafka 允许附加的最大包大小"
        },
        "default_value": 1000012
    }, {
        "type": "int",
        "attribute_name": "message_max_bytes",
        "display_name": {
            "en": "message.max.bytes",
            "zh": "message.max.bytes",
        },
        "description": {
            "en": "The maximum size of message that the server can receive",
            "zh": "服务器能接受的最大包的大小"
        },
        "default_value": 1000012
    }, {
        "type": "int",
        "attribute_name": "num_partitions",
        "display_name": {
            "en": "num.partitions",
            "zh": "num.partitions",
        },
        "description": {
            "en": "The number of log partitions per topic",
            "zh": "每个 topic 的分区数"
        },
        "default_value": 1
    }, {
        "type": "int",
        "attribute_name": "replica_fetch_max_bytes",
        "display_name": {
            "en": "replica.fetch.max.bytes",
            "zh": "replica.fetch.max.bytes",
        },
        "description": {
            "en": "The number of bytes of messages to attempt to fetch for each partition",
            "zh": "从每个分区提取消息的字节数"
        },
        "default_value": 1048576
    }, {
        "type": "int",
        "attribute_name": "replica_fetch_response_max_bytes",
        "display_name": {
            "en": "replica.fetch.response.max.bytes",
            "zh": "replica.fetch.response.max.bytes"
        },
        "description": {
            "en": "Maximum bytes expected for the entire fetch response",
            "zh": "获取整个响应的最大字节数"
        },
        "default_value": 10485760
    }, {
        "type": "int",
        "attribute_name": "socket_request_max_bytes",
        "display_name": {
            "en": "socket.request.max.bytes",
            "zh": "socket.request.max.bytes"
        },
        "description": {
            "en": "The maximum number of bytes in a socket request",
            "zh": "套接字请求的最大字节数"
        },
        "default_value": 104857600
    }],
    "true_config": [{
        "type": "string",
        "attribute_name": "mount_data_dir",
        "display_name": {
            "en": "Data directory mount point",
            "zh": "数据目录挂载点"
        },
        "description": {
            "en": "This directory will be mounted to the Kafka data directory",
            "zh": "会将该目录挂载到kafka的数据目录"
        },
        "default_value": "/kafka/data"

    }]
}


class Kafka(AbstractFactory):

    def __init__(self, raw_data, namespace):
        super(Kafka, self).__init__(raw_data)
        self.meta_compose_path = self.base_template_path + '/meta_compose.yml'
        self.meta_container_without_volume_path = self.base_template_path + \
            '/meta_container_without_volume.yml'
        self.meta_container_path = self.base_template_path + '/meta_container.yml'

    def get_compose_data(self):
        basic_config_dict = self.raw_data['basic_config']
        advanced_config_dict = self.raw_data['advanced_config']

        node_count = basic_config_dict['num_of_nodes']
        ip_tag = basic_config_dict['ip_tag']
        kafka_size = basic_config_dict['kafka_size']
        cpu_quota = kafka_size['cpu']
        mem_limit = kafka_size['mem']
        image_tag = basic_config_dict['image_tag']

        suffix_dir = '/' + self.raw_data['name'] + datetime.now().strftime('_%Y%m%d%H%M')
        compose_content = str()
        if advanced_config_dict['mount_volume'] == 'true':
            container_path = self.meta_container_path
            # check the path and add suffix folder with app_name_datetime
            mount_data_dir = advanced_config_dict['mount_data_dir']
            self.check_path_and_suffix_app_name(mount_data_dir, suffix=False)
            advanced_config_dict['mount_data_dir'] = mount_data_dir + suffix_dir
        else:
            container_path = self.meta_container_without_volume_path
        meta_container_content = self.get_config_content(container_path)
        template = Template(meta_container_content)

        uuid = advanced_config_dict['zookeeper_application']

        resp = client.druid.client.DruidClient.application_info(uuid, True)
        try:
            ip_tag_zoo_servers = \
                [service['node_tag'].strip('ip:') + ':' +
                 service['instance_envvars']['ZOO_PORT'] for service in resp.data['services']]
        except:
            raise CatalogException('catalog_invalid_zookeeper_cluster')

        zoo_servers = ','.join(ip_tag_zoo_servers) + suffix_dir
        logger.debug("zookeeper servers for kafka: {}".format(zoo_servers))

        kafka_port = advanced_config_dict['advertised_port']

        if node_count != len(ip_tag):
            raise CatalogException('catalog_invalid_node_ip_tag')

        for i in range(node_count):
            container_content = template.render(image_index=self.image_index,
                                                image_tag=image_tag,
                                                node_name='kafka'+str(i+1),
                                                cpu_quota=cpu_quota,
                                                mem_limit=mem_limit,
                                                kafka_port=kafka_port,
                                                broker_id=i+1,
                                                node_tag=ip_tag[i],
                                                kafka_zoo_connect=zoo_servers,
                                                **advanced_config_dict)
            compose_content += container_content

        template = Template(self.get_config_content(self.meta_compose_path))
        compose_content = template.render(containers=compose_content)
        logger.info("{} got compose_content: \n{}".format(
            self.__class__.__name__, compose_content))
        return compose_content

    def __repr__(self):
        out = 'Kafka'
        return out
