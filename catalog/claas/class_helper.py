from catalog.exceptions import CatalogException
from client.jakiro.client import ENDPOINT, API_VERSION


class ClaasHelper(object):
    def __init__(self):
        pass

    # merge host list and host list
    # host_list='host1,host2,host3'
    # port_list='port1,port2,port3' or port_list='port1'
    # result='host1:port1,host2:port2,host3:port3' or result='host1:port1,host2:port1,host3:port1'
    @staticmethod
    def merge_host_port_info(hosts, ports):
        host_list = hosts.split(',')
        port_list = ports.split(',')
        if len(host_list) != len(port_list) and port_list != 1:
            raise CatalogException('host_port_is_not_match')
        i = 0
        merged_list = []
        for host in host_list:
            if len(port_list) == 1:
                port = port_list[0]
            else:
                port = port_list[i]
            merged_list.append('{}:{}'.format(host, port))
            i += 1
        return ','.join(merged_list)

    @staticmethod
    def genenerate_dispaly_app_url(app_id, namespace):
        return '{}/{}/applications/{}/{}/reserved_url/'.format(
            ENDPOINT, API_VERSION, namespace, app_id)
