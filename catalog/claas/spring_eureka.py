# -*- coding: utf-8 -*-
import logging
import yaml

from base_factory import AbstractFactory
from ..ui_element.app_info import AppInfo
from ..ui_element.app_instruction import AppInstruction
from ..ui_element.input_int import ElementInstanceNum, ElementInternalPort
from ..ui_element.input_option import ElementClusterSize, ElementImageTag
from ..ui_element.input_multi_option import ElementNodeTag
from ..ui_element.input_tags import ElementPodAntiAffinity
from ..ui_element.input_table import ElementAppTag
from ..resources.application import Application

logger = logging.getLogger(__name__)
__author__ = "jliao@alauda.io"


SPRINGEUREKA = {
    "name": "eureka",
    "info": AppInfo("service discovery", "服务发现与注册组件").to_json(),
    "instruction": AppInstruction("", "").to_json(),
    "basic_config": [
        ElementImageTag(['v0.0.2']).to_json(),
        ElementInstanceNum(2).to_json(),
        ElementNodeTag().to_json(),
        ElementInternalPort(8761).to_json(),
        ElementPodAntiAffinity().to_json(),
        ElementClusterSize(['S', 'M', 'L', 'XL', 'CUSTOMIZED']).to_json(),
        ElementAppTag().to_json()

    ]
}


class SpringEureka(AbstractFactory):
    def __init__(self, raw_data, namespace):
        super(SpringEureka, self).__init__(raw_data, namespace)

    def get_compose_data(self):
        app = Application()
        service = self._get_service()
        app.add_service(service)
        return yaml.safe_dump(app.to_yml_object(), allow_unicode=True)

    def _get_service(self, **server_args):
        service = super(SpringEureka, self)._get_service(**server_args)
        basic_config_dict = self.raw_data['basic_config']

        headless_service_name = '{}-hl-01'.format(service.get_name())
        service.add_head_less_service(headless_service_name)

        service.set_repository(
            self.image_index, 'claas/eureka', ElementImageTag.get_value(basic_config_dict))
        service.add_env_var('ALAUDA_SERVER_PORT', ElementInternalPort.get_value(basic_config_dict))
        service.add_env_var('ALAUDA_SPRING_APPLICATION_NAME', service.get_name())
        service.add_env_var(
            'ALAUDA_HEADLESSSERVICE',
            '{}.{}--{}.svc.cluster.local'.format(
                headless_service_name,
                service.get_name(),
                self.raw_data.get('space_name')
            ))

        internal_port = ElementInternalPort.get_value(basic_config_dict)
        service.add_http_port(internal_port, internal_port)

        return service

    def _get_other_eureka_addr(self, cur_ip_tag):

        eureka_address_list = []
        basic_config_dict = self.raw_data['basic_config']
        for ip_tag in basic_config_dict['ip_tag']:
            if ip_tag == cur_ip_tag:
                continue
            eureka_address_list.append(self._get_eureka_address(
                ip_tag, ElementInternalPort.get_value(basic_config_dict)))
        return ','.join(eureka_address_list)

    def _get_eureka_address(self, host, port):
        return "http://{}:{}/eureka".format(
                host, port)

    def _format_app_detail(self, data):
        detail_template = super(SpringEureka, self)._format_app_detail(data)
        eureka_address_value = "{}:{}".format(
            self.config_json.get_k8s_service_name(),
            self.config_json.get_internal_port())
        detail_template.add_basic_info(
            "eureka 地址:", "eureka server url:", eureka_address_value)
        return detail_template
