# -*- coding: utf-8 -*-
import logging
import yaml

from base_factory import AbstractFactory
from spring_config_server import SpringConfigServer
from ..ui_element.app_info import AppInfo
from ..ui_element.app_instruction import AppInstruction
from ..ui_element.input_int import ElementInternalPort, ElementInstanceNum
from ..ui_element.input_option import ElementClusterSize, ElementImageTag, \
    ElementConfigServer, ElementAlaudaLBName
from ..ui_element.input_multi_option import ElementNodeTag
from ..ui_element.input_tags import ElementPodAntiAffinity
from ..ui_element.input_table import ElementAppTag
from ..resources.application import Application
from client.mirana2.client import Mirana2Client

logger = logging.getLogger(__name__)
__author__ = "jliao@alauda.io"

SPRINGZIPKIN = {
    "name": "zipkin",
    "info": AppInfo("aggerator micro service service call track", "微服务数据调用链").to_json(),
    "instruction": AppInstruction("", "").to_json(),
    "basic_config": [
        ElementImageTag(['v0.0.1']).to_json(),
        ElementInstanceNum().to_json(),
        ElementNodeTag().to_json(),
        ElementAlaudaLBName().to_json(),
        ElementInternalPort().to_json(),
        ElementPodAntiAffinity().to_json(),
        ElementClusterSize(['S', 'M', 'L', 'XL', 'CUSTOMIZED']).to_json(),
        ElementAppTag().to_json(),
    ],
    "advanced_config": [
        ElementConfigServer().to_json()
    ]
}


class SpringZipkin(AbstractFactory):
    def __init__(self, raw_data, namespace):
        super(SpringZipkin, self).__init__(raw_data, namespace)

    def get_compose_data(self):
        app = Application()
        service = self._get_service()
        app.add_service(service)
        return yaml.safe_dump(app.to_yml_object(), allow_unicode=True)

    def _get_service(self):
        service = super(SpringZipkin, self)._get_service()

        basic_config_dict = self.raw_data['basic_config']
        service.set_repository(
            self.image_index, 'claas/zipkin',
            ElementImageTag.get_value(basic_config_dict))
        internal_port = ElementInternalPort.get_value(basic_config_dict)
        service.add_http_port(internal_port, internal_port)
        service.add_env_var(
            'ALAUDA_SERVER_PORT', ElementInternalPort.get_value(basic_config_dict))

        service.add_env_var('ALAUDA_SPRING_APPLICATION_NAME', service.get_name())

        advance_config_dict = self.raw_data['advanced_config']

        config_server_id = ElementConfigServer.get_value(advance_config_dict)
        config_server = SpringConfigServer(None, None)
        config_server.get_app_detail(config_server_id)
        service.add_env_var('ALAUDA_KAFKA_INFO', config_server.config_json.get_kafka_info())
        service.add_env_var('ALAUDA_ZOOKEEPER_INFO',
                            config_server.config_json.get_zookeeper_info())
        service.add_env_var('ALAUDA_CONFIG_SERVER_INFO',
                            "{}:{}".format(config_server.config_json.get_k8s_service_name(),
                                           config_server.config_json.get_internal_port()))
        return service

    def _format_app_detail(self, data):
        detail_template = super(SpringZipkin, self)._format_app_detail(data)
        dashboard_url = '{}/{}/'.format(Mirana2Client.get_alb_address(
            self.get_region_id(), self.dashboard_service_id), 'zipkin')
        detail_template.set_dashboard_url(dashboard_url)
        return detail_template
