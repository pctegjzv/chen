# -*- coding: utf-8 -*-
import base64
import logging
import yaml

from base_factory import AbstractFactory
from class_helper import ClaasHelper
from ..ui_element.app_info import AppInfo
from ..ui_element.app_instruction import AppInstruction
from ..ui_element.input_int import ElementInternalPort
from ..ui_element.input_option import ElementClusterSize, ElementImageTag
from ..ui_element.input_multi_option import ElementNodeTag
from ..ui_element.input_tags import ElementPodAntiAffinity
from ..ui_element.input_table import ElementAppTag
from ..resources.application import Application
from ..ui_element.input_int import ElementInstanceNum
from ..ui_element.input_code import ElementConfigServerRule
from ..ui_element.input_string import (ElementKafkaHost, ElementZooKeeperHost,
                                       ElementKafkaPort, ElementZooKeeperPort)

logger = logging.getLogger(__name__)
__author__ = "jliao@alauda.io"


SPRINGCONFIGSERVER = {
    "name": "hystrix dashboard",
    "info": AppInfo("micro service circuit breaker monitor", "微服务熔断监控").to_json(),
    "instruction": AppInstruction("", "").to_json(),
    "basic_config": [
        ElementImageTag(['v0.0.1']).to_json(),
        ElementInstanceNum().to_json(),
        ElementNodeTag().to_json(),
        ElementInternalPort().to_json(),
        ElementPodAntiAffinity().to_json(),
        ElementClusterSize(['S', 'M', 'L', 'XL', 'CUSTOMIZED']).to_json(),
        ElementAppTag().to_json()
    ],
    "advanced_config": [
        ElementKafkaHost().to_json(),
        ElementKafkaPort().to_json(),
        ElementZooKeeperHost().to_json(),
        ElementZooKeeperPort().to_json(),
        ElementConfigServerRule().to_json()
    ]
}


class SpringConfigServer(AbstractFactory):
    def __init__(self, raw_data, namespace):
        super(SpringConfigServer, self).__init__(raw_data, namespace)

    def get_compose_data(self):
        app = Application()
        service = self._get_service()
        app.add_service(service)
        return yaml.safe_dump(app.to_yml_object(), allow_unicode=True)

    def _get_service(self):
        service = super(SpringConfigServer, self)._get_service()

        basic_config_dict = self.raw_data['basic_config']
        service.set_repository(
            self.image_index, 'claas/config-server',
            ElementImageTag.get_value(basic_config_dict))
        internal_port = ElementInternalPort.get_value(basic_config_dict)
        service.add_tcp_port(internal_port, internal_port)
        service.add_env_var('ALAUDA_SERVER_PORT', internal_port)
        service.add_env_var('ALAUDA_SPRING_APPLICATION_NAME', service.get_name())

        advance_config_dict = self.raw_data['advanced_config']
        service.add_env_var('ALAUDA_KAFKA_INFO', ClaasHelper.merge_host_port_info(
            ElementKafkaHost.get_value(advance_config_dict),
            ElementKafkaPort.get_value(advance_config_dict)))
        service.add_env_var('ALAUDA_ZOOKEEPER_INFO', ClaasHelper.merge_host_port_info(
            ElementZooKeeperHost.get_value(advance_config_dict),
            ElementZooKeeperPort.get_value(advance_config_dict)))
        service.add_env_var('ALAUDA_CONFIG_SERVER_RULE', base64.b64encode(
            ElementConfigServerRule.get_value(advance_config_dict)))
        return service

    def _format_app_detail(self, data):
        detail_template = super(SpringConfigServer, self)._format_app_detail(data)
        kafka_info = self.config_json.get_kafka_info()
        detail_template.add_basic_info(
            'kafka 地址', 'kafka address',
            kafka_info)

        zookeeper_info = self.config_json.get_zookeeper_info()
        detail_template.add_basic_info(
            'zookeeper 地址', 'zookeeper address',
            zookeeper_info)

        config_server_rule = str(self.config_json.get_config_server_rule())
        detail_template.add_editable_info(
            ElementConfigServerRule.get_name(),
            'config server 规则', 'config server rule',
            ElementConfigServerRule.get_type(),
            config_server_rule)

        return detail_template

    def get_updated_yaml_content(self, data):
        self.raw_data['advanced_config'][ElementConfigServerRule.get_name()] = \
            data.get(ElementConfigServerRule.get_name())
        return self.get_compose_data()
