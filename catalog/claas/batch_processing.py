#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import absolute_import
from __future__ import unicode_literals

import logging

import yaml

from catalog.claas.base_factory import AbstractFactory

from catalog.claas.emr import EMRSuit, AppSceneDefine, \
    AppVersionMeta, EmrApp, KafkaFixedService, EsFixedService, ZkFixedService, \
    ZkAutoService, KafkaAutoService, EsAutoService, HBaseFixedService, HBaseAutoService, \
    BatchProcessFixedDeployParser, BatchProcessDependencyDefine, BatchProcessAutoDeployParser, \
    HadoopFixedService, HadoopAutoService
from catalog.exceptions import CatalogException
from catalog.ui_element.app_info import AppInfo
from catalog.ui_element.app_instruction import AppInstruction
from catalog.ui_element.input_option import ElementImageTag, ElementAlaudaLBName

logger = logging.getLogger(__name__)
__author__ = "junh@alauda.io"


class BatchProcessAppSceneDefine(AppSceneDefine):
    def __init__(self):
        super(BatchProcessAppSceneDefine, self).__init__()
        self.app_type = 'complex'
        self.app_name = EMRSuit.META_APP_BATCHPROCESS

        hdp_2_7_4 = AppVersionMeta(EMRSuit.META_SVR_HADOOP, '2.7.4')
        zk_3_4_10 = AppVersionMeta(EMRSuit.META_SVR_ZK, '3.4.10')
        hbase_1_2_6 = AppVersionMeta(EMRSuit.META_SVR_HBASE, '1.2.6')
        kafka_0_9_0_1 = AppVersionMeta(EMRSuit.META_SVR_KAFKA, '0.9.0.1')
        es_1_7_5 = AppVersionMeta(EMRSuit.META_SVR_ES, '1.7.5')

        bp_2_0 = AppVersionMeta(EMRSuit.META_APP_BATCHPROCESS, '0.2.0')
        bp_2_0.add_sub_version_meta(hdp_2_7_4)
        bp_2_0.add_sub_version_meta(zk_3_4_10)
        bp_2_0.add_sub_version_meta(hbase_1_2_6)
        bp_2_0.add_sub_version_meta(kafka_0_9_0_1)
        bp_2_0.add_sub_version_meta(es_1_7_5)

        self.http_port = {
            hdp_2_7_4.app_name: [
                8088, 50070, 50075, 50090, 50105], hbase_1_2_6.app_name: [
                10610, 16030], es_1_7_5.app_name: [9200]}
        self.tcp_port = {
            hdp_2_7_4.app_name: [
                9000,
                50010,
                50020,
                50100,
                19888,
                10020,
                8030,
                8031,
                8032,
                8033,
                8040,
                8041,
                8042,
                31010,
                8020],
            zk_3_4_10.app_name: [
                2181,
                2888,
                3888],
            hbase_1_2_6.app_name: [
                9090,
                16020],
            kafka_0_9_0_1.app_name: [9092],
            es_1_7_5.app_name: [9300]
        }

        self.add_version(bp_2_0)


versions = BatchProcessAppSceneDefine().version.keys()
BATCH_PROCESSING = {
    "name": "Batch Processing",
    "info": AppInfo("Bundled Batch Processing App for Big Data", "批量大数据处理应用包").to_json(),
    "instruction": AppInstruction("最小演示环境建议:master节点2C1G以上，"
                                  "slave节点1C512M以上。\n实际生产环境视节点数量增加。",
                                  "").to_json(),
    "basic_config": [
        ElementImageTag(versions).to_json(),
        ElementAlaudaLBName().to_json(),
        {
            "type": "cluster_size",
            "attribute_name": "master_size",

            "display_name": {
                "en": "Instance size",
                "zh": "Master实例大小"
            },
            "description": {
                "en": "Size of instances",
                "zh": "实例大小"
            },
            "min_value": {
                "mem": 1024,
                "cpu": 0.25
            }
        },
        {
            "type": "cluster_size",
            "attribute_name": "slaver_size",
            "display_name": {
                "en": "Instance size",
                "zh": "Slave实例大小"
            },
            "description": {
                "en": "Size of instances",
                "zh": "实例大小"
            },

            "min_value": {
                "mem": 512,
                "cpu": 0.125
            }
        },
    ],
    "advanced_config": [{
        "type": "int",
        "attribute_name": "alauda_dfs_replication",
        "display_name": {
            "en": "hdfs replication",
            "zh": "HDFS副本数"
        },
        "description": {
            "en": "when the datanode number less than hdfs replication,"
                  " the stored replication number is equal to datanode number",
            "zh": "hdfs副本数量最多为3，当定点部署主机数小于4时副本数为n-1，如主机数为3时，副本为2"
        },
        "default_value": 3
    },
        {
            "type": "radio_group_tab",
            "attribute_name": "is_fixed",
            "display_name": {
                "en": "choose deploy mode",
                "zh": "定点部署"
            },
            "description": {
                "en": "choose auto deploy",
                "zh": "启用定点部署"
            },
            "option": [
                "true", "false"
            ]
        },
        # need seven support
        # {
        #     "type": "mount_points",
        #     "attribute_name": "hadoop_mount_points",
        #     "display_name": {
        #         "en": "choose hadoop config files",
        #         "zh": "指定hadoop配置"
        #     },
        #     "required": False
        # },
        # {
        #     "type": "mount_points",
        #     "attribute_name": "zk_mount_points",
        #     "display_name": {
        #         "en": "choose zookeeper config files",
        #         "zh": "指定zookeeper配置"
        #     },
        #     "required": False
        # },
        # {
        #     "type": "mount_points",
        #     "attribute_name": "hbase_mount_points",
        #     "display_name": {
        #         "en": "choose hbase config files",
        #         "zh": "指定hbase配置"
        #     },
        #     "required": False
        # },
        {
            "type": "radio_group_tab",
            "attribute_name": "enable_kafka",
            "display_name": {
                "en": "deploy kafka",
                "zh": "部署 kafka"
            },
            "option": [
                "kafka_enable", "kafka_disable"
            ]
        },
        {
            "type": "radio_group_tab",
            "attribute_name": "enable_es",
            "display_name": {
                "en": "deploy elasticsearch",
                "zh": "部署 elasticsearch"
            },
            "option": [
                "es_enable", "es_disable"
            ]
        },

    ],
    "false_config": [
        {
            "type": "string",
            "attribute_name": "data_dir",
            "pattern": "/^\/[\w\d\-_.,\/]+$/",
            "pattern_hint": {
                "en": "The data directory format is invalid, \
                    it can only contain characters like slashes, numbers, letters, \
                    hyphens(-), underscores(_), comma(,) and dots(.), and it must be absolute path",
                "zh": "路径格式错误，只能含有斜杠、数字、字母、中线、下划线、逗号和点号，且需要为绝对路径。"
            },
            "display_name": {
                "en": "Data directory",
                "zh": "存储卷"
            },
            "description": {
                "en": "support multi-path,split by comma",
                "zh": "支持多目录，以英文\",\"分割"
            },
            "default_value": "/alauda_data/emr/batchprocessing"

        }, {
            "type": "int",
            "attribute_name": "num_of_hadoop",
            "display_name": {
                "en": "number of hadoop corenode",
                "zh": "hadoop节点数"
            },
            "description": {
                "en": "",
                "zh": ""
            },
            "default_value": "3",
            "min_value": 3
        },
        {
            "type": "int",
            "attribute_name": "num_of_hbase",
            "display_name": {
                "en": "number of hbase corenode",
                "zh": "hbase节点数"
            },
            "description": {
                "en": "",
                "zh": ""
            },
            "default_value": "3",
            "min_value": 3
        },
    ],
    "true_config": [
        {
            "type": "multi_option",
            "attribute_name": "ip_tag",
            "display_name": {
                "en": "Node tag",
                "zh": "主机标签"
            },
            "description": {
                "en": "Deploy to specific host",
                "zh": "待部署的主机",
            },
            "option": {
            }
        },
        {
            "type": "string",
            "attribute_name": "data_dir",
            "pattern": "/^\/[\w\d\-_.,\/]+$/",
            "pattern_hint": {
                "en": "The data directory format is invalid, \
                    it can only contain characters like slashes, numbers, letters, \
                    hyphens(-), underscores(_), comma(,) and dots(.), and it must be absolute path",
                "zh": "路径格式错误，只能含有斜杠、数字、字母、中线、下划线、逗号和点号，且需要为绝对路径。"
            },
            "display_name": {
                "en": "Data directory",
                "zh": "本地目录"
            },
            "description": {
                "en": "support multi-path,split by comma",
                "zh": "支持多目录，以英文\",\"分割"
            },
            "default_value": "/alauda_data/emr/batchprocessing"

        },
        {
            "type": "single_ip_tag",
            "attribute_name": "master_addr",
            "display_name": {
                "en": "master address",
                "zh": "master"
            },
            "description": {
                "en": "Set the IP of master server",
                "zh": "设置 master 的 IP 地址"
            }
        },
        {
            "type": "single_ip_tag",
            "attribute_name": "alauda_secondary_namenode_addr",
            "display_name": {
                "en": "backup-master address",
                "zh": "backup-master"
            },
            "description": {
                "en": "Set the IP of backup-master server",
                "zh": "设置 backup-master 的 IP 地址"
            }
        }
    ],
    "es_enable_config": [{
        "type": "int",
        "attribute_name": "num_of_es",
        "display_name": {
            "en": "number of elasticsearch ",
            "zh": "elasticsearch节点数"
        },
        "description": {
            "en": "",
            "zh": ""
        },
        "default_value": "2",
        "min_value": 2
    },
        # need seven support
        # {
        #     "type": "mount_points",
        #     "attribute_name": "es_mount_points",
        #     "display_name": {
        #         "en": "choose elastic search config files",
        #         "zh": "指定elastic search配置"
        #     },
        #     "required": False
        # },

    ],
    "kafka_enable_config": [{
        "type": "int",
        "attribute_name": "num_of_kafka",
        "display_name": {
            "en": "number of kafka ",
            "zh": "kafka节点数"
        },
        "description": {
            "en": "",
            "zh": ""
        },
        "default_value": "2",
        "min_value": 2
    }
        # need seven support
        # {
        # "type": "mount_points",
        #     "attribute_name": "kafka_mount_points",
        #     "display_name": {
        #         "en": "choose kafka config files",
        #         "zh": "指定kafka配置"
        #     },
        #     "required": False
        # }
    ]

}


class BatchProcessing(AbstractFactory):
    def __init__(self, raw_data, namespace):
        super(BatchProcessing, self).__init__(raw_data, namespace)
        self.raw_data = raw_data
        self.app = BatchProcessEmrApp()

    def get_compose_data(self):
        self.app.init_config(self.raw_data)
        yml = yaml.safe_dump(self.app.to_yml_object(), allow_unicode=True)
        return yml

    def get_updated_yaml_content(self, new_data):
        self.app.update_config(self.raw_data, new_data)
        yml = yaml.safe_dump(self.app.to_yml_object(), allow_unicode=True)
        return yml

    def get_app_detail(self, app_id):
        logger.debug("BatchProcessing::get_app_detail")
        self.init_by_app_id(app_id)
        self.app.init_config(self.raw_data)
        return self.app.get_app_detail()


# --------------------------------------------------------------------------------------------------------------------


class BatchProcessEmrApp(EmrApp):
    def __init__(self):
        super(BatchProcessEmrApp, self).__init__()

    def get_app_scene_define(self):
        return BatchProcessAppSceneDefine()

    def get_app_dependency_define(self):
        return BatchProcessDependencyDefine()

    def pre_process(self):
        pass

    def validate(self):
        # ---- check rules
        if self.is_fixed_deploy():
            if self.parser.get_master_ip() == self.parser.get_backup_master_ip():
                raise CatalogException('secondary_name_node_invalid_node_ip_tag')

    def _get_user_deselect_services(self):
        unsel_list = []
        if not self.app_config.enable_es:
            unsel_list.append(EMRSuit.META_SVR_ES)

        if not self.app_config.enable_kafka:
            unsel_list.append(EMRSuit.META_SVR_KAFKA)
        return unsel_list

    def _get_parser(self):
        return BatchProcessFixedDeployParser() if self.is_fixed_deploy() \
            else BatchProcessAutoDeployParser()

    def _update_config(self, new_data):
        if self.is_fixed_deploy():
            # required
            self.raw_data['advanced_config']['ip_tag'] = \
                new_data['advanced_config']['ip_tag']
        else:
            # required
            self.raw_data['advanced_config']['num_of_hadoop'] = \
                new_data['advanced_config']['num_of_hadoop']
            self.raw_data['advanced_config']['num_of_hbase'] = \
                new_data['advanced_config']['num_of_hbase']
        # optional
        self.raw_data['advanced_config']['enable_kafka'] = \
            new_data['advanced_config']['enable_kafka']
        self.raw_data['advanced_config']['enable_es'] = \
            new_data['advanced_config']['enable_es']
        if new_data['advanced_config'].get('num_of_kafka'):
            self.raw_data['advanced_config']['num_of_kafka'] = \
                new_data['advanced_config']['num_of_kafka']
        if new_data['advanced_config'].get('num_of_es'):
            self.raw_data['advanced_config']['num_of_es'] = \
                new_data['advanced_config']['num_of_es']

        self.app_config = self.parser.to_config()

    def _create_app(self):
        self._create_inner_dependent_app()

    def _create_sub_services(self, app, service_type):
        if service_type == EMRSuit.META_SVR_KAFKA:
            return KafkaFixedService(app) if self.is_fixed_deploy() else KafkaAutoService(app)
        elif service_type == EMRSuit.META_SVR_ES:
            return EsFixedService(app) if self.is_fixed_deploy() else EsAutoService(app)
        elif service_type == EMRSuit.META_SVR_ZK:
            return ZkFixedService(app) if self.is_fixed_deploy() else ZkAutoService(app)
        elif service_type == EMRSuit.META_SVR_HADOOP:
            return HadoopFixedService(app) if self.is_fixed_deploy() else HadoopAutoService(app)
        elif service_type == EMRSuit.META_SVR_HBASE:
            return HBaseFixedService(app) if self.is_fixed_deploy() else HBaseAutoService(app)
