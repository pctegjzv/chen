# -*- coding: utf-8 -*-
import logging

import yaml

from catalog.claas.base_factory import AbstractFactory

from catalog.claas.emr import AppSceneDefine, \
    EMRSuit, AppVersionMeta, EmrApp, HBaseFixedService, HBaseAutoService, HBaseDependencyDefine, \
    HBaseFixedDeployParser, HBaseAutoDeployParser
from catalog.ui_element.app_info import AppInfo
from catalog.ui_element.app_instruction import AppInstruction
from ..ui_element.input_option import ElementImageTag, ElementAlaudaLBName
import sys

reload(sys)
sys.setdefaultencoding('utf-8')
logger = logging.getLogger(__name__)


class HBaseAppSceneDefine(AppSceneDefine):
    def __init__(self):
        super(HBaseAppSceneDefine, self).__init__()
        self.app_name = EMRSuit.META_APP_HBASE
        hbase_1_2_6 = AppVersionMeta(EMRSuit.META_SVR_HBASE, '1.2.6')
        self.add_version(hbase_1_2_6)
        self.http_port = {self.app_name: [10610]}
        self.tcp_port = {}


versions = HBaseAppSceneDefine().version.keys()

HBASE = {
    "name": "hbase",
    "info": AppInfo("deploy hbase cluster", "用于部署hbase集群").to_json(),
    "instruction": AppInstruction("", "").to_json(),
    "basic_config": [
        ElementImageTag(versions).to_json(),
        ElementAlaudaLBName().to_json(),
        {
            "type": "cluster_size",
            "attribute_name": "master_size",

            "display_name": {
                "en": "Instance size",
                "zh": "Master实例大小"
            },
            "description": {
                "en": "Size of instances",
                "zh": "实例大小"
            },
            "option": [
                "XXS", "XS", "S", "M", "L", "XL", "CUSTOMIZED"
            ]
        },
        {
            "type": "cluster_size",
            "attribute_name": "slaver_size",
            "display_name": {
                "en": "Instance size",
                "zh": "Slaver实例大小"
            },
            "description": {
                "en": "Size of instances",
                "zh": "实例大小"
            },
            "option": [
                "XXS", "XS", "S", "M", "L", "XL", "CUSTOMIZED"
            ]
        },
        # ElementIpTag().to_json(),
    ],
    "advanced_config": [
        {
            "type": "string",
            "attribute_name": "zookeeper_quorum",
            "display_name": {
                "en": "external zookeeper quorum",
                "zh": "已部署的zookeeper 集群"
            },
            "description": {
                "en": "external zookeeper quorum,if more than one use comma to connect",
                "zh": "外部已部署好的zookeeper 集群,多个实例以英文,分割"
            }
        },
        {
            "type": "int",
            "attribute_name": "client_port",
            "display_name": {
                "en": "external zookeeper port",
                "zh": "已部署的zookeeper 端口"
            },
            "description": {
                "en": "external zookeeper port",
                "zh": "外部已部署好的zookeeper 端口"
            }
        },
        {
            "type": "int",
            "attribute_name": "alauda_dfs_replication",
            "display_name": {
                "en": "hdfs replication",
                "zh": "HDFS副本数"
            },
            "description": {
                "en": "when the datanode number less than hdfs replication,"
                      " the stored replication number is equal to datanode number",
                "zh": "当datanode数量小于此副本数量,hdfs中的副本数量等于datanode数量"
            },
            "default_value": 3
        },
        {
            "type": "radio_group_tab",
            "attribute_name": "is_fixed",
            "display_name": {
                "en": "choose deploy mode",
                "zh": "定点部署"
            },
            "description": {
                "en": "choose auto deploy",
                "zh": "启用定点部署"
            },
            "option": [
                "true", "false"
            ]
        }
    ],
    "false_config": [
        {
            "type": "string",
            "attribute_name": "master_addr",
            "display_name": {
                "en": "master service name",
                "zh": "已部署的master 服务名"
            },
            "description": {
                "en": "Set the name of master service",
                "zh": "设置 master 的 服务名"
            }
        },
        {
            "type": "int",
            "attribute_name": "num_of_slavers",
            "display_name": {
                "en": "number of hbase corenode",
                "zh": "hbase节点数"
            },
            "description": {
                "en": "",
                "zh": ""
            },
            "default_value": "3",
            "min_value": 3
        },
    ],
    "true_config": [
        {
            "type": "multi_option",
            "attribute_name": "ip_tag",
            "display_name": {
                "en": "Node tag",
                "zh": "主机标签"
            },
            "description": {
                "en": "Deploy to specific host",
                "zh": "待部署的主机",
            },
            "option": {
            }
        },
        {
            "type": "single_ip_tag",
            "attribute_name": "master_addr",
            "display_name": {
                "en": "master address",
                "zh": "已部署的master IP"
            },
            "description": {
                "en": "Set the IP of master server",
                "zh": "设置 master 的 IP 地址"
            }
        },
        {
            "type": "single_ip_tag",
            "attribute_name": "alauda_secondary_namenode_addr",
            "display_name": {
                "en": "backup-master address",
                "zh": "已部署的 backup-master"
            },
            "description": {
                "en": "Set the IP of backup-master server",
                "zh": "设置 backup-master 的 IP 地址"
            }
        }
    ]

}


# --------------------------------------------------------------------------------------------------------------------


class Hbase(AbstractFactory):
    def __init__(self, raw_data, namespace):
        super(Hbase, self).__init__(raw_data, namespace)
        self.raw_data = raw_data
        self.app = HBaseEmrApp()

    def get_compose_data(self):
        self.app.init_config(self.raw_data)
        yml = yaml.safe_dump(self.app.to_yml_object(), allow_unicode=True)
        return yml

    def get_updated_yaml_content(self, new_data):
        self.app.update_config(new_data)
        yml = yaml.safe_dump(self.app.to_yml_object(), allow_unicode=True)
        return yml

    def get_app_detail(self, app_id):
        logger.debug("Hbase::get_app_detail")
        self.init_by_app_id(app_id)
        self.app.init_config(self.raw_data)
        return self.app.get_app_detail()


# ---------------------------------------------------------------------------------------------------------------------


class HBaseEmrApp(EmrApp):
    def __init__(self):
        super(HBaseEmrApp, self).__init__()

    def get_app_scene_define(self):
        return HBaseAppSceneDefine()

    def get_app_dependency_define(self):
        return HBaseDependencyDefine()

    def pre_process(self):
        pass

    def validate(self):
        pass

    def _get_parser(self):
        return HBaseFixedDeployParser() if self.is_fixed_deploy() else HBaseAutoDeployParser()

    def _update_config(self, new_data):
        if self.is_fixed_deploy():
            self.raw_data['advanced_config']['ip_tag'] = \
                new_data['advanced_config']['ip_tag']
        else:
            self.raw_data['advanced_config']['num_of_hbase'] = \
                new_data['advanced_config'].get('num_of_hbase', '')
        self.app_config = self.parser.to_config()

    def _create_app(self):
        self._create_config_dependent_app()

    def _create_services(self, app):
        return HBaseFixedService(app) if self.is_fixed_deploy() else HBaseAutoService(app)
