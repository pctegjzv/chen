# -*- coding: utf-8 -*-
import logging
import yaml
from urllib import quote

from base_factory import AbstractFactory
from ..ui_element.app_info import AppInfo
from ..ui_element.app_instruction import AppInstruction
from ..ui_element.input_int import ElementInternalPort
from ..ui_element.input_option import ElementClusterSize, ElementImageTag, ElementAlaudaLBName
from ..ui_element.input_multi_option import ElementNodeTag
from ..ui_element.input_tags import ElementPodAntiAffinity
from ..ui_element.input_table import ElementAppTag
from ..resources.application import Application
from client.mirana2.client import Mirana2Client

logger = logging.getLogger(__name__)
__author__ = "jliao@alauda.io"


SPRINGHYSTRIXDASHBOARD = {
    "name": "hystrix dashboard",
    "info": AppInfo("micro service circuit breaker monitor", "微服务熔断监控").to_json(),
    "instruction": AppInstruction("", "").to_json(),
    "basic_config": [
        ElementImageTag(['v0.0.1']).to_json(),
        ElementNodeTag().to_json(),
        ElementAlaudaLBName().to_json(),
        ElementInternalPort().to_json(),
        ElementPodAntiAffinity().to_json(),
        ElementClusterSize(['S', 'M', 'L', 'XL', 'CUSTOMIZED']).to_json(),
        ElementAppTag().to_json()
    ]
}


class SpringHystrixDashboard(AbstractFactory):
    def __init__(self, raw_data, namespace):
        super(SpringHystrixDashboard, self).__init__(raw_data, namespace)

    def get_compose_data(self):
        app = Application()
        service = self._get_service()
        app.add_service(service)
        return yaml.safe_dump(app.to_yml_object(), allow_unicode=True)

    def _get_service(self):
        service = super(SpringHystrixDashboard, self)._get_service()
        basic_config_dict = self.raw_data['basic_config']
        service.set_repository(
            self.image_index, 'claas/hystrix-dashboard',
            ElementImageTag.get_value(basic_config_dict))
        internal_port = ElementInternalPort.get_value(basic_config_dict)
        service.add_http_port(internal_port, internal_port)
        service.add_env_var('ALAUDA_SERVER_PORT', internal_port)
        service.add_env_var('ALAUDA_SPRING_APPLICATION_NAME', service.get_name())
        return service

    def _format_app_detail(self, data):
        detail_template = super(SpringHystrixDashboard, self)._format_app_detail(data)
        dashboard_url = Mirana2Client.get_alb_address(
            self.get_region_id(), self.dashboard_service_id)
        detail_template.set_dashboard_url(dashboard_url)
        return detail_template

    def get_dashboard_url(self, query_params):
        from .catalog_factory import CatalogFactory
        turbine_uuid = query_params.get('trubine_uuid')
        turbine = CatalogFactory.get_factory('spring_turbine', None, self.namespace)
        turbine.get_app_detail(turbine_uuid)
        turbine_url = quote("http://{}:{}".format(
            turbine.config_json.get_k8s_service_name(), 8989))
        dashboard_url = Mirana2Client.get_alb_address(
            self.get_region_id(), self.dashboard_service_id)
        return '{}/hystrix/monitor?stream={}'.format(dashboard_url, turbine_url)
