# -*- coding: utf-8 -*-
import logging
import yaml

from base_factory import AbstractFactory
from spring_config_server import SpringConfigServer
from ..ui_element.app_info import AppInfo
from ..ui_element.app_instruction import AppInstruction
# from ..ui_element.input_int import ElementInternalPort
from ..ui_element.input_option import ElementClusterSize, ElementImageTag, ElementConfigServer
from ..ui_element.input_multi_option import ElementNodeTag
from ..ui_element.input_tags import ElementPodAntiAffinity
from ..ui_element.input_table import ElementAppTag
from ..resources.application import Application

logger = logging.getLogger(__name__)
__author__ = "jliao@alauda.io"


SPRINGTURBINE = {
    "name": "turbine",
    "info": AppInfo("aggerator micro service circuit breaker data", "微服务熔断数据聚合").to_json(),
    "instruction": AppInstruction("", "").to_json(),
    "basic_config": [
        ElementImageTag(['v0.0.1']).to_json(),
        ElementNodeTag().to_json(),
        ElementPodAntiAffinity().to_json(),
        ElementClusterSize(['S', 'M', 'L', 'XL', 'CUSTOMIZED']).to_json(),
        ElementAppTag().to_json(),
    ],
    "advanced_config": [
        ElementConfigServer().to_json()
    ]
}


class SpringTurbine(AbstractFactory):
    def __init__(self, raw_data, namespace):
        super(SpringTurbine, self).__init__(raw_data, namespace)

    def get_compose_data(self):
        app = Application()
        service = self._get_service()
        app.add_service(service)
        return yaml.safe_dump(app.to_yml_object(), allow_unicode=True)

    def _get_service(self):
        service = super(SpringTurbine, self)._get_service()

        basic_config_dict = self.raw_data['basic_config']
        service.set_repository(
            self.image_index, 'claas/turbine',
            ElementImageTag.get_value(basic_config_dict))
        service.add_tcp_port(8989, 8989)
        # service.add_env_var(
        # 'ALAUDA_SERVER_PORT', ElementInternalPort.get_value(basic_config_dict))
        service.add_env_var('ALAUDA_SPRING_APPLICATION_NAME', service.get_name())

        advance_config_dict = self.raw_data['advanced_config']

        config_server_id = ElementConfigServer.get_value(advance_config_dict)
        config_server = SpringConfigServer(None, None)
        config_server.get_app_detail(config_server_id)
        service.add_env_var('ALAUDA_KAFKA_INFO', config_server.config_json.get_kafka_info())
        service.add_env_var('ALAUDA_ZOOKEEPER_INFO', config_server.config_json.get_zookeeper_info())
        service.add_env_var('ALAUDA_CONFIG_SERVER_INFO',
                            "{}:{}".format(config_server.config_json.get_k8s_service_name(),
                                           config_server.config_json.get_internal_port()))
        return service
