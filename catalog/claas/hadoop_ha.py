#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import absolute_import
from __future__ import unicode_literals

import logging

import yaml

from catalog.claas.base_factory import AbstractFactory

from catalog.claas.emr import EMRSuit, AppSceneDefine, \
    AppVersionMeta, EmrApp, HadoopHADependencyDefine, HadoopHAFixedDeployParser, \
    HadoopHAAutoDeployParser, JournalFixedService, JournalAutoService, HadoopHAFixedService, \
    HadoopHAAutoService, ZkAutoService, ZkMasterOwnedFixedService
from catalog.exceptions import CatalogException
from catalog.ui_element.app_info import AppInfo
from catalog.ui_element.app_instruction import AppInstruction
from catalog.ui_element.input_option import ElementImageTag, ElementAlaudaLBName

logger = logging.getLogger(__name__)
__author__ = "yuanfang@alauda.io"


class HadoopHAAppSceneDefine(AppSceneDefine):
    def __init__(self):
        super(HadoopHAAppSceneDefine, self).__init__()
        self.app_type = 'complex'
        self.app_name = EMRSuit.META_SVR_HADOOP_HA

        # specific services
        hdp_2_7_4 = AppVersionMeta(EMRSuit.META_SVR_HADOOP_HA, '2.7.4')
        zk_3_4_10 = AppVersionMeta(EMRSuit.META_SVR_ZK, '3.4.10')
        jn_2_7_4 = AppVersionMeta(EMRSuit.META_SVR_JOURNAL_NODE, '2.7.4')

        # desc app
        hadoop_ha = AppVersionMeta(EMRSuit.META_SVR_HADOOP_HA, '2.7.4')
        hadoop_ha.add_sub_version_meta(hdp_2_7_4)
        hadoop_ha.add_sub_version_meta(zk_3_4_10)
        hadoop_ha.add_sub_version_meta(jn_2_7_4)

        self.http_port = {
            hdp_2_7_4.app_name: [
                8088, 50070, 50075, 50090, 50105]}
        self.tcp_port = {
            hdp_2_7_4.app_name: [
                9000, 50010, 50020, 50100, 19888, 10020,
                8030, 8031, 8032, 8033, 8040, 8041, 8042,
                31010, 8020],
            zk_3_4_10.app_name: [
                2181, 2888, 3888],
            jn_2_7_4.app_name: [
                8480, 8485]
        }

        self.add_version(hadoop_ha)


versions = HadoopHAAppSceneDefine().version.keys()
HADOOPHA = {
    "name": "Hadoop 集群",
    "info": AppInfo("Hadoop HA Cluster", "Hadoop大数据处理集群").to_json(),
    "instruction": AppInstruction("最小演示环境建议:master节点2C1G以上，"
                                  "slave节点1C512M以上。\n实际生产环境视节点数量增加。",
                                  "").to_json(),
    "basic_config": [
        ElementImageTag(versions).to_json(),
        ElementAlaudaLBName().to_json(),
        {
            "type": "cluster_size",
            "attribute_name": "master_size",

            "display_name": {
                "en": "Instance size",
                "zh": "Master实例大小"
            },
            "description": {
                "en": "Size of instances",
                "zh": "实例大小"
            },
            "min_value": {
                "mem": 1024,
                "cpu": 0.25
            }
        },
        {
            "type": "cluster_size",
            "attribute_name": "slaver_size",
            "display_name": {
                "en": "Instance size",
                "zh": "Slave实例大小"
            },
            "description": {
                "en": "Size of instances",
                "zh": "实例大小"
            },

            "min_value": {
                "mem": 512,
                "cpu": 0.125
            }
        },
    ],
    "advanced_config": [{
        "type": "string",
        "attribute_name": "hdfs_ha_name_service",
        "display_name": {
            "en": "nameservice",
            "zh": "nameservice名称"
        },
        "description": {
            "en": "dfs.nameservices property in hdfs-site.xml",
            "zh": "hdfs ha服务命名，定义在hdfs-site.xml"
        },
        "default_value": 'mycluster'
    },
        {
            "type": "int",
            "attribute_name": "alauda_dfs_replication",
            "display_name": {
                "en": "hdfs replication",
                "zh": "HDFS副本数"
            },
            "description": {
                "en": "when the datanode number less than hdfs replication,"
                      " the stored replication number is equal to datanode number",
                "zh": "hdfs副本数量最多为3，当定点部署slave小于3时,副本数等于slave数."
            },
            "default_value": 3
        },
        {
            "type": "radio_group_tab",
            "attribute_name": "is_fixed",
            "display_name": {
                "en": "choose deploy mode",
                "zh": "定点部署"
            },
            "description": {
                "en": "choose auto deploy",
                "zh": "启用定点部署"
            },
            "option": [
                "true",
                # "false"
            ]
        },
        {
            "type": "radio_group_tab",
            "attribute_name": "zk_dep_type",
            "display_name": {
                "en": "choose zookeeper",
                "zh": "指定zookeeper服务"
            },
            "description": {
                "en": "choose external zookeeper or create new instances on master nodes.",
                "zh": "指定外部zookeeper服务，如不指定将在master节点上创建内部zookeeper服务."
            },
            "option": [
                "zk_disable", "zk_enable"
            ]
        },

        # need seven support
        {
            "type": "mount_points",
            "attribute_name": "hadoop_mount_points",
            "display_name": {
                "en": "choose hadoop config files",
                "zh": "hadoop配置"
            },
            "required": False
        },
        {
            "type": "mount_points",
            "attribute_name": "zk_mount_points",
            "display_name": {
                "en": "choose zookeeper config files",
                "zh": "zookeeper配置"
            },
            "required": False
        }

    ],
    # 待具备 自动部署的zookeeper 后 ，再开放该功能.
    # "false_config": [
    #     {
    #         "type": "string",
    #         "attribute_name": "data_dir",
    #         "pattern": "/^\/[\w\d\-_.,\/]+$/",
    #         "pattern_hint": {
    #             "en": "The data directory format is invalid, \
    #                 it can only contain characters like slashes, numbers, letters, \
    #                 hyphens(-), underscores(_), comma(,) and dots(.),
    #                 and it must be absolute path",
    #             "zh": "路径格式错误，只能含有斜杠、数字、字母、中线、下划线、逗号和点号，且需要为绝对路径。"
    #         },
    #         "display_name": {
    #             "en": "Data directory",
    #             "zh": "存储卷"
    #         },
    #         "description": {
    #             "en": "support multi-path,split by comma",
    #             "zh": "支持多目录，以英文\",\"分割"
    #         },
    #         "default_value": "/alauda_data/emr/hadoop"
    #
    #     }, {
    #         "type": "int",
    #         "attribute_name": "num_of_hadoop",
    #         "display_name": {
    #             "en": "number of hadoop corenode",
    #             "zh": "hadoop节点数"
    #         },
    #         "description": {
    #             "en": "",
    #             "zh": ""
    #         },
    #         "default_value": "3",
    #         "min_value": 3
    #     },
    # ],
    "true_config": [
        {
            "type": "multi_option",
            "attribute_name": "ip_tag",
            "display_name": {
                "en": "Node tag",
                "zh": "主机标签"
            },
            "description": {
                "en": "need more than 4 hosts to deploy",
                "zh": "选择至少4台待部署的主机",
            },
            "option": {
            }
        },
        {
            "type": "string",
            "attribute_name": "data_dir",
            "pattern": "/^\/[\w\d\-_.,\/]+$/",
            "pattern_hint": {
                "en": "The data directory format is invalid, \
                    it can only contain characters like slashes, numbers, letters, \
                    hyphens(-), underscores(_), comma(,) and dots(.), and it must be absolute path",
                "zh": "路径格式错误，只能含有斜杠、数字、字母、中线、下划线、逗号和点号，且需要为绝对路径。"
            },
            "display_name": {
                "en": "Data directory",
                "zh": "本地目录"
            },
            "description": {
                "en": "support multi-path,split by comma",
                "zh": "支持多目录，以英文\",\"分割"
            },
            "default_value": "/alauda_data/emr/hadoop"

        },
        {
            "type": "single_ip_tag",
            "attribute_name": "master_addr",
            "display_name": {
                "en": "master1",
                "zh": "master1"
            },
            "description": {
                "en": "contain name node,resource manager,journal node and zookeeper",
                "zh": "包含name node,resource manager,journal node 和 zookeeper进程"
            }
        },
        {
            "type": "single_ip_tag",
            "attribute_name": "alauda_secondary_namenode_addr",
            "display_name": {
                "en": "master2",
                "zh": "master2"
            },
            "description": {
                "en": "contain name node,resource manager,journal node and zookeeper",
                "zh": "包含name node,resource manager,journal node 和 zookeeper进程"
            }
        },
        {
            "type": "single_ip_tag",
            "attribute_name": "master3_addr",
            "display_name": {
                "en": "master3",
                "zh": "master3"
            },
            "description": {
                "en": "contain 包含job history,journal node and zookeeper",
                "zh": "包含job history,journal node 和 zookeeper进程"
            }
        }
    ],
    "zk_enable_config": [
        {
            "type": "string",
            "attribute_name": "zookeeper_quorum",
            "display_name": {
                "en": "external zookeeper quorum",
                "zh": "zookeeper 集群"
            },
            "description": {
                "en": "external zookeeper quorum,if more than one use comma to connect",
                "zh": "外部已部署好的zookeeper 集群,多个实例以英文,分割"
            }
        },
        {
            "type": "int",
            "attribute_name": "client_port",
            "display_name": {
                "en": "external zookeeper port",
                "zh": "zookeeper 端口"
            },
            "description": {
                "en": "external zookeeper port",
                "zh": "外部已部署好的zookeeper 端口"
            }
        }
    ]

}


class HadoopHA(AbstractFactory):
    def __init__(self, raw_data, namespace):
        super(HadoopHA, self).__init__(raw_data, namespace)
        self.raw_data = raw_data
        self.app = HadoopHAEmrApp()

    def get_compose_data(self):
        self.app.init_config(self.raw_data)
        yml = yaml.safe_dump(self.app.to_yml_object(), allow_unicode=True)
        return yml

    def get_updated_yaml_content(self, new_data):
        self.app.update_config(self.raw_data, new_data)
        yml = yaml.safe_dump(self.app.to_yml_object(), allow_unicode=True)
        return yml

    def get_app_detail(self, app_id):
        logger.debug("Hadoop::get_app_detail")
        self.init_by_app_id(app_id)
        self.app.init_config(self.raw_data)
        # cause zk and jn are master role daemons ,reset from core to master
        # so that front page handle it as usual
        lbl_master = 'master'
        lbl_backup_master = 'backup_master'
        lbl_other = 'other'
        lbl_zk = 'zookeeper'
        lbl_jn = 'journal-node'
        reset_detail = self.app.get_app_detail()
        for s in reset_detail['service']:
            if s['role'] == lbl_backup_master or s['role'] == lbl_other \
                    or lbl_zk in s['name'] or lbl_jn in s['name']:
                s['role'] = lbl_master
                s['scalable'] = 0
        return reset_detail


# --------------------------------------------------------------------------------------------------------------------


class HadoopHAEmrApp(EmrApp):
    def __init__(self):
        super(HadoopHAEmrApp, self).__init__()

    def get_app_scene_define(self):
        return HadoopHAAppSceneDefine()

    def get_app_dependency_define(self):
        return HadoopHADependencyDefine()

    def pre_process(self):
        pass

    def validate(self):
        required_num_of_min_nodes = 4
        required_num_of_min_master_nodes = 3
        # ---- check rules
        if self.is_fixed_deploy():
            if len(self.parser.get_hosts_ip()) < required_num_of_min_nodes:
                raise CatalogException('nodes_not_enough')

            s = set()
            s.add(self.parser.get_master_ip())
            s.add(self.parser.get_backup_master_ip())
            s.add(self.parser.get_master3_ip())
            if len(s) != required_num_of_min_master_nodes:
                raise CatalogException('master_ip_repeated', 'masters ip can not repeat')
        else:
            if len(self.parser.get_num_of_slavers()) < required_num_of_min_nodes:
                raise CatalogException('nodes_not_enough')

    def _get_parser(self):
        return HadoopHAFixedDeployParser() if self.is_fixed_deploy() \
            else HadoopHAAutoDeployParser()

    def _update_config(self, new_data):
        if self.is_fixed_deploy():
            # required
            self.raw_data['advanced_config']['ip_tag'] = \
                new_data['advanced_config']['ip_tag']
        else:
            # required
            self.raw_data['advanced_config']['num_of_hadoop'] = \
                new_data['advanced_config']['num_of_hadoop']
        # optional

        self.app_config = self.parser.to_config()

    def _create_app(self):
        self._create_mixed_depentent_app()

    def _create_sub_services(self, app, service_type):
        if service_type == EMRSuit.META_SVR_JOURNAL_NODE:
            return JournalFixedService(app) if self.is_fixed_deploy() else JournalAutoService(app)
        elif service_type == EMRSuit.META_SVR_ZK:
            return ZkMasterOwnedFixedService(app) if self.is_fixed_deploy() else ZkAutoService(app)
        elif service_type == EMRSuit.META_SVR_HADOOP_HA:
            return HadoopHAFixedService(app) if self.is_fixed_deploy() else HadoopHAAutoService(app)
