#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import absolute_import

import logging
from jinja2 import Template
from catalog.claas.base_factory import AbstractFactory
from catalog.exceptions import CatalogException
from django.conf import settings

logger = logging.getLogger(__name__)
__author__ = "junh@alauda.io"


ZOOKEEPER = {
    "name": "zookeeper",
    "info": {
        "en": "This template creates a multinodes ZooKeeper cluster.",
        "zh": "该模板用于部署 ZooKeeper 集群"
    },
    "instruction": {
        "en": "Enter the number of nodes, instance size, etc. for the ZooKeeper cluster.  \n\
Change the following ZooKeeper default parameters, if you needed: \n\
\n\
- Tick time: The number of milliseconds of each tick (default value: 2000)  \n\
- Init limit: The number of ticks that the initial synchronization phase can take (default value: 10)  \n\
- Sync limit: The number of ticks that can pass between sending a request (default value: 2)\n\
\n\
The directory where ZooKeeper data is stored: `/zookeeper_data` \n\
\n And getting an acknowledgement. \n\
Click deploy. done.  \n\
Note: Scaling the cluster are not supported, yet.  \n\
Please checkout [zookeeper document](https://zookeeper.apache.org/doc/r3.4.9/) for more information.\n\
",   # noqa

        "zh": "配置 ZooKeeper 集群的节点数，实例大小等等    \n\
如果需要的话，可以修改 ZooKeeper 的默认参数：  \n\
\n\
* Tick time: 每个 tick 的毫秒数（默认值：2000） \n\
* Init limit: 首次同步时可以用的 tick 数（默认值：10）  \n\
* Sync limit: 发送请求与收到回应之间可用的最大 tick 数（默认值：2） \n\
\n\
存放 ZooKeeper 数据的目录：`/zookeeper_data` \n\
\n 点击创建即可。   \n\
注意：暂时不支持更新操作。  \n\
请查阅 [zookeeper 文档](https://zookeeper.apache.org/doc/r3.4.9/)获得更多信息"
  },
    "basic_config": [{
        "type": "int",
        "attribute_name": "num_of_nodes",
        "display_name": {
            "en": "Number of nodes",
            "zh": "集群节点数"
        },
        "description": {
            "en": "Desired cluster size. 3, 5, or 7 are good choices. You will need this many hosts to reach your desired scale.",   # noqa
            "zh": "实例数量，最好是填3，5，7这样的奇数。当然你需要有这么多的主机才能成功部署"
        },
        "pattern": "odd"
      }, {
          "type": "cluster_size",
          "attribute_name": "zookeeper_size",
          "display_name": {
              "en": "Instance size",
              "zh": "实例大小"
          },
          "description": {
              "en": "Size of instances",
              "zh": "实例大小"
          },
          "min_value": {
            "mem": 512,
            "cpu": 0.25
          }
      }, {
          "type": "multi_option",
          "attribute_name": "ip_tag",
          "display_name": {
              "en": "Node tag",
              "zh": "主机标签"
          },
          "description": {
              "en": "Deploy to specific host",
              "zh": "定点部署",
          },
          "option": [],
      }, {
          "type": "option",
          "attribute_name": "image_tag",
          "display_name": {
              "en": "image tag",
              "zh": "镜像标签"
          },
          "description": {
              "en": "",
              "zh": ""
          },
          "key": "version",
          "value": "version",
          "option": settings.ZOOKEEPER_VERSION_LIST
      }
    ],
    "advanced_config": [{
        "type": "int",
        "attribute_name": "client_port",
        "display_name": {
            "en": "Client port",
            "zh": "客户端连接端口"
        },
        "description": {
            "en": "The client port of Zookeeper. \
                    You should modify the default value for security reasons.",
            "zh": "Zookeeper客户端连接端口，为安全起见建议修改默认端口。"
        },
        "default_value": 2181,
        "max_value": 65535,
        "min_value": 1024
    }, {
        "type": "int",
        "attribute_name": "server_port1",
        "display_name": {
            "en": "Server port 1",
            "zh": "服务端通讯端口1"
        },
        "description": {
            "en": "The server port of Zookeeper. \
                    You should modify the default value for security reasons.",
            "zh": "Zookeeper服务端通讯端口，为安全起见建议修改默认端口。"
        },
        "default_value": 2888,
        "max_value": 65535,
        "min_value": 1024
    }, {
        "type": "int",
        "attribute_name": "server_port2",
        "display_name": {
            "en": "Server port 2",
            "zh": "服务端通讯端口2"
        },
        "description": {
            "en": "The server port of Zookeeper. \
                    You should modify the default value for security reasons.",
            "zh": "Zookeeper服务端通讯端口，为安全起见建议修改默认端口。"
        },
        "default_value": 3888,
        "max_value": 65535,
        "min_value": 1024
    }, {
        "type": "int",
        "attribute_name": "admin_port",
        "display_name": {
            "en": "AdminServer port, 3.5 version and above support",
            "zh": "AdminServer端口,3.5版本以上支持"
        },
        "description": {
            "en": "AdminServer service listening port, to avoid conflict suggested changes",
            "zh": "AdminServer服务的监听端口，为避免冲突建议修改"
        },
        "default_value": 9098,
        "max_value": 65535,
        "min_value": 1024
    }, {
        "type": "string",
        "attribute_name": "data_dir",
        "display_name": {
            "en": "Data directory",
            "zh": "数据目录"
        },
        "description": {
            "en": "Zookeeper data directory",
            "zh": "Zookeeper 数据目录"
        },
        "default_value": "/zookeeper/data"
    }, {
        "type": "string",
        "attribute_name": "data_log_dir",
        "display_name": {
            "en": "Data log directory",
            "zh": "日志目录"
        },
        "description": {
            "en": "Zookeeper data log directory",
            "zh": "Zookeeper 日志目录"
        },
        "default_value": "/zookeeper/datalog"
    }, {
        "type": "radio_group_tab",
        "attribute_name": "mount_volume",
        "display_name": {
            "en": "Mount volume",
            "zh": "启用挂载数据卷"
        },
        "description": {
            "en": "",
            "zh": "指定数据卷挂载到容器内部使用"
        },
        "option": [
            "true", "false"
        ],
        "default_value": "false"
    }, {
        "type": "int",
        "attribute_name": "tick_time",
        "display_name": {
            "en": "Tick time",
            "zh": "Tick time"
        },
        "description": {
            "en": "The number of milliseconds of each tick",
            "zh": "每一个 tick 的长度"
        },
        "default_value": 2000,
        "max_value": 3000,
        "min_value": 1000
    }, {
        "type": "int",
        "attribute_name": "init_limit",
        "display_name": {
            "en": "Init limit",
            "zh": "Init limit"
        },
        "description": {
            "en": "Amount of time, in ticks (see tickTime), to allow followers to connect and sync to a leader. Increased this value as needed, if the amount of data managed by ZooKeeper is large.",  # noqa
            "zh": "followers 初始同步 leader 时，可用的 tick 总数, 如果数据量较大，可以增加该值"
        },
        "default_value": 10,
        "max_value": 20,
        "min_value": 5
    }, {
        "type": "string",
        "attribute_name": "sync_limit",
        "display_name": {
            "en": "Sync limit",
            "zh": "Sync limit"
        },
        "description": {
            "en": "Amount of time, in ticks (see tickTime), to allow followers to sync with ZooKeeper. If followers fall too far behind a leader, they will be dropped.",  # noqa
            "zh": "followers 与 ZooKeeper 同步时，可用的 tick 总数，如果 followers 没在限制时间内同步，就会被放弃。"   # noqa
        },
        "default_value": 5
    }],
    "true_config": [{
        "type": "string",
        "attribute_name": "mount_data_dir",
        "display_name": {
            "en": "Data directory mount point",
            "zh": "数据目录挂载点"
        },
        "description": {
            "en": "This directory will be mounted to the zookeeper data directory",
            "zh": "会将该目录挂载到zookeeper的数据目录"
        },
        "default_value": "/zookeeper/data"
    }, {
        "type": "string",
        "attribute_name": "mount_log_dir",
        "display_name": {
            "en": "Log directory mount point",
            "zh": "日志目录挂载点"
        },
        "description": {
            "en": "This directory will be mounted to the zookeeper log directory",
            "zh": "会将该目录挂载到zookeeper的日志目录"
        },
        "default_value": "/zookeeper/datalog"
    }]
}


class ZooKeeper(AbstractFactory):

    def __init__(self, raw_data, namespace):
        super(ZooKeeper, self).__init__(raw_data)
        self.meta_compose_path = self.base_template_path + '/meta_compose.yml'
        self.meta_container_without_volume_path = self.base_template_path + \
            '/meta_container_without_volume.yml'
        self.meta_container_path = self.base_template_path + '/meta_container.yml'

    def get_compose_data(self):
        basic_config_dict = self.raw_data['basic_config']
        advanced_config_dict = self.raw_data['advanced_config']

        node_count = basic_config_dict['num_of_nodes']
        ip_tag = basic_config_dict['ip_tag']
        zookeeper_size = basic_config_dict['zookeeper_size']
        cpu_quota = zookeeper_size['cpu']
        mem_limit = zookeeper_size['mem']
        image_tag = basic_config_dict['image_tag']

        if node_count != len(ip_tag):
            raise CatalogException('catalog_invalid_node_ip_tag')

        if node_count % 2 == 0:
            raise CatalogException('zookeeper_even_number')

        compose_content = str()
        if advanced_config_dict['mount_volume'] == 'true':
            container_path = self.meta_container_path
            # check the path and add suffix folder with app_name_datetime
            advanced_config_dict['mount_data_dir'] = \
                self.check_path_and_suffix_app_name(advanced_config_dict['mount_data_dir'])
            advanced_config_dict['mount_log_dir'] = \
                self.check_path_and_suffix_app_name(advanced_config_dict['mount_log_dir'])
        else:
            container_path = self.meta_container_without_volume_path
        advanced_config_dict.pop('mount_volume')

        client_port = advanced_config_dict['client_port']
        server_port1 = advanced_config_dict['server_port1']
        server_port2 = advanced_config_dict['server_port2']
        admin_port = advanced_config_dict['admin_port']
        self.check_conflict_port([client_port, server_port1, server_port2, admin_port],
                                 ['client port', 'server port1', 'server port2', 'admin_port'])

        meta_container_content = self.get_config_content(container_path)

        template = Template(meta_container_content)
        ip_tag_servers = ['server.' + str(idx + 1) + '=' + val + ':' + str(server_port1) +
                          ':' + str(server_port2) for idx, val in enumerate(ip_tag)]
        servers = ' '.join(ip_tag_servers)
        for i in range(node_count):
            container_content = template.render(image_index=self.image_index,
                                                image_tag=image_tag,
                                                node_name='zoo'+str(i+1),
                                                myid=i+1,
                                                servers=servers,
                                                node_tag="ip:"+ip_tag[i],
                                                cpu_quota=cpu_quota,
                                                mem_limit=mem_limit,
                                                **advanced_config_dict)
            compose_content += container_content

        meta_compose_content = self.get_config_content(self.meta_compose_path)
        template = Template(meta_compose_content)
        compose_content = template.render(containers=compose_content)

        logger.info("{} got compose_content: \n{}".format(
            self.__class__.__name__, compose_content))
        return compose_content

    def __repr__(self):
        out = 'ZooKeeper'
        return out
