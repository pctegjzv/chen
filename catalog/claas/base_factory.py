#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import absolute_import
from __future__ import unicode_literals

import os
import json
import logging
import re
import base64
import yaml
from datetime import datetime

from rest_framework import serializers
from catalog.exceptions import CatalogException

from ..ui_element.input_int import ElementInstanceNum
from ..ui_element.input_option import ElementClusterSize, ElementAlaudaLBName
from ..ui_element.input_multi_option import ElementNodeTag
from ..ui_element.input_tags import ElementPodAntiAffinity
from ..ui_element.input_table import ElementAppTag
from ..resources.service import Service
from ..models import Application
from .app_detail_template import AppDetailTemplate
from .app_config_json import AppConfigJson

logger = logging.getLogger(__name__)
CATALOG_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

__author__ = "junh@alauda.io"

NETWORK_MODE_BRIDGE_LIST = ['flannel', 'bridge', 'macvlan']
NETWORK_MODE_HOST = 'host'


class ApplicationDetailSerializer(serializers.ModelSerializer):

    class Meta:
        model = Application
        exclude = ('id',)


class AbstractFactory(object):

    def __init__(self, raw_data, namespace=None):
        self.namespace = namespace
        self.raw_data = raw_data
        self.data_for_render = None
        self.base_template_path = \
            CATALOG_DIR + '/meta_catalog' + '/' + self.__class__.__name__.lower()
        self.metric_yml_path = CATALOG_DIR + '/metrics/' + self.__class__.__name__.lower() + '.yml'
        self.image_index = os.environ.get('IMAGE_INDEX')
        self.config_json = None
        self.app_id = None
        self.app_query_set = None
        self.dashboard_service_id = None

    def get_compose_data(self):
        raise NotImplementedError()

    def _get_service(self, **server_args):
        sub_name = server_args.get('sub_name', '')
        is_host_network_mode = server_args.get('is_host_network_mode', False)

        basic_config_dict = self.raw_data['basic_config']
        service = Service('{}{}'.format(self.raw_data.get('name'), sub_name))

        if is_host_network_mode:
            service.set_network_mode(NETWORK_MODE_HOST)
        else:
            service.set_network_mode(
                self._get_bridge_mode())

        service.set_alauda_lb(
            ElementAlaudaLBName.get_value(basic_config_dict),
            self.raw_data.get('region_alb_version'))

        service.set_number(ElementInstanceNum.get_value(basic_config_dict))
        service.set_size(ElementClusterSize.get_value(basic_config_dict))
        service.set_lable_list(ElementNodeTag.get_value(basic_config_dict))
        service.set_app_tag_list(ElementAppTag.get_value(basic_config_dict))
        service.set_pod_ant_affinity_list(ElementPodAntiAffinity.get_value(basic_config_dict))
        return service

    def _get_bridge_mode(self):
        input_mode_list = self.raw_data.get('network_mode')
        for mode in NETWORK_MODE_BRIDGE_LIST:
            if mode in input_mode_list:
                return mode

    def get_meta_template(self, meta_compose_yml, **kwargs):
        pass

    def get_config_content(self, file_path):
        """
        THOUGHT: use database? s3? config center may be the best choice
        :return:
        """
        with open(file_path) as f:
            meta_container_content = f.read()
            f.close()
            return meta_container_content

    @classmethod
    def write_compose_content(cls, file_path, content):
        """
        for debug
        :param file_path:
        :param content:
        :return:
        """
        with open(file_path, 'w') as f:
            f.write(content)

    def _get_app_db_data(self, app_id):
        self.app_query_set = Application.objects.filter(uuid=app_id)
        if not self.app_query_set.exists():
            return None
        app_serializer = ApplicationDetailSerializer(self.app_query_set.get())
        return app_serializer.data

    def init_by_app_id(self, app_id):
        data = self._get_app_db_data(app_id)
        self.raw_data = json.loads(data.get('config_json'))
        self.config_json = AppConfigJson(data.get('config_json'))

    def get_app_detail(self, app_id):
        data = self._get_app_db_data(app_id)
        self.config_json = AppConfigJson(data.get('config_json'))
        self.app_id = app_id
        template = self._format_app_detail(data)
        return template.to_json()

    def _format_app_detail(self, data):
        detail_template = AppDetailTemplate()
        return detail_template

    def update_config_json(self):
        app = self. app_query_set.get()
        app.config_json = json.dumps(self.raw_data)
        app.save()

    def get_updated_yaml_content(self, data):
        pass

    def get_uuid(self):
        return self.app_query_set.get().uuid

    def get_dashboard_url(self, query_params):
        pass

    def set_dashboard_service_id(self, dashboard_service_id):
        self.dashboard_service_id = dashboard_service_id

    def get_region_id(self):
        return self.config_json.get_region_id()

    def check_conflict_port(self, ports, messages):
        if len(ports) != len(messages) or len(ports) < 2:
            raise CatalogException('catalog_invalid_config')
        for i in range(0, len(ports) - 1):
            for j in range(i + 1, len(ports)):
                if ports[i] == ports[j]:
                    raise CatalogException('catalog_port_conflict',
                                           message_params=[messages[i], messages[j]])

    def check_path_and_suffix_app_name(self, path, suffix=True):
        if not path:
            raise CatalogException('catalog_mount_data_dir_cant_be_empty')
        else:
            # check the path and add suffix folder with app_name_datetime
            if not re.search(r"^/[\w\d\-_./]+$", path):
                raise CatalogException('catalog_mount_data_dir_format_invalid')
            if suffix:
                path += '/' + self.raw_data['name'] + \
                    datetime.now().strftime('_%Y%m%d%H%M')
            return path

    def verify_password(self, password, password_name, encode=True):
        if not password:
            raise CatalogException('password_cant_be_empty', message_params=[password_name])

        if len(password) < 8:
            raise CatalogException('password_too_short', message_params=[password_name])
        if len(password) > 32:
            raise CatalogException('password_too_long', message_params=[password_name])
        if password.lower() == password \
                or password.upper() == password \
                or not re.search(r"\d", password):
            raise CatalogException('catalog_password_too_weak', message_params=[password_name])
        # must be printable ASCII characters and can not be ' or "
        if not re.search(r"^[\x21\x23-\x26\x28-\x7E]+$", password):
            raise CatalogException('password_invalid', message_params=[password_name])

        # encode the password
        if encode:
            password = base64.b64encode(password)
        return password

    def check_port(self, port, name, min, max):
        if type(port) != int or port >= max or port <= min:
            raise CatalogException('catalog_invalid_port',
                                   message_params=[name, str(min), str(max)])

    def get_metrics(self):
        yml_stream = open(self.metric_yml_path)
        metrics_obj = yaml.load(yml_stream)
        return metrics_obj.get('metrics')
