# -*- coding: utf-8 -*-
import logging

import yaml

from catalog.resources.application import Application
from catalog.resources.service import Service

logger = logging.getLogger(__name__)


class EMRSuit(object):
    IMAGE_META_REGISTRY_ROOT = 'index.alauda.cn'
    IMAGE_META_EMR_ROOT_DIR = '/alauda_data'

    META_DEPENDENCY_TYPE_CREATE = 'create'
    META_DEPENDENCY_TYPE_EXISTS = 'exists'
    META_DEPENDENCY_OPTIONAL = 'optional'
    META_DEPENDENCY_REQUIRED = 'required'

    META_DEPLOY_TYPE_FIXED = 'fixed'
    META_DEPLOY_TYPE_AUTO = 'auto'

    META_GROUP_ROLE_MASTER = 'master'
    META_GROUP_ROLE_BACKUP_MASTER = 'backup_master'
    META_GROUP_ROLE_OTHER_MASTER = 'other'
    META_GROUP_ROLE_CORE = 'core'  # equal to R_data + R_compute
    META_GROUP_ROLE_DATA = 'data'
    META_GROUP_ROLE_COMPUTE = 'compute'

    META_SVR_HADOOP = 'hadoop'
    META_SVR_HADOOP_HA = 'hadoop-ha'
    META_SVR_JOURNAL_NODE = 'journal-node'
    META_SVR_HBASE = 'hbase'
    META_SVR_ES = 'elasticsearch'
    META_SVR_KAFKA = 'kafka'
    META_SVR_ZK = 'zookeeper'
    META_SVR_SPARK = 'spark'

    META_APP_BATCHPROCESS = 'batch-processing'
    META_APP_HADOOP = 'hadoop'
    META_APP_HADOOP_HA = 'hadoop-ha'
    META_APP_HBASE = 'hbase'
    META_APP_ES = 'elasticsearch'
    META_APP_KAFKA = 'kafka'
    META_APP_ZK = 'zookeeper'
    META_APP_SPARK = 'spark'

    META_NET_FLANNEL = 'flannel'
    META_NET_HOST = 'host'


# --------------------------------------------------------------------------------------------------------------------

class EmrConfig(object):
    def __init__(self):
        # basic
        self.cluster_name = ''
        self.alb_name = ''
        self.alb_version = ''
        self.app_name = ''
        self.master_node_size = ''
        self.worker_node_size = ''
        self.image_version = ''
        # advance
        self.deploy_type = ''
        self.config_file_dict = dict()
        # 归一
        self.num_of_instances = ''

        # user's dependency map
        # the real configs composed by predefined and user's selections
        self.runtime_dependency = dict()


class FixedConfig(object):
    def __init__(self):
        self.master_addr = ''
        self.backup_master_addr = ''
        self.hosts_ip = ''
        self.slavers_ip = ''


class AutoConfig(object):
    def __init__(self):
        pass


class HadoopConfig(EmrConfig):
    def __init__(self):
        super(HadoopConfig, self).__init__()
        self.dfs_replicas = ''
        self.data_dir = ''
        self.data_dir_num = ''
        # self.master_node_size = ''


class HadoopAutoConfig(HadoopConfig, AutoConfig):
    def __init__(self):
        # super(HadoopAutoConfig, self).__init__()
        HadoopConfig.__init__(self)
        AutoConfig.__init__(self)
        self.num_of_hadoop = ''


class HadoopFixedConfig(HadoopConfig, FixedConfig):
    def __init__(self):
        HadoopConfig.__init__(self)
        FixedConfig.__init__(self)


class HBaseConfig(HadoopConfig):
    def __init__(self):
        super(HBaseConfig, self).__init__()
        # zk dependency
        self.need_zookeeper_quorum = ''
        self.need_client_port = ''

        # hadoop dependency from hadoop config
        # self.need_dfs_replicas = ''
        # self.need_hadoop_master_addr = ''

        # hbase
        self.hbase_root_dir = ''


class HBaseAutoConfig(HBaseConfig, AutoConfig):
    def __init__(self):
        HBaseConfig.__init__(self)
        AutoConfig.__init__(self)


class HBaseFixedConfig(HBaseConfig, FixedConfig):
    def __init__(self):
        HBaseConfig.__init__(self)
        FixedConfig.__init__(self)


class BatchProcessConfig(HadoopConfig):
    def __init__(self):
        super(BatchProcessConfig, self).__init__()
        # advance optional
        self.enable_kafka = ''
        self.enable_es = ''
        self.num_of_es = ''
        self.num_of_kafka = ''
        # self.dfs_replicas = ''
        # self.data_dir = ''
        # self.data_dir_num = ''
        self.num_of_hadoop = ''
        self.num_of_hbase = ''


class BatchProcessAutoConfig(BatchProcessConfig, AutoConfig):
    def __init__(self):
        BatchProcessConfig.__init__(self)
        AutoConfig.__init__(self)


class BatchProcessFixedConfig(BatchProcessConfig, FixedConfig):
    def __init__(self):
        BatchProcessConfig.__init__(self)
        FixedConfig.__init__(self)


class HadoopHAConfig(HadoopConfig):
    def __init__(self):
        super(HadoopHAConfig, self).__init__()
        # zk dependency
        self.need_zookeeper_quorum = ''
        self.need_client_port = ''
        # advance optional
        self.num_of_hadoop = ''
        self.hdfs_name_service = ''
        self.master_nodes = ''
        self.core_nodes = ''
        self.jn_nodes = ''
        self.zk_nodes = ''


class HadoopHAAutoConfig(HadoopHAConfig, AutoConfig):
    def __init__(self):
        HadoopHAConfig.__init__(self)
        AutoConfig.__init__(self)


class HadoopHAFixedConfig(HadoopHAConfig, FixedConfig):
    def __init__(self):
        HadoopHAConfig.__init__(self)
        FixedConfig.__init__(self)
        self.master3_addr = ''


# --------------------------------------------------------------------------------------------------------------------


class Dependency(object):
    def __init__(self, _name, _type, _optional):
        self.name = _name
        self.type = _type
        self.optional = _optional


class DependencyDefine(object):
    def __init__(self):
        self.name = ''
        self.dependency = dict()
        self.app_type = 'simple'

    def add_dependency(self, dependency):
        if not self.dependency.get(self.name):
            self.dependency[self.name] = []
        if dependency:
            self.dependency[self.name].append(dependency)

    def get_dependency(self):
        return self.dependency[self.name]


class HadoopDependencyDefine(DependencyDefine):
    def __init__(self):
        super(HadoopDependencyDefine, self).__init__()
        self.name = EMRSuit.META_APP_HADOOP
        self.add_dependency(None)


class HBaseDependencyDefine(DependencyDefine):
    def __init__(self):
        super(HBaseDependencyDefine, self).__init__()
        self.name = EMRSuit.META_APP_HBASE
        hbase_need1 = Dependency(
            EMRSuit.META_SVR_ZK,
            EMRSuit.META_DEPENDENCY_TYPE_CREATE,
            EMRSuit.META_DEPENDENCY_REQUIRED)
        hbase_need2 = Dependency(
            EMRSuit.META_SVR_HADOOP,
            EMRSuit.META_DEPENDENCY_TYPE_CREATE,
            EMRSuit.META_DEPENDENCY_REQUIRED)
        self.add_dependency(hbase_need1)
        self.add_dependency(hbase_need2)


class SparkDependencyDefine(DependencyDefine):
    def __init__(self):
        super(SparkDependencyDefine, self).__init__()
        self.name = EMRSuit.META_APP_SPARK
        self.add_dependency(None)


class ComplexDependencyDefine(DependencyDefine):
    def __init__(self):
        super(ComplexDependencyDefine, self).__init__()
        self.app_type = 'complex'

    def add_dependency(self, svr_name, dependency):
        if not self.dependency.get(self.name):
            self.dependency[self.name] = dict()
        if not self.dependency[self.name].get(svr_name):
            self.dependency[self.name][svr_name] = []
        if dependency:
            self.dependency[self.name][svr_name].append(dependency)

    def get_dependency_by_service(self, svr_name):
        return self.dependency[self.name].get(svr_name, [])


class BatchProcessDependencyDefine(DependencyDefine):
    def __init__(self):
        super(BatchProcessDependencyDefine, self).__init__()
        self.app_type = 'complex'
        self.name = EMRSuit.META_APP_BATCHPROCESS
        create_zk_first = Dependency(
            EMRSuit.META_SVR_ZK,
            EMRSuit.META_DEPENDENCY_TYPE_CREATE,
            EMRSuit.META_DEPENDENCY_REQUIRED)
        create_hadoop_first = Dependency(
            EMRSuit.META_SVR_HADOOP,
            EMRSuit.META_DEPENDENCY_TYPE_CREATE,
            EMRSuit.META_DEPENDENCY_REQUIRED)
        self.add_dependency(EMRSuit.META_SVR_HBASE, create_zk_first)
        self.add_dependency(EMRSuit.META_SVR_HBASE, create_hadoop_first)
        self.add_dependency(EMRSuit.META_SVR_KAFKA, create_zk_first)

    def add_dependency(self, svr_name, dependency):
        if not self.dependency.get(self.name):
            self.dependency[self.name] = dict()
        if not self.dependency[self.name].get(svr_name):
            self.dependency[self.name][svr_name] = []
        if dependency:
            self.dependency[self.name][svr_name].append(dependency)

    def get_dependency_by_service(self, svr_name):
        return self.dependency[self.name].get(svr_name, [])


class HadoopHADependencyDefine(ComplexDependencyDefine):
    def __init__(self):
        super(HadoopHADependencyDefine, self).__init__()
        self.name = EMRSuit.META_APP_HADOOP_HA
        create_zk_first = Dependency(
            EMRSuit.META_SVR_ZK,
            EMRSuit.META_DEPENDENCY_TYPE_CREATE,
            EMRSuit.META_DEPENDENCY_REQUIRED)
        create_jn_first = Dependency(
            EMRSuit.META_SVR_JOURNAL_NODE,
            EMRSuit.META_DEPENDENCY_TYPE_CREATE,
            EMRSuit.META_DEPENDENCY_REQUIRED)
        self.add_dependency(EMRSuit.META_SVR_HADOOP_HA, create_zk_first)
        self.add_dependency(EMRSuit.META_SVR_HADOOP_HA, create_jn_first)


# --------------------------------------------------------------------------------------------------------------------
class AppVersionMeta(object):
    def __init__(self, name, version):
        self.app_name = name
        self.app_version = version
        self.app_version_metas = []

    def add_sub_version_meta(self, app_version_meta):
        self.app_version_metas.append(app_version_meta)

    def get_sub_version_meta(self):
        return self.app_version_metas


class AppSceneDefine(object):
    def __init__(self):
        self.app_type = 'simple'
        # self.service=[]
        self.version = dict()

    def add_version(self, app_version_meta):
        if app_version_meta:
            self.version[app_version_meta.app_version] = app_version_meta

    def get_version(self, ver):
        return self.version[ver]


class HadoopAppSceneDefine(AppSceneDefine):
    def __init__(self):
        super(HadoopAppSceneDefine, self).__init__()
        self.app_name = EMRSuit.META_APP_HADOOP
        # self.add_service(EMRSuit.META_SVR_HADOOP)
        # self.version_list=['2.7.4']
        hdp_2_7_4 = AppVersionMeta(EMRSuit.META_SVR_HADOOP, '2.7.4')
        self.add_version(hdp_2_7_4)
        self.http_port = {self.app_name: [8080, 50070]}
        self.tcp_port = {}


class HBaseAppSceneDefine(AppSceneDefine):
    def __init__(self):
        super(HBaseAppSceneDefine, self).__init__()
        self.app_name = EMRSuit.META_APP_HBASE
        hbase_1_2_6 = AppVersionMeta(EMRSuit.META_SVR_HBASE, '1.2.6')
        self.add_version(hbase_1_2_6)
        self.http_port = {self.app_name: [10610]}
        self.tcp_port = {}


class BatchProcessAppSceneDefine(AppSceneDefine):
    def __init__(self):
        super(BatchProcessAppSceneDefine, self).__init__()
        self.app_type = 'complex'
        self.app_name = EMRSuit.META_APP_BATCHPROCESS

        hdp_2_7_4 = AppVersionMeta(EMRSuit.META_SVR_HADOOP, '2.7.4')
        zk_3_4_10 = AppVersionMeta(EMRSuit.META_SVR_ZK, '3.4.10')
        hbase_1_2_6 = AppVersionMeta(EMRSuit.META_SVR_HBASE, '1.2.6')
        kafka_0_9_0_1 = AppVersionMeta(EMRSuit.META_SVR_KAFKA, '0.9.0.1')
        es_1_7_5 = AppVersionMeta(EMRSuit.META_SVR_ES, '1.7.5')

        bp_2_0 = AppVersionMeta(EMRSuit.META_APP_BATCHPROCESS, '0.2.0')
        bp_2_0.add_sub_version_meta(hdp_2_7_4)
        bp_2_0.add_sub_version_meta(zk_3_4_10)
        bp_2_0.add_sub_version_meta(hbase_1_2_6)
        bp_2_0.add_sub_version_meta(kafka_0_9_0_1)
        bp_2_0.add_sub_version_meta(es_1_7_5)

        self.http_port = {
            hdp_2_7_4.app_name: [
                8088, 50070, 50075, 50090, 50105], hbase_1_2_6.app_name: [
                10610, 16030], es_1_7_5.app_name: [9200]}
        self.tcp_port = {
            hdp_2_7_4.app_name: [
                9000,
                50010,
                50020,
                50100,
                19888,
                10020,
                8030,
                8031,
                8032,
                8033,
                8040,
                8041,
                8042,
                31010,
                8020],
            zk_3_4_10.app_name: [
                2181,
                2888,
                3888],
            hbase_1_2_6.app_name: [
                9090,
                16020],
            kafka_0_9_0_1.app_name: [9092],
            es_1_7_5.app_name: [9300]}
        self.add_version(bp_2_0)


class HadoopHAAppSceneDefine(AppSceneDefine):
    def __init__(self):
        super(HadoopHAAppSceneDefine, self).__init__()
        self.app_type = 'complex'
        self.app_name = EMRSuit.META_APP_HADOOP_HA

        # specific services
        hdp_2_7_4 = AppVersionMeta(EMRSuit.META_SVR_HADOOP_HA, '2.7.4')
        zk_3_4_10 = AppVersionMeta(EMRSuit.META_SVR_ZK, '3.4.10')
        jn_2_7_4 = AppVersionMeta(EMRSuit.META_SVR_JOURNAL_NODE, '2.7.4')

        # desc app
        hadoop_ha = AppVersionMeta(EMRSuit.META_APP_HADOOP_HA, '2.7.4')
        hadoop_ha.add_sub_version_meta(hdp_2_7_4)
        hadoop_ha.add_sub_version_meta(zk_3_4_10)
        hadoop_ha.add_sub_version_meta(jn_2_7_4)

        self.http_port = {
            hdp_2_7_4.app_name: [
                8088, 50070, 50075, 50090, 50105, 8188]}
        self.tcp_port = {
            hdp_2_7_4.app_name: [
                9000,
                50010,
                50020,
                50100,
                10020,
                8030,
                8031,
                8032,
                8033,
                8040,
                8041,
                8042,
                31010,
                8020],
            zk_3_4_10.app_name: [
                2181,
                2888,
                3888],
            jn_2_7_4.app_name: [
                8480,
                8485]
        }

        self.add_version(hadoop_ha)


# --------------------------------------------------------------------------------------------------------------------

class InstanceInjector(object):
    def __init__(self, srv_tpl):
        self.service_tpl = srv_tpl

    @staticmethod
    def _check_valid_role(check_role, current_role):
        if check_role == current_role:
            return True
        elif current_role == EMRSuit.META_GROUP_ROLE_CORE:
            if check_role == EMRSuit.META_GROUP_ROLE_DATA or \
                    check_role == EMRSuit.META_GROUP_ROLE_COMPUTE:
                return True
        elif current_role == EMRSuit.META_GROUP_ROLE_MASTER or current_role == \
                EMRSuit.META_GROUP_ROLE_BACKUP_MASTER:
            if check_role == EMRSuit.META_GROUP_ROLE_MASTER or check_role == \
                    EMRSuit.META_GROUP_ROLE_BACKUP_MASTER:
                return True
        else:
            return False

    def add_config_file(self, current_service):
        file_kv_list = self.service_tpl.app_config.config_file_dict
        if isinstance(file_kv_list, dict):
            for srv_name, config_list in file_kv_list.items():
                if srv_name == current_service.service_type and isinstance(
                        config_list, list):
                    [current_service.add_mount_point(kv.get('path'), '{}/{}'.format(
                        kv.get('value').get('name'), kv.get('value').get('key')))
                     for kv in config_list]

    def inject_dependency(self, current_service):
        current_service.add_env_var(
            '__ALAUDA_HOSTNAME__',
            current_service.name)
        self.add_config_file(current_service)


class DependencyInjector(object):
    def __init__(self, srv_tpl):
        self.service_tpl = srv_tpl

    def inject_dependency(self, current_service):
        # self.add_config_file(current_service)
        pass


class ConfigDependencyInjector(DependencyInjector):
    def __init__(self, tpl):
        super(ConfigDependencyInjector, self).__init__(tpl)

    def inject_dependency(self, current_service):
        super(ConfigDependencyInjector,
              self).inject_dependency(current_service)
        # add  hint needed env variables.
        for e in dir(self.service_tpl.app_config):
            if e.startswith('need_'):
                current_service.add_env_var(e[e.index('_') + 1:].upper(),
                                            self.service_tpl.app_config.__getattribute__(e))


class ServiceDependencyInjector(DependencyInjector):
    def __init__(self, tpl):
        super(ServiceDependencyInjector, self).__init__(tpl)

    def inject_dependency(self, current_service):
        super(ServiceDependencyInjector,
              self).inject_dependency(current_service)


class KafkaServiceDependencyInjector(ServiceDependencyInjector):
    def __init__(self, tpl):
        super(KafkaServiceDependencyInjector, self).__init__(tpl)

    def inject_dependency(self, current_service):
        super(KafkaServiceDependencyInjector,
              self).inject_dependency(current_service)
        if len(
                self.service_tpl.dependent_services.get(
                    EMRSuit.META_DEPENDENCY_TYPE_CREATE,
                    [])) > 0:
            zk_srv_list = [
                s for s in self.service_tpl.dependent_services.get(
                    EMRSuit.META_DEPENDENCY_TYPE_CREATE,
                    []) if s.service_type == EMRSuit.META_SVR_ZK]
        name_port_list = []
        for zk in zk_srv_list:
            name_port_list.append(
                '{}:{}'.format(
                    zk.get_env_val('SERVER_NAME'),
                    zk.get_env_val('CLIENT_PORT')))
            # add dep links
            current_service.add_links(zk.name)

        current_service.add_env_var('ZK_CONNECT', ','.join(name_port_list))


class KafkaConfigDependencyInjector(ServiceDependencyInjector):
    def __init__(self, tpl):
        super(KafkaConfigDependencyInjector, self).__init__(tpl)

    def inject_dependency(self, current_service):
        super(KafkaConfigDependencyInjector,
              self).inject_dependency(current_service)


class MixedDependencyInjector(DependencyInjector):
    def __init__(self, tpl):
        super(MixedDependencyInjector, self).__init__(tpl)
        self.config_injector = MixedConfigDependencyInjector(tpl)
        self.service_injector = None

    def set_injectors(self, service_injector, config_injector=None):
        if config_injector is not None:
            self.config_injector = config_injector
        if service_injector is not None:
            self.service_injector = service_injector

    def inject_dependency(self, current_service):
        super(MixedDependencyInjector, self).inject_dependency(current_service)
        self.config_injector.inject_dependency(current_service)
        self.service_injector.inject_dependency(current_service)


class MixedConfigDependencyInjector(DependencyInjector):
    def inject_dependency(self, current_service):
        super(MixedConfigDependencyInjector, self).inject_dependency(current_service)

        # inject env params from "runtime_dependency"
        for env_list in self.service_tpl.app_config.runtime_dependency.values():
            for e in env_list:
                if e.startswith('need_'):
                    current_service.add_env_var(e[e.index('_') + 1:].upper(),
                                                self.service_tpl.app_config.__getattribute__(e))


class HadoopHAServiceDependencyInjector(ServiceDependencyInjector):
    def __init__(self, tpl):
        super(HadoopHAServiceDependencyInjector, self).__init__(tpl)

    def inject_dependency(self, current_service):
        super(HadoopHAServiceDependencyInjector,
              self).inject_dependency(current_service)
        if len(
                self.service_tpl.dependent_services.get(
                    EMRSuit.META_DEPENDENCY_TYPE_CREATE,
                    [])) == 0:
            return
        # zookeeper dependency
        zk_srv_list = [
            s for s in self.service_tpl.dependent_services.get(
                EMRSuit.META_DEPENDENCY_TYPE_CREATE,
                []) if s.service_type == EMRSuit.META_SVR_ZK]
        if len(zk_srv_list) > 0:
            name_list = []
            client_port = ''
            for zk in zk_srv_list:
                if client_port == '':
                    client_port = zk.get_env_val('CLIENT_PORT')
                name_list.append('{}:{}'.format(zk.get_env_val('SERVER_NAME'), client_port))
                # add dep links
                current_service.add_links(zk.name)
            current_service.add_env_var('HA_ZK_QUORUM', ','.join(name_list))

        # journal node dependency
        jn_srv_list = [
            s for s in self.service_tpl.dependent_services.get(
                EMRSuit.META_DEPENDENCY_TYPE_CREATE,
                []) if s.service_type == EMRSuit.META_SVR_JOURNAL_NODE]

        if len(jn_srv_list) > 0:
            dep_jn_srv = jn_srv_list[0]
            current_service.add_env_var(
                'QJOURNAL_LIST', dep_jn_srv.get_env_val('QJOURNAL_LIST'))
            for jn_svr in jn_srv_list:
                # add dep links
                current_service.add_links(jn_svr.name)


class HadoopHAConfigDependencyInjector(ServiceDependencyInjector):
    def __init__(self, tpl):
        super(HadoopHAConfigDependencyInjector, self).__init__(tpl)

    def inject_dependency(self, current_service):
        super(HadoopHAConfigDependencyInjector,
              self).inject_dependency(current_service)
        # inject env params from "runtime_dependency"
        for env_list in self.service_tpl.app_config.runtime_dependency.values():
            for e in env_list:
                if e.startswith('need_'):
                    current_service.add_env_var(e[e.index('_') + 1:].upper(),
                                                self.service_tpl.app_config.__getattribute__(e))


class HBaseServiceDependencyInjector(ServiceDependencyInjector):
    def __init__(self, tpl):
        super(HBaseServiceDependencyInjector, self).__init__(tpl)

    def inject_dependency(self, current_service):
        super(HBaseServiceDependencyInjector,
              self).inject_dependency(current_service)

        if len(
                self.service_tpl.dependent_services.get(
                    EMRSuit.META_DEPENDENCY_TYPE_CREATE,
                    [])) == 0:
            return
        # zookeeper dependency
        zk_srv_list = [
            s for s in self.service_tpl.dependent_services.get(
                EMRSuit.META_DEPENDENCY_TYPE_CREATE,
                []) if s.service_type == EMRSuit.META_SVR_ZK]
        if len(zk_srv_list) > 0:
            name_list = []
            client_port = ''
            for zk in zk_srv_list:
                name_list.append(zk.get_env_val('SERVER_NAME'))
                if client_port == '':
                    client_port = zk.get_env_val('CLIENT_PORT')
                # add dep links
                current_service.add_links(zk.name)
            current_service.add_env_var(
                'ZOOKEEPER_QUORUM', ','.join(name_list))
            current_service.add_env_var('CLIENT_PORT', client_port)

        # hadoop dependency
        hadoop_srv_list = [
            s for s in self.service_tpl.dependent_services.get(
                EMRSuit.META_DEPENDENCY_TYPE_CREATE,
                []) if s.service_type == EMRSuit.META_SVR_HADOOP]

        if len(hadoop_srv_list) > 0:
            dep_hadoop_srv = hadoop_srv_list[0]
            # previous namnode_addr value is equal to hbase master hostname,so
            # reset correct namenode_addr ,
            current_service.add_env_var(
                'NAMENODE_ADDR', dep_hadoop_srv.get_env_val('NAMENODE_ADDR'))
            current_service.add_env_var(
                'NAMENODE_URL',
                'hdfs://{}:{}'.format(
                    dep_hadoop_srv.get_env_val('NAMENODE_ADDR'),
                    dep_hadoop_srv.get_env_val('NAMENODE_PORT')))
            for hdp_svr in hadoop_srv_list:
                # add dep links
                current_service.add_links(hdp_svr.name)


class SparkServiceDependencyInjector(ServiceDependencyInjector):
    def __init__(self, tpl):
        super(SparkServiceDependencyInjector, self).__init__(tpl)

    def inject_dependency(self, current_service):
        pass


# --------------------------------------------------------------------------------------------------------------------


class DeploymentMixin(object):
    @staticmethod
    def dot2minus(ip):
        return ip.replace('.', '-')

    def _get_app_config(self):
        return self.app_config

    def _get_service_prefix(self):
        return '{}-{}'.format(self.app_config.app_name, self.service_type)

    def _get_instance_prefix(self):
        return self.service_type

    def _fill_public_env(self, srv, role, name=''):
        #  DEPLOY_MODE - mixin at emrservice init
        #  GROUP_ROLE
        #  SERVICE_NAME
        #  APP_NAME - create at emrservice init
        #  INSTANCE_NAME
        srv.add_env_var('GROUP_ROLE', role)
        srv.add_env_var('SERVICES', srv.service_type)
        srv.set_number(1)
        # mode settings
        if self._get_app_config().deploy_type == EMRSuit.META_DEPLOY_TYPE_FIXED:
            srv.set_network_mode(EMRSuit.META_NET_HOST)
        else:
            srv.set_network_mode(EMRSuit.META_NET_FLANNEL)
        # role settings
        if role == EMRSuit.META_GROUP_ROLE_MASTER:
            srv.set_size(self._get_app_config().master_node_size)
            name = 'master'
        elif role == EMRSuit.META_GROUP_ROLE_BACKUP_MASTER:
            srv.set_size(self._get_app_config().master_node_size)
            name = 'backup-master'
        else:
            srv.set_size(self._get_app_config().worker_node_size)
        srv.add_env_var('INSTANCE_NAME',
                        '{}-{}'.format(self._get_instance_prefix(), name))

    def _fill_private_env(self, srv):
        pass

    def _fill_dependent_env(self, svr):
        if hasattr(self, 'dependent_inject'):
            self.dependent_inject.inject_dependency(svr)

    def _fill_instance_env(self, svr):
        if hasattr(self, 'instance_inject'):
            self.instance_inject.inject_dependency(svr)

    def fill_proper_env(self, srv, role, name=''):
        self._fill_public_env(srv, role, name)
        self._fill_private_env(srv)
        self._fill_instance_env(srv)
        self._fill_dependent_env(srv)

    def fill_grouped_proper_env(self, srv, group, role, name=''):
        self._fill_grouped_public_env(srv, group, role, name)
        self._fill_private_env(srv)
        self._fill_instance_env(srv)
        self._fill_dependent_env(srv)

    def _fill_grouped_public_env(self, srv, group, role, name=''):
        srv.add_env_var('GROUP', group)
        srv.add_env_var('GROUP_ROLE', role)
        srv.add_env_var('SERVICES', srv.service_type)
        srv.set_number(1)
        # mode settings
        if self._get_app_config().deploy_type == EMRSuit.META_DEPLOY_TYPE_FIXED:
            srv.set_network_mode(EMRSuit.META_NET_HOST)
        else:
            srv.set_network_mode(EMRSuit.META_NET_FLANNEL)
        # role settings
        if group == EMRSuit.META_GROUP_ROLE_MASTER:
            srv.set_size(self._get_app_config().master_node_size)
            name = 'master{}'.format(name)
        elif group == EMRSuit.META_GROUP_ROLE_CORE:
            srv.set_size(self._get_app_config().worker_node_size)

        srv.add_env_var('INSTANCE_NAME',
                        '{}-{}'.format(self._get_instance_prefix(), name))

    # 取fixedmode实际部署的工作节点
    def _get_slave_hosts(self):
        return self._get_app_config().slavers_ip

    # 取automode实际部署的工作节点
    def _get_num_of_slaves(self):
        pass

    def _get_alb_version(self):
        return self._get_app_config().alb_version

    def _get_alb_name(self):
        return self._get_app_config().alb_name


class MSDeploymentMixin(DeploymentMixin):
    def _get_master_name(self):
        return '{}-{}'.format(self._get_service_prefix(), 'master')

    def _get_backup_master_name(self):
        return '{}-{}'.format(self._get_service_prefix(), 'backup-master')


class DecentralizationDeploymentMixin(DeploymentMixin):
    def create_all_distributed_service(self):
        self.create_core_group_services()

    def create_core_group_services(self):
        pass


# master-slave arch
class CommonHadoopMSDeploymentMixin(MSDeploymentMixin):
    def _get_hdfs_replicas(self):
        return min(self._get_app_config().dfs_replicas,
                   self._get_app_config().num_of_hadoop)

    def _get_proper_master_addr(self):
        if self._get_app_config().deploy_type == EMRSuit.META_DEPLOY_TYPE_FIXED:
            return self._get_app_config().master_addr
        else:
            return self._get_master_name()

    def _get_env_service_name(self, instance_name):
        if self._get_app_config().deploy_type == EMRSuit.META_DEPLOY_TYPE_FIXED:
            return self.service_type
        else:
            return instance_name

    def _fill_private_env(self, ref):
        _local_master_addr = self._get_proper_master_addr()
        ref.add_env_var('MASTER_HOSTNAME', _local_master_addr)
        ref.add_env_var('MASTER_IPADDR', _local_master_addr)
        ref.add_env_var('NAMENODE_ADDR', _local_master_addr)
        ref.add_env_var('NAMENODE_PORT', self._get_app_config().name_node_port)
        ref.add_env_var('YARN_RESOURCEMANAGER_ADDR', _local_master_addr)
        ref.add_env_var('DFS_REPLICAS', self._get_hdfs_replicas())
        ref.add_env_var('DFS_JOURNALNODE_DIR', '')
        ref.add_env_var('DFS_TMP_DIR', '')
        ref.add_env_var('DFS_NAMENODE_DIR', '')
        ref.add_env_var('DFS_DATANODE_DIR', '')
        ref.add_env_var('DFS_CHECKPOINT_DIR', '')
        ref.add_env_var('VOLUME_NUM', self._get_app_config().data_dir_num)

    def create_all_distributed_service(self):
        self.create_master_group_services()
        self.create_core_group_services()

    def create_master_group_services(self):
        self._create_master_services()
        self._create_backup_master_services()

    def _create_master_services(self):
        # self._create_common_env()
        master_srv = eval(str(self.__class__.__name__))(self.app)
        master_srv.set_service_name(self._get_master_name())
        master_srv.set_alauda_lb(self._get_alb_name(), self._get_alb_version())
        self.fill_proper_env(master_srv, EMRSuit.META_GROUP_ROLE_MASTER)

        if self._get_app_config().deploy_type == EMRSuit.META_DEPLOY_TYPE_FIXED:
            master_srv.add_lable('ip', self._get_app_config().master_addr)

        # 1. 服务的一个bug，在非定点部署时，需要暴露端口从而识别 cluster_ip
        # 2. expose alb port , no matter what deploy mode it is.
        for http in self.app.get_app_scene_define().http_port.get(
                master_srv.service_type, []):
            master_srv.add_http_port(http, str(http))
        self.app.service_list.append(master_srv)

    def _create_backup_master_services(self):
        # backup_master_srv = copy.deepcopy(self)
        backup_master_srv = eval(str(self.__class__.__name__))(self.app)
        backup_master_srv.set_service_name(self._get_backup_master_name())
        backup_master_srv.set_alauda_lb(self._get_alb_name(), self._get_alb_version())
        self.fill_proper_env(
            backup_master_srv,
            EMRSuit.META_GROUP_ROLE_BACKUP_MASTER)
        if self._get_app_config().deploy_type == EMRSuit.META_DEPLOY_TYPE_FIXED:
            backup_master_srv.add_lable(
                'ip', self._get_app_config().backup_master_addr)

        # 1. 服务的一个bug，在非定点部署时，需要暴露端口从而识别 cluster_ip
        # 2. expose alb port , no matter what deploy mode it is.
        for http in self.app.get_app_scene_define().http_port.get(
                backup_master_srv.service_type, []):
            backup_master_srv.add_http_port(http, str(http))
        self.app.service_list.append(backup_master_srv)


class CommonHadoopAutoMSDeploymentMixin(CommonHadoopMSDeploymentMixin):
    def __init__(self):
        super(CommonHadoopMSDeploymentMixin, self).__init__()

    def _get_num_of_slaves(self):
        return self._get_app_config().num_of_hadoop

    def create_core_group_services(self):
        num_of_instances = self._get_num_of_slaves()
        core_list = []
        for i in xrange(num_of_instances):
            core_srv = eval(str(self.__class__.__name__))(self.app)
            core_srv.set_service_name(
                '{}-{}'.format(self._get_service_prefix(), i + 1))
            self.fill_proper_env(core_srv, EMRSuit.META_GROUP_ROLE_CORE, i + 1)
            # 非定点部署，需要暴露端口
            for http in self.app.get_app_scene_define().http_port.get(core_srv.service_type, []):
                core_srv.add_http_port(http, str(http))
            for tcp in self.app.get_app_scene_define().tcp_port.get(core_srv.service_type, []):
                core_srv.add_tcp_port(tcp, str(tcp))
            core_list.append(core_srv)
        self.app.service_list += core_list


class CommonHadoopFixedMSDeploymentMixin(CommonHadoopMSDeploymentMixin):
    def __init__(self):
        super(CommonHadoopFixedMSDeploymentMixin, self).__init__()

    def create_core_group_services(self):
        slave_hosts = self._get_slave_hosts()
        core_list = []

        for i, ip in enumerate(slave_hosts, 1):
            core_srv = eval(str(self.__class__.__name__))(self.app)
            core_srv.set_service_name(
                '{}-{}'.format(self._get_service_prefix(), self.dot2minus(ip)))
            self.fill_proper_env(
                core_srv,
                EMRSuit.META_GROUP_ROLE_CORE,
                self.dot2minus(ip))
            core_srv.add_lable('ip', ip)
            core_list.append(core_srv)
        self.app.service_list += core_list


class CommonHBaseAutoMSDeploymentMixin(CommonHadoopAutoMSDeploymentMixin):
    def __init__(self):
        super(CommonHBaseAutoMSDeploymentMixin, self).__init__()

    def _get_num_of_slaves(self):
        return self._get_app_config().num_of_hbase

    def _get_hdfs_replicas(self):
        return self._get_app_config().dfs_replicas

    def _fill_private_env(self, ref):
        super(CommonHBaseAutoMSDeploymentMixin, self)._fill_private_env(ref)
        ref.add_env_var('HBASE_ROOT_DIR', '/hbase')


class CommonHBaseFixedMSDeploymentMixin(CommonHadoopFixedMSDeploymentMixin):
    def __init__(self):
        super(CommonHBaseFixedMSDeploymentMixin, self).__init__()

    def _get_hdfs_replicas(self):
        return self._get_app_config().dfs_replicas

    def _fill_private_env(self, ref):
        super(CommonHBaseFixedMSDeploymentMixin, self)._fill_private_env(ref)
        ref.add_env_var('HBASE_ROOT_DIR', '/hbase')


class EsMSDeploymentMixin(MSDeploymentMixin):
    def __init__(self):
        super(EsMSDeploymentMixin, self).__init__()

    def create_all_distributed_service(self):
        self.create_master_group_services()
        self.create_core_group_services()

    def create_master_group_services(self):
        self._create_master_services()

    def _create_master_services(self):
        master_srv = eval(str(self.__class__.__name__))(self.app)
        master_srv.set_service_name(self._get_master_name())
        master_srv.set_alauda_lb(self._get_alb_name(), self._get_alb_version())
        self.fill_proper_env(master_srv, EMRSuit.META_GROUP_ROLE_MASTER)
        if self._get_app_config().deploy_type == EMRSuit.META_DEPLOY_TYPE_FIXED:
            master_srv.add_lable('ip', self._get_app_config().master_addr)

        # 1. 服务的一个bug，在非定点部署时，需要暴露端口从而识别 cluster_ip
        # 2. expose alb port , no matter what deploy mode it is.
        for http in self.app.get_app_scene_define().http_port.get(
                master_srv.service_type, []):
            master_srv.add_http_port(http, str(http))

        self.app.service_list.append(master_srv)

    def _fill_public_env(self, srv, role, name=''):
        super(EsMSDeploymentMixin, self)._fill_public_env(srv, role, name)
        if role == EMRSuit.META_GROUP_ROLE_MASTER:
            srv.add_env_var('NODE_ROLE', 'master')
        else:
            srv.add_env_var('NODE_ROLE', 'worker')

    def _fill_private_env(self, srv):
        srv.add_env_var('CLUSTER_NAME',
                        '{}-es-cluster'.format(self.app_config.app_name))
        srv.add_env_var('NODE_RACK',
                        '{}-default-rack'.format(self.app_config.app_name))
        srv.add_env_var('NODE_NAME', srv.name)


class EsMSFixedDeploymentMixin(EsMSDeploymentMixin):
    def _get_slave_hosts(self):
        num_of_ins = self._get_app_config().num_of_es
        return self._get_app_config().slavers_ip[0:num_of_ins]

    def create_core_group_services(self):
        slave_hosts = self._get_slave_hosts()
        core_list = []
        for i, ip in enumerate(slave_hosts, 1):
            core_srv = eval(str(self.__class__.__name__))(self.app)
            core_srv.set_service_name(
                '{}-{}'.format(self._get_service_prefix(), self.dot2minus(ip)))
            self.fill_proper_env(
                core_srv,
                EMRSuit.META_GROUP_ROLE_CORE,
                self.dot2minus(ip))
            core_srv.add_lable('ip', ip)
            core_list.append(core_srv)
        self.app.service_list += core_list


class EsMSAutoDeploymentMixin(EsMSDeploymentMixin):
    def _get_num_of_slaves(self):
        return self._get_app_config().num_of_es

    def create_core_group_services(self):
        num_of_instances = self._get_num_of_slaves()
        core_list = []
        for i in xrange(1, num_of_instances + 1):
            core_srv = eval(str(self.__class__.__name__))(self.app)
            core_srv.set_service_name(
                '{}-{}'.format(self._get_service_prefix(), i))
            self.fill_proper_env(core_srv, EMRSuit.META_GROUP_ROLE_CORE, i)
            # 非定点部署，需要暴露端口
            for http in self.app.get_app_scene_define().http_port.get(core_srv.service_type, []):
                core_srv.add_http_port(http, str(http))
            for tcp in self.app.get_app_scene_define().tcp_port.get(core_srv.service_type, []):
                core_srv.add_tcp_port(tcp, str(tcp))
            core_list.append(core_srv)
        self.app.service_list += core_list


class DecentralizationFixedDeploymentMixin(DecentralizationDeploymentMixin):
    def _get_slave_hosts(self):
        return self._get_app_config().slavers_ip

    def create_core_group_services(self):
        slave_hosts = self._get_slave_hosts()
        core_list = []

        for i, ip in enumerate(slave_hosts, 1):
            core_srv = eval(str(self.__class__.__name__))(self.app)
            core_srv.set_service_name(
                '{}-{}'.format(self._get_service_prefix(), self.dot2minus(ip)))
            self.fill_grouped_proper_env(
                core_srv,
                EMRSuit.META_GROUP_ROLE_CORE,
                EMRSuit.META_GROUP_ROLE_CORE,
                self.dot2minus(ip))
            core_srv.add_lable('ip', ip)
            core_list.append(core_srv)
        self.app.service_list += core_list

    def _fill_dependent_env(self, svr):
        if hasattr(self, 'dependent_inject'):
            self.dependent_inject.inject_dependency(svr)


class DecentralizationAutoDeploymentMixin(DecentralizationDeploymentMixin):
    def create_core_group_services(self):
        num_of_instances = self._get_num_of_slaves()
        core_list = []
        for i in xrange(1, num_of_instances + 1):
            core_srv = eval(str(self.__class__.__name__))(self.app)
            core_srv.set_service_name(
                '{}-{}'.format(self._get_service_prefix(), i))
            self.fill_grouped_proper_env(core_srv, EMRSuit.META_GROUP_ROLE_CORE,
                                         EMRSuit.META_GROUP_ROLE_CORE, i)
            # 非定点部署，需要暴露端口
            for http in self.app.get_app_scene_define().http_port.get(core_srv.service_type, []):
                core_srv.add_http_port(http, str(http))
            for tcp in self.app.get_app_scene_define().tcp_port.get(core_srv.service_type, []):
                core_srv.add_tcp_port(tcp, str(tcp))
            core_list.append(core_srv)
        self.app.service_list += core_list


class ZkDecentralizationFixedDeploymentMixin(DecentralizationFixedDeploymentMixin):
    # zk fixed used all hosts,and take odd number.
    def _get_slave_hosts(self):
        return self._get_app_config(
        ).hosts_ip[0:self._get_app_config().num_of_zk]

    def _fill_private_env(self, srv):
        srv.add_env_var('SERVER_PORT', 2888)
        srv.add_env_var('ELECTION_PORT', 3888)
        srv.add_env_var('CLIENT_PORT', 2181)
        srv.add_env_var('CLEAN_ZK_DATA', 'yes')
        ip_list = []
        slave_hosts = self._get_slave_hosts()
        for i, ip in enumerate(slave_hosts, 1):
            if srv.name == '{}-{}'.format(self._get_service_prefix(),
                                          self.dot2minus(ip)):
                srv.add_env_var('SERVER_NAME', ip)
            ip_list.append(ip)
        srv.add_env_var('SERVER_LIST', ','.join(ip_list))


class ZkDecentralizationAutoDeploymentMixin(DecentralizationAutoDeploymentMixin):
    def _get_num_of_slaves(self):
        return self._get_app_config().num_of_zk

    def _fill_private_env(self, srv):
        srv.add_env_var('SERVER_PORT', 2888)
        srv.add_env_var('ELECTION_PORT', 3888)
        srv.add_env_var('CLIENT_PORT', 2181)
        srv.add_env_var('CLEAN_ZK_DATA', 'yes')
        srv.add_env_var('SERVER_NAME', srv.name)
        name_list = []
        for i in xrange(1, self._get_app_config().num_of_zk + 1):
            name_list.append('{}-{}'.format(self._get_service_prefix(), i))
        srv.add_env_var('SERVER_LIST', ','.join(name_list))


class ZkMasterOwnedFixedDeploymentMixin(ZkDecentralizationFixedDeploymentMixin):
    # zk fixed used all hosts,and take odd number.
    def _get_slave_hosts(self):
        return self._get_app_config().master_nodes


class KafkaDecentralizationFixedDeploymentMixin(DecentralizationFixedDeploymentMixin):
    def _get_slave_hosts(self):
        num_of_ins = self._get_app_config().num_of_kafka
        return self._get_app_config().slavers_ip[0:num_of_ins]

    def _fill_private_env(self, srv):
        srv.add_env_var('SERVER_PORT', 9092)
        ip_list = []
        slave_hosts = self._get_app_config().slavers_ip
        for i, ip in enumerate(slave_hosts, 1):
            if srv.name == '{}-{}'.format(self._get_service_prefix(),
                                          self.dot2minus(ip)):
                srv.add_env_var('SERVER_NAME', ip)
            ip_list.append(ip)
        srv.add_env_var('SERVER_LIST', ','.join(ip_list))


class KafkaDecentralizationAutoDeploymentMixin(DecentralizationAutoDeploymentMixin):
    def _get_num_of_slaves(self):
        return self._get_app_config().num_of_kafka

    def _fill_private_env(self, srv):
        srv.add_env_var('SERVER_PORT', 9092)
        name_list = []
        for i in xrange(1, self._get_app_config().num_of_kafka + 1):
            if srv.name == '{}-{}'.format(self._get_service_prefix(), i):
                srv.add_env_var('SERVER_NAME',
                                '{}-{}'.format(self._get_service_prefix(), i))
            name_list.append('{}-{}'.format(self._get_service_prefix(), i))
        srv.add_env_var('SERVER_LIST', ','.join(name_list))


class HadoopHADeploymentMixin(CommonHadoopMSDeploymentMixin):
    def _get_master_name(self):
        return '{}-{}'.format(self._get_service_prefix(), 'master1')

    def _get_backup_master_name(self):
        return '{}-{}'.format(self._get_service_prefix(), 'master2')

    def _get_master3_name(self):
        return '{}-{}'.format(self._get_service_prefix(), 'master3')

    def _get_proper_backup_master_addr(self):
        if self._get_app_config().deploy_type == EMRSuit.META_DEPLOY_TYPE_FIXED:
            return self._get_app_config().backup_master_addr
        else:
            return self._get_backup_master_name()

    def _get_proper_master3_addr(self):
        if self._get_app_config().deploy_type == EMRSuit.META_DEPLOY_TYPE_FIXED:
            return self._get_app_config().get_master3_ip()
        else:
            return self._get_master3_name()

    def _fill_private_env(self, ref):
        _local_master_addr = self._get_proper_master_addr()
        _local_backup_master_addr = self._get_proper_backup_master_addr()
        # ref.add_env_var('MASTER_HOSTNAME', _local_master_addr)
        # ref.add_env_var('MASTER_IPADDR', _local_master_addr)
        # ref.add_env_var('NAMENODE_ADDR', _local_master_addr)
        ref.add_env_var('NAMENODE_PORT', self._get_app_config().name_node_port)

        ref.add_env_var('DFS_REPLICAS', self._get_hdfs_replicas())
        ref.add_env_var('DFS_JOURNALNODE_DIR', '')
        ref.add_env_var('DFS_TMP_DIR', '')
        ref.add_env_var('DFS_NAMENODE_DIR', '')
        ref.add_env_var('DFS_DATANODE_DIR', '')
        ref.add_env_var('DFS_CHECKPOINT_DIR', '')
        ref.add_env_var('VOLUME_NUM', self._get_app_config().data_dir_num)

        ref.add_env_var('DATA_PATH', self._get_app_config().data_dir)
        ref.add_env_var('YARN_RESOURCEMANAGER_ADDR1', _local_master_addr)
        ref.add_env_var('YARN_RESOURCEMANAGER_ADDR2', _local_backup_master_addr)
        ref.add_env_var('NAMENODE_ADDR1', _local_master_addr)
        ref.add_env_var('NAMENODE_ADDR2', _local_backup_master_addr)
        ref.add_env_var('NAME_SERVICE', self._get_app_config().hdfs_name_service)

        ref.add_env_var('QJOURNAL_LIST', _local_master_addr)  # injector
        ref.add_env_var('HA_ZK_QUORUM', _local_master_addr)  # injector

    def create_master_group_services(self):
        self._create_master_services()
        self._create_backup_master_services()
        self._create_master_history_services()

    def _create_master_services(self):
        # self._create_common_env()
        master_srv = eval(str(self.__class__.__name__))(self.app)
        master_srv.set_service_name(self._get_master_name())
        master_srv.add_env_var('GROUP', 'master')
        master_srv.add_env_var('COMPONENT', 'nn,rm')
        master_srv.set_alauda_lb(self._get_alb_name(), self._get_alb_version())
        self.fill_grouped_proper_env(master_srv, EMRSuit.META_GROUP_ROLE_MASTER,
                                     EMRSuit.META_GROUP_ROLE_MASTER, 1)

        if self._get_app_config().deploy_type == EMRSuit.META_DEPLOY_TYPE_FIXED:
            master_srv.add_lable('ip', self._get_app_config().master_addr)

        # 1. 服务的一个bug，在非定点部署时，需要暴露端口从而识别 cluster_ip
        # 2. expose alb port , no matter what deploy mode it is.
        for http in self.app.get_app_scene_define().http_port.get(
                master_srv.service_type, []):
            master_srv.add_http_port(http, str(http))
        self.app.service_list.append(master_srv)

    def _create_backup_master_services(self):
        # backup_master_srv = copy.deepcopy(self)
        backup_master_srv = eval(str(self.__class__.__name__))(self.app)
        backup_master_srv.add_env_var('GROUP', 'master')
        backup_master_srv.add_env_var('COMPONENT', 'nn,rm')
        backup_master_srv.set_service_name(self._get_backup_master_name())
        backup_master_srv.set_alauda_lb(self._get_alb_name(), self._get_alb_version())
        self.fill_grouped_proper_env(backup_master_srv, EMRSuit.META_GROUP_ROLE_MASTER,
                                     EMRSuit.META_GROUP_ROLE_BACKUP_MASTER, 2)
        if self._get_app_config().deploy_type == EMRSuit.META_DEPLOY_TYPE_FIXED:
            backup_master_srv.add_lable(
                'ip', self._get_app_config().backup_master_addr)

        # 1. 服务的一个bug，在非定点部署时，需要暴露端口从而识别 cluster_ip
        # 2. expose alb port , no matter what deploy mode it is.
        for http in self.app.get_app_scene_define().http_port.get(
                backup_master_srv.service_type, []):
            backup_master_srv.add_http_port(http, str(http))
        self.app.service_list.append(backup_master_srv)

    def _create_master_history_services(self):
        srv = eval(str(self.__class__.__name__))(self.app)
        srv.set_service_name(self._get_master3_name())
        srv.set_alauda_lb(self._get_alb_name(), self._get_alb_version())
        srv.add_env_var('GROUP', 'master')
        srv.add_env_var('COMPONENT', 'job-his')
        self.fill_grouped_proper_env(
            srv, EMRSuit.META_GROUP_ROLE_MASTER,
            EMRSuit.META_GROUP_ROLE_OTHER_MASTER, 3)
        if self._get_app_config().deploy_type == EMRSuit.META_DEPLOY_TYPE_FIXED:
            srv.add_lable(
                'ip', self._get_app_config().master3_addr)

        # 1. 服务的一个bug，在非定点部署时，需要暴露端口从而识别 cluster_ip
        # 2. expose alb port , no matter what deploy mode it is.
        for http in self.app.get_app_scene_define().http_port.get(
                srv.service_type, []):
            srv.add_http_port(http, str(http))
        self.app.service_list.append(srv)


class HadoopHAFixedDeploymentMixin(HadoopHADeploymentMixin):
    def __init__(self):
        super(HadoopHAFixedDeploymentMixin, self).__init__()

    def _get_slave_hosts(self):
        return self._get_app_config().core_nodes

    def create_core_group_services(self):
        slave_hosts = self._get_slave_hosts()
        core_list = []
        for i, ip in enumerate(slave_hosts, 1):
            core_srv = eval(str(self.__class__.__name__))(self.app)
            core_srv.add_env_var('GROUP', 'core')
            core_srv.add_env_var('COMPONENT', 'dn,nm')
            core_srv.set_service_name(
                '{}-{}'.format(self._get_service_prefix(), self.dot2minus(ip)))
            self.fill_proper_env(
                core_srv,
                EMRSuit.META_GROUP_ROLE_CORE,
                self.dot2minus(ip))
            core_srv.add_lable('ip', ip)
            core_list.append(core_srv)
        self.app.service_list += core_list


class HadoopHAAutoDeploymentMixin(HadoopHADeploymentMixin):
    def __init__(self):
        super(HadoopHAAutoDeploymentMixin, self).__init__()

    def _get_num_of_slaves(self):
        return self._get_app_config().num_of_hadoop

    def create_core_group_services(self):
        num_of_instances = self._get_num_of_slaves()
        core_list = []
        for i in xrange(num_of_instances):
            core_srv = eval(str(self.__class__.__name__))(self.app)
            core_srv.add_env_var('GROUP', 'core')
            core_srv.add_env_var('COMPONENT', 'dn,nm')
            core_srv.set_service_name(
                '{}-{}'.format(self._get_service_prefix(), i + 1))
            self.fill_proper_env(core_srv, EMRSuit.META_GROUP_ROLE_CORE, i + 1)
            # 非定点部署，需要暴露端口
            for http in self.app.get_app_scene_define().http_port.get(core_srv.service_type, []):
                core_srv.add_http_port(http, str(http))
            for tcp in self.app.get_app_scene_define().tcp_port.get(core_srv.service_type, []):
                core_srv.add_tcp_port(tcp, str(tcp))
            core_list.append(core_srv)
        self.app.service_list += core_list


class JNDecentralizationFixedDeploymentMixin(HadoopHAFixedDeploymentMixin):
    # jn fixed used all master hosts,and take odd number.
    def _get_slave_hosts(self):
        return self._get_app_config().jn_nodes

    def create_all_distributed_service(self):
        self.create_core_group_services()

    def _fill_private_env(self, srv):
        super(JNDecentralizationFixedDeploymentMixin, self)._fill_private_env(srv)
        default_port = 8485
        srv.add_env_var('JN_PORT', default_port)
        ip_list = []
        slave_hosts = self._get_slave_hosts()
        for i, ip in enumerate(slave_hosts, 1):
            ip_list.append('{}:{}'.format(ip, default_port))
        srv.add_env_var('QJOURNAL_LIST', ';'.join(ip_list))


class JNDecentralizationAutoDeploymentMixin(HadoopHAAutoDeploymentMixin):
    def _get_num_of_slaves(self):
        return self._get_app_config().num_of_jn

    def create_all_distributed_service(self):
        self.create_core_group_services()

    def _fill_private_env(self, srv):
        default_port = 8485
        srv.add_env_var('JN_PORT', default_port)
        name_list = []
        for i in xrange(1, self._get_app_config().num_of_zk + 1):
            name_list.append('{}-{}:{}'.format(self._get_service_prefix(), i, default_port))
        srv.add_env_var('QJOURNAL_LIST', ','.join(name_list))


# -------------------------------------------------------------------------------------------------------------------

class EmrApp(Application):
    def __init__(self):
        super(EmrApp, self).__init__()
        self.app_scene_define = self.get_app_scene_define()
        self.app_dependency_define = self.get_app_dependency_define()
        # set value after call init()
        self.raw_data = None
        self.parser = None
        self.app_config = None

    # -----------------------------------------
    # methods implements by subclass (begin)
    # -----------------------------------------
    def get_app_scene_define(self):
        pass

    def get_app_dependency_define(self):
        pass

    def pre_process(self):
        pass

    def validate(self):
        pass

    def _get_parser(self):
        return FixedDeployParser() if self.is_fixed_deploy() else AutoDeployParser()

    def _update_config(self, new_data):
        pass

    # -----------------------------------------
    # methods implements by subclass (end)
    # -----------------------------------------

    def init_config(self, raw_data):
        if raw_data is not None:
            self.raw_data = raw_data
        self.pre_process()
        self.parser = self._get_parser()
        self.parser.init(self.raw_data)
        self.validate()
        self.app_config = self.parser.to_config()
        #
        self._create_app()

    def update_config(self, factory_raw_data, new_data):
        self.raw_data = factory_raw_data
        self.pre_process()
        self.parser = self._get_parser()
        self.parser.init(self.raw_data)
        self.validate()
        self._update_config(new_data)
        # self.app_config = self.parser.to_config()
        self._clear()
        self._create_app()

    def _clear(self):
        self.service_list = []

    def get_app_detail(self):
        if len(self.service_list) == 0:
            raise Exception('not found services')
        # import copy
        # config = copy.deepcopy(self.raw_data)
        config = self.raw_data.copy()
        if self.is_fixed_deploy():
            config['service'] = \
                [{"name": srv.name,
                  "role": srv.get_env_val('GROUP_ROLE'),
                  "ip": srv.get_instance_ip(),
                  "scalable": 0 if srv.get_env_val('GROUP_ROLE') ==
                    EMRSuit.META_GROUP_ROLE_MASTER or srv.get_env_val(
                      'GROUP_ROLE') == EMRSuit.META_GROUP_ROLE_BACKUP_MASTER else 1
                  }
                 for srv in self.service_list]
        else:
            config['service'] = \
                [{"name": srv.name,
                  "role": srv.get_env_val('GROUP_ROLE'),
                  "scalable": 0 if srv.get_env_val('GROUP_ROLE') ==
                    EMRSuit.META_GROUP_ROLE_MASTER or srv.get_env_val(
                      'GROUP_ROLE') == EMRSuit.META_GROUP_ROLE_BACKUP_MASTER else 1
                  }
                 for srv in self.service_list]

        return config

    def is_fixed_deploy(self):
        if self.raw_data.get('advanced_config').get(
                'is_fixed').lower() == 'true':
            return True
        else:
            return False

    def has_dependency(self):
        return len(self.app_dependency_define.get_dependency()) > 0

    def _get_scene_support_services(self):
        supported = []
        if self.app_scene_define.app_type == 'simple':
            supported.append(
                self.app_scene_define.get_version(
                    self.app_config.image_version).app_name)
        elif self.app_scene_define.app_type == 'complex':
            supported += [
                meta.app_name for meta in self.app_scene_define.get_version(
                    self.app_config.image_version).get_sub_version_meta()]
        return supported

    # override by subclass
    def _get_user_deselect_services(self):
        return []

    def get_tobe_created_services(self):
        exact_created = self._get_scene_support_services()[:]
        if len(self._get_user_deselect_services()) > 0:
            exact_created = [
                i for i in exact_created if i not in self._get_user_deselect_services()]
        return exact_created

    def _get_dependency(self, svr_name):
        if self.app_scene_define.app_type == 'simple':
            return self.app_dependency_define.get_dependency()
        elif self.app_scene_define.app_type == 'complex':
            return self.app_dependency_define.get_dependency_by_service(
                svr_name)

    @staticmethod
    def is_all_dependency_ok(needed, already_processed):
        r = [item for item in needed if item.name not in already_processed]
        return len(r) == 0

    def get_svr_ins_by_type(self, svr_type):
        return [s for s in self.service_list if s.service_type == svr_type]

    def _create_simple_app(self):
        tobe_created_svrs = self.get_tobe_created_services()
        processed = []
        while len(tobe_created_svrs) > 0:
            for svr_name in tobe_created_svrs:
                # 1.没有依赖
                if len(self._get_dependency(svr_name)) == 0:
                    svr = self._create_services(self)
                    svr.create_all_distributed_service()
                    # self.service_list += svr_list
                    processed.append(svr_name)
                    tobe_created_svrs.remove(svr_name)

    # 自包含依赖
    def _create_inner_dependent_app(self):
        tobe_created_svrs = self.get_tobe_created_services()
        processed = []
        while len(tobe_created_svrs) > 0:
            for svr_name in tobe_created_svrs:
                if len(self._get_dependency(svr_name)) == 0:
                    svr = self._create_sub_services(self, svr_name)
                    svr.create_all_distributed_service()
                    # self.service_list += svr_list
                    processed.append(svr_name)
                    tobe_created_svrs.remove(svr_name)

                elif self.is_all_dependency_ok(self._get_dependency(svr_name), processed):
                    svr = self._create_sub_services(self, svr_name)
                    for dep in self._get_dependency(svr_name):
                        svr.add_dependent_services(
                            dep.type, self.get_svr_ins_by_type(dep.name))
                    svr.create_all_distributed_service()
                    # self.service_list += svr_list
                    processed.append(svr_name)
                    tobe_created_svrs.remove(svr_name)

    # 外部提供依赖
    def _create_config_dependent_app(self):
        tobe_created_svrs = self.get_tobe_created_services()
        processed = []
        while len(tobe_created_svrs) > 0:
            for svr_name in tobe_created_svrs:
                svr = self._create_services(self)
                # svr.inject_dependency() #TODO test
                svr.create_all_distributed_service()
                processed.append(svr_name)
                tobe_created_svrs.remove(svr_name)

    # 处理混合依赖：
    # 外部已存在依赖和内部需要新建的依赖
    def _create_mixed_depentent_app(self):
        tobe_created_svrs = self.get_tobe_created_services()
        processed = []
        while len(tobe_created_svrs) > 0:
            for svr_name in tobe_created_svrs:
                # 无依赖的服务
                if len(self._get_dependency(svr_name)) == 0:
                    # 当前svr_name是外部提供的服务，不需要创建
                    if svr_name in self.app_config.runtime_dependency.keys():
                        pass
                    else:
                        svr = self._create_sub_services(self, svr_name)
                        svr.create_all_distributed_service()
                    processed.append(svr_name)
                    tobe_created_svrs.remove(svr_name)
                # 有依赖，并且所有依赖都得到解决的服务
                elif self.is_all_dependency_ok(self._get_dependency(svr_name), processed):
                    svr = self._create_sub_services(self, svr_name)
                    for dep in self._get_dependency(svr_name):
                        if dep.name in self.app_config.runtime_dependency.keys():
                            svr.add_dependent_configs(dep.type,
                                                      self.app_config.runtime_dependency[dep.name])
                        else:
                            svr.add_dependent_services(dep.type, self.get_svr_ins_by_type(dep.name))
                    svr.create_all_distributed_service()
                    processed.append(svr_name)
                    tobe_created_svrs.remove(svr_name)

    def _create_app(self):
        pass


class HadoopEmrApp(EmrApp):
    def __init__(self):
        super(HadoopEmrApp, self).__init__()

    def get_app_scene_define(self):
        return HadoopAppSceneDefine()

    def get_app_dependency_define(self):
        return HadoopDependencyDefine()

    def pre_process(self):
        # TODO
        pass

    def validate(self):
        # TODO
        pass

    def _get_parser(self):
        return HadoopFixedDeployParser() if self.is_fixed_deploy() else HadoopAutoDeployParser()

    def _update_config(self, new_data):
        if self.is_fixed_deploy():
            self.raw_data['advanced_config']['ip_tag'] = new_data['advanced_config']['ip_tag']
        else:
            self.raw_data['advanced_config']['num_of_hadoop'] = new_data['advanced_config'].get(
                'num_of_hadoop', '')
        self.app_config = self.parser.to_config()

    def _create_app(self):
        self._create_simple_app()

    def _create_services(self, app):
        return HadoopFixedService(
            app) if self.is_fixed_deploy() else HadoopAutoService(app)


class HBaseEmrApp(EmrApp):
    def __init__(self):
        super(HBaseEmrApp, self).__init__()

    def get_app_scene_define(self):
        return HBaseAppSceneDefine()

    def get_app_dependency_define(self):
        return HBaseDependencyDefine()

    def pre_process(self):
        # TODO
        pass

    def validate(self):
        # TODO
        pass

    def _get_parser(self):
        return HBaseFixedDeployParser() if self.is_fixed_deploy() else HBaseAutoDeployParser()

    def _update_config(self, new_data):
        if self.is_fixed_deploy():
            self.raw_data['advanced_config']['ip_tag'] = new_data['advanced_config']['ip_tag']
        else:
            self.raw_data['advanced_config']['num_of_hbase'] = new_data['advanced_config'].get(
                'num_of_hbase', '')
        self.app_config = self.parser.to_config()

    def _create_app(self):
        self._create_config_dependent_app()

    def _create_services(self, app):
        return HBaseFixedService(
            app) if self.is_fixed_deploy() else HBaseAutoService(app)


class BatchProcessEmrApp(EmrApp):
    def __init__(self):
        super(BatchProcessEmrApp, self).__init__()

    def get_app_scene_define(self):
        return BatchProcessAppSceneDefine()

    def get_app_dependency_define(self):
        return BatchProcessDependencyDefine()

    def pre_process(self):
        # TODO
        pass

    def validate(self):
        # TODO
        pass

    def _get_user_deselect_services(self):
        unsel_list = []
        if not self.app_config.enable_es:
            unsel_list.append(EMRSuit.META_SVR_ES)

        if not self.app_config.enable_kafka:
            unsel_list.append(EMRSuit.META_SVR_KAFKA)
        return unsel_list

    def _get_parser(self):
        return BatchProcessFixedDeployParser() if self.is_fixed_deploy(
        ) else BatchProcessAutoDeployParser()

    def _update_config(self, new_data):
        if self.is_fixed_deploy():
            # required
            self.raw_data['advanced_config']['ip_tag'] = new_data['advanced_config']['ip_tag']
            # optional
            self.raw_data['advanced_config']['enable_kafka'] = \
                new_data['advanced_config']['enable_kafka']
            self.raw_data['advanced_config']['num_of_kafka'] = \
                new_data['advanced_config']['num_of_kafka']
            self.raw_data['advanced_config']['enable_es'] = \
                new_data['advanced_config']['enable_es']
            self.raw_data['advanced_config']['num_of_es'] = \
                new_data['advanced_config']['num_of_es']

        else:
            # required
            self.raw_data['advanced_config']['num_of_hadoop'] = \
                new_data['advanced_config']['num_of_hadoop']
            self.raw_data['advanced_config']['num_of_hbase'] = \
                new_data['advanced_config']['num_of_hbase']
            # optional
            self.raw_data['advanced_config']['enable_kafka'] = \
                new_data['advanced_config']['enable_kafka']
            self.raw_data['advanced_config']['num_of_kafka'] = \
                new_data['advanced_config']['num_of_kafka']
            self.raw_data['advanced_config']['enable_es'] = \
                new_data['advanced_config']['enable_es']
            self.raw_data['advanced_config']['num_of_es'] = \
                new_data['advanced_config']['num_of_es']

        self.app_config = self.parser.to_config()

    def _create_app(self):
        self._create_inner_dependent_app()

    def _create_sub_services(self, app, service_type):
        if service_type == EMRSuit.META_SVR_KAFKA:
            return KafkaFixedService(
                app) if self.is_fixed_deploy() else KafkaAutoService(app)
        elif service_type == EMRSuit.META_SVR_ES:
            return EsFixedService(
                app) if self.is_fixed_deploy() else EsAutoService(app)
        elif service_type == EMRSuit.META_SVR_ZK:
            return ZkFixedService(
                app) if self.is_fixed_deploy() else ZkAutoService(app)
        elif service_type == EMRSuit.META_SVR_HADOOP:
            return HadoopFixedService(
                app) if self.is_fixed_deploy() else HadoopAutoService(app)
        elif service_type == EMRSuit.META_SVR_HBASE:
            return HBaseFixedService(
                app) if self.is_fixed_deploy() else HBaseAutoService(app)


class HadoopHAEmrApp(EmrApp):
    def __init__(self):
        super(HadoopHAEmrApp, self).__init__()

    def get_app_scene_define(self):
        return HadoopHAAppSceneDefine()

    def get_app_dependency_define(self):
        return HadoopHADependencyDefine()

    def pre_process(self):
        pass

    def validate(self):
        required_num_of_min_nodes = 4
        required_num_of_min_master_nodes = 3
        # ---- check rules
        if self.is_fixed_deploy():
            if len(self.parser.get_hosts_ip()) < required_num_of_min_nodes:
                print('nodes_not_enough')

            s = set()
            s.add(self.parser.get_master_ip())
            s.add(self.parser.get_backup_master_ip())
            s.add(self.parser.get_master3_ip())
            if len(s) != required_num_of_min_master_nodes:
                # TODO
                print('master_ip_repeated', 'masters ip can not repeat')
        else:
            if self.parser.get_num_of_hadoop() < required_num_of_min_nodes:
                print('nodes_not_enough')

    def _get_parser(self):
        return HadoopHAFixedDeployParser() if self.is_fixed_deploy() \
            else HadoopHAAutoDeployParser()

    def _update_config(self, new_data):
        if self.is_fixed_deploy():
            # required
            self.raw_data['advanced_config']['ip_tag'] = \
                new_data['advanced_config']['ip_tag']
        else:
            # required
            self.raw_data['advanced_config']['num_of_hadoop'] = \
                new_data['advanced_config']['num_of_hadoop']
        # optional

        self.app_config = self.parser.to_config()

    def _create_app(self):
        self._create_mixed_depentent_app()

    def _create_sub_services(self, app, service_type):
        if service_type == EMRSuit.META_SVR_JOURNAL_NODE:
            return JournalFixedService(app) if self.is_fixed_deploy() else JournalAutoService(app)
        elif service_type == EMRSuit.META_SVR_ZK:
            return ZkMasterOwnedFixedService(app) if self.is_fixed_deploy() else ZkAutoService(app)
        elif service_type == EMRSuit.META_SVR_HADOOP_HA:
            return HadoopHAFixedService(app) if self.is_fixed_deploy() else HadoopHAAutoService(app)


# --------------------------------------------------------------------------------------------------------------------


class EmrService(Service):
    REGISTRY_HOST = EMRSuit.IMAGE_META_REGISTRY_ROOT
    REGISTRY_PATH = ''
    SERVICE_NAME_TPL = '{}-tpl'

    def __init__(self, app):
        self.app = app
        self.app_config = app.app_config
        super(
            EmrService, self).__init__(
            self.SERVICE_NAME_TPL.format(
                self.service_type))
        self.app_name = self.app_config.app_name
        if self.app.get_app_scene_define().app_type == 'complex':
            for sub_service_version in self.app.get_app_scene_define().get_version(
                    self.app_config.image_version).get_sub_version_meta():
                if sub_service_version.app_name == self.service_type:
                    self.set_repository_tag(sub_service_version.app_version)
                    break
        else:
            self.set_repository_tag(self.app_config.image_version)

        self.add_outer_volume(self.app_config.data_dir)
        # -----------   description -----------
        # {
        #  create:[svr_list], # 外部已存在依赖
        #  exists:[svr_list]  # 应用创建的依赖
        # }
        self.dependent_services = {}
        self.dependent_configs = {}
        self._env_init()
        self.instance_inject = InstanceInjector(self)

    def set_service_name(self, name):
        self.name = name

    def _env_init(self):
        self.add_env_var('APP_NAME', self.app_name)
        self.add_env_var('SERVICES', self.service_type)
        if hasattr(self, '_mode_env_init'):
            self._mode_env_init()

    def set_repository_tag(self, tag):
        self.set_repository(self.REGISTRY_HOST, self.REGISTRY_PATH, tag)

    def get_env_val(self, key):
        return self.env_var_dict.get(key, '')

    def add_outer_volume(self, outer_path):
        path_tpl = '{}/data{}'
        out_volumes = outer_path.split(',')
        for idx, path in enumerate(out_volumes):
            if path.strip() != '':
                inner_folder = path_tpl.format(
                    EMRSuit.IMAGE_META_EMR_ROOT_DIR, idx + 1)
                self.add_volume(path, inner_folder)

    def add_dependent_services(self, dep_type, service):
        if not self.dependent_services.get(dep_type):
            self.dependent_services[dep_type] = []
        if isinstance(service, list):
            self.dependent_services[dep_type] += service
        elif isinstance(service, EmrService):
            self.dependent_services[dep_type].append(service)

    def add_dependent_configs(self, dep_type, parameter):
        if not self.dependent_configs.get(dep_type):
            self.dependent_configs[dep_type] = []
        if isinstance(parameter, list):
            self.dependent_configs[dep_type] += parameter
        elif isinstance(parameter, str):
            self.dependent_configs[dep_type].append(parameter)

    def get_instance_ip(self):
        if len(self.node_lable_list) > 0:
            for i in self.node_lable_list:
                if i.label_key == 'ip':
                    return i.label_value

        return ''


class FixedServiceMixin(object):
    def _mode_env_init(self):
        self.add_env_var('DEPLOY_MODE', 'fixed')


class AutoServiceMixin(object):
    def _mode_env_init(self):
        self.add_env_var('DEPLOY_MODE', 'auto')


class HadoopService(EmrService):
    REGISTRY_PATH = 'claas/emr-hadoop'

    def __init__(self, app):
        self.service_type = EMRSuit.META_SVR_HADOOP
        super(HadoopService, self).__init__(app)


class HadoopFixedService(HadoopService, FixedServiceMixin, CommonHadoopFixedMSDeploymentMixin):
    def __init__(self, app):
        super(HadoopFixedService, self).__init__(app)


class HadoopAutoService(HadoopService, AutoServiceMixin, CommonHadoopAutoMSDeploymentMixin):
    def __init__(self, name):
        super(HadoopAutoService, self).__init__(name)


class HBaseService(EmrService):
    REGISTRY_PATH = 'claas/emr-hbase'

    def __init__(self, app):
        self.service_type = EMRSuit.META_SVR_HBASE
        super(HBaseService, self).__init__(app)
        self.dependent_inject = HBaseServiceDependencyInjector(self) \
            if self.app.app_scene_define.app_type == 'complex' else ConfigDependencyInjector(self)


class HBaseFixedService(HBaseService, FixedServiceMixin, CommonHBaseFixedMSDeploymentMixin):
    def __init__(self, app):
        super(HBaseFixedService, self).__init__(app)


class HBaseAutoService(HBaseService, AutoServiceMixin, CommonHBaseAutoMSDeploymentMixin):
    def __init__(self, name):
        super(HBaseAutoService, self).__init__(name)


class SparkService(EmrService):
    REGISTRY_PATH = 'claas/emr-spark'

    def __init__(self, app):
        self.service_type = EMRSuit.META_SVR_SPARK
        super(SparkService, self).__init__(app)
        self.dependent_inject = SparkServiceDependencyInjector(self) \
            if self.app.app_scene_define.app_type == 'complex' else ConfigDependencyInjector(self)


class SparkFixedService(SparkService, FixedServiceMixin, CommonHBaseFixedMSDeploymentMixin):
    def __init__(self, app):
        super(SparkFixedService, self).__init__(app)


class SparkAutoService(SparkService, AutoServiceMixin, CommonHBaseAutoMSDeploymentMixin):
    def __init__(self, name):
        super(SparkAutoService, self).__init__(name)


class KafkaService(EmrService):
    REGISTRY_PATH = 'claas/emr-kafka'

    def __init__(self, app):
        self.service_type = EMRSuit.META_SVR_KAFKA
        super(KafkaService, self).__init__(app)
        self.dependent_inject = KafkaServiceDependencyInjector(self) \
            if self.app.app_scene_define.app_type == 'complex' \
            else KafkaConfigDependencyInjector(self)


class KafkaFixedService(KafkaService, FixedServiceMixin, KafkaDecentralizationFixedDeploymentMixin):
    def __init__(self, app):
        super(KafkaFixedService, self).__init__(app)


class KafkaAutoService(KafkaService, AutoServiceMixin, KafkaDecentralizationAutoDeploymentMixin):
    def __init__(self, name):
        super(KafkaAutoService, self).__init__(name)


class EsService(EmrService):
    REGISTRY_PATH = 'claas/emr-es'

    def __init__(self, app):
        self.service_type = EMRSuit.META_SVR_ES
        super(EsService, self).__init__(app)


class EsFixedService(EsService, FixedServiceMixin, EsMSFixedDeploymentMixin):
    def __init__(self, app):
        super(EsFixedService, self).__init__(app)


class EsAutoService(EsService, AutoServiceMixin, EsMSAutoDeploymentMixin):
    def __init__(self, name):
        super(EsAutoService, self).__init__(name)


class ZkService(EmrService):
    REGISTRY_PATH = 'claas/emr-zk'

    # INNER_DATA_ROOT_DIR = 'zk_data'

    def __init__(self, app):
        self.service_type = EMRSuit.META_SVR_ZK
        super(ZkService, self).__init__(app)


class ZkFixedService(ZkService, FixedServiceMixin, ZkDecentralizationFixedDeploymentMixin):
    def __init__(self, app):
        super(ZkFixedService, self).__init__(app)


class ZkAutoService(ZkService, AutoServiceMixin, ZkDecentralizationAutoDeploymentMixin):
    def __init__(self, name):
        super(ZkAutoService, self).__init__(name)


class JournalService(EmrService):
    REGISTRY_PATH = 'claas/emr-hadoop-ha'

    def __init__(self, app):
        self.service_type = EMRSuit.META_SVR_JOURNAL_NODE
        super(JournalService, self).__init__(app)


class JournalFixedService(JournalService, FixedServiceMixin,
                          JNDecentralizationFixedDeploymentMixin):
    def __init__(self, app):
        super(JournalFixedService, self).__init__(app)


class JournalAutoService(JournalService, AutoServiceMixin,
                         JNDecentralizationAutoDeploymentMixin):
    def __init__(self, name):
        super(JournalAutoService, self).__init__(name)


class ZkMasterOwnedFixedService(ZkService, FixedServiceMixin,
                                ZkMasterOwnedFixedDeploymentMixin):
    def __init__(self, app):
        super(ZkMasterOwnedFixedService, self).__init__(app)


class HadoopHAService(EmrService):
    REGISTRY_PATH = 'claas/emr-hadoop-ha'

    def __init__(self, app):
        self.service_type = EMRSuit.META_SVR_HADOOP_HA
        super(HadoopHAService, self).__init__(app)


class HadoopHAFixedService(HadoopHAService, FixedServiceMixin, HadoopHAFixedDeploymentMixin):
    def __init__(self, app):
        super(HadoopHAFixedService, self).__init__(app)
        self.dependent_inject = MixedDependencyInjector(self)
        self.dependent_inject.set_injectors(
            service_injector=HadoopHAServiceDependencyInjector(self),
            config_injector=HadoopHAConfigDependencyInjector(self))


class HadoopHAAutoService(HadoopHAService, AutoServiceMixin, HadoopHAAutoDeploymentMixin):
    def __init__(self, name):
        super(HadoopHAAutoService, self).__init__(name)
        self.dependent_inject = HadoopHAServiceDependencyInjector(self) \
            if self.app.app_scene_define.app_type == 'complex' else ConfigDependencyInjector(self)


# -------------------------------------------------------------------------------------------------------------------

class HadoopFacade(object):
    def __init__(self, raw_data, namespace):
        self.raw_data = raw_data
        self.app = HadoopEmrApp()

    def get_compose_data(self):
        self.app.init_config(self.raw_data)
        yml = yaml.safe_dump(self.app.to_yml_object(), allow_unicode=True)
        return yml

    def get_updated_yaml_content(self, factory_raw_data, new_data):
        # self.app.init_config(self.raw_data)  # TODO 正式使用时取消注释
        self.app.update_config(factory_raw_data, new_data)
        yml = yaml.safe_dump(self.app.to_yml_object(), allow_unicode=True)
        return yml

    def get_app_detail(self, app_id):
        return self.app.get_app_detail()


class HBaseFacade(object):
    def __init__(self, raw_data, namespace):
        self.raw_data = raw_data
        self.app = HBaseEmrApp()

    def get_compose_data(self):
        self.app.init_config(self.raw_data)
        yml = yaml.safe_dump(self.app.to_yml_object(), allow_unicode=True)
        return yml

    def get_updated_yaml_content(self, factory_raw_data, new_data):
        # self.app.init_config(self.raw_data)  # TODO 正式使用时取消注释
        self.app.update_config(factory_raw_data, new_data)
        import yaml
        yml = yaml.safe_dump(self.app.to_yml_object(), allow_unicode=True)
        return yml

    def get_app_detail(self, app_id):
        return self.app.get_app_detail()


class BatchProcessFacade(object):
    def __init__(self, raw_data, namespace):
        self.raw_data = raw_data
        self.app = BatchProcessEmrApp()

    def get_compose_data(self):
        self.app.init_config(self.raw_data)
        yml = yaml.safe_dump(self.app.to_yml_object(), allow_unicode=True)
        return yml

    def get_updated_yaml_content(self, factory_raw_data, new_data):
        # self.app.init_config(self.raw_data)  # TODO 正式使用时取消注释
        self.app.update_config(factory_raw_data, new_data)
        yml = yaml.safe_dump(self.app.to_yml_object(), allow_unicode=True)
        return yml

    def get_app_detail(self, app_id):
        return self.app.get_app_detail()


class HadoopHAFacade(object):
    def __init__(self, raw_data, namespace):
        self.raw_data = raw_data
        self.app = HadoopHAEmrApp()

    def get_compose_data(self):
        self.app.init_config(self.raw_data)
        yml = yaml.safe_dump(self.app.to_yml_object(), allow_unicode=True)
        return yml

    def get_updated_yaml_content(self, factory_raw_data, new_data):
        # self.app.init_config(self.raw_data)  # TODO 正式使用时取消注释
        self.app.update_config(factory_raw_data, new_data)
        yml = yaml.safe_dump(self.app.to_yml_object(), allow_unicode=True)
        return yml

    def get_app_detail(self, app_id):
        return self.app.get_app_detail()


# -------------------------------------------------------------------------------------------------------------------

class DeployParser(object):
    def __init__(self):
        self.config = EmrConfig()
        self.raw_data = None

    def init(self, raw_data):
        self.raw_data = raw_data

    def to_config(self):
        self.config.app_name = self.get_app_name()
        self.config.cluster_name = self.get_region_name()
        self.config.alb_name = self.get_alb_name()
        self.config.alb_version = self.get_alb_version()
        self.config.deploy_type = EMRSuit.META_DEPLOY_TYPE_FIXED if self.raw_data.get(
            'advanced_config').get('is_fixed').lower() == 'true' else EMRSuit.META_DEPLOY_TYPE_AUTO
        self.config.image_version = self.get_image_version()
        self.config.master_node_size = self.get_master_spec()
        self.config.worker_node_size = self.get_slaver_spec()
        self.config.config_file_dict = self.get_config_file()
        return self.config

    def get_data_dir(self):
        return self.raw_data['advanced_config'].get('data_dir')

    # def get_config_file(self):
    #     return self.raw_data['advanced_config'].get('mount_points')

    def get_alb_version(self):
        return self.raw_data.get('region_alb_version', '')

    def get_alb_name(self):
        return self.raw_data['basic_config'].get('alauda_lb_name', '')

    def get_image_version(self):
        return self.raw_data['basic_config'].get('image_tag', '')

    # def get_num_of_nodes(self):
    #     return self.raw_data['basic_config'].get('num_of_nodes', 2)

    def get_cluster_spec(self):
        return self.raw_data['basic_config'].get('cluster_size')

    def get_master_spec(self):
        return self.raw_data['basic_config'].get('master_size')

    def get_slaver_spec(self):
        return self.raw_data['basic_config'].get('slaver_size')

    def get_app_name(self):
        return self.raw_data.get('name', '').lower()

    def get_region_name(self):
        return self.raw_data.get('region', '')

        # - basic info  end


class FixedDeployParser(DeployParser):
    def __init__(self):
        super(FixedDeployParser, self).__init__()

    def to_config(self):
        super(FixedDeployParser, self).to_config()
        self.config.master_addr = self.get_master_ip()
        self.config.backup_master_addr = self.get_backup_master_ip()
        self.config.hosts_ip = self.get_hosts_ip()
        self.config.num_of_instances = len(self.get_slavers_ip())
        self.config.slavers_ip = self.get_slavers_ip()

        return self.config

    def is_deploy_data_on_master(self):
        return self.raw_data['advanced_config'].get(
            'mix_deploy_datanode', 'false') == 'true'

    def get_hosts_ip(self):
        return self.raw_data['advanced_config'].get('ip_tag', [])

    def get_master_ip(self):
        return self.raw_data['advanced_config'].get('master_addr')

    def get_backup_master_ip(self):
        return self.raw_data['advanced_config'].get(
            'alauda_secondary_namenode_addr', '')

    # use by hdfs-ha mode
    def get_master3_ip(self):
        return self.raw_data['advanced_config'].get(
            'master3_addr', '')

    # ----- compute workers -----
    def get_slavers_ip(self):
        all_hosts_ip = self.raw_data['advanced_config'].get('ip_tag', [])
        return filter(lambda ip: not ip == self.get_master_ip(), all_hosts_ip)

    # ----- update page config -----
    def update_hosts_ip(self, new_data):
        self.raw_data['advanced_config']['ip_tag'] = new_data['advanced_config'].get(
            'ip_tag', [])


class AutoDeployParser(DeployParser):
    def __init__(self):
        super(AutoDeployParser, self).__init__()

    def to_config(self):
        super(AutoDeployParser, self).to_config()
        self.config.num_of_instances = self.get_num_of_slavers()
        return self.config

    def get_num_of_slavers(self):
        return self.raw_data['advanced_config'].get('num_of_slavers')


class BaseParserMixin(object):
    def get_config_file_by_svr(self, svr_name):
        config_dict = dict()
        if self.raw_data['advanced_config'].get(
                '{}_mount_points'.format(svr_name)):
            config_dict[svr_name] = self.raw_data['advanced_config'].get(
                '{}_mount_points'.format(svr_name))
        return config_dict


class HadoopParserMixin(BaseParserMixin):
    def get_name_node_port(self):
        return 9000

    def get_hdfs_replicas(self):
        return self.raw_data['advanced_config'].get('alauda_dfs_replication')

    def get_num_of_hadoop(self):
        return self.raw_data['advanced_config'].get('num_of_hadoop', '')

    def get_config_file(self):
        return self.get_config_file_by_svr(EMRSuit.META_SVR_HADOOP)


class HadoopFixedDeployParser(FixedDeployParser, HadoopParserMixin):
    def __init__(self):
        super(HadoopFixedDeployParser, self).__init__()
        self.config = HadoopFixedConfig()

    def to_config(self):
        super(HadoopFixedDeployParser, self).to_config()
        self.config.data_dir = self.get_data_dir()
        self.config.data_dir_num = len(self.config.data_dir.split(','))
        self.config.dfs_replicas = self.get_hdfs_replicas()
        self.config.name_node_port = self.get_name_node_port()
        self.config.num_of_hadoop = len(self.get_slavers_ip())

        return self.config


class HadoopAutoDeployParser(AutoDeployParser, HadoopParserMixin):
    def __init__(self):
        super(HadoopAutoDeployParser, self).__init__()
        self.config = HadoopAutoConfig()

    def to_config(self):
        super(HadoopAutoDeployParser, self).to_config()
        self.config.data_dir = self.get_data_dir()
        self.config.data_dir_num = len(self.config.data_dir.split(','))
        self.config.dfs_replicas = self.get_hdfs_replicas()
        self.config.name_node_port = self.get_name_node_port()
        self.config.num_of_hadoop = self.get_num_of_hadoop()
        return self.config


class HBaseParserMixin(BaseParserMixin):
    def __init__(self):
        super(HBaseParserMixin, self).__init__()

    def get_zookeeper_quorum(self):
        return self.raw_data['advanced_config'].get('zookeeper_quorum', '')

    # def get_zookeeper_port(self):
    #     return self.raw_data['advanced_config'].get('client_port', '')

    def get_zookeeper_port(self):
        return 2181

    def get_num_of_hbase(self):
        return self.raw_data['advanced_config'].get('num_of_hbase', '')

    def get_config_file(self):
        return self.get_config_file_by_svr(EMRSuit.META_SVR_HBASE)


class HBaseFixedDeployParser(FixedDeployParser, HBaseParserMixin, HadoopParserMixin):
    def __init__(self):
        super(HBaseFixedDeployParser, self).__init__()
        self.config = HBaseFixedConfig()

    def to_config(self):
        super(HBaseFixedDeployParser, self).to_config()
        # needed will auto inject to env
        self.config.need_zookeeper_quorum = self.get_zookeeper_quorum()
        self.config.need_client_port = self.get_zookeeper_port()
        self.config.name_node_port = self.get_name_node_port()
        self.config.need_dfs_replicas = self.get_hdfs_replicas()
        self.config.name_node_port = self.get_name_node_port()

        self.config.need_namenode_url = 'hdfs://{}:{}'.format(
            self.get_master_ip(), self.get_name_node_port())

        self.config.num_of_hbase = len(self.get_slavers_ip())
        self.config.slavers_ip = self.get_slavers_ip()

        return self.config


class HBaseAutoDeployParser(AutoDeployParser, HBaseParserMixin, HadoopParserMixin):
    def __init__(self):
        super(HBaseAutoDeployParser, self).__init__()
        self.config = HBaseAutoConfig()

    def to_config(self):
        super(HBaseAutoDeployParser, self).to_config()
        # needed will auto inject to env
        self.config.need_zookeeper_quorum = self.get_zookeeper_quorum()
        self.config.need_client_port = self.get_zookeeper_port()
        self.config.name_node_port = self.get_name_node_port()
        self.config.need_dfs_replicas = self.get_hdfs_replicas()
        self.config.name_node_port = self.get_name_node_port()

        self.config.need_namenode_url = 'hdfs://{}:{}'.format(
            self.get_need_master_service_name(), self.get_name_node_port())

        self.config.num_of_hbase = self.get_num_of_hbase()
        return self.config

    def get_need_master_service_name(self):
        return self.raw_data['advanced_config'].get('master_addr', '')


class SparkFixedDeployParser(FixedDeployParser, HBaseParserMixin, HadoopParserMixin):
    def __init__(self):
        super(SparkFixedDeployParser, self).__init__()
        self.config = HBaseFixedConfig()

    def to_config(self):
        super(SparkFixedDeployParser, self).to_config()
        # needed will auto inject to env
        self.config.need_zookeeper_quorum = self.get_zookeeper_quorum()
        self.config.need_client_port = self.get_zookeeper_port()
        self.config.name_node_port = self.get_name_node_port()
        self.config.need_dfs_replicas = self.get_hdfs_replicas()
        self.config.name_node_port = self.get_name_node_port()

        self.config.need_namenode_url = 'hdfs://{}:{}'.format(
            self.get_master_ip(), self.get_name_node_port())

        self.config.num_of_hbase = len(self.get_slavers_ip())
        self.config.slavers_ip = self.get_slavers_ip()

        return self.config


class SparkAutoDeployParser(AutoDeployParser, HBaseParserMixin, HadoopParserMixin):
    def __init__(self):
        super(SparkAutoDeployParser, self).__init__()
        self.config = HBaseAutoConfig()

    def to_config(self):
        super(SparkAutoDeployParser, self).to_config()
        # needed will auto inject to env
        self.config.need_zookeeper_quorum = self.get_zookeeper_quorum()
        self.config.need_client_port = self.get_zookeeper_port()
        self.config.name_node_port = self.get_name_node_port()
        self.config.need_dfs_replicas = self.get_hdfs_replicas()
        self.config.name_node_port = self.get_name_node_port()

        self.config.need_namenode_url = 'hdfs://{}:{}'.format(
            self.get_need_master_service_name(), self.get_name_node_port())

        self.config.num_of_hbase = self.get_num_of_hbase()
        return self.config

    def get_need_master_service_name(self):
        return self.raw_data['advanced_config'].get('master_addr', '')


class EsParserMixin(BaseParserMixin):
    def get_num_of_es(self):
        if self.raw_data['advanced_config'].get('num_of_es'):
            return self.raw_data['advanced_config'].get('num_of_es')


class KafkaParserMixin(BaseParserMixin):
    def get_num_of_kafka(self):
        if self.raw_data['advanced_config'].get('num_of_kafka'):
            return self.raw_data['advanced_config'].get('num_of_kafka')


class BatchProcessParserMixin(HadoopParserMixin, HBaseParserMixin, EsParserMixin, KafkaParserMixin):
    def is_kafka_enable(self):
        return self.raw_data['advanced_config'].get(
            'enable_kafka', '') == "kafka_enable"

    def is_es_enable(self):
        return self.raw_data['advanced_config'].get(
            'enable_es', '') == "es_enable"

    def _get_num_of_zk(self, num_of_hosts):
        if num_of_hosts < 3:
            num = 1
        elif num_of_hosts < 10:
            num = 3
        else:
            num = 5
        return num

    def get_config_file(self):
        config_dict = dict()
        if self.raw_data['advanced_config'].get('hadoop_mount_points'):
            config_dict[EMRSuit.META_SVR_HADOOP] = self.raw_data['advanced_config'].get(
                'hadoop_mount_points')
        if self.raw_data['advanced_config'].get('hbase_mount_points'):
            config_dict[EMRSuit.META_SVR_HBASE] = self.raw_data['advanced_config'].get(
                'hbase_mount_points')
        if self.raw_data['advanced_config'].get('zk_mount_points'):
            config_dict[EMRSuit.META_SVR_ZK] = self.raw_data['advanced_config'].get(
                'zk_mount_points')
        if self.raw_data['advanced_config'].get('kafka_mount_points'):
            config_dict[EMRSuit.META_SVR_KAFKA] = self.raw_data['advanced_config'].get(
                'kafka_mount_points')
        if self.raw_data['advanced_config'].get('es_mount_points'):
            config_dict[EMRSuit.META_SVR_ES] = self.raw_data['advanced_config'].get(
                'es_mount_points')

        return config_dict


class BatchProcessFixedDeployParser(FixedDeployParser, BatchProcessParserMixin):
    def __init__(self):
        super(BatchProcessFixedDeployParser, self).__init__()
        self.config = BatchProcessFixedConfig()

    def to_config(self):
        super(BatchProcessFixedDeployParser, self).to_config()
        # hadoop
        self.config.data_dir = self.get_data_dir()
        self.config.data_dir_num = len(self.get_data_dir().split(","))
        self.config.name_node_port = self.get_name_node_port()

        self.config.enable_es = self.is_es_enable()
        self.config.enable_kafka = self.is_kafka_enable()
        self.config.num_of_es = self.get_num_of_es()
        self.config.num_of_kafka = self.get_num_of_kafka()
        self.config.num_of_zk = self.get_num_of_zk()
        self.config.num_of_hadoop = len(self.get_slavers_ip())
        self.config.num_of_hbase = len(self.get_slavers_ip())
        return self.config

    def get_num_of_zk(self):
        return self._get_num_of_zk(len(self.get_hosts_ip()))


class BatchProcessAutoDeployParser(AutoDeployParser, BatchProcessParserMixin):
    def __init__(self):
        super(BatchProcessAutoDeployParser, self).__init__()
        self.config = BatchProcessAutoConfig()

    def to_config(self):
        super(BatchProcessAutoDeployParser, self).to_config()

        self.config.data_dir = self.get_data_dir()
        self.config.data_dir_num = len(self.get_data_dir().split(","))
        self.config.name_node_port = self.get_name_node_port()

        # needed
        # self.config.need_zookeeper_quorum = self.get_zookeeper_quorum()
        # self.config.need_client_port = self.get_zookeeper_port()

        self.config.enable_es = self.is_es_enable()
        self.config.enable_kafka = self.is_kafka_enable()
        self.config.num_of_es = self.get_num_of_es()
        self.config.num_of_kafka = self.get_num_of_kafka()
        self.config.num_of_zk = self.get_num_of_zk()
        self.config.num_of_hadoop = self.get_num_of_hadoop()
        self.config.num_of_hbase = self.get_num_of_hbase()

        return self.config

    def get_num_of_zk(self):
        return self._get_num_of_zk(self.get_num_of_hbase())


class HadoopHAParserMixin(HadoopParserMixin):

    def get_config_file(self):
        config_dict = dict()
        if self.raw_data['advanced_config'].get('zk_mount_points'):
            config_dict[EMRSuit.META_SVR_ZK] = self.raw_data['advanced_config'].get(
                'zk_mount_points')
        if self.raw_data['advanced_config'].get('hadoop_mount_points'):
            config_dict[EMRSuit.META_SVR_HADOOP_HA] = self.raw_data['advanced_config'].get(
                'hadoop_mount_points')
        return config_dict

    def get_name_service(self):
        return self.raw_data['advanced_config'].get('hdfs_ha_name_service')

    # handle runtime dependency
    def set_external_dependency_config(self, config):
        external_deps = dict()
        if self.raw_data['advanced_config'].get('zk_dep_type') == 'zk_enable':
            external_deps[EMRSuit.META_SVR_ZK] = []
            external_deps[EMRSuit.META_SVR_ZK].append('need_zookeeper_quorum')
            external_deps[EMRSuit.META_SVR_ZK].append('need_client_port')
            config.need_zookeeper_quorum = self.raw_data['advanced_config'].get('zookeeper_quorum')
            config.need_client_port = self.raw_data['advanced_config'].get('client_port')
        config.runtime_dependency = external_deps


class HadoopHAFixedDeployParser(FixedDeployParser, HadoopHAParserMixin):
    def __init__(self):
        super(HadoopHAFixedDeployParser, self).__init__()
        self.config = HadoopHAFixedConfig()

    def to_config(self):
        super(HadoopHAFixedDeployParser, self).to_config()
        # hadoop
        self.config.data_dir = self.get_data_dir()
        self.config.data_dir_num = len(self.get_data_dir().split(","))
        self.config.name_node_port = self.get_name_node_port()

        self.config.num_of_zk = len(self.get_jn_zk_nodes())
        self.config.num_of_hadoop = len(self.get_core_nodes())
        self.config.hdfs_name_service = self.get_name_service()
        self.config.master_nodes = self.get_master_nodes()
        self.config.core_nodes = self.get_core_nodes()
        self.config.jn_nodes = self.get_jn_zk_nodes()
        self.config.zk_nodes = self.get_jn_zk_nodes()
        self.config.master3_addr = self.get_master3_ip()
        # 设置外部依赖
        self.set_external_dependency_config(self.config)

        return self.config

    def get_master_nodes(self):
        return [self.get_master_ip(), self.get_backup_master_ip(), self.get_master3_ip()]

    def get_core_nodes(self):
        _master_nodes = self.get_master_nodes()
        return filter(lambda ip: ip not in _master_nodes, self.get_hosts_ip())

    def get_jn_zk_nodes(self):
        num = 3
        if len(self.get_master_nodes()) > 5:
            num = 5
        return self.get_master_nodes()[0:num]


class HadoopHAAutoDeployParser(AutoDeployParser, HadoopHAParserMixin):
    def __init__(self):
        super(HadoopHAAutoDeployParser, self).__init__()
        self.config = HadoopHAAutoConfig()

    def to_config(self):
        super(HadoopHAAutoDeployParser, self).to_config()

        self.config.data_dir = self.get_data_dir()
        self.config.data_dir_num = len(self.get_data_dir().split(","))
        self.config.name_node_port = self.get_name_node_port()
        self.config.num_of_zk = self.get_num_of_jn_zk()
        self.config.num_of_jn = self.get_num_of_jn_zk()
        self.config.num_of_hadoop = self.get_num_of_hadoop()
        self.config.hdfs_name_service = self.get_name_service()
        return self.config

    def get_num_of_jn_zk(self):
        """
        # assume there are 3 nodes in master group,
        # 1st is master
        # 2nd is backup master
        # 3th is job-history
          zookeeper on all master nodes
        """

        return 3


# ----------------------------------------------------------------------------------------------------------------------------------

# ----------------------------------------------------------------------------------------------------------------------------------

def test_get_hadoop_auto_json():
    return {
        "region": "app_test",
        "name": "bpauto1",
        "space_name": "in-catalog", "region_alb_version": "v2", "namespace": "admin",
        "basic_config": {
            "cluster_size": "M",
            # "ip_tag": ["192.168.0.6", "192.168.0.5", "192.168.0.7"],
            "image_tag": "2.7.4",
            "alauda_lb_name": "haproxy-139-219-98-176"},
        "advanced_config": {"is_fixed": 'false',
                            "alauda_dfs_replication": 3,
                            "data_dir": "/my_data/batchprocessing",
                            "num_of_hadoop": 3,
                            }
    }


def test_get_update_hadoop_auto_json():
    return {
        "region": "app_test",
        "name": "bpauto1",
        "space_name": "in-catalog",
        "region_alb_version": "v2",
        "namespace": "admin",
        "basic_config": {
            "cluster_size": "M",
            "image_tag": "2.7.4",
            "alauda_lb_name": "haproxy-139-219-98-176"},
        "advanced_config": {
            "is_fixed": 'false',
            "alauda_dfs_replication": 3,
            "data_dir": "/my_data/batchprocessing",
            "num_of_hadoop": 1}}


def test_get_hadoop_fixed_json():
    return {
        "advanced_config": {
            "alauda_dfs_replication": 3,
            "is_fixed": "true",
            "enable_kafka": "kafka_enable",
            "enable_es": "es_enable",
            "data_dir": "/my_data/batchprocessing",
            "master_addr": "192.168.0.5",
            "alauda_secondary_namenode_addr": "192.168.0.6",
            "ip_tag": ["192.168.0.5", "192.168.0.6"],
            "num_of_es": 2,
            "num_of_kafka": 2,
            "mount_point": {EMRSuit.META_SVR_HADOOP: [
                {"path": "/alauda_data/core-site.xml", "config": "bigdata-xufei/core_site_xml",
                 "role": EMRSuit.META_GROUP_ROLE_MASTER},
                {"path": "/alauda_data/hdfs-site.xml", "config": "bigdata-xufei/hdfs_site_xml",
                 "role": EMRSuit.META_GROUP_ROLE_DATA}]
            }
        },
        "basic_config": {
            "image_tag": "2.7.4",
            "alauda_lb_name": "haproxy-40-125-200-37",
            "cluster_size": "XXS",

        },
        "name": "tfixed",
        "region": "int_bigdata",
        "region_alb_version": "v2",
        "network_mode": ["flannel"],
        "namespace": "alauda"
    }


def test_get_update_hadoop_fixed_json():
    return {
        "advanced_config": {
            "alauda_dfs_replication": 3,
            "is_fixed": "true",
            "data_dir": "/my_data/batchprocessing",
            "master_addr": "192.168.0.5",
            "alauda_secondary_namenode_addr": "192.168.0.6",
            "ip_tag": ["192.168.0.5", "192.168.0.6", "192.168.0.7"]
        },
        "basic_config": {
            "image_tag": "2.7.4",
            "alauda_lb_name": "haproxy-40-125-200-37",
            "cluster_size": "XS",

        },
        "name": "fixedhdp",
        "region": "int_bigdata",
        "region_alb_version": "v2",
        "network_mode": ["flannel"],
        "namespace": "alauda"
    }


# ----------------------------------------------------------------------------------------------------------------------------------

def test_get_hbase_auto_json():
    return {
        "advanced_config": {
            "zookeeper_quorum": "outer-zk-1,outer-zk-2,outer-zk-3",
            "zookeeper_port": "2181",
            "alauda_dfs_replication": 3,
            "is_fixed": "false",
            "data_dir": "/my_data/batchprocessing",
            "master_addr": "outer-hadoop-master",
            "num_of_hbase": 2
        },
        "basic_config": {
            "image_tag": "1.2.6",
            "alauda_lb_name": "haproxy-40-125-200-37",
            "master_size": "S", "slaver_size": "XS"

        },
        "name": "tauto",
        "region": "int_bigdata",
        "region_alb_version": "v2",
        "network_mode": ["flannel"],
        "namespace": "alauda"
    }


def test_get_update_hbase_auto_json():
    return {
        "advanced_config": {
            "zookeeper_quorum": "192.168.0.5,192.168.0.6,192.168.0.7",
            "zookeeper_port": "2181",
            "alauda_dfs_replication": 3,
            "is_fixed": "false",
            "data_dir": "/my_data/batchprocessing",
            "num_of_hbase": 3
        },
        "basic_config": {
            "image_tag": "1.2.6",
            "alauda_lb_name": "haproxy-40-125-200-37",
            "master_size": "S", "slaver_size": "XS"
        },
        "name": "tauto",
        "region": "int_bigdata",
        "region_alb_version": "v2",
        "network_mode": ["flannel"],
        "namespace": "alauda"
    }


def test_get_hbase_fixed_json():
    return {
        "advanced_config": {
            "zookeeper_quorum": "192.168.0.5,192.168.0.6,192.168.0.7",
            "zookeeper_port": "2181",
            "alauda_dfs_replication": 3,
            "is_fixed": "true",
            "data_dir": "/my_data/batchprocessing",
            "master_addr": "192.168.0.5",
            "alauda_secondary_namenode_addr": "192.168.0.6",
            "ip_tag": ["192.168.0.5", "192.168.0.6", "192.168.0.7"],
            "hbase_mount_points": [
                {"path": "/etc/hadoop/core-site.xml", "type": "config",
                 "value": {"name": "bigdata-xufei", "key": "hdfs_site_xml"}}]
        },
        "basic_config": {
            "image_tag": "1.2.6",
            "alauda_lb_name": "haproxy-40-125-200-37",
            "master_size": "S", "slaver_size": "XS"
        },
        "name": "tfixed",
        "region": "int_bigdata",
        "region_alb_version": "v2",
        "network_mode": ["flannel"],
        "namespace": "alauda"
    }


def test_get_update_hbase_fixed_json():
    return {
        "advanced_config": {
            "zookeeper_quorum": "192.168.0.5,192.168.0.6,192.168.0.7",
            "zookeeper_port": "2181",
            "alauda_dfs_replication": 3,
            "is_fixed": "true",
            "data_dir": "/my_data/batchprocessing",
            "master_addr": "192.168.0.5",
            "alauda_secondary_namenode_addr": "192.168.0.6",
            "ip_tag": [
                "192.168.0.5",
                "192.168.0.6",
                "192.168.0.7",
                "192.168.0.8"]},
        "basic_config": {
            "image_tag": "1.2.6",
            "alauda_lb_name": "haproxy-40-125-200-37",
            "master_size": "S",
            "slaver_size": "XS"},
        "name": "tfixed",
        "region": "int_bigdata",
        "region_alb_version": "v2",
        "network_mode": ["flannel"],
        "namespace": "alauda"}


# ----------------------------------------------------------------------------------------------------------------------------------

def test_get_bp_auto_json():
    return {
        "advanced_config": {
            "alauda_dfs_replication": 3,
            "is_fixed": "false",
            "enable_kafka": "kafka_enable",
            "enable_es": "es_enable",
            "data_dir": "/my_data/batchprocessing,/my_data/batchprocessing1",
            "num_of_hadoop": 3,
            "num_of_hbase": 3,
            "num_of_es": 2,
            "num_of_kafka": 3
        },
        "basic_config": {
            "image_tag": "0.2.0",
            "alauda_lb_name": "haproxy-40-125-200-37",
            "master_size": "S", "slaver_size": "XS"
        },
        "name": "tauto",
        "region": "int_bigdata",
        "region_alb_version": "v2",
        "network_mode": ["flannel"],
        "namespace": "alauda"
    }


def test_get_update_bp_auto_json():
    return {
        "advanced_config": {
            "alauda_dfs_replication": 3,
            "is_fixed": "false",
            "enable_kafka": "kafka_enable",
            "enable_es": "es_enable",
            "data_dir": "/my_data/batchprocessing",
            "num_of_hadoop": 3,
            "num_of_hbase": 3,
            "num_of_es": 1,
            "num_of_kafka": 2
        },
        "basic_config": {
            "image_tag": "0.2.0",
            "alauda_lb_name": "haproxy-40-125-200-37",
            "cluster_size": "XXS",
        },
        "name": "tauto",
        "region": "int_bigdata",
        "region_alb_version": "v2",
        "network_mode": ["flannel"],
        "namespace": "alauda"
    }


def test_get_bp_fixed_json():
    return {"advanced_config": {"alauda_dfs_replication": 3,
                                "is_fixed": "true",
                                "hadoop_mount_points": [{"path": "/etc/hadoop/core-site.xml",
                                                         "type": "config",
                                                         "value": {"name": "bigdata-xufei",
                                                                   "key": "hdfs_site_xml"}}],
                                "hbase_mount_points": [{"path": "/etc/hbase/hbase-site.xml",
                                                        "type": "config",
                                                        "value": {"name": "bigdata-xufei",
                                                                  "key": "hbase_site_xml"}}],
                                "enable_kafka": "kafka_enable",
                                "enable_es": "es_enable",
                                "num_of_es": 3,
                                "num_of_kafka": 2,
                                "ip_tag": ["192.168.0.5",
                                           "192.168.0.6"],
                                "data_dir": "/alauda_data/emr/batchprocessing",
                                "master_addr": "192.168.0.5",
                                "alauda_secondary_namenode_addr": "192.168.0.6"},
            "basic_config": {"image_tag": "0.2.0",
                             "alauda_lb_name": "abac",
                             "master_size": "S",
                             "slaver_size": "XS"},
            "name": "bptest2",
            "region": "int_bigdata",
            "region_alb_version": "v2",
            "network_mode": ["flannel"],
            "namespace": "alauda"}


def test_get_update_bp_fixed_json():
    return {
        "advanced_config": {
            "alauda_dfs_replication": 3,
            "is_fixed": "true",
            "enable_kafka": "kafka_enable",
            "enable_es": "es_enable",
            "data_dir": "/my_data/batchprocessing",
            "master_addr": "192.168.0.5",
            "alauda_secondary_namenode_addr": "192.168.0.6",
            "num_of_es": 3,
            "num_of_kafka": 4,
            "ip_tag": [
                "192.168.0.5",
                "192.168.0.6",
                "192.168.0.7",
                "192.168.0.8",
                "192.168.0.9"]},
        "basic_config": {
            "image_tag": "0.2.0",
            "alauda_lb_name": "haproxy-40-125-200-37",
            "cluster_size": "XXS",
        },
        "name": "tfixed",
        "region": "int_bigdata",
        "region_alb_version": "v2",
        "network_mode": ["flannel"],
        "namespace": "alauda"}


def test_get_ha_hdp_fixed_json():
    return {
        "advanced_config": {
            "hdfs_ha_name_service": "mycluster17",
            "alauda_dfs_replication": 3,
            "is_fixed": "true",
            "zk_dep_type": "zk_enable",
            "ip_tag": ["10.1.0.5", "10.1.0.6", "10.1.0.7", "10.1.0.8"],
            "data_dir": "/alauda_data/emr/hadoop",
            "master_addr": "10.1.0.5",
            "alauda_secondary_namenode_addr": "10.1.0.6",
            "master3_addr": "10.1.0.7",
            "zookeeper_quorum": "10.1.0.5,10.1.0.6,10.1.0.7",
            "client_port": 2145
        },
        "basic_config": {
            "image_tag": "2.7.4",
            "alauda_lb_name": "haproxy-13-75-43-75",
            "master_size": {
                "size": "CUSTOMIZED",
                "cpu": 0.5,
                "mem": 1024
            },
            "slaver_size": {
                "size": "CUSTOMIZED",
                "cpu": 0.5,
                "mem": 1024
            }
        },
        "space_name": "global",
        "name": "hdp17",
        "region": "fang",
        "region_alb_version": "v2",
        "network_mode": ["flannel"],
        "namespace": "alauda"
    }


def test_get_update_ha_hdp_fixed_json():
    return {
        "advanced_config": {
            "alauda_dfs_replication": 3,
            "is_fixed": "true",
            "data_dir": "/my_data/batchprocessing",
            "master_addr": "192.168.0.5",
            "alauda_secondary_namenode_addr": "192.168.0.6",
            "master3_addr": "192.168.0.7",
            "ip_tag": [
                "192.168.0.5",
                "192.168.0.6",
                "192.168.0.7",
                "192.168.0.8",
                "192.168.0.9"],
            "hdfs_ha_name_service": "mycluster",
        },
        "basic_config": {
            "image_tag": "2.7.4",
            "alauda_lb_name": "haproxy-40-125-200-37",
            "cluster_size": "XXS",
        },
        "name": "ha-hadoop-fix",
        "region": "int_bigdata",
        "region_alb_version": "v2",
        "network_mode": ["flannel"],
        "namespace": "alauda"}


def test_get_ha_hdp_auto_json():
    return {
        "advanced_config": {
            "alauda_dfs_replication": 3,
            "hdfs_ha_name_service": "mycluster",
            "is_fixed": "false",
            "data_dir": "/my_data/batchprocessing,/my_data/batchprocessing1",
            "num_of_hadoop": 3,

        },
        "basic_config": {
            "image_tag": "2.7.4",
            "alauda_lb_name": "haproxy-40-125-200-37",
            "master_size": "S", "slaver_size": "XS"
        },
        "name": "ha_hdp_auto",
        "region": "int_bigdata",
        "region_alb_version": "v2",
        "network_mode": ["flannel"],
        "namespace": "alauda"
    }


def test_get_update_ha_hdp_auto_json():
    return {
        "advanced_config": {
            "alauda_dfs_replication": 3,
            "hdfs_ha_name_service": "mycluster",
            "is_fixed": "false",
            "data_dir": "/my_data/batchprocessing",
            "num_of_hadoop": 3,

        },
        "basic_config": {
            "image_tag": "2.7.4",
            "alauda_lb_name": "haproxy-40-125-200-37",
            "cluster_size": "XXS",
        },
        "name": "ha_hdp_auto",
        "region": "int_bigdata",
        "region_alb_version": "v2",
        "network_mode": ["flannel"],
        "namespace": "alauda"
    }


def hadoop_facade_test(n=1):
    if n == 1:
        json = test_get_hadoop_fixed_json()
        upd_json = test_get_update_hadoop_fixed_json()
    else:
        json = test_get_hadoop_auto_json()
        upd_json = test_get_update_hadoop_auto_json()

    ins = HadoopFacade(json, None)
    print 'hadoop::get_compose_data\n{}'.format(ins.get_compose_data())
    print 'hadoop::update_compose_data\n{}'.format(ins.get_updated_yaml_content(json, upd_json))
    print 'hadoop::get_app_detail\n{}'.format(ins.get_app_detail(''))


def hbase_facade_test(n=1):
    if n == 1:
        json = test_get_hbase_fixed_json()
        upd_json = test_get_update_hbase_fixed_json()
    else:
        json = test_get_hbase_auto_json()
        upd_json = test_get_update_hbase_auto_json()

    ins = HBaseFacade(json, None)
    print 'hbase::get_compose_data\n{}'.format(ins.get_compose_data())
    print 'hbase::update_compose_data\n{}'.format(ins.get_updated_yaml_content(json, upd_json))
    print 'hbase::get_app_detail\n{}'.format(
        ins.get_app_detail(''))


def batchprocess_facade_test(n=1):
    if n == 1:
        json = test_get_bp_fixed_json()
        upd_json = test_get_update_bp_fixed_json()
    else:
        json = test_get_bp_auto_json()
        upd_json = test_get_update_bp_auto_json()

    ins = BatchProcessFacade(json, None)
    print 'batchprocess::get_compose_data\n{}'.format(ins.get_compose_data())
    print 'batchprocess::update_compose_data\n{}'.format(
        ins.get_updated_yaml_content(json, upd_json))

    print 'batchprocess::get_app_detail\n{}'.format(
        ins.get_app_detail(''))


def ha_hadoop_facade_test(n=1):
    if n == 1:
        json = test_get_ha_hdp_fixed_json()
        upd_json = test_get_update_bp_fixed_json()
    else:
        json = test_get_ha_hdp_auto_json()
        upd_json = test_get_update_ha_hdp_auto_json()

    ins = HadoopHAFacade(json, None)
    print 'hadoop_ha::get_compose_data\n{}'.format(ins.get_compose_data())
    print 'hadoop_ha::update_compose_data\n{}'.format(
        ins.get_updated_yaml_content(json, upd_json))

    tt = ins.get_app_detail('')
    for s in tt['service']:
        if s['role'] == 'backup_master' or s['role'] == 'other' \
                or 'zookeeper' in s['name'] or 'journal-node' in s['name']:
            s['role'] = 'master'
    print 'hadoop_ha::get_app_detail\n{}'.format(
        ins.get_app_detail(''))
    print 'hadoop_ha::get_app_detail\n{}'.format(
        tt)


if __name__ == '__main__':
    # hadoop_facade_test(1)
    # hbase_facade_test(0)
    # batchprocess_facade_test(0)
    ha_hadoop_facade_test(1)
