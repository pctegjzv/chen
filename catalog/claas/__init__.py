#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from batch_processing import BATCH_PROCESSING
from hbase import HBASE
from elasticsearch import ELASTICSEARCH
from elk import ELK
from gitlab import GITLAB
from hadoop_ha import HADOOPHA
from kafka import KAFKA
from redis import REDIS
from mongodb import MONGODB
from mysql import MYSQL
from spark import SPARK
from stream_processing import STREAM_PROCESSING
from zookeeper import ZOOKEEPER
from spring_eureka import SPRINGEUREKA
from spring_hystrix_dashboard import SPRINGHYSTRIXDASHBOARD
from spring_config_server import SPRINGCONFIGSERVER
from spring_zuul import SPRINGZUUL
from spring_turbine import SPRINGTURBINE
from spring_zipkin import SPRINGZIPKIN
__author__ = "junh@alauda.io"

app_name_detail_dict = {
    "batch_processing": BATCH_PROCESSING,
    "elasticsearch": ELASTICSEARCH,
    "elk": ELK,
    "gitlab": GITLAB,
    "hadoop": HADOOPHA,
    "hbase": HBASE,
    "kafka": KAFKA,
    "redis": REDIS,
    "mongodb": MONGODB,
    "mysql": MYSQL,
    "spark": SPARK,
    "stream_processing": STREAM_PROCESSING,
    "zookeeper": ZOOKEEPER,
    "spring_eureka": SPRINGEUREKA,
    "spring_hystrix_dashboard": SPRINGHYSTRIXDASHBOARD,
    "spring_config_server": SPRINGCONFIGSERVER,
    'spring_turbine': SPRINGTURBINE,
    'spring_zuul': SPRINGZUUL,
    'spring_zipkin': SPRINGZIPKIN
}

need_config_server_list = [
    'spring_turbine', 'spring_zuul', 'spring_zipkin'
]

ADVANCED_CONFIG = 'advanced_config'

APP_STATUS = {
    "batch_processing": "ready",
    "cassandra": "developing",
    "elasticsearch": "ready",
    "elk": "developing",
    "gitlab": "developing",
    "hadoop": "ready",
    "hbase": "developing",
    "kafka": "ready",
    "mysql": "ready",
    "redis": "ready",
    "mongodb": "ready",
    "spark": "developing",
    "stream_processing": "developing",
    "wordpress": "developing",
    "zookeeper": "ready",
    "spring_turbine": "ready",
    "spring_zipkin": "ready",
    "spring_config_server": "ready",
    "spring_eureka": "ready",
    "spring_zuul": "ready",
    "spring_hystrix_dashboard": "ready"
}

SUPPORTED_APPS = APP_STATUS.keys()

ALL_APPS = {
    "result": [
        {
            "app_name": "cassandra",
            "display_name": "Apache Cassandra",
            "display_info": {
                "en": "Distributed database MS",
                "zh": "开源分布式NoSQL数据库系统"
            },
            "app_num": 0,
            "scalable": 0,
            "type": ["database"],
            "status": APP_STATUS["cassandra"]
        },
        {
            "app_name": "hadoop",
            "display_name": "Apache Hadoop",
            "display_info": {
                "en": "Hadoop HA cluster",
                "zh": "Hadoop HA 集群"
            },
            "app_num": 0,
            "scalable": 1,
            "type": ["Clustering"],
            "status": APP_STATUS["hadoop"]
        },
        {
            "app_name": "hbase",
            "display_name": "Apache HBase",
            "display_info": {
                "en": "Hadoop database",
                "zh": "Hadoop database"
            },
            "app_num": 0,
            "scalable": 0,
            "type": ["database"],
            "status": APP_STATUS["hbase"]
        },
        {
            "app_name": "kafka",
            "display_name": "Apache Kafka",
            "display_info": {
                "en": "A distributed streaming platform",
                "zh": "一个分布式消息平台"
            },
            "app_num": 0,
            "scalable": 0,
            "type": ["Clustering"],
            "status": APP_STATUS["kafka"],
        },
        {
            "app_name": "spark",
            "display_name": "Apache Spark",
            "display_info": {
                "en": "Lightning-fast cluster computing",
                "zh": "快速大数据分析"
            },
            "app_num": 0,
            "scalable": 0,
            "type": ["Clustering"],
            "status": APP_STATUS["spark"]
        },
        {
            "app_name": "zookeeper",
            "display_name": "Apache ZooKeeper",
            "display_info": {
                "en": "ZooKeeper cluster",
                "zh": "ZooKeeper 集群"
            },
            "app_num": 0,
            "scalable": 0,
            "type": ["Clustering"],
            "status": APP_STATUS["zookeeper"]
        },
        {
            "app_name": "batch_processing",
            "display_name": "Batch Processing",
            "display_info": {
                "en": "Hadoop, HBase, Kafka, Elasticsearch",
                "zh": "Hadoop, HBase, Kafka, Elasticsearch"
            },
            "app_num": 0,
            "scalable": 1,
            "type": ["Big Data", "Solution"],
            "status": APP_STATUS["batch_processing"]
        },
        {
            "app_name": "elasticsearch",
            "display_name": "Elasticsearch",
            "display_info": {
                "en": "Elasticsearch cluster",
                "zh": "Elasticsearch 集群"
            },
            "app_num": 0,
            "scalable": 0,
            "type": ["Data", "App Stack"],
            "status": APP_STATUS["elasticsearch"]
        },
        {
            "app_name": "elk",
            "display_name": "ELK Stack",
            "display_info": {
                "en": "Elasticsearch, Logstash, Kibana",
                "zh": "Elasticsearch, Logstash, Kibana"
            },
            "app_num": 0,
            "scalable": 0,
            "type": ["App Stack"],
            "status": APP_STATUS["elk"]
        },
        {
            "app_name": "gitlab",
            "display_name": "GitLab CE",
            "display_info": {
                "en": "Version control on your server",
                "zh": "实现自托管的Git项目仓库"
            },
            "app_num": 0,
            "scalable": 0,
            "type": ["Community"],
            "status": APP_STATUS["gitlab"]
        },
        {
            "app_name": "mysql",
            "display_name": "MySQL",
            "display_info": {
                "en": "MySQL cluster",
                "zh": "MySQL 集群"
            },
            "app_num": 0,
            "scalable": 0,
            "type": ["Database", "Clustering"],
            "status": APP_STATUS["mysql"]
        },
        {
            "app_name": "redis",
            "display_name": "Redis",
            "display_info": {
                "en": "Redis cluster",
                "zh": "Redis 集群"
            },
            "app_num": 0,
            "scalable": 0,
            "type": ["Clustering"],
            "status": APP_STATUS["redis"]
        },
        {
            "app_name": "mongodb",
            "display_name": "Mongodb",
            "avatar": "https://s3.cn-north-1.amazonaws.com.cn/homepage/"
                      "catalog/Mongodb.png",
            "display_info": {
                "en": "Mongodb cluster",
                "zh": "Mongodb 集群"
            },
            "app_num": 0,
            "scalable": 0,
            "type": ["Clustering"],
            "status": APP_STATUS["mongodb"]
        },
        {
            "app_name": "stream_processing",
            "display_name": "Stream Processing",
            "display_info": {
                "en": "Zookeeper, Kafka, Storm",
                "zh": "Zookeeper, Kafka, Storm"
            },
            "app_num": 0,
            "scalable": 0,
            "type": ["Big data", "Solution"],
            "status": APP_STATUS["stream_processing"]
        },
        {
            "app_name": "wordpress",
            "display_name": "WordPress",
            "display_info": {
                "en": "A blogging tool and a CMS ",
                "zh": "一个博客软件和内容管理系统"
            },
            "app_num": 0,
            "scalable": 0,
            "type": ["blog"],
            "status": APP_STATUS["wordpress"]
        },
        {
            "app_name": "spring_eureka",
            "display_name": "Eureka",
            "avatar": "https://s3.cn-north-1.amazonaws.com.cn/homepage/"
                      "catalog/Eureka.png",
            "display_info": {
                "en": "Service Discovery in a Microservices Architecture",
                "zh": "云端服务发现"
            },
            "app_num": 0,
            "scalable": 0,
            "type": ["micro_service"],
            "status": APP_STATUS["spring_eureka"]
        },
        {
            "app_name": "spring_config_server",
            "display_name": "Config Server",
            "avatar": "https://s3.cn-north-1.amazonaws.com.cn/homepage/"
                      "catalog/Config+Server.png",
            "display_info": {
                "en": "Config Server",
                "zh": "配置管理工具包"
            },
            "app_num": 0,
            "scalable": 0,
            "type": ["micro_service"],
            "status": APP_STATUS["spring_config_server"]
        },
        {
            "app_name": "spring_zuul",
            "display_name": "Zuul",
            "avatar": "https://s3.cn-north-1.amazonaws.com.cn/homepage/"
                      "catalog/Zuul.png",
            "display_info": {
                "en": "A gateway service provides dynamic routing, "
                      "security, and more",
                "zh": "动态路由、监控等网管服务"
            },
            "app_num": 0,
            "scalable": 0,
            "type": ["micro_service"],
            "status": APP_STATUS["spring_zuul"]
        },
        {
            "app_name": "spring_hystrix_dashboard",
            "display_name": "Hystrix Dashboard",
            "avatar": "https://s3.cn-north-1.amazonaws.com.cn/homepage/"
                      "catalog/Hystrix+Dashboard.png",
            "display_info": {
                "en": "Hystrix Dashboard",
                "zh": "Hystrix 控制面板"
            },
            "app_num": 0,
            "scalable": 0,
            "type": ["micro_service"],
            "status": APP_STATUS["spring_hystrix_dashboard"]
        },
        {
            "app_name": "spring_zipkin",
            "display_name": "Zipkin",
            "avatar": "https://s3.cn-north-1.amazonaws.com.cn/homepage/"
                      "catalog/Zipkin.png",
            "display_info": {
                "en": "A distributed tracing system",
                "zh": "分布式跟踪系统"
            },
            "app_num": 0,
            "scalable": 0,
            "type": ["micro_service"],
            "status": APP_STATUS["spring_zipkin"]
        },
        {
            "app_name": "spring_turbine",
            "display_name": "Turbine",
            "avatar": "https://s3.cn-north-1.amazonaws.com.cn/homepage/"
                      "catalog/Turbine.png",
            "display_info": {
                "en": "SSE Stream Aggregator",
                "zh": "聚合服务器发送事件流数据"
            },
            "app_num": 0,
            "scalable": 0,
            "type": ["micro_service"],
            "status": APP_STATUS["spring_turbine"]
        }
    ]
}

LIST_APPS_NUM = {
    "result": [{"app_name": item, "app_num": 0} for item in SUPPORTED_APPS]}
