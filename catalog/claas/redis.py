#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import absolute_import

import logging
import re
import base64

from jinja2 import Template
from catalog.claas.base_factory import AbstractFactory
from catalog.exceptions import CatalogException

logger = logging.getLogger(__name__)
__author__ = "junh@alauda.io"


REDIS = {
    "name": "redis",
    "info": {
        "en": "Set up a redis application.",
        "zh": "部署Redis应用"
    },
    "instruction": {
        "en": "Enter the number of nodes, instance size for the Redis application. \
                Support standalone mode, replication mode and cluster mode（At least 6 nodes）",  # noqa
        "zh": "可以输入节点数，实例大小。支持单点模式、复制模式和集群模式（至少6个节点）"
    },
    "basic_config": [{
        "type": "option",
        "attribute_name": "image_tag",
        "display_name": {
            "en": "Redis version",
            "zh": "Redis版本"
        },
        "description": {
            "en": "",
            "zh": ""
        },
        "key": "version",
        "value": "version",
        "option": [{"version": "3.2.10"}, {"version": "3.2.9"},
                   {"version": "3.0.5"}, {"version": "3.0.1"}]
    }, {
        "type": "radio_group_tab",
        "attribute_name": "mode",
        "display_name": {
            "en": "Operating mode",
            "zh": "运行模式"
        },
        "description": {
            "en": "You can choose standalone、replication or cluster mode",
            "zh": "你可以选择单点、复制或集群模式"
        },
        "option": [
            "standalone", "replication", "cluster"
        ]
    },
    {
        "type": "multi_option",
        "attribute_name": "ip_tag",
        "display_name": {
            "en": "Node tag",
            "zh": "主机标签"
        },
        "description": {
            "en": "Deploy to specific host",
            "zh": "定点部署",
        },
        "option": {
        }
    }, {
        "type": "option",
        "attribute_name": "cluster_size",
        "display_name": {
            "en": "Instance size",
            "zh": "实例大小"
        },
        "description": {
            "en": "Size of instances",
            "zh": "实例大小"
        },
        "min_value": {
            "mem": 512,
            "cpu": 0.25
        }
    }, {
        "type": "string",
        "attribute_name": "mount_data_dir",
        "pattern": "/^\/[\w\d\-_.\/]+$/",
        "pattern_hint": {
            "en": "The data directory format is invalid, \
                    it can only contain characters like slashes, numbers, letters, \
                    hyphens(-), underscores(_) and dots(.), and it must be absolute path",
            "zh": "路径格式错误，只能含有斜杠、数字、字母、中线、下划线和点号，且需要为绝对路径。"
        },
        "display_name": {
            "en": "Data directory",
            "zh": "主机数据存储路径"
        },
        "description": {
            "en": "The data directory of redis in the host. \
                    The ultimate path will append the application name to avoid duplication.",
            "zh": "Redis数据在主机上的存储路径，程序会自动将应用名称作为目录名添加到该路径后面，以避免路径重复。"
        }
    }],
    "advanced_config": [{
        "type": "int",
        "attribute_name": "port",
        "display_name": {
            "en": "Redis Port",
            "zh": "Redis端口"
        },
        "description": {
            "en": "The client port of redis. \
                    You should modify the default value for security reasons.",
            "zh": "客户端连接Redis的端口，为安全起见建议修改默认端口。"
        },
        "default_value": 6379,
        "max_value": 55534,
        "min_value": 1025
    }, {
        "type": "string",
        "attribute_name": "password",
        "pattern": "/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])[!#%&(-_a-~]{8,32}$/",
        "pattern_hint": {
            "en": "The password must be printable ASCII characters, \
                    and it's length must at least 8 characters and at most 32 characters, \
                    and can not contain space/dollar/order quotes/single quote/double quote, \
                    and must contain numbers, upper and lowercase letters",
            "zh": "密码长度需要在8位至32位，只能为可打印的ASCII字符，\
                            不能含有空格、美元符、开单引号、单引号或双引号，且需要包含大小写字母和数字。"
        },
        "required": False,
        "display_name": {
            "en": "Redis Password",
            "zh": "Redis密码"
        },
        "description": {
            "en": "The password of redis. You should set it for security reasons. \
                    If you set, it must be at least 8 characters and at most 32 characters, \
                    and may contain numbers, upper and lowercase letters, \
                    but can not contain single quote or double quote.",
            "zh": "客户端连接Redis的密码，为安全起见建议设置较复杂密码。\
                    如若设置，密码长度需要在8位至32位，包含大小写字母和数字。且不能含有单引号或双引号。"
        }
    }],
    "standalone_config": [

    ],
    "replication_config": [{
        "type": "single_ip_tag",
        "attribute_name": "master_node_addr",
        "display_name": {
            "en": "Master node address",
            "zh": "Master 节点地址"
        },
        "description": {
            "en": "Set the IP of master server.",
            "zh": "设置主服务器的 ip 地址"
        }
    }, {
        "type": "int",
        "attribute_name": "replicas_per_shard",
        "display_name": {
            "en": "Slave nodes number",
            "zh": "从节点数"
        },
        "description": {
            "en": "Set the number of slave nodes.",
            "zh": "设置从节点的个数"
        },
        "min_value": 1
    }],
    "cluster_config": [{
        "type": "int",
        "attribute_name": "num_of_shards",
        "display_name": {
            "en": "Number of shards",
            "zh": "分区数"
        },
        "description": {
            "en": "Set the shards number of your redis cluster.",
            "zh": "设置集群的分区数目"
        },
        "min_value": 3
    }, {
        "type": "int",
        "attribute_name": "replicas_per_shard",
        "display_name": {
            "en": "Replicas per shard",
            "zh": "从节点数"
        },
        "description": {
            "en": "Set the number of slave nodes per shards.",
            "zh": "设置每个分区从节点的个数"
        },
        "min_value": 1
    }]
}

MODE_STANDALONE = 'standalone'
MODE_REPLICATION = 'replication'
MODE_CLUSTER = 'cluster'

MODE_DICT = {
    MODE_STANDALONE: '单点模式',
    MODE_REPLICATION: '复制模式',
    MODE_CLUSTER: '集群模式',
}

# unit: bytes
SIZE_DEFINE = {
    'XXS': 256*1024*1024,
    'XS': 512*1024*1024,
    'S': 1024*1024*1024,
    'M': 2*1024*1024*1024,
    'L': 4*1024*1024*1024,
    'XL': 8*1024*1024*1024
}


class Redis(AbstractFactory):

    def __init__(self, raw_data, namespace):
        super(Redis, self).__init__(raw_data)
        self.standalone_meta_compose_path = self.base_template_path + '/standalone/meta_compose.yml'
        self.standalone_meta_container_path = self.base_template_path + \
            '/standalone/meta_container.yml'
        self.replica_meta_compose_path = self.base_template_path + '/replica/meta_compose.yml'
        self.replica_meta_container_path = self.base_template_path + '/replica/meta_container.yml'
        self.cluster_meta_compose_path = self.base_template_path + '/cluster/meta_compose.yml'
        self.cluster_meta_container_path = self.base_template_path + '/cluster/meta_container.yml'
        self.namespace = namespace

    def get_compose_data(self):
        logger.debug('Testing, namespace????{}'.format(self.namespace))
        basic_config_dict = self.raw_data['basic_config']
        advanced_config_dict = self.raw_data['advanced_config']

        # basic config dict
        image_tag = basic_config_dict['image_tag']
        ip_tag = basic_config_dict['ip_tag']
        all_nodes_num = len(ip_tag)

        # deal size
        cluster_size = basic_config_dict['cluster_size']
        # cluster_size is always dict
        size = str(cluster_size.get('size'))
        mem = cluster_size.get('mem')
        cpu_quota = 'cpu_quota: ' + str(cluster_size.get('cpu'))
        mem_limit = 'mem_limit: ' + str(mem)
        mem = mem*1024*1024

        # set redis max memory to 75%
        redis_max_memory = int(0.75 * mem)

        # mount data directory
        mount_data_dir = basic_config_dict['mount_data_dir']
        if not mount_data_dir:
            raise CatalogException('catalog_mount_data_dir_cant_be_empty')
        else:
            # check the path
            if not re.search(r"^/[\w\d\-_./]+$", mount_data_dir):
                raise CatalogException('catalog_mount_data_dir_format_invalid')
            mount_data_dir = mount_data_dir + '/' + self.raw_data['name']

        mode = basic_config_dict['mode']
        if mode == MODE_STANDALONE:
            replicas_num = 0
        else:
            replicas_num = int(basic_config_dict['replicas_per_shard'])
            # replicas number can not be empty
            if replicas_num < 1:
                raise CatalogException('catalog_replicas_per_shard_cant_be_empty')

        port = int(advanced_config_dict['port'])
        # check whether or not the redis port is correct
        if not port:
            port = 6379
        else:
            self.check_port(port, 'port of redis', 1024, 55535)

        password = password_origin = ''
        if 'password' in advanced_config_dict.keys():
            password = password_origin = advanced_config_dict['password']
            # check whether or not the redis password is strong
            if len(password) < 8:
                raise CatalogException('redis_password_too_short')
            if len(password) > 32:
                raise CatalogException('redis_password_too_long')
            if password.lower() == password \
                    or password.upper() == password \
                    or not re.search(r"\d", password):
                raise CatalogException('catalog_redis_password_too_weak')
            # must be ASCII characters and can not contain ' or "
            if not re.search(r"^[\x21\x23-\x26\x28-\x7E]+$", password):
                raise CatalogException('redis_password_invalid')

            # encode the password
            password = base64.b64encode(password)

        if not password_origin:
            monitor_password_statement = ''
        else:
            monitor_password_statement = 'ALAUDA_MONITOR_SERVICE__REDIS__1__PASSWORD: ' + \
                                         password_origin

        meta_compose_path = meta_container_path = ''
        compose_args = container_args = {
            'image_index': self.image_index,
            'image_tag': image_tag,
            'size': size,
            'cpu_quota': cpu_quota,
            'mem_limit': mem_limit,
            'maxmemory': redis_max_memory,
            'port': port,
            'password': password,
            'password_origin': password_origin,
            'monitor_password_statement': monitor_password_statement,
            'mount_data_dir': mount_data_dir
        }
        if mode == MODE_REPLICATION:
            # check whether or not all nodes number is correct
            if all_nodes_num != 1+replicas_num:
                raise CatalogException('catalog_invalid_node_ip_tag')

            master_node_addr = basic_config_dict['master_node_addr']
            # master node ip should be in the ip tag list
            if master_node_addr not in ip_tag:
                raise CatalogException('catalog_invalid_master_node_addr')

            # remove master node
            ip_tag.remove(master_node_addr)
            all_nodes_num -= 1

            container_args.update({
                'node_name': 'redis-slave',
                'redis_slave_of': master_node_addr + ' ' + str(port)
            })
            compose_args.update({
                'master_node_addr': master_node_addr,
                'master_node_ip': master_node_addr
            })
            meta_compose_path = self.replica_meta_compose_path
            meta_container_path = self.replica_meta_container_path

        elif mode == MODE_CLUSTER:
            # the shards num should not be less than 3
            shards_num = basic_config_dict['num_of_shards']
            if shards_num < 3:
                raise CatalogException('catalog_invalid_shards_num')

            # check whether or not all nodes number is correct
            if all_nodes_num != shards_num*(1+replicas_num) or all_nodes_num < 6:
                raise CatalogException('catalog_invalid_node_ip_tag')

            trib_parameter = ' '.join([item + (':%s' % port) for item in ip_tag])

            container_args.update({
                'node_name': 'redis-cluster',
                'trib_parameter': trib_parameter,
                'replicas_num': replicas_num
            })
            meta_compose_path = self.cluster_meta_compose_path
            meta_container_path = self.cluster_meta_container_path

        elif mode == MODE_STANDALONE:
            # all nodes number should be 1
            if all_nodes_num != 1:
                raise CatalogException('catalog_invalid_node_ip_tag')

            container_args.update({
                'node_name': 'redis'
            })
            meta_compose_path = self.standalone_meta_compose_path
            meta_container_path = self.standalone_meta_container_path

        else:
            raise CatalogException('catalog_invalid_config')

        # render the yml file
        meta_container_content = self.get_config_content(meta_container_path)
        template = Template(meta_container_content)

        compose_content = ''
        node_name = container_args['node_name']
        for i in range(all_nodes_num):
            container_args['node_name'] = node_name + str(i + 1)
            container_args['node_ip'] = ip_tag[i]
            container_args['node_tag'] = 'ip:' + ip_tag[i]

            container_content = template.render(container_args)
            compose_content += container_content

        meta_compose_content = self.get_config_content(meta_compose_path)
        template = Template(meta_compose_content)

        compose_args['containers'] = compose_content
        compose_content = template.render(compose_args)

        logger.info('{}, mode:{}, got compose_content: \n{}'.format(
            self.__class__.__name__, mode, compose_content))

        return compose_content

    def _format_app_detail(self, data):
        detail_template = super(Redis, self)._format_app_detail(data)

        # add basic info
        mode = self.config_json.get_mode()
        detail_template.add_basic_info('运行模式', 'Operating mode', MODE_DICT.get(mode))
        detail_template.add_basic_info('Redis版本', 'Redis version',
                                       self.config_json.get_image_tag())
        detail_template.add_basic_info('Redis端口', 'Redis Port', self.config_json.get_port())

        if mode == MODE_CLUSTER:
            detail_template.add_basic_info('分区数', 'Number of shards',
                                           self.config_json.get_num_of_shards())

        replicas_en = ''
        if mode == MODE_CLUSTER:
            replicas_en = 'Replicas per shard'
        elif mode == MODE_REPLICATION:
            replicas_en = 'Slave nodes number'
        if replicas_en:
            detail_template.add_basic_info('从节点数', replicas_en,
                                           self.config_json.get_replicas_per_shard())

        mount_data_dir = self.config_json.get_mount_data_dir() + '/' + \
            self.config_json.get_app_name()
        detail_template.add_basic_info('主机数据存储路径', 'Data directory', mount_data_dir)

        if mode == MODE_REPLICATION:
            node_list = [self.config_json.get_master_node_addr(), ]
        else:
            node_list = []
        node_list.extend(self.config_json.get_ip_tag())
        node_list = ', '.join(node_list)
        detail_template.add_basic_info('主机列表', 'Node list', node_list)

        # add metrics
        detail_template.set_metrics(self.get_metrics())

        return detail_template

    def __repr__(self):
        out = 'Redis'
        return out
