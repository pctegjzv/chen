#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import absolute_import


import logging
from jinja2 import Template
from catalog.claas.base_factory import AbstractFactory
from catalog.exceptions import CatalogException


logger = logging.getLogger(__name__)
__author__ = "junh@alauda.io"


STREAM_PROCESSING = {
    "name": "Stream Processing",
    "info": {
        "en": "Zookeeper, Kafka, Storm",
        "zh": "Zookeeper, Kafka, Storm"
    },
    "instruction": {
        "en": "### Zookeeper \n\
Enter the number of nodes, instance size, etc. for the ZooKeeper cluster.  \n\
Change the following ZooKeeper default parameters, if you needed: \n\
\n\
- Tick time: The number of milliseconds of each tick (default value: 2000)  \n\
- Init limit: The number of ticks that the initial synchronization phase can take (default value: 10)  \n\
- Sync limit: The number of ticks that can pass between sending a request (default value: 2)\n\
\n\
The directory where ZooKeeper data is stored: `/zookeeper_data` \n\
\n And getting an acknowledgement. \n\
Click deploy. done.  \n\
Note: Scaling the cluster are not supported, yet.  \n\
Please checkout [zookeeper document](https://zookeeper.apache.org/doc/r3.4.9/) for more information.\n\
### Kafka \n\
Use the built-in zookeeper  \n\
Default ports used by Kafka: \n\
* ports for kafka server: 9092 \n\
\n\
### Storm \n\
Default ports used by Storm: \n\
\n\
* Storm REST API: 8080 (nimbus node)\n\
\n\
Please checkout [Storm REST API document](http://storm.apache.org/releases/0.10.0/STORM-UI-REST-API.html) for more information \n\
",   # noqa
        "zh": "### Zookeeper \n\
配置 ZooKeeper 集群的节点数，实例大小等等    \n\
如果需要的话，可以修改 ZooKeeper 的默认参数：  \n\
\n\
* Tick time: 每个 tick 的毫秒数（默认值：2000） \n\
* Init limit: 首次同步时可以用的 tick 数（默认值：10）  \n\
* Sync limit: 发送请求与收到回应之间可用的最大 tick 数（默认值：2） \n\
\n\
存放 ZooKeeper 数据的目录：`/zookeeper_data` \n\
\n 点击创建即可。   \n\
注意：暂时不支持更新操作。  \n\
请查阅 [zookeeper 文档](https://zookeeper.apache.org/doc/r3.4.9/) 获得更多信息 \n\
### Kafka \n\
使用一同部署的 zookeeper 集群\n\
\n\
Kafka 使用的默认端口：\n\
\n\
* kafka 服务端口: 9092 \n\
\n\
### Storm \n\
Storm 使用的默认端口： \n\
* Storm REST API: 8080 (nimbus 节点)\n\
\n\
请查阅 [Storm REST API 文档](http://storm.apache.org/releases/0.10.0/STORM-UI-REST-API.html) 获得更多信息 \n\
"   # noqa
    },
    "basic_config": [{
        "type": "int",
        "attribute_name": "num_of_nodes",
        "display_name": {
            "en": "Number of nodes",
            "zh": "集群节点数"
        },
        "description": {
            "en": "Desired cluster size. 3, 5, or 7 are good choices. You will need the same amount of hosts to deploy.",   # noqa
            "zh": "实例数量，最好是填3，5，7这样的奇数。当然你需要有这么多的主机才能成功部署"
        },
        "pattern": "odd"
    }, {
        "type": "option",
        "attribute_name": "cluster_size",
        "display_name": {
            "en": "Instance size",
            "zh": "实例大小"

        },
        "description": {
            "en": "Size of instances",
            "zh": "实例大小"
        },
        "option": [
            "XXS", "XS", "S", "M", "L", "XL"
        ]
    }, {
          "type": "multi_option",
          "attribute_name": "ip_tag",
          "display_name": {
              "en": "Node tag",
              "zh": "主机标签"
          },
          "description": {
              "en": "Deploy to specific host",
              "zh": "定点部署",
          },
          "option": [],
      }, {
          "type": "option",
          "attribute_name": "image_tag",
          "display_name": {
              "en": "image tag",
              "zh": "镜像标签"
          },
          "description": {
              "en": "",
              "zh": ""
          },
          "key": "version",
          "value": "version",
          "option": [{"version": "3.4.6-2.11-0.10.1.0-0.10.0"}, {"version": "latest"}]
      }
    ],
    "advanced_config": [{
        "type": "int",
        "attribute_name": "tick_time",
        "display_name": {
            "en": "Tick time",
            "zh": "Tick time"
        },
        "description": {
            "en": "The number of milliseconds of each tick",
            "zh": "每一个 tick 的长度"
        },
        "default_value": 2000,
        "max_value": 3000,
        "min_value": 1000
    }, {
        "type": "int",
        "attribute_name": "init_limit",
        "display_name": {
            "en": "Init limit",
            "zh": "Init limit"
        },
        "description": {
            "en": "Amount of time, in ticks (see tickTime), to allow followers to connect and sync to a leader. Increased this value as needed, if the amount of data managed by ZooKeeper is large.",  # noqa
            "zh": "followers 初始同步 leader 时，可用的 tick 总数, 如果数据量较大，可以增加该值"
        },
        "default_value": 10,
        "max_value": 20,
        "min_value": 5
    }, {
        "type": "string",
        "attribute_name": "sync_limit",
        "display_name": {
            "en": "Sync limit",
            "zh": "Sync limit"
        },
        "description": {
            "en": "Amount of time, in ticks (see tickTime), to allow followers to sync with ZooKeeper. If followers fall too far behind a leader, they will be dropped.",  # noqa
            "zh": "followers 与 ZooKeeper 同步时，可用的 tick 总数，如果 followers 没在限制时间内同步，就会被放弃。"   # noqa
        },
        "default_value": 2
    }, {
        "type": "string",
        "attribute_name": "advertised_port",
        "display_name": {
            "en": "advertised.port",
            "zh": "advertised.port"
        },
        "description": {
            "en": "The port to publish to ZooKeeper for clients to use",
            "zh": "给 ZooKeeper 的客户端使用的端口"
        },
        "default_value": "9092"
    }, {
        "type": "string",
        "attribute_name": "heap_opts",
        "display_name": {
            "en": "heap.opts",
            "zh": "heap.opts"
        },
        "description": {
            "en": "set the Java option",
            "zh": "配置 java 相关选项"
        },
        "default_value": "-Xmx2G -Xms2G"
    }, {
        "type": "int",
        "attribute_name": "log_roll_hours",
        "display_name": {
            "en": "log.roll.hours",
            "zh": "log.roll.hours",
        },
        "description": {
            "en": "The maximum time before a new log segment is rolled out",
            "zh": "日志保留的最长时间"
        },
        "default_value": 24
    }, {
        "type": "string",
        "attribute_name": "log_retention_bytes",
        "display_name": {
            "en": "log.retention.bytes",
            "zh": "log.retention.bytes"
        },
        "description": {
            "en": "The maximum size of the log before deleting it",
            "zh": "日志数据存储的最大字节数",
        },
        "default_value": 100000000
    }, {
        "type": "int",
        "attribute_name": "log_retention_hours",
        "display_name": {
            "en": "log.retention.hours",
            "zh": "log.retention.hours",
        },
        "description": {
            "en": 'Controls the maximum time we will retain a log before we will discard old log segments to free up space if we are using the "delete" retention policy',  # noqa
            "zh": "默认采用 delete 策略，定期地清理数据，释放空间，该参数可以控制保存日志的最大时间",
        },
        "default_value": 168
    }, {
        "type": "int",
        "attribute_name": "max_request_size",
        "display_name": {
            "en": "max_request_size",
            "zh": "max_request_size",
        },
        "description": {
            "en": "The maximum size of a request in bytes",
            "zh": "请求的最大字节大小"
        },
        "default_value": 1048576
    }, {
        "type": "int",
        "attribute_name": "max_message_bytes",
        "display_name": {
            "en": "max.message.bytes",
            "zh": "max.message.bytes",
        },
        "description": {
            "en": "This is largest message size Kafka will allow to be appended.",
            "zh": "kafka 允许附加的最大包大小"
        },
        "default_value": 1000012
    }, {
        "type": "int",
        "attribute_name": "message_max_bytes",
        "display_name": {
            "en": "message.max.bytes",
            "zh": "message.max.bytes",
        },
        "description": {
            "en": "The maximum size of message that the server can receive",
            "zh": "服务器能接受的最大包的大小"
        },
        "default_value": 1000012
    }, {
        "type": "int",
        "attribute_name": "num_partitions",
        "display_name": {
            "en": "num.partitions",
            "zh": "num.partitions",
        },
        "description": {
            "en": "The number of log partitions per topic",
            "zh": "每个 topic 的分区数"
        },
        "default_value": 1
    }, {
        "type": "int",
        "attribute_name": "replica_fetch_max_bytes",
        "display_name": {
            "en": "replica.fetch.max.bytes",
            "zh": "replica.fetch.max.bytes",
        },
        "description": {
            "en": "The number of bytes of messages to attempt to fetch for each partition",
            "zh": "从每个分区提取消息的字节数"
        },
        "default_value": 1048576
    }, {
        "type": "int",
        "attribute_name": "replica_fetch_response_max_bytes",
        "display_name": {
            "en": "replica.fetch.response.max.bytes",
            "zh": "replica.fetch.response.max.bytes"
        },
        "description": {
            "en": "Maximum bytes expected for the entire fetch response",
            "zh": "获取整个响应的最大字节数"
        },
        "default_value": 10485760
    }, {
        "type": "int",
        "attribute_name": "socket_request_max_bytes",
        "display_name": {
            "en": "socket.request.max.bytes",
            "zh": "socket.request.max.bytes"
        },
        "description": {
            "en": "The maximum number of bytes in a socket request",
            "zh": "套接字请求的最大字节数"
        },
        "default_value": 104857600
    }]
}


class StreamProcessing(AbstractFactory):
    def __init__(self, raw_data, namespace):
        super(StreamProcessing, self).__init__(raw_data)
        self.meta_compose_path = self.base_template_path + '/meta_compose.yml'
        self.meta_container_path = self.base_template_path + '/meta_container.yml'

    def get_compose_data(self):
        basic_config_dict = self.raw_data['basic_config']
        advanced_config_dict = self.raw_data['advanced_config']

        node_count = basic_config_dict['num_of_nodes']
        ip_tag = basic_config_dict['ip_tag']
        cluster_size = basic_config_dict['cluster_size']
        image_tag = basic_config_dict['image_tag']

        if node_count != len(ip_tag):
            raise CatalogException('catalog_invalid_node_ip_tag')

        if node_count % 2 == 0:
            raise CatalogException('zookeeper_even_number')

        compose_content = str()
        meta_container_content = self.get_config_content(self.meta_container_path)
        template = Template(meta_container_content)

        ip_tag_servers = \
            ['server.' + str(idx + 1) + '=' + val + ':2888:3888' for idx, val in enumerate(ip_tag)]
        servers = ' '.join(ip_tag_servers)
        kafka_zoo_connect = ','.join([val + ':2281' for val in ip_tag])
        storm_zoo_connect = ','.join([val for val in ip_tag])

        kafka_port_init = 9092
        kafka_log_dir_init = '/kafka'
        storm_role_list = ["supervisor" if i != (node_count-1) else "nimbus"
                           for i in range(node_count)]

        for i in range(node_count-1):
            container_content = template.render(image_index=self.image_index,
                                                image_tag=image_tag,
                                                node_name='supervisor'+str(i+1),
                                                size=cluster_size,
                                                kafka_port=kafka_port_init+i,
                                                log_dirs=kafka_log_dir_init+str(i),
                                                broker_id=i+1,
                                                myid=i+1,
                                                servers=servers,
                                                node_tag=ip_tag[i],
                                                kafka_zoo_connect=kafka_zoo_connect,
                                                storm_zoo_connect=storm_zoo_connect,
                                                storm_role=storm_role_list[i],
                                                **advanced_config_dict)
            compose_content += container_content

        meta_compose_content = self.get_config_content(self.meta_compose_path)
        template = Template(meta_compose_content)
        compose_content = template.render(image_index=self.image_index,
                                          image_tag=image_tag,
                                          containers=compose_content,
                                          node_name='nimbus',
                                          size=cluster_size,
                                          kafka_port=kafka_port_init+i,
                                          log_dirs=kafka_log_dir_init+str(node_count-1),
                                          broker_id=node_count,
                                          myid=node_count,
                                          servers=servers,
                                          node_tag=ip_tag[node_count-1],
                                          kafka_zoo_connect=kafka_zoo_connect,
                                          storm_zoo_connect=storm_zoo_connect,
                                          storm_role=storm_role_list[node_count-1],
                                          **advanced_config_dict)

        logger.info("{} got compose_content: \n{}".format(
            self.__class__.__name__, compose_content))
        return compose_content

    def __repr(self):
        out = 'StreamProcessing'
        return out
