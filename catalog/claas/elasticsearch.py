#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import absolute_import

import logging
from jinja2 import Template
from catalog.claas.base_factory import AbstractFactory
from catalog.exceptions import CatalogException
from catalog.resources.load_balances import AlaudaLBFactory
from catalog.resources.port import PortHttp


logger = logging.getLogger(__name__)
__author__ = "junh@alauda.io"


ELASTICSEARCH = {
    "name": "elasticsearch",
    "info": {
        "en": "Creates a Elasticsearch cluster",
        "zh": "用来创建一个 Elasticsearch 集群"
    },
    "instruction": {
        "en": "### Elasticsearch \n\
** You need to increase the `vm.max_map_count` kernel setting on your Docker host, Please consult [Install Elasticsearch with Docker](https://www.elastic.co/guide/en/elasticsearch/reference/current/docker.html#docker-cli-run-prod-mode) for more information **\n\
\n\
default ports used by Elasticsearch: \n\
\n\
* 9200 Elasticsearch HTTP Service \n\
* 9300 native Elasticsearch transport protocol \n\
\n\
### Kibana \n\
\n\
default port used by Kibana: \n\
* 5601 Kibana UI  \n\
",  # noqa
        "zh": "### Elasticsearch \n\
** 请确保你已经修改了宿主机的内核参数 `vm.max_map_count`，请参考文档：[Install Elasticsearch with Docker](https://www.elastic.co/guide/en/elasticsearch/reference/current/docker.html#docker-cli-run-prod-mode) **\n\
\n\
elasticsearch 默认暴露的端口: \n\
\n\
* 9200 Elasticsearch HTTP 服务端口 \n\
* 9300 Elasticsearch 传输协议端口 \n\
\n\
### Kibana \n\
\n\
Kibana 默认使用的端口：\n\
* 5601 Kibana UI \n\
"  # noqa
    },
    "basic_config": [{
        "type": "int",
        "attribute_name": "num_of_nodes",
        "display_name": {
            "en": "Number of nodes",
            "zh": "集群节点数"
        },
        "description": {
            "en": "Desired cluster size",
            "zh": "所需的实例数量",
        }
    }, {
        "type": "cluster_size",
        "attribute_name": "elastic_search_size",
        "display_name": {
            "en": "Instance size",
            "zh": "实例大小",
        },
        "description": {
            "en": "Size of instances",
            "zh": "实例大小"
        },
        "min_value": {
            "mem": 1024,
            "cpu": 0.5
        }
    }, {
        "type": "multi_option",
        "attribute_name": "ip_tag",
        "display_name": {
            "en": "Node tag",
            "zh": "主机标签"
        },
        "description": {
            "en": "Deploy to specific host",
            "zh": "定点部署"
        },
        "option": [],
    }, {
        "type": "option",
        "attribute_name": "image_tag",
        "display_name": {
            "en": "image tag",
            "zh": "镜像标签"
        },
        "description": {
            "en": "",
            "zh": ""
        },
        "key": "version",
        "value": "version",
        "option": [{"version": "5.2"}, {"version": "latest"}]
    }, {
        "type": "option",
        "attribute_name": "alauda_lb_name",
        "display_name": {
            "en": "load balance",
            "zh": "负载均衡"
        },
        "description": {
            "en": "",
            "zh": ""
        }
    }]
}


class Elasticsearch(AbstractFactory):

    def __init__(self, raw_data, namespace):
        super(Elasticsearch, self).__init__(raw_data)
        self.meta_compose_path = self.base_template_path + '/meta_compose.yml'
        self.meta_container_path = self.base_template_path + '/meta_container.yml'

    def get_compose_data(self):
        region_alb_version = self.raw_data['region_alb_version']
        basic_config_dict = self.raw_data['basic_config']

        ip_tag = basic_config_dict['ip_tag']

        compose_content = str()
        meta_container_content = self.get_config_content(self.meta_container_path)
        template = Template(meta_container_content)
        es_cluster_ips = ','.join([val for val in ip_tag])

        if basic_config_dict['num_of_nodes'] != len(basic_config_dict['ip_tag']):
            raise CatalogException('catalog_invalid_node_ip_tag')

        # deal size
        elastic_search_size = basic_config_dict['elastic_search_size']
        cpu_quota = elastic_search_size['cpu']
        mem_limit = elastic_search_size['mem']

        for i in range(basic_config_dict['num_of_nodes']-1):
            container_content = template.render(image_index=self.image_index,
                                                image_tag=basic_config_dict['image_tag'],
                                                node_name='esnode'+str(i+2),
                                                cpu_quota=cpu_quota,
                                                mem_limit=mem_limit,
                                                es_cluster_ips=es_cluster_ips,
                                                node_tag=basic_config_dict['ip_tag'][i])
            compose_content += container_content

        alauda_lb_name = basic_config_dict.get('alauda_lb_name')

        alauda_lb_type, lb_instance = AlaudaLBFactory.get_alauda_lb_instance(
            region_alb_version, alauda_lb_name, self._get_kibana_external_ports())

        meta_compose_content = self.get_config_content(self.meta_compose_path)
        template = Template(meta_compose_content)
        compose_content = template.render(
            image_index=self.image_index,
            image_tag=basic_config_dict['image_tag'],
            cpu_quota=cpu_quota,
            mem_limit=mem_limit,
            alauda_lb_type=alauda_lb_type,
            alauda_external_ports=lb_instance.get_external_ports_str(),
            containers=compose_content,
            es_cluster_ips=es_cluster_ips,
            node_tag=basic_config_dict['ip_tag'][-1],
            alauda_network_mode=self._get_bridge_mode())

        logger.info("{} got compose_content: \n{}".format(
            self.__class__.__name__, compose_content))
        return compose_content

    def __repr__(self):
        out = 'elasticsearch'
        return out

    def _get_kibana_external_ports(self):
        http_ports = ["5601:5601"]
        return [PortHttp(port=port) for port in http_ports]
