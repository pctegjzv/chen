#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import absolute_import

import logging
from jinja2 import Template
from catalog.claas.base_factory import AbstractFactory
from catalog.exceptions import CatalogException
from catalog.resources.port import PortHttp, PortTcp
from catalog.resources.load_balances import AlaudaLBFactory


logger = logging.getLogger(__name__)
__author__ = "junh@alauda.io"

HADOOP = {
    "name": "hadoop",
    "info": {
        "en": "This Hadoop is supposed to be used as a cluster.",
        "zh": "用于部署 Hadoop 集群"
    },
    "instruction": {
        "en": "These are the default ports used by the namenode/resource manager node.  \
\n\
* Resource manager WebUI: 8088  \n\
* Namenode WebUI: 50070 \n\
* Resource manager port: 8032 \n\
* Resource manager admin interface: 8033  \n\
\n\
Please consult [hadoop documentation](http://hadoop.apache.org/docs/r2.7.3/) for more information.  \n\
\n\
#### YARN CONFIGURATION ENV  \n\
These are also set as common. The numbers below are the default values used:  \n\
\n\
* YARN_MIN_ALLOC=128 Sets the minimum memory allocation for each task in MB \n\
* YARN_MAX_ALLOC=2048 Sets the maximum memory allocation for each task in MB \n\
* YARN_MIN_VCORES_NUM=1 Sets the minumum number of virtual cores used by each task \n\
* YARN_MAX_VCORES_NUM=2 Sets the maximmum number of virtual cores used by each task \n\
* YARN_RESOURCE_MEM=4096 Sets the total amount of memory used by the resource node \n\
* YARN_CORES=4 Sets the total number of cores used by the resource node",  # noqa

        "zh": "下面是 namenode/resource 管理节点使用的默认端口号  \
\n\
* Resource manager WebUI: 8088 \n\
* Namenode WebUI: 50070  \n\
* Resource manager 端口：8032  \n\
* Resource manager admin 接口：8033 \n\
\n\
请查看 [Hadoop 文档](http://hadoop.apache.org/docs/r2.7.3/)获得更多信息。   \n\
#### YARN 配置环境变量 \n\
这些配置是通用的。 使用的是下面的默认值：\n\
\n\
* YARN_MIN_ALLOC=128  每个任务最小分配的内存(MB) \n\
* YARN_MAX_ALLOC=2048 每个任务最大分配的内存(MB) \n\
* YARN_MIN_VCORES_NUM=1 每个任务使用的最小虚拟核数 \n\
* YARN_MAX_VCORES_NUM=2 每个任务使用的最大虚拟核数 \n\
* YARN_RESOURCE_MEM=4096 资源节点使用的内存大小 \n\
* YARN_CORES=4 资源节点可使用的虚拟核数",
    },
    "basic_config": [{
        "type": "int",
        "attribute_name": "num_of_nodes",
        "display_name": {
            "en": "Number of nodes",
            "zh": "集群节点数"
        },
        "description": {
            "en": "Desired cluster size. At least two nodes",
            "zh": "实例数量。至少两个节点"
        },
        "min_value": 2
      }, {
          "type": "option",
          "attribute_name": "cluster_size",
          "display_name": {
              "en": "Instance size",
              "zh": "实例大小"
          },
          "description": {
              "en": "Size of instances",
              "zh": "实例大小"
          },
          "option": [
              "XXS", "XS", "S", "M", "L", "XL"
          ]
      }, {
          "type": "multi_option",
          "attribute_name": "ip_tag",
          "display_name": {
              "en": "Node tag",
              "zh": "主机标签"
          },
          "description": {
              "en": "Deploy to specific host",
              "zh": "定点部署",
          },
          "option": {
          }
      }, {
          "type": "option",
          "attribute_name": "image_tag",
          "display_name": {
              "en": "image tag",
              "zh": "镜像标签"
          },
          "description": {
              "en": "",
              "zh": ""
          },
          "key": "version",
          "value": "version",
          "option": [{"version": "2.7.3"}, {"version": "latest"}]
      }, {
        "type": "option",
        "attribute_name": "alauda_lb_name",
        "display_name": {
            "en": "load balance",
            "zh": "负载均衡"
        },
        "description": {
            "en": "",
            "zh": ""
        }
    }],
    "advanced_config": [{
        "type": "single_ip_tag",
        "attribute_name": "name_node_addr",
        "display_name": {
            "en": "Namenode address",
            "zh": "Namenode 地址"
        },
        "description": {
            "en": "Set the IP of the namenode server. Currently also used as the resource manager IP",    # noqa
            "zh": "设置 namenode 服务的 IP。目前也用作资源管理者 IP"
        },
    }, {
        "type": "bool",
        "attribute_name": "format_namenode",
        "display_name": {
            "en": "Format namenode",
            "zh": "格式化 namenode"
        },
        "description": {
            "en": "format the namenode everytime the node is run",
            "zh": "每次节点运行时，格式化 namenode"
        },
        "default_value": "true"
    }, {
        "type": "string",
        "attribute_name": "data_dir",
        "display_name": {
            "en": "Data directory",
            "zh": "数据目录"
        },
        "description": {
            "en": "The directory format as HDFS",
            "zh": "HDFS 格式的目录"
        },
        "default_value": "/data/hdfs"
    }, {
        "type": "bool",
        "attribute_name": "test",
        "display_name": {
            "en": "Run test after set up",
            "zh": "配置完运行测试"
        },
        "description": {
            "en": "Run a run-wordcount.sh task after the namenode is setup",
            "zh": "namenode 设置好之后运行 run-wordcount"
        },
        "default_value": "true"
    }, {
        "type": "radio_group_tab",
        "attribute_name": "start_yarn",
        "display_name": {
            "en": "Start yarn",
            "zh": "启用 Yarn "
        },
        "description": {
            "en": "If not set the cluster will only be set as a HDFS cluster",
            "zh": "如果未勾选，将只部署 HDFS 集群。"
        },
        "option": [
            "true", "false"
        ]
    }],
    "true_config": [{
        "type": "string",
        "attribute_name": "yarn_min_alloc",
        "display_name": {
            "en": "Yarn min allocation",
            "zh": "Yarn 最小分配内存"
        },
        "description": {
            "en": "Sets the minimum memory allocation for each task in MB",
            "zh": "每个任务最小分配的内存(MB)"
        },
        "default_value": "128"
    }, {
        "type": "string",
        "attribute_name": "yarn_max_alloc",
        "display_name": {
            "en": "Yarn max allocation",
            "zh": "Yarn 最大分配内存"
        },
        "description": {
            "en": "Sets the maximum memory allocation for each task in MB",
            "zh": "每个任务最大分配的内存(MB) "
        },
        "default_value": "2048"
    }, {
        "type": "string",
        "attribute_name": "yarn_min_vcores_num",
        "display_name": {
            "en": "Yarn min vcores num",
            "zh": "Yarn 最小虚拟核数"
        },
        "description": {
            "en": "Sets the minumum number of virtual cores used by each task",
            "zh": "每个任务使用的最小虚拟核数"
        },
        "default_value": "1"
    }, {
        "type": "string",
        "attribute_name": "yarn_max_vcores_num",
        "display_name": {
            "en": "Yarn max vcores num",
            "zh": "Yarn 最大虚拟核数"
        },
        "description": {
            "en": "Sets the maximmum number of virtual cores used by each task",
            "zh": "每个任务使用的最大拟核数"
        },
        "default_value": "2"
    }, {
        "type": "string",
        "attribute_name": "yarn_resource_mem",
        "display_name": {
            "en": "Yarn resource memory",
            "zh": "Yarn 资源节点内存"
        },
        "description": {
            "en": "Sets the total amount of memory used by the resource node",
            "zh": "资源节点使用的内存大小"
        },
        "default_value": "4096"
    }, {
        "type": "string",
        "attribute_name": "yarn_core",
        "display_name": {
            "en": "Yarn core",
            "zh": "Yarn 核数"
        },
        "description": {
            "en": "Sets the total number of cores used by the resource node",
            "zh": "资源节点可使用的虚拟核数"
        },
        "default_value": "4"
    }]
}


class Hadoop(AbstractFactory):

    def __init__(self, raw_data, namespace):
        super(Hadoop, self).__init__(raw_data)
        self.meta_compose_path = self.base_template_path + '/meta_compose.yml'
        self.meta_container_path = self.base_template_path + '/meta_container.yml'

    def get_compose_data(self):
        region_alb_version = self.raw_data['region_alb_version']
        basic_config_dict = self.raw_data['basic_config']
        advanced_config_dict = self.raw_data['advanced_config']

        image_tag = basic_config_dict['image_tag']

        for item in HADOOP['advanced_config']:
            if item['type'] == 'bool':
                advanced_config_dict[item['attribute_name']] = \
                    str(advanced_config_dict[item['attribute_name']]).lower()

        node_ips = filter(lambda x: x != advanced_config_dict['name_node_addr'],
                          basic_config_dict['ip_tag'])

        if basic_config_dict['num_of_nodes'] != len(basic_config_dict['ip_tag']):
            raise CatalogException('catalog_invalid_node_ip_tag')

        compose_content = str()
        meta_container_content = self.get_config_content(self.meta_container_path)
        template = Template(meta_container_content)
        if advanced_config_dict['start_yarn'] != 'true':
            advanced_config_dict.pop('start_yarn')

        for i in range(basic_config_dict['num_of_nodes']-1):
            container_content = template.render(image_index=self.image_index,
                                                image_tag=image_tag,
                                                node_name='hadoop-node'+str(i+1),
                                                cluster_size=basic_config_dict['cluster_size'],
                                                node_ip=node_ips[i],
                                                node_ips=",".join(node_ips),
                                                **advanced_config_dict)
            compose_content += container_content

        alauda_lb_name = basic_config_dict.get('alauda_lb_name')

        alauda_lb_type, lb_instance = AlaudaLBFactory.get_alauda_lb_instance(
            region_alb_version, alauda_lb_name, self._get_namenode_external_ports())

        meta_compose_content = self.get_config_content(self.meta_compose_path)
        template = Template(meta_compose_content)
        compose_content = template.render(
            image_index=self.image_index,
            image_tag=image_tag,
            alauda_lb_type=alauda_lb_type,
            alauda_external_ports=lb_instance.get_external_ports_str(),
            containers=compose_content,
            cluster_size=basic_config_dict['cluster_size'],
            node_ips=",".join(node_ips),
            **advanced_config_dict)

        logger.info("{} got compose_content: \n{}".format(self.__class__.__name__,
                                                          compose_content))
        return compose_content

    @classmethod
    def write_compose_content(cls, file_path, content):
        """
        for debug
        :param file_path:
        :param content:
        :return:
        """
        with open(file_path, 'w') as f:
            f.write(content)

    def _get_namenode_external_ports(self):
        http_ports = ["8088:8088"]
        tcp_ports = ["50070:50070", "9000:9000", "9999:9999"]
        port_instance_list = []

        for port in http_ports:
            port_instance = PortHttp(port=port)
            port_instance_list.append(port_instance)

        for port in tcp_ports:
            port_instance = PortTcp(port=port)
            port_instance_list.append(port_instance)

        return port_instance_list
