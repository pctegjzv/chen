#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import absolute_import

import logging
from jinja2 import Template
from catalog.claas.base_factory import AbstractFactory
from catalog.resources.load_balances import AlaudaLBFactory
from catalog.resources.port import PortHttp

logger = logging.getLogger(__name__)
__author__ = "junh@alauda.io"


ELK = {
    "name": "elk",
    "info": {
        "en": "One-click deployment of ELK(Elasticsearch, Logtash, Kibana) stack ",
        "zh": "一键式运行 ELK（Elasticsearch， Logstash，Kibana）"
    },
    "instruction": {
        "en": "### Elasticsearch \n\
** You need to increase the `vm.max_map_count` kernel setting on your Docker host, Please consult [Install Elasticsearch with Docker](https://www.elastic.co/guide/en/elasticsearch/reference/current/docker.html#docker-cli-run-prod-mode) for more information **\n\
\n\
default ports used by Elasticsearch: \n\
\n\
* 9200 Elasticsearch HTTP Service \n\
* 9300 native Elasticsearch transport protocol \n\
\n\
### Kibana \n\
\n\
default port used by Kibana: \n\
* 5601 Kibana UI  \n\
\n\
### Logstash \n\
default configuration used by logstash: \n\
* Input: TCP port 5000 \n\
* Output: Elasticsearch \n\
\n\
",  # noqa
        "zh": "### Elasticsearch \n\
** 请确保你已经修改了宿主机的内核参数 `vm.max_map_count`，请参考文档：[Install Elasticsearch with Docker](https://www.elastic.co/guide/en/elasticsearch/reference/current/docker.html#docker-cli-run-prod-mode) **\n\
\n\
elasticsearch 默认暴露的端口: \n\
\n\
* 9200 Elasticsearch HTTP 服务端口 \n\
* 9300 Elasticsearch 传输协议端口 \n\
\n\
### Kibana \n\
\n\
Kibana 默认使用的端口：\n\
* 5601 Kibana UI \n\
\n\
### Logstash \n\
Logstash 采用如下默认配置：\n\
\n\
* Input: TCP port 5000 \n\
* Output: Elasticsearch \n\
\n\
"  # noqa
    },
    "basic_config": [{
        "type": "cluster_size",
        "attribute_name": "es_size",
        "display_name": {
            "en": "Elasticsearch instance size",
            "zh": "Elasticsearch 实例大小"
        },
        "description": {
            "en": "Size of Elasticsearch instances",
            "zh": "Elasticsearch 实例大小"
        },
        "option": [
            "XXS", "XS", "S", "M", "L", "XL"
        ]
    }, {
        "type": "cluster_size",
        "attribute_name": "logstash_size",
        "display_name": {
            "en": "Logstash instance size",
            "zh": "Logstash 实例大小"
        },
        "description": {
            "en": "Size of logstash instances",
            "zh": "logstash 实例大小"
        },
        "option": [
            "XXS", "XS", "S", "M", "L", "XL"
        ]
    }, {
        "type": "cluster_size",
        "attribute_name": "kibana_size",
        "display_name": {
            "en": "Kibana Instance size",
            "zh": "Kibana 实例大小"
        },
        "description": {
            "en": "Size of kibana instances",
            "zh": "Kibana 实例大小"
        },
        "option": [
            "XXS", "XS", "S", "M", "L", "XL"
        ]
    }, {
        "type": "multi_option",
        "attribute_name": "ip_tag",
        "display_name": {
            "en": "Node tag",
            "zh": "主机标签"
        },
        "description": {
            "en": "Deploy to specific host",
            "zh": "定点部署"
        },
        "option": {
        }
    }, {
        "type": "option",
        "attribute_name": "image_tag",
        "display_name": {
            "en": "image tag",
            "zh": "镜像标签"
        },
        "description": {
            "en": "",
            "zh": ""
        },
        "key": "version",
        "value": "version",
        "option": [{"version": "latest"}, {"version": "5.2.1"}]
    }, {
        "type": "option",
        "attribute_name": "alauda_lb_name",
        "display_name": {
            "en": "load balance",
            "zh": "负载均衡"
        },
        "description": {
            "en": "",
            "zh": ""
        }
    }],
    "advanced_config": [{
        "type": "single_ip_tag",
        "attribute_name": "es_node_tag",
        "display_name": {
            "en": "Elasticsearch address",
            "zh": "Elasticsearch 地址"
        },
        "description": {
            "en": "Set the IP of elasticsearch",
            "zh": "设置 elasticsearch 的 ip"
        }
    }, {
        "type": "single_ip_tag",
        "attribute_name": "logstash_node_tag",
        "display_name": {
            "en": "Logstash address",
            "zh": "Logstash 地址"
        },
        "description": {
            "en": "Set the IP of logstash",
            "zh": "设置 logstash 的 ip"
        }
    }, {
        "type": "single_ip_tag",
        "attribute_name": "kibana_node_tag",
        "display_name": {
            "en": "kibana address",
            "zh": "kibana 地址"
        },
        "description": {
            "en": "Set the IP of kibana",
            "zh": "设置 kibana 的地址"
        }
    }]
}


class Elk(AbstractFactory):

    def __init__(self, raw_data, namespace):
        super(Elk, self).__init__(raw_data)
        self.meta_compose_path = self.base_template_path + '/meta_compose.yml'

    def get_compose_data(self):
        region_alb_version = self.raw_data['region_alb_version']
        basic_config_dict = self.raw_data['basic_config']
        advanced_config_dict = self.raw_data['advanced_config']

        basic_config_dict.update(advanced_config_dict)

        logger.debug("basic_config_dict: {}".format(basic_config_dict))
        logger.debug("advanced_config_dict: {}".format(advanced_config_dict))

        meta_container_content = self.get_config_content(self.meta_compose_path)
        template = Template(meta_container_content)

        alauda_lb_name = basic_config_dict.get('alauda_lb_name')

        alauda_lb_type, lb_instance = AlaudaLBFactory.get_alauda_lb_instance(
            region_alb_version, alauda_lb_name, self._get_kibana_external_ports())

        container_content = template.render(
            image_index=self.image_index, alauda_lb_type=alauda_lb_type,
            alauda_external_ports=lb_instance.get_external_ports_str(),
            **basic_config_dict)

        logger.info("{} got compose_content: \n{}".format(
            self.__class__.__name__, container_content))
        return container_content

    def __repr__(self):
        out = 'elk'
        return out

    def _get_kibana_external_ports(self):
        http_ports = ["5601:5601"]
        return [PortHttp(port=port) for port in http_ports]
