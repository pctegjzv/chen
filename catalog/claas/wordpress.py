#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import absolute_import
from __future__ import unicode_literals

import logging
from catalog.claas.base_factory import AbstractFactory

logger = logging.getLogger(__name__)
__author__ = "junh@alauda.io"


wordpress = {
    "name": "wordpress",
    "info": {
        "en": "Deploy WordPress with several clicks",
        "zh": "快速部署 WordPress"
    },
    "instruction": {
        "en": "You can choose instance size for the Redis cluster, deployed on a specific machine",
        "zh": "可以选择实例大小，部署在特定的机器上"
    },
    "basic_config": [{
        "type": "option",
        "attribute_name": "cluster_size",
        "display_name": {
            "en": "Instance size",
            "zh": "实例大小"
        },
        "description": {
            "en": "Size of instances",
            "zh": "实例大小"
        },
        "option": [
            "XXS", "XS", "S", "M", "L", "XL"
        ]
    }, {
        "type": "multi_option",
        "attribute_name": "ip_tag",
        "display_name": {
            "en": "Node tag",
            "zh": "主机标签"
        },
        "description": {
            "en": "Deploy to specific host",
            "zh": "定点部署",
        },
        "option": {
        },
        "max_num": 2,
        "min_num": 2
    }]
}


class WordPress(AbstractFactory):

    def __init__(self, raw_data):
        super(WordPress, self).__init__(raw_data)
        self.compose = self.base_template_path + '/compose.yml'

    def get_compose_data(self):
        # ip_tag = self.raw_data['basic_config']['ip_tag']
        # cluster_size = self.raw_data['basic_config']['cluster_size']

        mode = self.raw_data['advanced_config']['mode']

        compose_content = str()
        logger.debug("{}, mode:{}, got compose_content: \n{}".format(
            self.__class__.__name__, mode, compose_content))

        return compose_content

    def __repr__(self):
        out = 'WordPress'
        return out
