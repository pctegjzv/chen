#!/usr/bin/env python
# -*- coding: utf-8 -*-

from batch_processing import BatchProcessing
from catalog.claas.hbase import Hbase
from elasticsearch import Elasticsearch
from elk import Elk
from gitlab import Gitlab
from hadoop_ha import HadoopHA
from kafka import Kafka
from redis import Redis
from mongodb import Mongodb
from mysql import Mysql
from spark import Spark
from stream_processing import StreamProcessing
from zookeeper import ZooKeeper
from spring_eureka import SpringEureka
from spring_hystrix_dashboard import SpringHystrixDashboard
from spring_config_server import SpringConfigServer
from spring_turbine import SpringTurbine
from spring_zuul import SpringZuul
from spring_zipkin import SpringZipkin
from plugin_center.plugins.applications.cliar import Clair
from plugin_center.plugins.applications.sonarqube import SonarQube

__author__ = "junh@alauda.io"


class CatalogFactory(object):
    factory = {
        "batch_processing": BatchProcessing,
        "elasticsearch": Elasticsearch,
        "elk": Elk,
        "gitlab": Gitlab,
        "hadoop": HadoopHA,
        "hbase": Hbase,
        "kafka": Kafka,
        "redis": Redis,
        "mongodb": Mongodb,
        "mysql": Mysql,
        "spark": Spark,
        "stream_processing": StreamProcessing,
        "zookeeper": ZooKeeper,
        "spring_eureka": SpringEureka,
        "spring_hystrix_dashboard": SpringHystrixDashboard,
        "spring_config_server": SpringConfigServer,
        "spring_turbine": SpringTurbine,
        "spring_zuul": SpringZuul,
        "spring_zipkin": SpringZipkin,
        "SonarQube": SonarQube,
        "Clair": Clair
    }

    @staticmethod
    def get_factory(app_name, raw_data, namespace=None):
        try:
            return CatalogFactory.factory[app_name](raw_data, namespace=namespace)
        except KeyError:
            raise TypeError('Unknown catalog factory. app name: {}'.format(app_name))
