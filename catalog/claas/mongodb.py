#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import absolute_import

import logging
import client

from jinja2 import Template
from catalog.claas.base_factory import AbstractFactory
from catalog.exceptions import CatalogException

logger = logging.getLogger(__name__)
__author__ = "fanyang@alauda.io"


MONGODB = {
    "name": "mongodb",
    "info": {
        "en": "Set up a MongoDB application.",
        "zh": "部署MongoDB应用"
    },
    "instruction": {
        "en": "Enter the number of nodes, instance size for the MongoDB application. \
                Support standalone mode, replica_set set mode（At least 3 nodes）",  # noqa
        "zh": "可以输入节点数，实例大小。支持单点模式、副本集模式（至少3个节点）"
    },
    "basic_config": [{
        "type": "option",
        "attribute_name": "image_tag",
        "display_name": {
            "en": "MongoDB version",
            "zh": "MongoDB 版本"
        },
        "description": {
            "en": "",
            "zh": ""
        },
        "key": "version",
        "value": "version",
        "option": [{"version": "3.4.6"}]
    }, {
        "type": "multi_option",
        "attribute_name": "ip_tag",
        "display_name": {
            "en": "Node tag",
            "zh": "主机标签"
        },
        "description": {
            "en": "Deploy to specific host",
            "zh": "定点部署",
        },
        "option": {
        }
    }, {
        "type": "radio_group_tab",
        "attribute_name": "mode",
        "display_name": {
            "en": "Operating mode",
            "zh": "运行模式"
        },
        "description": {
            "en": "You can choose standalone or replica_set set mode",
            "zh": "你可以选择单点或副本集模式"
        },
        "option": [
            "standalone", "replica_set"
        ]
    }, {
        "type": "cluster_size",
        "attribute_name": "mongodb_size",
        "display_name": {
            "en": "Instance size",
            "zh": "实例大小"
        },
        "description": {
            "en": "Size of instances",
            "zh": "实例大小"
        },
        "min_value": {
            "mem": 512,
            "cpu": 0.25
        }
    }, {
        "type": "string",
        "attribute_name": "mount_data_dir",
        "pattern": "/^\/[\w\d\-_.\/]+$/",
        "pattern_hint": {
            "en": "The data directory format is invalid, \
                    it can only contain characters like slashes, numbers, letters, \
                    hyphens(-), underscores(_) and dots(.), and it must be absolute path",
            "zh": "路径格式错误，只能含有斜杠、数字、字母、中线、下划线和点号，且需要为绝对路径。"
        },
        "display_name": {
            "en": "Data directory",
            "zh": "主机数据存储路径"
        },
        "description": {
            "en": "The data directory of MongoDB in the host. \
                    The ultimate path will append the application name to avoid duplication.",
            "zh": "MongoDB数据在主机上的存储路径，程序会自动将应用名称作为目录名添加到该路径后面，以避免路径重复。"
        },
        "default_value": "/mongodb/data"
    }],
    "advanced_config": [{
        "type": "int",
        "attribute_name": "port",
        "display_name": {
            "en": "MongoDB Port",
            "zh": "MongoDB 端口"
        },
        "description": {
            "en": "The client port of MongoDB. \
                    You should modify the default value for security reasons.",
            "zh": "客户端连接MongoDB的端口，为安全起见建议修改默认端口。"
        },
        "default_value": 27017,
        "max_value": 65534,
        "min_value": 1025
    }, {
        "type": "string",
        "attribute_name": "root_user",
        "pattern": "/^[a-zA-Z][\w]{0,31}$/",
        "pattern_hint": {
            "en": "The name format is invalid, \
            it can only contain characters like letters, numbers \
            and underscores(_), maximum 32 bytes and begins with a letter.",
            "zh": "名称格式错误，只能含有字母、数字、下划线，最长32字节且需要以字母开头。"
        },
        "required": False,
        "display_name": {
            "en": "MongoDB user ",
            "zh": "MongoDB 用户名"
        },
        "description": {
            "en": "The root user name of MongoDB client.",
            "zh": "客户端连接MongoDB的root用户名."
        }
    }, {
        "type": "string",
        "attribute_name": "root_password",
        "pattern": "/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])[!#%&(-[\]-_a-~]{8,32}$/",
        "pattern_hint": {
            "en": "The password must be printable ASCII characters, \
                    and it's length must at least 8 characters and at most 32 characters, \
                    and can not contain space/dollar/backslash/order quotes/single quote\
                    /double quote, and must contain numbers, upper and lowercase letters",
            "zh": "密码长度需要在8位至32位，只能为可打印的ASCII字符，\
                    不能含有空格、美元符、反斜杠、开单引号、单引号或双引号，且需要包含大小写字母和数字。"
        },
        "required": False,
        "display_name": {
            "en": "MongoDB Password",
            "zh": "MongoDB 密码"
        },
        "description": {
            "en": "The root password of MongoDB. You should set it for security reasons. \
                    If you set, it must be at least 8 characters and at most 32 characters, \
                    and may contain numbers, upper and lowercase letters, \
                    but can not contain single quote or double quote.",
            "zh": "客户端连接MongoDB的root密码，为安全起见建议设置较复杂密码。\
                    如若设置，密码长度需要在8位至32位，包含大小写字母和数字。且不能含有单引号或双引号。"
        }
    }],
    "standalone_config": [

    ],
    "replica_set_config": [{
        "type": "single_ip_tag",
        "attribute_name": "primary_node_addr",
        "display_name": {
            "en": "Primary node address",
            "zh": "主节点地址"
        },
        "description": {
            "en": "Set the IP of Primary server.",
            "zh": "设置主服务器的 ip 地址"
        }
    }, {
        "type": "string",
        "attribute_name": "replset_name",
        "pattern": "/^[a-zA-Z][\w]{0,31}$/",
        "pattern_hint": {
            "en": "The name format is invalid, \
                    it can only contain characters like letters, numbers \
                    and underscores(_), maximum 32 bytes and begins with a letter.",
            "zh": "名称格式错误，只能含有字母、数字、下划线，最长32字节且需要以字母开头。"
        },
        "required": True,
        "display_name": {
            "en": "replica_set name ",
            "zh": "副本集名称"
        },
        "description": {
            "en": "The name of replication set.",
            "zh": "副本集的名称"
        },
        "default_value": "replset"
    }, {
        "type": "int",
        "attribute_name": "secondary_node_number",
        "display_name": {
            "en": "Secondary nodes number",
            "zh": "副本节点数"
        },
        "description": {
            "en": "Set the number of Secondary nodes, \
                    It has to be greater than or equal to 2.",
            "zh": "设置副本节点的个数，必须大于或等于2。"
        },
        "min_value": 2
    }]
}

MODE_STANDALONE = 'standalone'
MODE_REPLICATION = 'replica_set'

MODE_DICT = {
    MODE_STANDALONE: '单点模式',
    MODE_REPLICATION: '副本集模式',
}


class Mongodb(AbstractFactory):

    def __init__(self, raw_data, namespace):
        super(Mongodb, self).__init__(raw_data)
        self.standalone_meta_compose_path = self.base_template_path + '/standalone/meta_compose.yml'
        self.standalone_meta_container_path = self.base_template_path + \
            '/standalone/meta_container.yml'
        self.replica_meta_compose_path = self.base_template_path + '/replica/meta_compose.yml'
        self.replica_meta_container_path = self.base_template_path + '/replica/meta_container.yml'

        self.namespace = namespace

    def get_compose_data(self):
        logger.debug('Testing, namespace????{}'.format(self.namespace))
        basic_config_dict = self.raw_data['basic_config']
        advanced_config_dict = self.raw_data['advanced_config']

        # basic config dict
        image_tag = basic_config_dict['image_tag']
        ip_tag = basic_config_dict['ip_tag']
        all_nodes_num = len(ip_tag)

        # deal size
        mongodb_size = basic_config_dict['mongodb_size']
        cpu_quota = mongodb_size['cpu']
        mem_limit = mongodb_size['mem']

        # mount data directory
        mount_data_dir = basic_config_dict['mount_data_dir']
        mount_data_dir = self.check_path_and_suffix_app_name(mount_data_dir)

        mode = basic_config_dict['mode']
        if mode == MODE_STANDALONE:
            replicas_num = 0
        else:
            replicas_num = int(basic_config_dict['secondary_node_number'])
            # secondary node number must >= 2
            if replicas_num < 2:
                raise CatalogException('catalog_mongodb_secondary_node_number_cant_less_two')

        port = int(advanced_config_dict['port'])
        # check whether or not the MongoDB port is correct
        if not port:
            port = 27017
        else:
            self.check_port(port, "port of MongoDB", 1024, 65535)

        root_user = ''
        root_password = ''
        monitor_service_user = ''
        monitor_service_password = ''
        if 'root_user' in advanced_config_dict.keys():
            root_user = advanced_config_dict['root_user']
            if 'root_password' in advanced_config_dict.keys():
                root_password = advanced_config_dict['root_password']
                monitor_service_user = \
                    'ALAUDA_MONITOR_SERVICE__MONGO__1__USER: {}'.format(root_user)
                monitor_service_password = \
                    'ALAUDA_MONITOR_SERVICE__MONGO__1__PASSWORD: {}'.format(root_password)
                # check whether or not the MongoDB password is strong
                root_password = self.verify_password(root_password, 'MongoDB password')
                # format as parm name: param value
                root_user = 'MONGODB_ROOT_USER: {}'.format(root_user)
                root_password = 'MONGODB_ROOT_PASSWORD: {}'.format(root_password)
            else:
                raise CatalogException('catalog_mongodb_root_password_cant_be_empty')

        # meta_compose_path = meta_container_path = ''
        container_args = {
            'image_index': self.image_index,
            'image_tag': image_tag,
            'cpu_quota': cpu_quota,
            'mem_limit': mem_limit,
            'mount_data_dir': mount_data_dir,
            'port': port,
            'root_user': root_user,
            'root_password': root_password,
            'monitor_service_user': monitor_service_user,
            'monitor_service_password': monitor_service_password
        }
        if mode == MODE_STANDALONE:
            # all nodes number should be 1
            if all_nodes_num != 1:
                raise CatalogException('catalog_invalid_node_ip_tag')

            container_args.update({
                'node_name': 'mongodb'
            })
            meta_compose_path = self.standalone_meta_compose_path
            meta_container_path = self.standalone_meta_container_path
        elif mode == MODE_REPLICATION:
            # check whether or not all nodes number is correct
            if all_nodes_num != replicas_num + 1:
                raise CatalogException('catalog_invalid_node_ip_tag')
            primary_node_addr = basic_config_dict['primary_node_addr']
            # master node ip should be in the ip tag list
            if primary_node_addr not in ip_tag:
                raise CatalogException('catalog_invalid_primary_node_addr')

            primary_priority = 10
            secondary_priority = 1
            vsftpd_hosts = [ip + ':' + str(port) for ip in ip_tag]
            vsftpd_hosts_str = ','.join(vsftpd_hosts)
            vsftpd_hosts.remove(primary_node_addr + ':' + str(port))
            replset_hosts = [host + ':' + str(secondary_priority) for host in vsftpd_hosts]
            replset_hosts_str = primary_node_addr + ':' + str(port) + \
                ':' + str(primary_priority) + ',' + ','.join(replset_hosts)

            replset_name = basic_config_dict['replset_name']
            container_args.update({
                'node_name': 'mongodb-rs',
                'vsftpd_hosts': vsftpd_hosts_str,
                'replset_hosts': replset_hosts_str,
                'replset_name': replset_name
            })

            meta_compose_path = self.replica_meta_compose_path
            meta_container_path = self.replica_meta_container_path

        else:
            raise CatalogException('catalog_invalid_config')

        # render the yml file
        meta_container_content = self.get_config_content(meta_container_path)
        template = Template(meta_container_content)

        compose_content = ''
        if mode == MODE_STANDALONE:
            container_args['node_ip'] = ip_tag[0]
            container_args['node_tag'] = 'ip:' + ip_tag[0]
            compose_content = template.render(container_args)
        elif mode == MODE_REPLICATION:
            node_name = container_args['node_name']
            for i in range(all_nodes_num):
                container_args['node_name'] = node_name + str(i + 1)
                container_args['node_ip'] = ip_tag[i]
                container_args['node_tag'] = 'ip:' + ip_tag[i]
                if ip_tag[i] == primary_node_addr:
                    container_args['is_primary'] = 1
                else:
                    container_args['is_primary'] = 0
                container_content = template.render(container_args)
                compose_content += container_content

        meta_compose_content = self.get_config_content(meta_compose_path)
        template = Template(meta_compose_content)
        compose_content = template.render({'containers': compose_content})

        logger.info('{}, mode:{}, got compose_content: \n{}'.format(
            self.__class__.__name__, mode, compose_content))

        return compose_content

    def _format_app_detail(self, data):
        detail_template = super(Mongodb, self)._format_app_detail(data)

        # add basic info
        mode = self.config_json.get_mode()
        detail_template.add_basic_info('运行模式', 'Operating mode', MODE_DICT.get(mode))
        detail_template.add_basic_info('MongoDB版本', 'MongoDB version',
                                       self.config_json.get_image_tag())
        detail_template.add_basic_info('MongoDB端口', 'MongoDB Port', self.config_json.get_port())

        if mode == MODE_REPLICATION:
            detail_template.add_basic_info('副本节点数', 'Secondary node number',
                                           self.config_json.get_secondary_node_number())

        try:
            resp = client.druid.client.DruidClient.application_info(self.app_id, True)
            mount_data_dir = resp.data['services'][0]['volumes'][0]['volume_name']
        except:
            mount_data_dir = self.config_json.get_mount_data_dir() + '/' + \
                self.config_json.get_app_name()
        detail_template.add_basic_info('主机数据存储路径', 'Data directory', mount_data_dir)

        node_list = ', '.join(self.config_json.get_ip_tag())
        detail_template.add_basic_info('主机列表', 'Node list', node_list)

        # add metrics
        detail_template.set_metrics(self.get_metrics())

        return detail_template

    def __repr__(self):
        out = 'MongoDB'
        return out
