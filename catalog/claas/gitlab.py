#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import absolute_import

import logging
from catalog.claas.base_factory import AbstractFactory

__author__ = "junh@alauda.io"
logger = logging.getLogger(__name__)

GITLAB = {
  "name": "gitlab CE",
  "info": {
    "en": "balabala",
    "zh": "balabala"
  },
  "instruction": {
    "en": "instruction, balabala",
    "zh": "instruction, zh, balabala"
  },
  "basic_config": [
    {
      "type": "option",
      "attribute_name": "cluster_size",
      "display_name": {
        "en": "Instance Size",
        "zh": "实例大小"
      },
      "description": {
        "en": "size of instances",
        "zh": "实例大小"
      },
      "option": [
        "XXS", "XS", "S", "M", "L", "XL"
      ]
    },
    {
      "type": "option",
      "attribute_name": "ip_tag",
      "display_name": {
        "en": "Node Tag",
        "zh": "主机标签"
      },
      "description": {
        "en": "deploy to specific host",
        "zh": "定点部署",
      }
    }
  ],
}


class Gitlab(AbstractFactory):

    def __init__(self, raw_data, namespace):
        super(Gitlab, self).__init__(raw_data)
        self.docker_compose_path = self.base_template_path + '/docker_compose.yml'

    def get_compose_data(self):
        compose_content = self.get_config_content(self.docker_compose_path)
        logger.debug("Gitlab compose_content\n: {}".format(compose_content))
        return compose_content

    @classmethod
    def write_compose_content(cls, file_path, content):
        """
        for debug
        :param file_path:
        :param content:
        :return:
        """
        with open(file_path, 'w') as f:
            f.write(content)
        f.close()
