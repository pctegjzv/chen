#!/usr/bin/env python
# -*- coding: utf-8 -*-
import json

from .class_helper import ClaasHelper
from ..ui_element.input_multi_option import ElementIpTag, ElementNodeTag
from ..ui_element.input_int import ElementInternalPort
from ..ui_element.input_string import (ElementKafkaHost, ElementKafkaPort,
                                       ElementZooKeeperHost, ElementZooKeeperPort)
from ..ui_element.input_code import ElementConfigServerRule, ElementZuulRouteRule

BASIC_CONFIG = 'basic_config'
ADVANCED_CONFIG = 'advanced_config'


class AppConfigJson(object):

    def __init__(self, config_json_str):
        self.config_json = json.loads(config_json_str)

    def get_region_id(self):
        return self.config_json.get('region_uuid')

    def get_ip_tag(self):
        return self.config_json.get(BASIC_CONFIG).get(
            ElementIpTag.get_name())

    def get_internal_port(self):
        return self.config_json.get(BASIC_CONFIG).get(
            ElementInternalPort.get_name())

    def get_node_tag(self):
        return self.config_json.get(BASIC_CONFIG).get(
            ElementNodeTag.get_name())

    def get_config_server_rule(self):
        return self.config_json.get(ADVANCED_CONFIG).get(
            ElementConfigServerRule.get_name())

    def get_zuul_route_rule(self):
        return self.config_json.get(ADVANCED_CONFIG).get(
            ElementZuulRouteRule.get_name())

    def get_k8s_service_name(self):
        name = self.config_json.get('name')
        space_name = self.config_json.get('space_name')
        return "{}.{}--{}".format(name, name, space_name)

    def get_kafka_info(self):

        host = self.config_json.get(ADVANCED_CONFIG).get(
            ElementKafkaHost.get_name())
        port = self.config_json.get(ADVANCED_CONFIG).get(
            ElementKafkaPort.get_name())
        return ClaasHelper.merge_host_port_info(host, port)

    def get_zookeeper_info(self):
        host = self.config_json.get(ADVANCED_CONFIG).get(
            ElementZooKeeperHost.get_name())
        port = self.config_json.get(ADVANCED_CONFIG).get(
            ElementZooKeeperPort.get_name())
        return ClaasHelper.merge_host_port_info(host, port)

    def get_mode(self):
        return self.config_json.get(BASIC_CONFIG).get('mode')

    def get_image_tag(self):
        return self.config_json.get(BASIC_CONFIG).get('image_tag')

    def get_port(self):
        return self.config_json.get(ADVANCED_CONFIG).get('port')

    def get_num_of_shards(self):
        return self.config_json.get(BASIC_CONFIG).get('num_of_shards')

    def get_replicas_per_shard(self):
        return self.config_json.get(BASIC_CONFIG).get('replicas_per_shard')

    def get_secondary_node_number(self):
        return self.config_json.get(BASIC_CONFIG).get('secondary_node_number')

    def get_mount_data_dir(self):
        return self.config_json.get(BASIC_CONFIG).get('mount_data_dir')

    def get_app_name(self):
        return self.config_json.get('name')

    def get_master_node_addr(self):
        return self.config_json.get(BASIC_CONFIG).get('master_node_addr')

    def get_primary_node_addr(self):
        return self.config_json.get(BASIC_CONFIG).get('primary_node_addr')
