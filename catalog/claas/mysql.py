#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from __future__ import absolute_import

import logging
import re
import random

from jinja2 import Template
from catalog.claas.base_factory import AbstractFactory
from catalog.exceptions import CatalogException
from client.mirana2.client import Mirana2Client
from catalog.resources.port import PortHttp

logger = logging.getLogger(__name__)
__author__ = "chhu@alauda.io"


MYSQL = {
    "name": "mysql",
    "info": {
        "en": "Set up a MySQL application.",
        "zh": "实现 MySQL 应用的不同模式部署。"
    },
    "instruction": {
        "en": "Enter the number of nodes, instance size for the MySQL application. \
                Support standalone mode, replication mode and cluster mode.",  # noqa
        "zh": "可以输入节点数，实例大小。支持单点、主从和集群模式。"
    },
    "basic_config": [{
        "type": "option",
        "attribute_name": "image_tag",
        "display_name": {
            "en": "MySQL version",
            "zh": "MySQL版本"
        },
        "description": {
            "en": "",
            "zh": ""
        },
        "key": "version",
        "value": "version",
        "option": [{"version": "5.6"}, {"version": "5.5"}, {"version": "5.7"}]
    }, {
        "type": "radio_group_tab",
        "attribute_name": "mode",
        "display_name": {
            "en": "Operating mode",
            "zh": "运行模式"
        },
        "description": {
            "en": "You can choose standalone、replication or cluster mode",
            "zh": "可以选择单点、主从或集群模式"
        },
        "option": [
            "standalone", "replication", "cluster"
        ]
    }, {
        "type": "multi_option",
        "attribute_name": "ip_tag",
        "display_name": {
            "en": "Node tag",
            "zh": "主机标签"
        },
        "description": {
            "en": "Deploy to specific host",
            "zh": "定点部署",
        },
        "option": {
        }
    }, {
        "type": "option",
        "attribute_name": "cluster_size",
        "display_name": {
            "en": "Instance size",
            "zh": "实例大小"
        },
        "description": {
            "en": "Size of instances",
            "zh": "实例大小"
        },
        "min_value": {
            "mem": 1024,
            "cpu": 0.5
        }
    }, {
        "type": "string",
        "attribute_name": "mount_data_dir",
        "pattern": "/^\/[\w\d\-_.\/]+$/",
        "pattern_hint": {
            "en": "The data directory format is invalid, \
                    it can only contain characters like slashes, numbers, letters, \
                    hyphens(-), underscores(_) and dots(.), and it must be absolute path",
            "zh": "路径格式错误，只能含有斜杠、数字、字母、中线、下划线和点号，且需要为绝对路径。"
        },
        "display_name": {
            "en": "Data directory",
            "zh": "主机数据存储路径"
        },
        "description": {
            "en": "The data directory of MySQL in the host. \
                    The ultimate path will append the application name to avoid duplication.",
            "zh": "MySQL数据在主机上的存储路径，程序会自动将应用名称作为目录名添加到该路径后面，以避免路径重复。"
        }
    }, {
        "type": "string",
        "attribute_name": "default_db",
        "pattern": "/^[a-zA-Z][\w]{0,31}$/",
        "pattern_hint": {
            "en": "The name format is invalid, \
                    it can only contain characters like letters, numbers \
                    and underscores(_), maximum 32 bytes and begins with a letter.",
            "zh": "名称格式错误，只能含有字母、数字、下划线，最长32字节且需要以字母开头。"
        },
        "required": False,
        "display_name": {
            "en": "MySQL default database",
            "zh": "默认创建数据库名"
        },
        "description": {
            "en": "The name of default database.",
            "zh": "MySQL默认创建的业务数据库名"
        }
    }, {
        "type": "string",
        "attribute_name": "password",
        "pattern": "/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])[!#%&(-[\]-_a-~]{8,32}$/",
        "pattern_hint": {
            "en": "The password must be printable ASCII characters, \
                    and it's length must at least 8 characters and at most 32 characters, \
                    and can not contain space/dollar/backslash/order quotes/single quote\
                    /double quote, and must contain numbers, upper and lowercase letters",
            "zh": "密码长度需要在8位至32位，只能为可打印的ASCII字符，\
                    不能含有空格、美元符、反斜杠、开单引号、单引号或双引号，且需要包含大小写字母和数字。"
        },
        "required": True,
        "display_name": {
            "en": "MySQL root Password",
            "zh": "root密码"
        },
        "description": {
            "en": "The password of MySQL root. You should set it for security reasons. \
                    If you set, it must be at least 8 characters and at most 32 characters, \
                    and may contain numbers, upper and lowercase letters, \
                    but can not contain single quote or double quote.",
            "zh": "MySQL root 账号的密码，为安全起见建议设置较复杂密码。\
                    如若设置，密码长度需要在8位至32位，包含大小写字母和数字。且不能含有单引号或双引号。"
        }
    }],
    "advanced_config": [{
        "type": "int",
        "attribute_name": "port",
        "display_name": {
            "en": "MySQL Port",
            "zh": "MySQL端口"
        },
        "description": {
            "en": "The client port of MySQL. \
                    You should modify the default value for security reasons.",
            "zh": "客户端连接MySQL的端口，为安全起见建议修改默认端口。"
        },
        "default_value": 3306,
        "max_value": 65534,
        "min_value": 1025
    }, {
        "type": "string",
        "attribute_name": "client_user",
        "pattern": "/^[a-zA-Z][\w]{0,31}$/",
        "pattern_hint": {
            "en": "The name format is invalid, \
                    it can only contain characters like letters, numbers \
                    and underscores(_), maximum 32 bytes and begins with a letter.",
            "zh": "名称格式错误，只能含有字母、数字、下划线，最长32字节且需要以字母开头。"
        },
        "required": True,
        "display_name": {
            "en": "MySQL client user name",
            "zh": "业务用户名"
        },
        "description": {
            "en": "The user name of MySQL client.",
            "zh": "客户端连接MySQL的用户名"
        }
    }, {
        "type": "string",
        "attribute_name": "client_password",
        "pattern": "/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])[!#%&(-[\]-_a-~]{8,32}$/",
        "pattern_hint": {
            "en": "The password must be printable ASCII characters, \
                    and it's length must at least 8 characters and at most 32 characters, \
                    and can not contain space/dollar/backslash/order quotes/single quote\
                    /double quote, and must contain numbers, upper and lowercase letters",
            "zh": "密码长度需要在8位至32位，只能为可打印的ASCII字符，\
                   不能含有空格、美元符、反斜杠、开单引号、单引号或双引号，且需要包含大小写字母和数字。"
        },
        "required": True,
        "display_name": {
            "en": "MySQL client Password",
            "zh": "业务用户密码"
        },
        "description": {
            "en": "The password of MySQL. You should set it for security reasons. \
                    If you set, it must be at least 8 characters and at most 32 characters, \
                    and may contain numbers, upper and lowercase letters, \
                    but can not contain single quote or double quote.",
            "zh": "客户端连接MySQL的密码，为安全起见建议设置较复杂密码。\
                    如若设置，密码长度需要在8位至32位，包含大小写字母和数字。且不能含有单引号或双引号。"
        }
    }, {
        "type": "int",
        "attribute_name": "max_connections",
        "required": True,
        "display_name": {
            "en": "MySQL max clients connections",
            "zh": "最大连接数"
        },
        "description": {
            "en": "The max number of MySQL clients connections.",
            "zh": "MySQL客户端的最大连接数"
        },
        "default_value": 151,
    }, {
        "type": "option",
        "attribute_name": "charset",
        "display_name": {
            "en": "MySQL support charset",
            "zh": "支持字符集"
        },
        "description": {
            "en": "",
            "zh": ""
        },
        "option": ["utf8mb4", "utf8", "latin1", "gbk", "gb2312", "ascii", "big5", "binary"]
    }],
    "standalone_config": [

    ],
    "replication_config": [{
        "type": "single_ip_tag",
        "attribute_name": "master_node_addr",
        "display_name": {
            "en": "Master node address",
            "zh": "Master 节点地址"
        },
        "description": {
            "en": "Set the IP of master server.",
            "zh": "设置主服务器的 ip 地址"
        }
    }, {
        "type": "int",
        "attribute_name": "replicas_per_shard",
        "display_name": {
            "en": "Slave nodes number",
            "zh": "从节点数"
        },
        "description": {
            "en": "Set the number of slave nodes.",
            "zh": "设置从节点的个数，目前平台限制暂时只能为1。如果需要多机互备且对数据一致性要求较高建议使用集群模式"
        },
        "min_value": 1,
        "max_value": 1
    }, {
        "type": "string",
        "attribute_name": "replication_user",
        "pattern": "/^[a-zA-Z][\w]{0,31}$/",
        "pattern_hint": {
            "en": "The name format is invalid, \
                    it can only contain characters like letters, numbers \
                    and underscores(_), maximum 32 bytes and begins with a letter.",
            "zh": "名称格式错误，只能含有字母、数字、下划线，最长32字节且需要以字母开头。"
        },
        "required": True,
        "display_name": {
            "en": "Replication user name",
            "zh": "主从复制用户名"
        },
        "description": {
            "en": "The user name for MySQL replication.",
            "zh": "用于主从同步的用户名，与业务用户区分"
        }
    }, {
        "type": "string",
        "attribute_name": "replication_password",
        "pattern": "/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])[!#%&(-[\]-_a-~]{8,32}$/",
        "pattern_hint": {
            "en": "The password must be printable ASCII characters, \
                    and it's length must at least 8 characters and at most 32 characters, \
                    and can not contain space/dollar/backslash/order quotes/single quote\
                    /double quote, and must contain numbers, upper and lowercase letters",
            "zh": "密码长度需要在8位至32位，只能为可打印的ASCII字符，\
                   不能含有空格、美元符、反斜杠、开单引号、单引号或双引号，且需要包含大小写字母和数字。"
        },
        "required": True,
        "display_name": {
            "en": "Replication user password",
            "zh": "主从复制用户密码"
        },
        "description": {
            "en": "The password of MySQL replication user.",
            "zh": "主从同步用户的密码"
        }
    }, {
        "type": "radio_group_tab",
        "attribute_name": "enable_dbproxy",
        "display_name": {
            "en": "Enable DBProxy",
            "zh": "是否使用DBProxy"
        },
        "description": {
            "en": "You can use DBProxy to do read write",
            "zh": "可以使用DBProxy来进行读写分离"
        },
        "option": [
            "disable_dbproxy", "enable_dbproxy"
        ]
    }],
    "cluster_config": [{
        "type": "int",
        "attribute_name": "num_of_nodes",
        "display_name": {
            "en": "Number of nodes",
            "zh": "集群节点数"
        },
        "description": {
            "en": "Desired cluster size. At least three nodes",
            "zh": "实例数量。至少3个节点"
        },
        "min_value": 3
    }, {
        "type": "string",
        "attribute_name": "sst_user",
        "pattern": "/^[a-zA-Z][\w]{0,31}$/",
        "pattern_hint": {
            "en": "The name format is invalid, \
                    it can only contain characters like letters, numbers \
                    and underscores(_), maximum 32 bytes and begins with a letter.",
            "zh": "名称格式错误，只能含有字母、数字、下划线，最长32字节且需要以字母开头。"
        },
        "required": True,
        "display_name": {
            "en": "SST user name",
            "zh": "集群内部通信用户名"
        },
        "description": {
            "en": "The user name for State Snapshot Transfer",
            "zh": "集群内部通信(State Snapshot Transfer)用户名"
        }
    }, {
        "type": "string",
        "attribute_name": "sst_password",
         "pattern": "/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])[!#-%(-.0-9;-[\]-~]{8,32}$/",
        "pattern_hint": {
            "en": "The password must be printable ASCII characters, \
                    and it's length must at least 8 characters and at most 32 characters, \
                    and can not contain space/slash/backslash/ampersand/colon/ \
                    single quote/double quote, and must contain numbers, upper and \
                    lowercase letters",
            "zh": "密码长度需要在8位至32位，只能为可打印的ASCII字符，\
                   不能含有空格、斜杠、反斜杠、和号、冒号、单引号或双引号，且需要包含大小写字母和数字。"
        },
        "required": True,
        "display_name": {
            "en": "SST password",
            "zh": "集群内部通信用户密码"
        },
        "description": {
            "en": "The user password for State Snapshot Transfer.",
            "zh": "集群内部通信(State Snapshot Transfer)用户密码"
        }
    }],
    "enable_dbproxy_config": [{
        "type": "string",
        "attribute_name": "dbproxy_admin_user",
        "pattern": "/^[a-zA-Z][\w]{0,31}$/",
        "pattern_hint": {
            "en": "The name format is invalid, \
                    it can only contain characters like letters, numbers \
                    and underscores(_), maximum 32 bytes and begins with a letter.",
            "zh": "名称格式错误，只能含有字母、数字、下划线，最长32字节且需要以字母开头。"
        },
        "required": True,
        "display_name": {
            "en": "DBProxy Admin user",
            "zh": "DBProxy管理用户"
        },
        "description": {
            "en": "The user of DBProxy for management",
            "zh": "DBProxy管理用户"
        }
    }, {
        "type": "string",
        "attribute_name": "dbproxy_admin_password",
        "pattern":
            "/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])[!#-&(-~]{8,32}$/",
        "pattern_hint": {
            "en": "The password must be printable ASCII characters, \
                    and it's length must at least 8 characters and at most 32 characters, \
                    and can not contain space/single quote/double quote, \
                    and must contain numbers, upper and lowercase letters",
            "zh": "密码长度需要在8位至32位，只能为可打印的ASCII字符，\
                            不能含有空格、单引号或双引号，且需要包含大小写字母和数字。"
        },
        "required": True,
        "display_name": {
            "en": "DBProxy Admin password",
            "zh": "DBProxy管理密码"
        },
        "description": {
            "en": "The password of DBProxy for management",
            "zh": "DBProxy管理密码"
        }
    }, {
        "type": "int",
        "attribute_name": "dbproxy_admin_port",
        "display_name": {
            "en": "DBProxy admin Port",
            "zh": "DBProxy管理端口"
        },
        "description": {
            "en": "The admin port of DBPorxy. \
                    You should modify the default value for security reasons.",
            "zh": "客户端连接DBProxy的管理端口，为安全起见建议修改默认端口。"
        },
        "default_value": 3309,
        "max_value": 65534,
        "min_value": 1025
    }, {
        "type": "int",
        "attribute_name": "dbproxy_proxy_port",
        "display_name": {
            "en": "DBProxy proxy Port",
            "zh": "DBProxy代理端口"
        },
        "description": {
            "en": "The proxy port of DBPorxy. \
                    You should modify the default value for security reasons.",
            "zh": "客户端连接DBProxy的代理端口，为安全起见建议修改默认端口。"
        },
        "default_value": 3308,
        "max_value": 65534,
        "min_value": 1025
    }]
}

MODE_STANDALONE = 'standalone'
MODE_REPLICATION = 'replication'
MODE_CLUSTER = 'cluster'

MODE_DICT = {
    MODE_STANDALONE: '单点模式',
    MODE_REPLICATION: '主从模式',
    MODE_CLUSTER: '集群模式',
}

NODE_TYPE_MASTER = 'master'
NODE_TYPE_SLAVE = 'slave'


class Mysql(AbstractFactory):

    def __init__(self, raw_data, namespace):
        super(Mysql, self).__init__(raw_data)
        self.standalone_meta_compose_path = self.base_template_path + '/standalone/meta_compose.yml'
        self.standalone_meta_container_path = self.base_template_path + \
            '/standalone/meta_container.yml'
        self.replica_meta_compose_path = self.base_template_path + '/replica/meta_compose.yml'
        self.replica_meta_container_path = self.base_template_path + '/replica/meta_container.yml'
        self.cluster_meta_compose_path = self.base_template_path + '/cluster/meta_compose.yml'
        self.cluster_meta_container_path = self.base_template_path + '/cluster/meta_container.yml'

        self.phpmyadmin_container_path = self.base_template_path + '/phpmyadmin/meta_container.yml'
        self.dbproxy_container_path = self.base_template_path + '/dbproxy/meta_container.yml'
        self.namespace = namespace

    def get_compose_data(self):
        logger.debug('Testing, namespace????{}'.format(self.namespace))
        basic_config_dict = self.raw_data['basic_config']
        advanced_config_dict = self.raw_data['advanced_config']

        # deal config dict
        image_tag = basic_config_dict['image_tag']
        max_connections = advanced_config_dict['max_connections']
        charset = advanced_config_dict['charset']

        client_user = advanced_config_dict['client_user']
        self.verify_name(client_user, 'mysql client user')

        # deal size
        cluster_size = basic_config_dict['cluster_size']
        # cluster_size is always dict
        size = str(cluster_size.get('size'))
        mem = cluster_size.get('mem')
        cpu_quota = 'cpu_quota: ' + str(cluster_size.get('cpu'))
        mem_limit = 'mem_limit: ' + str(mem)

        # check the memory is too small
        if size == 'XXS':
            raise CatalogException('mysql_memory_is_too_small')
        if max_connections >= 1000:
            if size == 'XS' or int(mem) < 1024:
                raise CatalogException('mysql_memory_is_too_small')

        # mount data directory
        mount_data_dir = basic_config_dict['mount_data_dir']
        if not mount_data_dir:
            raise CatalogException('catalog_mount_data_dir_cant_be_empty')
        else:
            # check the path and add suffix folder with app_name_datetime
            mount_data_dir = self.check_path_and_suffix_app_name(mount_data_dir)

        mode = basic_config_dict['mode']
        replicas_num = 0
        if mode == MODE_REPLICATION:
            replicas_num = int(basic_config_dict['replicas_per_shard'])
            # replicas number can not be empty
            if replicas_num < 1:
                raise CatalogException('catalog_replicas_number_cant_be_empty')

        port = int(advanced_config_dict['port'])
        # check whether or not the mysql port is correct
        if not port:
            port = 3306
        else:
            self.check_port(port, 'port of mysql', 1024, 65535)

        # ip tag and list
        ip_tag = basic_config_dict['ip_tag']
        node_list = ','.join(ip_tag)
        all_nodes_num = len(ip_tag)
        ports = []
        for i in range(all_nodes_num):
            ports.append(str(port))
        port_list = ','.join(ports)

        random_node_ip = self.get_random_ip(ip_tag[:])

        password_origin = ''
        if 'password' in basic_config_dict.keys():
            password = password_origin = basic_config_dict['password']
            password = self.verify_password(password, 'mysql root password')
        else:
            raise CatalogException('password_cant_be_empty', message_params=['mysql root password'])

        client_password_origin = ''
        if 'client_password' in advanced_config_dict.keys():
            client_password = client_password_origin = advanced_config_dict['client_password']
            client_password = self.verify_password(client_password, 'mysql client password')
        else:
            raise CatalogException('password_cant_be_empty',
                                   message_params=['mysql client password'])

        if not client_password_origin:
            monitor_password_statement = ''
        else:
            monitor_password_statement = 'ALAUDA_MONITOR_SERVICE__MYSQL__1__PASSWORD: ' + \
                                         client_password_origin

        # support DBProxy
        dbproxy_admin_user = dbproxy_admin_password = ''
        dbproxy_admin_port = 3309
        dbproxy_proxy_port = 3308
        if mode == MODE_REPLICATION:
            enable_dbproxy = basic_config_dict['enable_dbproxy']
            if enable_dbproxy == 'enable_dbproxy':
                if mode != MODE_REPLICATION:
                    raise CatalogException('dbproxy_should_use_in_replication_mode')

                dbproxy_admin_user = basic_config_dict['dbproxy_admin_user']
                self.verify_name(dbproxy_admin_user, 'DBProxy admin user')

                if 'dbproxy_admin_password' in basic_config_dict.keys():
                    dbproxy_admin_password = basic_config_dict['dbproxy_admin_password']
                    dbproxy_admin_password = self.verify_password(dbproxy_admin_password,
                                                                  'DBProxy admin password')
                else:
                    raise CatalogException('password_cant_be_empty',
                                           message_params=['DBProxy admin password'])

                dbproxy_admin_port = int(basic_config_dict['dbproxy_admin_port'])
                # check whether or not the dbproxy admin port is correct
                if not dbproxy_admin_port:
                    dbproxy_admin_port = 3309
                else:
                    self.check_port(dbproxy_admin_port, 'admin port of dbproxy', 1024, 65535)

                dbproxy_proxy_port = int(basic_config_dict['dbproxy_proxy_port'])
                # check whether or not the dbproxy proxy port is correct
                if not dbproxy_proxy_port:
                    dbproxy_proxy_port = 3308
                else:
                    self.check_port(dbproxy_proxy_port, 'proxy port of dbproxy', 1024, 65535)

        if 'default_db' in basic_config_dict:
            default_db = basic_config_dict['default_db']
            create_default_db = 'MYSQL_DATABASE: ' + default_db
        else:
            create_default_db = ''

        # generate phpmyadmin port
        lb_name = self.get_lb_name(self.raw_data['region_uuid'])
        if not lb_name:
            phpmyadmin_port = PortHttp(port=9981).get_port_yaml()
        else:
            phpmyadmin_port = lb_name + ':80:' + PortHttp(port=9981).get_port_yaml()

        meta_compose_path = meta_container_path = ''
        compose_args = {
            'image_index': self.image_index,
            'image_tag': image_tag,
            'size': size,
            'cpu_quota': cpu_quota,
            'mem_limit': mem_limit,
            'port': port,
            'password': password,
            'password_origin': password_origin,
            'mount_data_dir': mount_data_dir,
            'create_default_db': create_default_db,
            'client_user': client_user,
            'client_password': client_password,
            'max_connections': max_connections,
            'charset': charset,
            'node_list': node_list,
            'port_list': port_list,
            'random_node_ip': random_node_ip,
            'app_name': self.raw_data['name'],
            'phpmyadmin_port': phpmyadmin_port,
            'dbproxy_admin_user': dbproxy_admin_user,
            'dbproxy_admin_password': dbproxy_admin_password,
            'dbproxy_admin_port': dbproxy_admin_port,
            'dbproxy_proxy_port': dbproxy_proxy_port,
            'monitor_password_statement': monitor_password_statement
        }
        container_args = compose_args.copy()
        if mode == MODE_REPLICATION:
            # check whether or not all nodes number is correct
            if all_nodes_num != 1+replicas_num:
                raise CatalogException('catalog_invalid_node_ip_tag')

            # port dbproxy_admin_port and dbproxy_proxy_port are retained for DBProxy
            if enable_dbproxy == 'enable_dbproxy':
                self.check_conflict_port([port, dbproxy_admin_port, dbproxy_proxy_port],
                                         ['mysql port', 'dbproxy admin port', 'dbproxy porxy port'])
                if image_tag == '5.7':
                    raise CatalogException('dbproxy_not_support_mysql_version',
                                           message_params=[image_tag])

            # generate mysql master port
            if not lb_name:
                mysql_master_port = port
            else:
                mysql_master_port = lb_name + ':' + str(port) + ':' + str(port) + '/tcp'

            master_node_addr = basic_config_dict['master_node_addr']
            # master node ip should be in the ip tag list
            if master_node_addr not in ip_tag:
                raise CatalogException('catalog_invalid_master_node_addr')

            # remove master node
            ip_tag.remove(master_node_addr)
            all_nodes_num -= 1
            slave_list = ','.join(ip_tag)

            replication_user = basic_config_dict['replication_user']
            self.verify_name(replication_user, 'mysql replication user')

            if replication_user == client_user:
                raise CatalogException('mysql_repl_user_conflict_with_client_user')

            if 'replication_password' in basic_config_dict.keys():
                replication_password = basic_config_dict['replication_password']
                replication_password = self.verify_password(replication_password,
                                                            'mysql replication password')
            else:
                raise CatalogException('password_cant_be_empty',
                                       message_params=['mysql replication password'])

            container_args.update({
                'node_type': NODE_TYPE_SLAVE,
                'node_name': 'mysql-slave',
                'master_node_addr': master_node_addr,
                'repl_user': replication_user,
                'repl_password': replication_password
            })
            compose_args.update({
                'node_type': NODE_TYPE_MASTER,
                'master_node_addr': master_node_addr,
                'repl_user': replication_user,
                'repl_password': replication_password,
                'slave_list': slave_list,
                'mysql_master_port': mysql_master_port
            })
            meta_compose_path = self.replica_meta_compose_path
            meta_container_path = self.replica_meta_container_path

        elif mode == MODE_CLUSTER:
            # check whether or not all nodes number is correct
            num_of_nodes = int(basic_config_dict['num_of_nodes'])
            if num_of_nodes != all_nodes_num or num_of_nodes < 3:
                raise CatalogException('mysql_nodes_num_invalid')

            # port 4567 is retained for pxc
            self.check_conflict_port([port, 4567], ['mysql port', 'PXC internal port'])

            sst_user = basic_config_dict['sst_user']
            self.verify_name(sst_user, 'mysql sst user')

            if 'sst_password' in basic_config_dict.keys():
                sst_password = basic_config_dict['sst_password']
                sst_password = self.verify_password(sst_password, 'mysql sst password')
            else:
                raise CatalogException('password_cant_be_empty',
                                       message_params=['mysql sst password'])

            container_args.update({
                'node_name': 'pxc-cluster',
                'sst_user': sst_user,
                'sst_password': sst_password
            })
            meta_compose_path = self.cluster_meta_compose_path
            meta_container_path = self.cluster_meta_container_path

        elif mode == MODE_STANDALONE:
            # all nodes number should be 1
            if all_nodes_num != 1:
                raise CatalogException('catalog_invalid_node_ip_tag')

            container_args.update({
                'node_name': 'mysql'
            })
            meta_compose_path = self.standalone_meta_compose_path
            meta_container_path = self.standalone_meta_container_path

        else:
            raise CatalogException('catalog_invalid_config')

        # render the yml file
        meta_container_content = self.get_config_content(meta_container_path)
        template = Template(meta_container_content)

        compose_content = ''
        node_name = container_args['node_name']
        for i in range(all_nodes_num):
            if mode == MODE_CLUSTER:
                if i == 0:
                    is_initiator = 1
                else:
                    is_initiator = 0
                container_args['is_initiator'] = is_initiator

            container_args['node_name'] = node_name + str(i + 1)
            container_args['node_ip'] = ip_tag[i]
            container_args['node_tag'] = 'ip:' + ip_tag[i]

            container_content = template.render(container_args)
            compose_content += container_content

        # render the phpmyadmin yml file
        pma_container_content = self.get_config_content(self.phpmyadmin_container_path)
        template = Template(pma_container_content)
        phpmyadmin_content = template.render(compose_args)

        # render the dbproxy yml file
        if mode == MODE_REPLICATION and enable_dbproxy == 'enable_dbproxy':
            dbproxy_container_content = self.get_config_content(self.dbproxy_container_path)
            template = Template(dbproxy_container_content)
            dbproxy_content = template.render(compose_args)
            compose_args['dbproxy_container'] = dbproxy_content
        else:
            compose_args['dbproxy_container'] = ''
        compose_args['containers'] = compose_content
        compose_args['phpmyadmin_container'] = phpmyadmin_content

        meta_compose_content = self.get_config_content(meta_compose_path)
        template = Template(meta_compose_content)
        compose_content = template.render(compose_args)

        logger.info('{}, mode:{}, got compose_content: \n{}'.format(
            self.__class__.__name__, mode, compose_content))

        return compose_content

    def _format_app_detail(self, data):
        detail_template = super(Mysql, self)._format_app_detail(data)

        # add basic info
        mode = self.config_json.get_mode()
        detail_template.add_basic_info('运行模式', 'Operating mode', MODE_DICT.get(mode))
        detail_template.add_basic_info('MySQL版本', 'MySQL version',
                                       self.config_json.get_image_tag())
        detail_template.add_basic_info('MySQL端口', 'MySQL Port', self.config_json.get_port())

        if mode == MODE_REPLICATION:
            replicas_en = 'Slave nodes number'
            detail_template.add_basic_info('从节点数', replicas_en,
                                           self.config_json.get_replicas_per_shard())

        mount_data_dir = self.config_json.get_mount_data_dir() + '/' + \
            self.config_json.get_app_name()
        detail_template.add_basic_info('主机数据存储路径', 'Data directory', mount_data_dir)

        if mode == MODE_REPLICATION:
            node_list = [self.config_json.get_master_node_addr(), ]
        else:
            node_list = []
        node_list.extend(self.config_json.get_ip_tag())
        node_list = ', '.join(node_list)
        detail_template.add_basic_info('主机列表', 'Node list', node_list)

        # add metrics
        detail_template.set_metrics(self.get_metrics())

        # add management
        detail_template.set_management(self.get_management())

        return detail_template

    def get_management(self):
        url = ''
        app_name = self.config_json.get_app_name()
        results = Mirana2Client.list(self.get_region_id(), '')
        if not results:
            return url

        manage_data = {
            "admin_url": self.get_phpmyadmin_url(results, app_name)
        }
        return manage_data

    def get_phpmyadmin_url(self, results, app_name):
        url = ''
        for result in results:
            listeners = result.get('listeners', [])
            for listener in listeners:
                domains = listener.get('domains')
                protocol = listener.get('protocol')
                if domains:
                    for domain in domains:
                        if re.search(r"phpmyadmin." + app_name, domain):
                            url = protocol + '://' + domain
                            return url
        return url

    def verify_name(self, name, name_param):
        if not re.search(r"^[\w\d\-_]+$", name):
            raise CatalogException('catalog_name_format_invalid', message_params=[name_param])

    # get a random ip from ip list
    def get_random_ip(self, ip_list):
        random.shuffle(ip_list)
        return ip_list[0]

    def get_lb_name(self, region_id):
        lb_res = Mirana2Client.list(region_id, '')
        if not lb_res:
            lb_name = None
        else:
            lb_name = lb_res[0].get('name')
        return lb_name

    def __repr__(self):
        out = 'Mysql'
        return out
