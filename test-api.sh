#!/bin/bash

USAGE="Usage: ./test-api.sh <package route to test>"


while getopts 'e:' OPT;
do
    echo "OPT: $OPT"
    case $OPT in
        e)
            ENV_FILE="$OPTARG"
            ;;
    esac;
done

OPERATION="${@:3}"


# verifying the environmental variables
if [ ! $ENV_FILE ]; then
    if [ -f "test.env" ]; then
        ENV_FILE="test.env"
        OPERATION="${@:1}"
    else
        echo "File \"test.env\" does not exists"
        echo "$USAGE"
        exit 1
    fi
fi

if [ "$#" -eq 0 ]; then
    # will run the whole suite
    OPERATION="test tests"
elif [ "$#" -eq 1 ]; then
    # will only run the needed suite
    OPERATION="test ${@:1}"
    echo "-----------------------------------------"
    echo "Will only run the following suite: ${@:1}"
    echo "-----------------------------------------"
fi

export $(cat $ENV_FILE | xargs)

python manage.py $OPERATION --settings=mathilde.settings_test
