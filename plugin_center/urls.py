from django.conf.urls import url
from .views import PluginCenterViewset
from django.conf import settings


urlpatterns = [
    url(r'^/?$', PluginCenterViewset.as_view(
        {
            'get': 'list_support_plugin'
        }
    ), name='/v1/plugin_types'),

    url(r'^(?P<plugin_type>{})/?$'.format(settings.URL_REGEX), PluginCenterViewset.as_view(
        {
            'get': 'get_plugin_config_template',
        }
    ), name='/v1/plugin_types/plugin_type'),

    url(r'(?P<plugin_type>{})/plugins/?$'.format(settings.URL_REGEX), PluginCenterViewset.as_view(
        {
            'get': 'list_running_plugins',
            'post': 'create_plugin'
        }
    ), name='/v1/plugin_types/plugin_type/plugins'),

    url(r'(?P<plugin_type>{})/plugins/(?P<plugin_uuid>{})/?$'.format(settings.URL_REGEX,
                                                                     settings.UUID_REGEX),
        PluginCenterViewset.as_view(
        {
            'get': 'get_plugin_info'
        }
    ), name='/v1/plugin_types/plugin_type/plugins/plugin_uuid'),

    url(r'(?P<plugin_type>{})/plugins/(?P<plugin_uuid>{})/status/?$'.format(settings.URL_REGEX,
                                                                            settings.UUID_REGEX),
        PluginCenterViewset.as_view(
            {
                'post': 'snyc_plugin_status'
            }
        ), name='/v1/plugin_types/plugin_type/plugins/plugin_uuid/status'),
]
