#!/usr/bin/env python
# -*- coding: utf-8 -*-
from rest_framework import serializers
from plugin_center.models import Plugin, PluginApplication, PluginIntegration


class PluginConfigValidate(serializers.Serializer):
    name = serializers.CharField(max_length=64)
    region_alb_version = serializers.CharField(required=False, max_length=2)
    region_lb_name = serializers.CharField(required=False, max_length=64)
    network_mode = serializers.CharField(max_length=64)
    region_uuid = serializers.CharField(max_length=36)
    region_name = serializers.CharField(max_length=64)
    space_uuid = serializers.CharField(max_length=36, required=False, allow_blank=True)
    space_name = serializers.CharField(max_length=64, required=False, allow_blank=True)
    project_name = serializers.CharField(max_length=64, allow_blank=True, default='')
    namespace = serializers.CharField(max_length=64)
    user_token = serializers.CharField(max_length=64)
    created_by = serializers.CharField(max_length=64)
    org = serializers.CharField(max_length=64)
    basic_config = serializers.JSONField(default={})
    advanced_config = serializers.JSONField(default={})
    integration_config = serializers.JSONField(default={})
    k8snamespace = serializers.JSONField(default={})


class PluginApplicationSerializers(serializers.ModelSerializer):

    class Meta:
        model = PluginApplication
        fields = ('uuid', 'name', 'status', 'created_at')


class PluginIntegrationSerializers(serializers.ModelSerializer):

    class Meta:
        model = PluginIntegration
        fields = ('uuid', 'name', 'status', 'enabled', 'created_at')


class PluginSerializers(serializers.ModelSerializer):
    config_json = serializers.JSONField(required=False, default={})
    installation_status = serializers.JSONField(required=False, default={})
    pluginapplication = PluginApplicationSerializers(required=False, default={})
    pluginintegration = PluginIntegrationSerializers(required=False, default={})
    space_uuid = serializers.CharField(max_length=36, required=False, allow_blank=True)

    class Meta:
        model = Plugin
        fields = ('uuid', 'type', 'families', 'status', 'config_json', 'installation_status',
                  'region_uuid', 'space_uuid', 'pluginapplication', 'pluginintegration',
                  'created_at', 'created_by')

    def get_status_info(self):
        data = self.data.copy()
        data.pop('config_json')
        return data

    @property
    def get_installation_status(self):
        return self.data['installation_status']

    def create(self, validated_data):
        application = validated_data.pop('pluginapplication')
        integration = validated_data.pop('pluginintegration')
        plugin = Plugin.objects.create(**validated_data)
        if application:
            PluginApplication.objects.create(plugin=plugin, **application)
        if integration:
            PluginIntegration.objects.create(plugin=plugin, **integration)
        return plugin

    def update(self, instance, validated_data):
        application = validated_data.pop('pluginapplication')
        integration = validated_data.pop('pluginintegration')
        for attr, value in validated_data.items():
            setattr(instance, attr, value)
        instance.save()

        for attr, value in application.items():
            setattr(instance.pluginapplication, attr, value)
        instance.pluginapplication.save()

        for attr, value in integration.items():
            setattr(instance.pluginintegration, attr, value)
        instance.pluginintegration.save()
        return instance
