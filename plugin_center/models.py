#!/usr/bin/env python
# -*- coding: utf-8 -*-
import uuid
from django.db import models
from jsonfield import JSONField

FAMILIES = (
    ('ImageScan', 'ImageScan'),
    ('CodeScan', 'CodeScan')
)


PLUGIN_STATUS_DEPLOYING = 'Deploying'
PLUGIN_STATUS_SUCCEED = 'Succeed'
PLUGIN_STATUS_FAILED = 'Failed'
PLUGIN_STATUS_COMPLETE = [PLUGIN_STATUS_SUCCEED, PLUGIN_STATUS_FAILED]
PLUGIN_STATUS = (
    (PLUGIN_STATUS_DEPLOYING, PLUGIN_STATUS_DEPLOYING),
    (PLUGIN_STATUS_SUCCEED, PLUGIN_STATUS_SUCCEED),
    (PLUGIN_STATUS_FAILED, PLUGIN_STATUS_FAILED)
)


class Plugin(models.Model):
    uuid = models.CharField(primary_key=True, default=uuid.uuid4, max_length=36)
    families = models.CharField(choices=FAMILIES, max_length=64)
    status = models.CharField(choices=PLUGIN_STATUS, default=PLUGIN_STATUS_DEPLOYING, max_length=64)
    type = models.CharField(max_length=64)
    region_uuid = models.CharField(max_length=36)
    space_uuid = models.CharField(max_length=36)
    config_json = JSONField(blank=True, default={})
    sync_count = models.IntegerField(blank=True, default=0)
    installation_status = JSONField(blank=True, default={})
    created_by = models.CharField(max_length=64)
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['-created_at']

    def __str__(self):
        return str(self.uuid)


APP_STATUS_NOT_CREATED = 'NotCreated'
APP_STATUS_CREATING = 'Creating'
APP_STATUS_CREATERROR = 'CreateError'
APP_STATUS_STARTING = 'Starting'
APP_STATUS_STARTERROR = 'StartError'
APP_STATUS_RUNNING = 'Running'
APP_STATUS_DELETED = 'Deleted'

APP_ERROR_STATUS = [APP_STATUS_CREATERROR, APP_STATUS_STARTERROR]


class PluginApplication(models.Model):
    plugin = models.OneToOneField(Plugin, primary_key=True, on_delete=models.CASCADE)
    uuid = models.CharField(blank=True, null=True, max_length=36)
    name = models.CharField(max_length=64)
    status = models.CharField(default=APP_STATUS_NOT_CREATED, max_length=64)
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['-created_at']

    def __str__(self):
        return str(self.uuid)


INTERGRATION_NOT_CREATED = 'NotCreated'
INTERGRATION_SUCCEED = 'Succeed'
INTERGRATION_FAILED = 'Failed'
INTERGRATION_DELETED = 'Deleted'
INTERGRATION_STATUS = (
    (INTERGRATION_NOT_CREATED, INTERGRATION_NOT_CREATED),
    (INTERGRATION_SUCCEED, INTERGRATION_SUCCEED),
    (INTERGRATION_FAILED, INTERGRATION_FAILED),
    (INTERGRATION_DELETED, INTERGRATION_DELETED)
)


class PluginIntegration(models.Model):
    plugin = models.OneToOneField(Plugin, primary_key=True, on_delete=models.CASCADE)
    uuid = models.CharField(blank=True, null=True, max_length=36)
    name = models.CharField(max_length=64)
    enabled = models.BooleanField(default=False)
    status = models.CharField(choices=INTERGRATION_STATUS, default=INTERGRATION_NOT_CREATED,
                              blank=True, max_length=64)
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['-created_at']

    def __str__(self):
        return str(self.uuid)
