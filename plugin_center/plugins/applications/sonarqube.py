# -*- coding: utf-8 -*-
import logging
import yaml
from catalog.claas.base_factory import AbstractFactory
from catalog.resources.application import Application
from catalog.ui_element.input_option import ElementImageTag
from plugin_center.plugins.ui_element.ui_sonarqube import ElementSonarQubePort, \
    ElementSonarQubeVolume, ElementSonarQubeSize, ElementSonarQubeInternalPort
from plugin_center.plugins.ui_element.ui_database import ElementDatabasePassword, \
    ElementDatabaseUser, ElementDatabasePort, ElementDatabaseVolume, ElementDatabaseSize,\
    ElementDatabaseName, ElementDatabaseInternalPort
from client.mirana2.client import Mirana2Client

logger = logging.getLogger(__name__)


class SonarQube(AbstractFactory):

    def __init__(self, raw_data, namespace):
        if 'region_lb_name' in raw_data:
            raw_data['basic_config']['alauda_lb_name'] = raw_data.pop('region_lb_name')
        super(SonarQube, self).__init__(raw_data, namespace)
        self.sonardb = self._get_database_service()
        self.sonarqube = self._get_sonarqube_service()

    def get_compose_data(self):
        app = Application()
        app.add_service(self.sonardb)
        app.add_service(self.sonarqube)
        return yaml.safe_dump(app.to_yml_object(), allow_unicode=True)

    def _get_sonarqube_service(self):
        service = super(SonarQube, self)._get_service()
        basic_config = self.raw_data['basic_config']
        advanced_config = self.raw_data['advanced_config']
        ElementSonarQubeSize.is_valid(basic_config)

        service.set_repository(self.image_index, 'claas/sonarqube',
                               ElementImageTag.get_value(basic_config))
        service.set_size(ElementSonarQubeSize.get_value(basic_config))

        service.add_volume(ElementSonarQubeVolume.get_value(advanced_config),
                           '/opt/sonarqube/data')
        service.add_tcp_port(ElementSonarQubePort.get_value(advanced_config),
                             ElementSonarQubeInternalPort().get_default_value)
        service.add_links(self.sonardb.get_name())

        for k, v in self._generate_sonarqube_env().items():
            service.add_env_var(k, v)
        return service

    def _get_database_service(self):
        service = super(SonarQube, self)._get_service(sub_name='db')
        basic_config = self.raw_data['basic_config']
        advanced_config = self.raw_data['advanced_config']
        ElementDatabaseSize.is_valid(basic_config)

        service.set_repository(self.image_index, 'claas/postgres',
                               ElementImageTag.get_value(basic_config))
        service.set_size(ElementDatabaseSize.get_value(basic_config))
        service.add_tcp_port(ElementDatabasePort.get_value(advanced_config),
                             ElementDatabaseInternalPort().get_default_value)
        service.add_volume(ElementDatabaseVolume.get_value(advanced_config),
                           '/var/lib/postgresql/data')

        for k, v in self._generate_database_env().items():
            service.add_env_var(k, v)
        return service

    def _generate_database_env(self):
        advanced_config = self.raw_data['advanced_config']
        return {
            'POSTGRES_PASSWORD': ElementDatabasePassword.get_value(advanced_config),
            'POSTGRES_USER': ElementDatabaseUser.get_value(advanced_config),
            'POSTGRES_DB': ElementDatabaseName.get_value(advanced_config),
            'PGDATA': '/var/lib/postgresql/data/{}'.format(self.raw_data['name'])
        }

    def _generate_sonarqube_env(self):
        advanced_config = self.raw_data['advanced_config']
        db_user = ElementDatabaseUser.get_value(advanced_config)
        db_password = ElementDatabasePassword.get_value(advanced_config)
        db_name = ElementDatabaseName.get_value(advanced_config)
        jdbc_url = 'jdbc:postgresql://{}:{}/{}'.format(
            self.sonardb.get_name(),
            ElementDatabaseInternalPort().get_default_value,
            db_name)
        return {
            'SONARQUBE_JDBC_URL': jdbc_url,
            'SONARQUBE_JDBC_USERNAME': db_user,
            'SONARQUBE_JDBC_PASSWORD': db_password
        }

    def generate_sonarqube_endpoint(self, services):
        advanced_config = self.raw_data['advanced_config']
        service_id = services[self.sonarqube.get_name()]
        region_id = self.raw_data['region_uuid']
        alauda_lb_name = self.raw_data['basic_config']['alauda_lb_name']
        port = ElementSonarQubePort.get_value(advanced_config)
        domain = Mirana2Client.get_alb_domain(region_id, service_id, alauda_lb_name)
        return 'http://{}:{}'.format(domain, port)
