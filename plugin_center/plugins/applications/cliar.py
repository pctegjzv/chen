# -*- coding: utf-8 -*-
import logging
import yaml
from catalog.claas.base_factory import AbstractFactory
from catalog.resources.application import Application
from catalog.ui_element.input_option import ElementImageTag
from plugin_center.plugins.ui_element.ui_database import ElementDatabasePassword, \
    ElementDatabaseUser, ElementDatabasePort, ElementDatabaseVolume, ElementDatabaseSize, \
    ElementDatabaseName, ElementDatabaseInternalPort
from plugin_center.plugins.ui_element.ui_clair import ElementClairHealthPort,\
    ElementClairPort, ElementClairSize, ElementClairHealthInternalPort, ElementClairInternalPort
from client.mirana2.client import Mirana2Client

logger = logging.getLogger(__name__)


class Clair(AbstractFactory):

    def __init__(self, raw_data, namespace):
        if 'region_lb_name' in raw_data:
            raw_data['basic_config']['alauda_lb_name'] = raw_data.pop('region_lb_name')
        super(Clair, self).__init__(raw_data, namespace)
        self.clairdb = self._get_database_service()
        self.clair = self._get_clair_service()

    def get_compose_data(self):
        app = Application()
        app.add_service(self.clairdb)
        app.add_service(self.clair)
        return yaml.safe_dump(app.to_yml_object(), allow_unicode=True)

    def _get_database_service(self):
        service = super(Clair, self)._get_service(sub_name='db')
        basic_config = self.raw_data['basic_config']
        advanced_config = self.raw_data['advanced_config']
        ElementDatabaseSize.is_valid(basic_config)

        service.set_repository(self.image_index, 'claas/clair-pg',
                               ElementImageTag.get_value(basic_config))
        service.set_size(ElementDatabaseSize.get_value(basic_config))
        service.add_tcp_port(ElementDatabasePort.get_value(advanced_config),
                             ElementDatabaseInternalPort().get_default_value)
        service.add_volume(ElementDatabaseVolume.get_value(advanced_config),
                           '/var/lib/postgresql/data')

        for k, v in self._generate_database_env().items():
            service.add_env_var(k, v)
        return service

    def _get_clair_service(self):
        service = super(Clair, self)._get_service()
        basic_config = self.raw_data['basic_config']
        advanced_config = self.raw_data['advanced_config']
        ElementClairSize.is_valid(basic_config)

        service.set_repository(self.image_index, 'claas/alauda-master-clair',
                               ElementImageTag.get_value(basic_config))
        service.set_size(ElementClairSize.get_value(basic_config))
        service.add_tcp_port(ElementClairPort.get_value(advanced_config),
                             ElementClairInternalPort().get_default_value)
        service.add_tcp_port(ElementClairHealthPort.get_value(advanced_config),
                             ElementClairHealthInternalPort().get_default_value)
        service.add_links(self.clairdb.get_name())

        for k, v in self._genarate_clair_env().items():
            service.add_env_var(k, v)
        return service

    def _generate_database_env(self):
        advanced_config = self.raw_data['advanced_config']
        return {
            'POSTGRES_PASSWORD': ElementDatabasePassword.get_value(advanced_config),
            'POSTGRES_USER': ElementDatabaseUser.get_value(advanced_config),
            'POSTGRES_DB': ElementDatabaseName.get_value(advanced_config),
            'PGDATA': '/var/lib/postgresql/data/{}'.format(self.raw_data['name'])
        }

    def _genarate_clair_env(self):
        advanced_config = self.raw_data['advanced_config']
        db_name = ElementDatabaseName.get_value(advanced_config)
        db_user = ElementDatabaseUser.get_value(advanced_config)
        db_password = ElementDatabasePassword.get_value(advanced_config)
        jdbc_url = 'postgresql://{}:{}@{}:{}/{}?sslmode=disable'.format(
            db_user, db_password, self.clairdb.get_name(),
            ElementDatabaseInternalPort().get_default_value, db_name)
        return {
            'POSTGRE_DATABASE_SOURCE': jdbc_url,
        }

    def generate_clair_endpoint(self, services):
        assert isinstance(services, dict)
        region_id = self.raw_data['region_uuid']
        service_id = services[self.clair.get_name()]
        alauda_lb_name = self.raw_data['basic_config']['alauda_lb_name']
        return Mirana2Client.get_alb_domain(region_id, service_id, alauda_lb_name)

    def generate_database_addr(self, services):
        assert isinstance(services, dict)
        advanced_config = self.raw_data['advanced_config']
        port = ElementDatabasePort.get_value(advanced_config)
        region_id = self.raw_data['region_uuid']
        service_id = services[self.clairdb.get_name()]
        alauda_lb_name = self.raw_data['basic_config']['alauda_lb_name']
        domain = Mirana2Client.get_alb_domain(region_id, service_id, alauda_lb_name)
        return '{}:{}'.format(domain, port)
