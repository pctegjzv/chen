# -*- coding: utf-8 -*-
from catalog.ui_element.input_string import InputString
from catalog.ui_element.input_int import InputInt
from catalog.ui_element.input_cluster_size import InputClusterSize


class ElementSonarQubePort(InputInt):
    name = 'sonarqube_port'

    def __init__(self, port=9000):
        max_value = 65535
        min_value = 0
        super(ElementSonarQubePort, self).__init__(
            min_value, max_value, 'SonarQube port', 'SonarQube外部端口', port
        )


class ElementSonarQubeInternalPort(InputInt):
    name = 'sonarqube_internal_port'

    def __init__(self, port=9000):
        max_value = 65535
        min_value = 0
        super(ElementSonarQubeInternalPort, self).__init__(
            min_value, max_value, 'SonarQube port', 'SonarQube容器端口', port
        )


class ElementSonarQubeVolume(InputString):
    name = 'sonarqube_volume'

    def __init__(self):
        pattern = '^[\/a-zA-Z_][\w\-_\/]+$'
        pattern_hint = {
            "en": "Wrong path format, can only contain numbers, letters, slash, line, "
                  "underline and dot, to slash at the beginning will mount a local directory. "
                  "Don't start with a forward slash in accordance with the distributed storage "
                  "volume mount",
            "zh": "路径格式错误，只能含有斜杠、数字、字母、中线、下划线和点号，以斜杠开头时会挂载本地目录。"
                  "不以斜杠开头会按照分布式存储卷挂载"
        }
        super(ElementSonarQubeVolume, self).__init__(
            'SonarQube volume', 'SonarQube数据卷', pattern=pattern,
            pattern_hint=pattern_hint
        )


class ElementSonarQubeSize(InputClusterSize):
    name = 'sonarqube_size'
    min_size = {
        'cpu': 1,
        'mem': 1024
    }

    def __init__(self, options=None):
        if not options:
            options = ['M', 'L', 'XL', 'CUSTOMIZED']
        super(ElementSonarQubeSize, self).__init__(
            'Size of instances, recommended instance size(CPU: 2 core, MEM: 2048 MB)',
            'SonarQube 实例大小, 推荐实例大小(CPU: 2 核, 内存: 2048 MB)',
            options, 'SonarQube Instance size', 'SonarQube实例大小',
            default_value={'size': 'CUSTOMIZED', 'cpu': 2, 'mem': 2048})
