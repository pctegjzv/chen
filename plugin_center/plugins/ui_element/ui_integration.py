# -*- coding: utf-8 -*-
from catalog.ui_element.input_string import InputString


class ElementIntegrationName(InputString):
    name = 'name'

    def __init__(self):
        pattern = '^[a-zA-Z_][\w\-_]+$'
        pattern_hint = {
            "en": "The instance name must be integrated by different "
                  "letters or underscores, cannot contain special characters",
            "zh": "集成实例名必须以大小写字母或下划线开头，不能包含特殊字符"
        }
        description = {
            "en": "The name is guaranteed to be unique",
            "zh": "集成实例名称要确保唯一"
        }
        super(ElementIntegrationName, self).__init__(
            display_name_en='Integration name',
            display_name_cn='集成实例名称',
            default_value='',
            description=description,
            pattern=pattern,
            pattern_hint=pattern_hint
        )


class ElementIntegrationDescription(InputString):
    name = 'description'

    def __init__(self):
        super(ElementIntegrationDescription, self).__init__(
            display_name_en='Integration description',
            display_name_cn='集成实例描述',
            default_value='',
            required=False
        )
