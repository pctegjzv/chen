# -*- coding: utf-8 -*-
from catalog.ui_element.input_string import InputString
from catalog.ui_element.input_int import InputInt
from catalog.ui_element.input_cluster_size import InputClusterSize


class ElementDatabaseVolume(InputString):
    name = 'db_volume'

    def __init__(self):
        pattern = '^[\/a-zA-Z_][\w\-_\/]+$'
        pattern_hint = {
            "en": "Wrong path format, can only contain numbers, letters, slash, line, "
                  "underline and dot, to slash at the beginning will mount a local directory. "
                  "Don't start with a forward slash in accordance with the distributed storage "
                  "volume mount",
            "zh": "路径格式错误，只能含有斜杠、数字、字母、中线、下划线和点号，以斜杠开头时会挂载本地目录。"
                  "不以斜杠开头会按照分布式存储卷挂载"
        }
        super(ElementDatabaseVolume, self).__init__(
            'Database volume', '数据库数据卷', pattern=pattern, pattern_hint=pattern_hint
        )


class ElementDatabasePort(InputInt):
    name = 'db_port'

    def __init__(self, port=5432):
        max_value = 65535
        min_value = 0

        super(ElementDatabasePort, self).__init__(
            min_value, max_value, 'Database port', '数据库外部端口', port
        )


class ElementDatabaseInternalPort(InputInt):
    name = 'db_internal_port'

    def __init__(self, port=5432):
        max_value = 65535
        min_value = 0
        super(ElementDatabaseInternalPort, self).__init__(
            min_value, max_value, 'Database port', '数据库容器端口', port
        )


class ElementDatabaseUser(InputString):
    name = 'db_user'

    def __init__(self):
        pattern = '^[a-zA-Z_][\w\-_]+$'
        pattern_hint = {
            "en": "The database user name must begin with a letter or "
                  "underscore, and cannot contain special characters",
            "zh": "数据库用户名必须以大小写字母或下划线开头，不能包含特殊字符"
        }
        super(ElementDatabaseUser, self).__init__(
            'Database user', '数据库用户名', 'alauda', pattern=pattern,
            pattern_hint=pattern_hint
        )


class ElementDatabasePassword(InputString):
    name = 'db_password'

    def __init__(self):
        pattern = '^[a-zA-Z1-9]{1}\w{5,15}$'
        pattern_hint = {
            "en": "The password length needs to be from 6 bits to 16 bits, only "
                  "printable ASCII characters, and cannot contain single quotation "
                  "marks or double quotation marks",
            "zh": "密码长度需要在6位至16位，只能为可打印的ASCII字符，不能含有单引号或双引号"
        }
        super(ElementDatabasePassword, self).__init__(
            'Database password', '数据库密码', 'alauda', pattern=pattern,
            pattern_hint=pattern_hint
        )


class ElementDatabaseName(InputString):
    name = 'db_name'

    def __init__(self):
        pattern = '^[a-zA-Z_][\w\-_]+$'
        pattern_hint = {
            "en": "The database name must write letters to size or underscore, "
                  "cannot contain special characters",
            "zh": "数据库户名必须以大小写字母或下划线开头，不能包含特殊字符"
        }
        super(ElementDatabaseName, self).__init__(
            'Database name', '数据库名称', 'alauda', pattern=pattern,
            pattern_hint=pattern_hint
        )


class ElementDatabaseSize(InputClusterSize):
    name = 'db_size'
    min_size = {
        'cpu': 0.5,
        'mem': 512
    }

    def __init__(self, options=None):
        if not options:
            options = ['XS', 'S', 'M', 'L', 'XL', 'CUSTOMIZED']
        super(ElementDatabaseSize, self).__init__(
            'Size of database, recommended instance size(CPU: 1 core, MEM: 1024 MB)',
            '数据库实例大小, 推荐实例大小(CPU: 1 核, 内存: 1024 MB)',
            options, 'Size of database', '数据库实例大小',
            default_value={'size': 'CUSTOMIZED', 'cpu': 1, 'mem': 1024})
