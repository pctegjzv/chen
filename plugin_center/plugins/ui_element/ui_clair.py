# -*- coding: utf-8 -*-
from catalog.ui_element.input_int import InputInt
from catalog.ui_element.input_cluster_size import InputClusterSize


class ElementClairPort(InputInt):
    name = 'clair_port'

    def __init__(self, port=6060):
        max_value = 65535
        min_value = 0
        super(ElementClairPort, self).__init__(
            min_value, max_value, 'Clair port', 'Clair外部端口', port
        )


class ElementClairInternalPort(InputInt):
    name = 'clair_internal_port'

    def __init__(self, port=6060):
        max_value = 65535
        min_value = 0
        super(ElementClairInternalPort, self).__init__(
            min_value, max_value, 'Clair port', 'Clair容器端口', port
        )


class ElementClairHealthPort(InputInt):
    name = 'clair_health_port'

    def __init__(self, port=6061):
        max_value = 65535
        min_value = 0
        super(ElementClairHealthPort, self).__init__(
            min_value, max_value, 'Clair health check port', 'Clair外部健康检查端口',
            port
        )


class ElementClairHealthInternalPort(InputInt):
    name = 'clair_health_internal_port'

    def __init__(self, port=6061):
        max_value = 65535
        min_value = 0
        super(ElementClairHealthInternalPort, self).__init__(
            min_value, max_value, 'Clair health check port', 'Clair容器健康检查端口',
            port
        )


class ElementClairSize(InputClusterSize):
    name = 'clair_size'
    min_size = {
        'cpu': 1,
        'mem': 1024
    }

    def __init__(self, options=None):
        if not options:
            options = ['M', 'L', 'XL', 'CUSTOMIZED']
        super(ElementClairSize, self).__init__(
            'Size of instances, recommended instance size(CPU: 2 core, MEM:2048 MB)',
            'Clair 实例大小, 推荐实例大小(CPU: 2 核, 内存: 2048 MB)',
            options, 'Size of instances', 'Clair 实例大小',
            default_value={'size': 'CUSTOMIZED', 'cpu': 2, 'mem': 2048})
