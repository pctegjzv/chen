#!/usr/bin/env python
# -*-  coding: utf-8 -*-
from plugin_center.plugins.clair_plugin import ClairPlugin
from plugin_center.plugins.sonarqube_plugin import SonarQubePlugin
from plugin_center.models import Plugin
from django.db.models import Q


class PluginFactory(object):
    factory = {
        'Clair': ClairPlugin,
        'SonarQube': SonarQubePlugin
    }

    @classmethod
    def get_plugin(cls, plugin_type, data=None, plugin_uuid=None):
        plugin_obj = None
        if plugin_uuid is not None:
            plugin_obj = Plugin.objects.get(Q(pk=plugin_uuid) & Q(type=plugin_type))
            if isinstance(data, dict):
                data = plugin_obj.config_json.update(data)
            else:
                data = plugin_obj.config_json

        if plugin_type in PluginFactory.factory:
            return PluginFactory.factory[plugin_type](data, plugin_obj, plugin_type)
        else:
            raise TypeError('Unknown plugin factory. plugin_type: {}'.format(plugin_type))

    @classmethod
    def get_plugin_class(cls, plugin_type):
        if plugin_type in PluginFactory.factory:
            return PluginFactory.factory[plugin_type]
        else:
            raise TypeError('Unknown plugin factory. plugin_type: {}'.format(plugin_type))
