import logging
import datetime
import requests
from mekansm.exceptions import MekAPIException


logger = logging.getLogger(__name__)


class SonarQubeClient(object):

    def __init__(self, endpoint, username='admin', password='admin'):
        self.endpoint = endpoint
        self.username = username
        self.password = password

    def _do_req(self, path, method, data=None, headers={}, timeout=5):
        logger.debug('headers={}'.format(headers))
        my_header = {
            'User-Agent': 'chen/v1.0'
        }
        auth = (self.username, self.password)
        my_header.update(headers)
        logging.info(auth)
        try:
            if not self.endpoint.startswith('http'):
                self.endpoint = 'http://{}'.format(self.endpoint)
            url = self.endpoint + path
            start_time = datetime.datetime.now()
            logger.debug("{} {} data={}, headers={}".format(method, url, data, my_header))
            resp = requests.request(method, url, data=data,
                                    headers=my_header, timeout=timeout, auth=auth)
            cost = datetime.datetime.now() - start_time
            logger.info('Get sonar from sonarqube server. | url={}, resp={}, cost={}'.format(
                url, resp, cost))
        except Exception as e:
            logger.error('sonar request error {}'.format(e.message))
            raise

        logger.info('SonarQube response code {}'.format(resp.status_code))
        if resp.status_code >= 300:
            logger.info('SonarQube response error {}'.format(resp.text))
            raise MekAPIException('server_error', message=resp.text)
        try:
            return resp.json()
        except:
            return resp.text

    def check_valid(self):
        path = '/api/authentication/validate'
        try:
            resp = self._do_req(path=path, method='GET', timeout=1)
        except Exception as e:
            logging.error(e.message)
            return False
        return resp.get('valid', False)

    def grant_scan_permission(self):
        path = '/api/permissions/add_group'
        data = {
            'groupName': 'sonar-administrators',
            'permission': 'scan'
        }
        try:
            self._do_req(path=path, data=data, method='POST')
        except Exception as e:
            logging.error(e.message)
            return False
        return True

    def generate_token(self):
        path = '/api/user_tokens/generate'
        data = {
            'login': 'admin',
            'name': 'sonar'
        }
        return self._do_req(path=path, method='POST', data=data)
