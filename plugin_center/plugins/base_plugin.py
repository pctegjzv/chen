#!/usr/bin/env python
# -*-  coding: utf-8 -*-
from __future__ import absolute_import

import logging
import time
import traceback
import yaml
from catalog.claas.catalog_factory import CatalogFactory
from plugin_center.serializers import PluginSerializers, PluginConfigValidate
from plugin_center.models import APP_STATUS_RUNNING, PLUGIN_STATUS_DEPLOYING,\
    PLUGIN_STATUS_FAILED, INTERGRATION_FAILED, APP_ERROR_STATUS, INTERGRATION_SUCCEED, \
    PLUGIN_STATUS_SUCCEED, APP_STATUS_DELETED, INTERGRATION_DELETED,\
    Plugin, PLUGIN_STATUS_COMPLETE
from client.jakiro.client import JakiroClient
from django.conf import settings
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db import transaction
from django.db.models import Q
from catalog.exceptions import CatalogException

logger = logging.getLogger(__name__)
TIMEOUT = int(settings.DEFAULT_TIMEOUT) * 60


class BasePlugin(object):

    def __init__(self, data=None, instance=None, plugin_type=None, families=None):
        assert plugin_type is not None
        assert families is not None
        validate = PluginConfigValidate(data=data)
        if not validate.is_valid():
            raise CatalogException(
                code='invalid_args',
                message='{}'.format(validate.errors)
            )
        self.config_data = validate.validated_data
        self.plugin_type = plugin_type
        self.families = families
        self.instance = instance
        self.application = CatalogFactory.get_factory(
            self.plugin_type, self.config_data,
            self.config_data['namespace']
        )
        self.app_compose_data = self.application.get_compose_data()
        self.services = None

    def generate_create_app_req(self):
        return {
            "name": self.config_data['name'],
            "values_yaml_content": self._get_values_yaml_content(),
            "template": self._get_template(),
            "namespace": self.config_data['k8snamespace'],
            "cluster": {
                "uuid": self.config_data['region_uuid'],
                "name": self.config_data['region_name']
            },
            "user_token": self.config_data['user_token'],
            "project_name": self.config_data['project_name'],
        }

    def _get_template(self):
        return None

    def _get_values_yaml_content(self):
        json_data = self._get_json_values_yaml()
        return yaml.dump(json_data)

    def _get_json_values_yaml(self):
        return None

    def get_app_compose_data(self):
        return self.app_compose_data

    def sync_status(self, namespace=None, user_token=None, project_name=None, is_new_k8s=False):
        assert self.instance is not None
        namespace = self.config_data['namespace'] if not namespace else namespace
        user_token = self.config_data['user_token'] if not user_token else user_token
        project_name = self.config_data['project_name'] if not project_name else project_name
        app_info = {}
        integration_info = {}

        serializer = PluginSerializers(instance=self.instance)
        data = serializer.data
        plugin_old_status = data['status']

        if data['pluginapplication']['uuid']:
            app_info = JakiroClient.get_app(
                namespace,
                data['pluginapplication']['uuid'],
                user_token, project_name, is_new_k8s
            )
            if app_info:
                logger.info('app current status: {}'.format(app_info['current_status']))
                data['pluginapplication']['status'] = app_info['current_status']
            else:
                data['pluginapplication']['status'] = APP_STATUS_DELETED
                data['status'] = PLUGIN_STATUS_FAILED

        if data['pluginintegration']['uuid']:
            integration_info = JakiroClient.get_integration(
                namespace,
                data['pluginintegration']['uuid'],
                user_token,
                project_name
            )
            if not integration_info:
                logger.info('')
                data['pluginintegration']['status'] = INTERGRATION_DELETED
                data['status'] = PLUGIN_STATUS_FAILED

        if data['status'] == PLUGIN_STATUS_SUCCEED:
            if data['pluginapplication']['status'] != APP_STATUS_RUNNING:
                data['status'] = PLUGIN_STATUS_FAILED
        elif data['status'] == PLUGIN_STATUS_DEPLOYING:
            if self.plugin_is_timeout() or \
                            data['pluginapplication']['status'] in APP_ERROR_STATUS:
                data['status'] = PLUGIN_STATUS_FAILED

            elif data['pluginapplication']['status'] == APP_STATUS_RUNNING:
                # create integration
                if self.app_is_ready(app_info):
                    try:
                        logger.info('app_info data:{}'.format(app_info))
                        payload = self.get_integration_config_data(app_info)
                        logger.info('itegration data:{}'.format(payload))
                        resp = JakiroClient.create_integration(
                            payload, namespace,
                            user_token, project_name
                        )
                        data['status'] = PLUGIN_STATUS_SUCCEED
                        data['pluginintegration']['status'] = INTERGRATION_SUCCEED
                        data['pluginintegration']['uuid'] = resp['id']
                    except Exception, e:
                        logger.error(traceback.format_exc())
                        logger.error(e.message)
                        data['status'] = PLUGIN_STATUS_FAILED
                        data['pluginintegration']['status'] = INTERGRATION_FAILED
        else:
            if data['pluginapplication']['status'] == APP_STATUS_RUNNING and \
                            data['pluginintegration']['status'] == INTERGRATION_SUCCEED:
                data['status'] = PLUGIN_STATUS_SUCCEED

        if plugin_old_status == PLUGIN_STATUS_DEPLOYING:
            if data['status'] in PLUGIN_STATUS_COMPLETE:
                data['installation_status'] = {
                    'status': data['status'],
                    'created_at': data['created_at'],
                    'pluginapplication': {
                        'status': data['pluginapplication']['status']
                    },
                    'pluginintegration': {
                        'status': data['pluginintegration']['status']
                    }
                }
            if data['status'] == PLUGIN_STATUS_FAILED:
                if app_info:
                    JakiroClient.delete_app(
                        namespace,
                        app_info['uuid'],
                        user_token,
                        project_name,
                        is_new_k8s
                    )
                if integration_info:
                    JakiroClient.delete_integration(
                        namespace,
                        integration_info['uuid'],
                        user_token,
                        project_name
                    )
        self.save(data=data)
        if 'config_json' in data:
            data.pop('config_json')
        return data

    @transaction.atomic()
    def save(self, data=None):
        if self.instance is None:
            return self._create()
        else:
            return self._update(data)

    def _update(self, data=None):
        if data is None or self.instance is None:
            return
        serializer = PluginSerializers(instance=self.instance, data=data)
        if not serializer.is_valid():
            raise CatalogException(
                code='invalid_args',
                message='{}'.format(serializer.errors)
            )
        self.instance = serializer.save()
        return serializer.data

    def _create(self):
        if '/' in self.config_data['created_by']:
            username = self.config_data['created_by'].split('/')
            created_by = username[1]
        else:
            created_by = self.config_data['created_by']

        plugin_data = {
            'families': self.families,
            'type': self.plugin_type,
            'region_uuid': self.config_data['region_uuid'],
            'space_uuid': self.config_data.get('space_uuid', ''),
            'config_json': self.config_data,
            'created_by': created_by,
            'pluginapplication': {
                'name': self.config_data['name']
            },
            'pluginintegration': {
                'name': self.config_data['name'],
                'enabled': self.config_data['integration_config'].get('enabled', True)
            }
        }

        serializer = PluginSerializers(data=plugin_data)
        if not serializer.is_valid():
            raise CatalogException(
                code='invalid_args',
                message='{}'.format(serializer.errors)
            )
        self.instance = serializer.save()
        return serializer.data

    def get_integration_config_data(self, app_info):
        assert isinstance(app_info, dict)
        self.services = {service['service_name']: service['uuid']
                         for service in app_info['services']}
        description = self.config_data['integration_config'].get('description')
        if description is None:
            description = ''
        return {
            "name": self.config_data['integration_config']['name'],
            "type": self.plugin_type,
            "description": description,
            "enabled": self.config_data['integration_config'].get('enabled', True),
            "space_name": self.config_data.get('space_name'),
        }

    def app_is_ready(self, app_info):
        return True

    def get_plugin_db_obj(self):
        return self.instance

    @classmethod
    def get_config_template(cls):
        raise NotImplementedError()

    def plugin_is_timeout(self):
        assert self.instance is not None
        started_time = int(time.mktime(self.instance.created_at.timetuple()))
        seconds = int(time.time() - started_time)
        logging.info('started_time: {}, seconds: {}'.format(started_time, seconds))
        if seconds > TIMEOUT:
            logging.info('plugin timeout')
            return True
        else:
            return False

    @classmethod
    def load_plugins(cls, region_uuid, page_size=20, page=1):
        if page_size is None:
            page_size = 20
        if page is None:
            page = 1
        plugins = []
        for subcls in BasePlugin.__subclasses__():
            if 'card_info' in dir(subcls) and isinstance(subcls.card_info, dict):
                latest_plugin = {}
                current_plugin = {}
                plugin_nums = 0
                current_plugin_set = Plugin.objects.filter(
                    Q(type=subcls.card_info.get('plugin_type')) &
                    Q(status__in=[PLUGIN_STATUS_DEPLOYING]) &
                    Q(region_uuid=region_uuid)
                )
                if current_plugin_set.exists():
                    serializer = PluginSerializers(instance=current_plugin_set.first())
                    current_plugin = serializer.get_status_info()

                latest_plugin_set = Plugin.objects.filter(
                    Q(type=subcls.card_info.get('plugin_type')) &
                    Q(status__in=[PLUGIN_STATUS_SUCCEED, PLUGIN_STATUS_FAILED]) &
                    Q(region_uuid=region_uuid)
                )
                if latest_plugin_set.exists():
                    serializer = PluginSerializers(instance=latest_plugin_set.first())
                    latest_plugin = serializer.get_installation_status
                    plugin_nums = latest_plugin_set.filter(status=PLUGIN_STATUS_SUCCEED).count()

                subcls.card_info['latest_plugin'] = latest_plugin
                subcls.card_info['current_plugin'] = current_plugin
                subcls.card_info['plugin_num'] = plugin_nums
                plugins.append(subcls.card_info)
        paginator = Paginator(plugins, page_size)
        try:
            page_plugin = paginator.page(page)
        except PageNotAnInteger:
            page_plugin = paginator.page(1)
        except EmptyPage:
            page_plugin = paginator.page(paginator.num_pages)

        result_plugins = {
            'num_pages': page,
            'page_size': page_size,
            'count': paginator.count,
            'next': page_plugin.next_page_number() if page_plugin.has_next() else None,
            'previous': page_plugin.previous_page_number() if page_plugin.has_previous() else None,
            'results': plugins
        }
        return result_plugins
