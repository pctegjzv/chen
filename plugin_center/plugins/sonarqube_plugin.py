#!/usr/bin/env python
# -*- coding: utf-8 -*-
import logging
from django.conf import settings
from plugin_center.plugins.base_plugin import BasePlugin
from catalog.ui_element.app_info import AppInfo
from catalog.ui_element.app_instruction import AppInstruction
from catalog.ui_element.input_option import ElementImageTag
from plugin_center.plugins.ui_element.ui_integration import ElementIntegrationName,\
    ElementIntegrationDescription
from plugin_center.plugins.ui_element.ui_database import ElementDatabaseUser, \
    ElementDatabasePassword, ElementDatabasePort, ElementDatabaseSize, \
    ElementDatabaseVolume, ElementDatabaseName
from plugin_center.plugins.ui_element.ui_sonarqube import ElementSonarQubePort,\
    ElementSonarQubeSize, ElementSonarQubeVolume
from plugin_center.plugins.plugin_client.sonarqube import SonarQubeClient

logger = logging.getLogger(__name__)


class SonarQubePlugin(BasePlugin):

    card_info = {
        'plugin_type': 'SonarQube',
        'display_name': 'SonarQube',
        'avatar': 'https://s3.cn-north-1.amazonaws.com.cn/'
                  'alauda-staging-integrations/assets/sonarqube-logo.png',
        'display_info': {
            "en": "SonarQube",
            "zh_cn": "SonarQube 代码扫描"
        },
        'status': 'ready',
        'families': ['CodeScan']
    }

    def __init__(self, data, instance, plugin_type, families=None):
        if not families:
            families = 'CodeScan'
        super(SonarQubePlugin, self).__init__(data=data,
                                              instance=instance,
                                              plugin_type=plugin_type,
                                              families=families)

    @classmethod
    def get_config_template(cls):
        config_template = {
            "name": 'sonarqube',
            "plugin_type": "SonarQube",
            "avatar": "https://s3.cn-north-1.amazonaws.com.cn/"
                      "alauda-staging-integrations/assets/sonarqube-logo.png",
            "info": AppInfo("SonarqubePlugin", "Sonarqube 插件").to_json(),
            "instruction": AppInstruction("", "Sonarqube 插件 用来代码扫描").to_json(),
            "region_lb_name": True,
            "basic_config": [
                ElementImageTag(['v1']).to_json(),
                ElementSonarQubeSize().to_json(),
                ElementDatabaseSize().to_json()
            ],
            "advanced_config": [
                ElementSonarQubeVolume().to_json(),
                ElementSonarQubePort().to_json(),
                ElementDatabaseVolume().to_json(),
                ElementDatabasePort().to_json(),
                ElementDatabaseName().to_json(),
                ElementDatabaseUser().to_json(),
                ElementDatabasePassword().to_json()
            ],
            "integration_config": [
                ElementIntegrationName().to_json(),
                ElementIntegrationDescription().to_json()
            ]
        }
        return config_template

    def get_integration_config_data(self, app_info):
        integration_config = super(SonarQubePlugin, self).get_integration_config_data(app_info)
        endpoint = self.application.generate_sonarqube_endpoint(self.services)
        sonarqube_client = SonarQubeClient(endpoint)
        resp = sonarqube_client.generate_token()
        integration_config['fields'] = {
            'token': resp['token'],
            'endpoint': endpoint
        }
        return integration_config

    def app_is_ready(self, app_info):
        self.services = {service['service_name']: service['uuid']
                         for service in app_info['services']}
        endpoint = self.application.generate_sonarqube_endpoint(self.services)
        sonarqube_client = SonarQubeClient(endpoint)
        return (sonarqube_client.check_valid() and
                sonarqube_client.grant_scan_permission())

    def _get_template(self):
        return settings.CHART_SONARQUBE_TEMPLATE

    def _get_json_values_yaml(self):
        basic_config = self.config_data['basic_config']
        advanced_config = self.config_data['advanced_config']
        return {
            "load_balance_name": basic_config['alauda_lb_name'],
            "imagePullPolicy": "IfNotPresent",
            "sonarqube": {
                "image": "{}/claas/sonarqube:{}".format(
                    settings.IMAGE_INDEX, ElementImageTag.get_value(basic_config)),
                "size": ElementSonarQubeSize.get_value(basic_config),
                "service_port": ElementSonarQubePort.get_value(advanced_config),
                "volume": ElementSonarQubeVolume.get_value(advanced_config)
            },
            "db": {
                "image": "{}/claas/postgres:{}".format(
                    settings.IMAGE_INDEX, ElementImageTag.get_value(basic_config)),
                "size": ElementDatabaseSize.get_value(basic_config),
                "name": ElementDatabaseName.get_value(advanced_config),
                "password": ElementDatabasePassword.get_value(advanced_config),
                "user": ElementDatabaseUser.get_value(advanced_config),
                "port": ElementDatabasePort.get_value(advanced_config),
                "volume": ElementDatabaseVolume.get_value(advanced_config)
            }
        }
