#!/usr/bin/env python
# -*- coding: utf-8 -*-
import logging
from django.conf import settings
from plugin_center.plugins.base_plugin import BasePlugin
from catalog.ui_element.app_info import AppInfo
from catalog.ui_element.app_instruction import AppInstruction
from plugin_center.plugins.ui_element.ui_database import ElementDatabaseUser, ElementDatabaseName, \
    ElementDatabasePassword, ElementDatabasePort, ElementDatabaseVolume, ElementDatabaseSize
from plugin_center.plugins.ui_element.ui_integration import ElementIntegrationName, \
    ElementIntegrationDescription
from plugin_center.plugins.ui_element.ui_clair import ElementClairHealthPort, \
    ElementClairPort, ElementClairSize
from catalog.ui_element.input_option import ElementImageTag

logger = logging.getLogger(__name__)


class ClairPlugin(BasePlugin):

    card_info = {
        'plugin_type': 'Clair',
        'display_name': 'Clair',
        'avatar': 'https://s3.cn-north-1.amazonaws.com.cn/'
                  'alauda-staging-integrations/assets/clair-logo.png',
        'display_info': {
            "en": "Clair",
            "zh_cn": "Clair 镜像扫描"
        },
        'status': 'ready',
        'families': ['ImageScan']
    }

    def __init__(self, data, instance, plugin_type, families=None):
        if not families:
            families = 'ImageScan'
        super(ClairPlugin, self).__init__(data=data,
                                          instance=instance,
                                          plugin_type=plugin_type,
                                          families=families)

    @classmethod
    def get_config_template(cls):
        config_template = {
            "name": "clair",
            "plugin_type": "Clair",
            "avatar": "https://s3.cn-north-1.amazonaws.com.cn/"
                      "alauda-staging-integrations/assets/clair-logo.png",
            "info": AppInfo("ClairPlugin", "Clair 插件").to_json(),
            "instruction": AppInstruction("", "Clair 插件 用来扫描镜像").to_json(),
            "region_lb_name": True,
            "basic_config": [
                ElementImageTag(['v1.1']).to_json(),
                ElementClairSize().to_json(),
                ElementDatabaseSize().to_json()
            ],
            "advanced_config": [
                ElementClairPort().to_json(),
                ElementClairHealthPort().to_json(),
                ElementDatabaseVolume().to_json(),
                ElementDatabasePort().to_json(),
                ElementDatabaseUser().to_json(),
                ElementDatabaseName().to_json(),
                ElementDatabasePassword().to_json()
            ],
            "integration_config": [
                ElementIntegrationName().to_json(),
                ElementIntegrationDescription().to_json()
            ]
        }
        return config_template

    def get_integration_config_data(self, app_info):
        integration_config = super(ClairPlugin, self).get_integration_config_data(app_info)
        advanced_config = self.config_data['advanced_config']
        integration_config['fields'] = {
            "endpoint": self.application.generate_clair_endpoint(self.services),
            "service_port": ElementClairPort.get_value(advanced_config),
            "health_port": ElementClairHealthPort.get_value(advanced_config),
            "database_addr": self.application.generate_database_addr(self.services),
            "database_name": ElementDatabaseName.get_value(advanced_config),
            "password": ElementDatabasePassword.get_value(advanced_config),
            "user_name": ElementDatabaseUser.get_value(advanced_config)
        }
        return integration_config

    def _get_template(self):
        return settings.CHART_CLAIR_TEMPLATE

    def _get_json_values_yaml(self):
        basic_config = self.config_data['basic_config']
        advanced_config = self.config_data['advanced_config']
        return {
            "load_balance_name": basic_config['alauda_lb_name'],
            "imagePullPolicy": "IfNotPresent",
            "clair": {
                "image": "{}/claas/alauda-master-clair:{}".format(
                    settings.IMAGE_INDEX, ElementImageTag.get_value(basic_config)),
                "size": ElementClairSize.get_value(basic_config),
                "service_port": ElementClairPort.get_value(advanced_config),
                "health_port": ElementClairHealthPort.get_value(advanced_config)
            },
            "db": {
                "image": "{}/claas/clair-pg:{}".format(
                    settings.IMAGE_INDEX, ElementImageTag.get_value(basic_config)),
                "size": ElementDatabaseSize.get_value(basic_config),
                "name": ElementDatabaseName.get_value(advanced_config),
                "password": ElementDatabasePassword.get_value(advanced_config),
                "user": ElementDatabaseUser.get_value(advanced_config),
                "port": ElementDatabasePort.get_value(advanced_config),
                "volume": ElementDatabaseVolume.get_value(advanced_config)
            }
        }
