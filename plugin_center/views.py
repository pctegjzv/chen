# -*- coding: utf-8 -*-
import logging
import sys
from rest_framework.viewsets import ViewSet
from rest_framework.response import Response
from rest_framework import status
from alauda.tracing import tracing_response_time
from client.jakiro.client import JakiroClient
from client.chen2.client import Chen2Client
from plugin_center.plugins.base_plugin import BasePlugin
from plugin_center.plugins.plugin_factory import PluginFactory
from plugin_center.models import Plugin, PLUGIN_STATUS_FAILED, APP_STATUS_CREATING, \
    PLUGIN_STATUS_SUCCEED, PLUGIN_STATUS_DEPLOYING, APP_STATUS_DELETED, INTERGRATION_DELETED
from plugin_center.serializers import PluginSerializers
from client.meudsa.client import MedusaClient
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from mekansm.exceptions import MekAPIException
from django.db.models import Q

reload(sys)
sys.setdefaultencoding('utf8')
logger = logging.getLogger(__name__)


class PluginCenterViewset(ViewSet):

    def __init__(self):
        self.plugin_queryset = Plugin.objects.all()

    @tracing_response_time()
    def list_support_plugin(self, request):
        logger.info('list_support_plugin')
        data = request.data
        region_uuid = request.query_params.get('region_uuid')
        is_new_k8s = request.query_params.get('is_new_k8s', 'false').lower() == 'true'
        for subcls in BasePlugin.__subclasses__():
            if 'card_info' in dir(subcls) and isinstance(subcls.card_info, dict):
                plugin_type = subcls.card_info.get('plugin_type')
                plugin_set = Plugin.objects.filter(
                    Q(type=plugin_type) &
                    Q(status__in=[PLUGIN_STATUS_DEPLOYING])
                )
                if region_uuid:
                    plugin_set = plugin_set.filter(region_uuid=region_uuid)
                plugin = plugin_set.first()
                if plugin:
                    self._sync_plugin_info(
                        request, plugin.type,
                        plugin.uuid,
                        is_new_k8s
                    )
        plugins = BasePlugin.load_plugins(
            region_uuid,
            data.get('page_size', 20),
            data.get('page', 1),
        )
        return Response(data=plugins)

    @tracing_response_time()
    def get_plugin_config_template(self, request, plugin_type):
        logger.info('get_plugin_config_template')
        plugin_cls = PluginFactory.get_plugin_class(plugin_type)
        return Response(plugin_cls.get_config_template())

    @tracing_response_time()
    def create_plugin(self, request, plugin_type):
        logger.info('create_plugin plugin_type: {}'.format(plugin_type))
        plugin = PluginFactory.get_plugin(plugin_type, data=request.data)
        data = plugin.save()
        try:

            config_data = plugin.config_data
            is_new_k8s = request.data.get("is_new_k8s", False)
            if is_new_k8s:
                response = Chen2Client.create_application(plugin.generate_create_app_req())
            else:
                compose_content = plugin.get_app_compose_data()
                logger.info('compose_content : {}'.format(compose_content))
                response = JakiroClient.create(
                    config_data['region_name'],
                    config_data['name'],
                    config_data['namespace'],
                    config_data['user_token'],
                    config_data['space_name'],
                    config_data['project_name'],
                    compose_content,
                    plugin_type,
                    source='PLUGIN'
                )
            data['pluginapplication']['uuid'] = response.get('uuid')
            data['pluginapplication']['name'] = response.get('app_name')
            data['pluginapplication']['status'] = APP_STATUS_CREATING
        except Exception as e:
            logger.error(e.message)
            data['status'] = PLUGIN_STATUS_FAILED
            plugin.save(data=data)
            raise
        plugin.save(data=data)
        # registry medusa
        self._add_status_sync_scheduler(plugin_type, data['uuid'], is_new_k8s)
        return Response(data={'uuid': data['uuid']}, status=status.HTTP_201_CREATED)

    @tracing_response_time()
    def get_plugin_info(self, request, plugin_type, plugin_uuid):
        logger.info('get_plugin_info type:{}  uuid:{}'.format(plugin_type, plugin_uuid))
        is_new_k8s = request.query_params.get('is_new_k8s', 'false').lower() == 'true'
        result = self._sync_plugin_info(request, plugin_type, plugin_uuid, is_new_k8s)
        return Response(data=result)

    @tracing_response_time()
    def list_running_plugins(self, request, plugin_type):
        logger.info('list_running_plugins plugin_type: {}'.format(plugin_type))
        regions = set()
        region_uuid = request.query_params.get('region_uuid')
        results = []
        if region_uuid:
            regions.add(region_uuid)
        else:
            plugin_set = self.plugin_queryset.filter(type=plugin_type)
            for plugin in plugin_set:
                regions.add(plugin.region_uuid)
        for region in regions:
            result_item = {}
            plugin_running_set = self.plugin_queryset.filter(
                Q(region_uuid=region) & Q(status=PLUGIN_STATUS_SUCCEED) & Q(type=plugin_type))
            if not plugin_running_set.exists():
                continue
            result_item['region_uuid'] = region
            result_item['plugins'] = []
            for plugin_item in plugin_running_set:
                result_item['plugins'].append(
                    {
                        "app_name": plugin_item.pluginapplication.name,
                        "app_uuid": plugin_item.pluginapplication.uuid,
                        "app_status": plugin_item.pluginapplication.status,
                        "plugin_uuid": plugin_item.uuid
                    }
                )
            results.append(result_item)
        logger.info(results)
        return Response(data={'results': results})

    @tracing_response_time()
    def snyc_plugin_status(self, request, plugin_type, plugin_uuid):
        logger.info('snyc_plugin_status  plugin_uuid: {}'.format(plugin_uuid))
        is_new_k8s = request.query_params.get("is_new_k8s", "false").lower() == "true"
        plugin = None
        try:
            plugin = PluginFactory.get_plugin(plugin_type, plugin_uuid=plugin_uuid)
            config_data = plugin.config_data
            result = plugin.sync_status(namespace=config_data['namespace'],
                                        user_token=config_data['user_token'],
                                        project_name=config_data['project_name'],
                                        is_new_k8s=is_new_k8s)
            if result['pluginapplication']['status'] == APP_STATUS_DELETED or \
               result['pluginintegration']['status'] == INTERGRATION_DELETED:
                return Response('Sync end', status=status.HTTP_410_GONE)

        except ObjectDoesNotExist as e:
            logger.error(e.message)
            return Response('Sync end', status=status.HTTP_410_GONE)
        except Exception as e:
            logger.error(e)
            instance = plugin.get_plugin_db_obj()
            instance.sync_count += 1
            if instance.sync_count >= settings.SYNC_COUNT:
                instance.status = PLUGIN_STATUS_FAILED
                logging.info('sync count: {}'.format(instance.sync_count))
            instance.save()
        return Response('sync plugin', status=status.HTTP_200_OK)

    def _sync_plugin_info(self, request, plugin_type, plugin_uuid, is_new_k8s):
        plugin = None
        try:
            plugin = PluginFactory.get_plugin(plugin_type, plugin_uuid=plugin_uuid)
            result = plugin.sync_status(request.query_params.get('namespace'),
                                        request.query_params.get('user_token'),
                                        request.query_params.get('project_name'),
                                        is_new_k8s)
        except ObjectDoesNotExist:
            raise MekAPIException('resource_not_exist')
        except Exception as e:
            logger.error(e.message)
            if not plugin:
                raise
            instance = plugin.get_plugin_db_obj()
            instance.sync_count += 1
            if instance.sync_count >= settings.SYNC_COUNT:
                instance.status = PLUGIN_STATUS_FAILED
            logging.info('sync count: {}'.format(instance.sync_count))
            instance.save()
            result = PluginSerializers(instance=instance).get_status_info()
        return result

    def _add_status_sync_scheduler(self, plugin_type, plugin_uuid, is_new_k8s):
        sync_url = self._get_plugin_status_sync_url(plugin_type, plugin_uuid, is_new_k8s)
        try:
            result = MedusaClient.create_schedule_event_config(
                settings.STATUS_SYNC_RULE, sync_url)
            logger.info("add sync scheduler to medusa, plugin_uuid ={}, result: {}".format(
                plugin_uuid, result))
        except Exception as e:
            logger.error('_add_status_sync_scheduler: {}'.format(e.message))

    def _get_plugin_status_sync_url(self, plugin_type, plugin_uuid, is_new_k8s):
        return '#ALAUDA_CHEN_ENDPOINT#/v1/plugin_types/{}/plugins/{}/status/?is_new_k8s={}'.format(
            plugin_type, plugin_uuid, is_new_k8s
        )
