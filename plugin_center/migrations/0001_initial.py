# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import jsonfield.fields
import uuid


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Plugin',
            fields=[
                ('uuid', models.CharField(default=uuid.uuid4, max_length=36, serialize=False, primary_key=True)),
                ('families', models.CharField(max_length=64, choices=[(b'ImageScan', b'ImageScan'), (b'CodeScan', b'CodeScan')])),
                ('status', models.CharField(default=b'Deploying', max_length=64, choices=[(b'Deploying', b'Deploying'), (b'Succeeded', b'Succeeded'), (b'Failed', b'Failed')])),
                ('type', models.CharField(max_length=64)),
                ('region_uuid', models.CharField(max_length=36)),
                ('space_uuid', models.CharField(max_length=36)),
                ('config_json', jsonfield.fields.JSONField(default={}, blank=True)),
                ('sync_count', models.IntegerField(blank=True, default=0)),
                ('created_by', models.CharField(max_length=64)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
            ],
            options={
                'ordering': ['created_at'],
            },
        ),
        migrations.CreateModel(
            name='PluginApplication',
            fields=[
                ('plugin', models.OneToOneField(primary_key=True, serialize=False, to='plugin_center.Plugin')),
                ('uuid', models.CharField(max_length=36, null=True, blank=True)),
                ('name', models.CharField(max_length=64)),
                ('status', models.CharField(default=b'NotCreated', max_length=64, choices=[(b'NotCreated', b'NotCreated'), (b'Creating', b'Creating'), (b'CreateError', b'CreateError'), (b'Starting', b'Starting'), (b'StartError', b'StartError'), (b'Running', b'Running')])),
                ('created_at', models.DateTimeField(auto_now_add=True)),
            ],
            options={
                'ordering': ['created_at'],
            },
        ),
        migrations.CreateModel(
            name='PluginIntegration',
            fields=[
                ('plugin', models.OneToOneField(primary_key=True, serialize=False, to='plugin_center.Plugin')),
                ('uuid', models.CharField(max_length=36, null=True, blank=True)),
                ('name', models.CharField(max_length=64)),
                ('enabled', models.BooleanField(default=False)),
                ('status', models.CharField(default=b'NotCreated', max_length=64, blank=True, choices=[(b'NotCreated', b'NotCreated'), (b'Succeeded', b'Succeeded'), (b'Failed', b'Failed')])),
                ('created_at', models.DateTimeField(auto_now_add=True)),
            ],
            options={
                'ordering': ['created_at'],
            },
        ),
    ]
