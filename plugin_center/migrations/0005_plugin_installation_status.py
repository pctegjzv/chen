# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import jsonfield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('plugin_center', '0004_auto_20171102_0852'),
    ]

    operations = [
        migrations.AddField(
            model_name='plugin',
            name='installation_status',
            field=jsonfield.fields.JSONField(default={}, blank=True),
        ),
    ]
