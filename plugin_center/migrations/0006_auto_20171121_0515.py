# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('plugin_center', '0005_plugin_installation_status'),
    ]

    operations = [
        migrations.AlterField(
            model_name='pluginapplication',
            name='status',
            field=models.CharField(default=b'NotCreated', max_length=64),
        ),
    ]
