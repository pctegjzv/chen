# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('plugin_center', '0002_auto_20171020_0400'),
    ]

    operations = [
        migrations.AlterField(
            model_name='pluginapplication',
            name='status',
            field=models.CharField(default=b'NotCreated', max_length=64, choices=[(b'NotCreated', b'NotCreated'), (b'Creating', b'Creating'), (b'CreateError', b'CreateError'), (b'Starting', b'Starting'), (b'StartError', b'StartError'), (b'Running', b'Running'), (b'Deleted', b'Deleted')]),
        ),
        migrations.AlterField(
            model_name='pluginintegration',
            name='status',
            field=models.CharField(default=b'NotCreated', max_length=64, blank=True, choices=[(b'NotCreated', b'NotCreated'), (b'Succeeded', b'Succeeded'), (b'Failed', b'Failed'), (b'Deleted', b'Deleted')]),
        ),
    ]
