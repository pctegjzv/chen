# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('plugin_center', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='plugin',
            options={'ordering': ['-created_at']},
        ),
        migrations.AlterModelOptions(
            name='pluginapplication',
            options={'ordering': ['-created_at']},
        ),
        migrations.AlterModelOptions(
            name='pluginintegration',
            options={'ordering': ['-created_at']},
        ),
    ]
