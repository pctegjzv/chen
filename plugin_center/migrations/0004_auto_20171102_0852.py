# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('plugin_center', '0003_auto_20171024_0542'),
    ]

    operations = [
        migrations.AlterField(
            model_name='plugin',
            name='status',
            field=models.CharField(default=b'Deploying', max_length=64, choices=[(b'Deploying', b'Deploying'), (b'Succeed', b'Succeed'), (b'Failed', b'Failed')]),
        ),
        migrations.AlterField(
            model_name='pluginintegration',
            name='status',
            field=models.CharField(default=b'NotCreated', max_length=64, blank=True, choices=[(b'NotCreated', b'NotCreated'), (b'Succeed', b'Succeed'), (b'Failed', b'Failed'), (b'Deleted', b'Deleted')]),
        ),
    ]
